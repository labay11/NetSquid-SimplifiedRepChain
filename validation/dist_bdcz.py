"""
This script is run by inputting a 2-column parameter file directly in the console, i.e.:

python3 simple_example.py file_name

The parameters are `number_of_runs` and in addition all parameters
that should be inputted in the function
`repchain.runtools.run_simulation.run_many_simulations`
"""
import sys
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

sys.path.insert(0, str(Path.cwd().parent))
sys.path.insert(0, str(Path.cwd().parent.parent.joinpath('nlblueprint')))

from repchain.runtools.simulation_setup import SimulationSetup  # noqa E402
from repchain.runtools.data_analysis_tools import add_default_tappers, cycle_to_Hz  # noqa E402
from repchain.utils.bcdzfunctions import num2level  # noqa E402


def default_params(nodes, total_dist):
    input_dic = {}
    input_dic['T1'] = 0.0
    input_dic['T2'] = 0.0
    input_dic['swap_quality'] = 1
    input_dic['elementary_link_fidelity'] = 1
    input_dic['elementary_link_succ_prob'] = 1
    input_dic['number_of_nodes'] = nodes
    input_dic['tot_num_qubits'] = 40
    input_dic['internode_distance'] = total_dist / (input_dic['number_of_nodes'] - 1)
    input_dic["use_swap_only"] = False
    input_dic['cycle_time'] = 3.5e3
    input_dic["single_qubit_gate_duration"] = 5
    input_dic["initialization_duration"] = 2000
    input_dic["measurement_duration"] = 3700
    input_dic["two_qubit_gate_duration"] = 1.542e6
    input_dic['swap_fidelity_threshold'] = min(input_dic['elementary_link_fidelity'] * 1.01, 1.)
    input_dic['number_distill_success'] = 1
    input_dic['werner'] = True
    return input_dic


def success_prob_from_failed(failed, unc, number_distill_success):
    return 1 - (failed / (failed + number_distill_success)),\
        abs(unc * number_distill_success / (failed + number_distill_success)**2)


def success_prob_by_levels(nodes, failed, unc, number_distill_success):
    levels = num2level(nodes)
    x, u = [],  []
    start = 0
    print(failed.shape, unc.shape)
    for level in range(levels):
        nodes_in_level = int((nodes - 1) / 2**level)
        a = failed[:, start:start+nodes_in_level]
        b = unc[:, start:start+nodes_in_level]
        a, b = success_prob_from_failed(a, b, number_distill_success)
        x.append(np.mean(a, axis=1))
        u.append(np.sqrt(np.sum(np.power(b, 2), axis=1)) / nodes_in_level)
        start += nodes_in_level
    return np.array(x).T, np.array(u).T


def entswap_bd_psi(r, s, p2=1):
    a = np.array([
        r[0]*s[1] + r[1]*s[0] + r[2]*s[3] + r[3]*s[2],
        np.sum(r*s),
        r[0]*s[2] + r[1]*s[3] + r[2]*s[0] + r[3]*s[1],
        r[0]*s[3] + r[1]*s[2] + r[2]*s[1] + r[3]*s[0]
    ])

    return p2 * a + (1 - p2) / 4


def abstract_entswap_bd(r, s, p2=1):
    return np.array([
        (1+p2)**2 + 2*p2*(-(1+p2)*(s[1]+s[2]) - 2*s[3] + 2*r[3]*(-1+s[1]+s[2]+2*s[3])
                          + r[1]*(-1-p2+2*(1+p2)*s[1]+2*p2*s[2]+2*s[3]) + r[2]*(2*p2*s[1]+(1+p2)*(-1+2*s[2])+2*s[3])),
        1-p2**2 + 2*p2*((1+p2-2*r[3])*s[1] + (-1+p2+2*r[3])*s[2] + r[1]*(1+p2-2*(1+p2)*s[1]-2*p2*s[2]-2*s[3])
                        + r[2]*(-1+p2+2*s[2]-2*p2*(s[1]+s[2])+2*s[3])),
        1-p2**2 + 2*p2*((-1+p2+2*r[3])*s[1] + (1+p2-2*r[3])*s[2] + r[2]*(1+p2-2*p2*s[1]-2*(1+p2)*s[2]-2*s[3])
                        + r[1]*(-1+p2-2*(-1+p2)*s[1]-2*p2*s[2]+2*s[3])),
        (1-p2)**2 + 2*p2*(-(-1+p2)*(s[1]+s[2]) + r[1]*(1-p2+2*(-1+p2)*s[1]+2*p2*s[2]-2*s[3])
                          + r[2]*(2*p2*s[1]+(-1+p2)*(-1+2*s[2])-2*s[3]) + 2*s[3] - 2*r[3]*(-1+s[1]+s[2]+2*s[3]))
    ]) / 4


def apply_x_to_bd(r):
    return np.array([r[1], r[0], r[3], r[2]])


def abstract_entswap_bd_psi(r, s, p2=1):
    rpsi, spsi = apply_x_to_bd(r), apply_x_to_bd(s)
    t = abstract_entswap_bd(rpsi, spsi, p2)
    return apply_x_to_bd(t)


def dejmps_psucc_full_psi(r, s, p2=1):
    return p2**2 * ((r[0] + r[2])*(s[0] + s[2]) + (r[1] + r[3])*(s[1] + s[3])) + (1 - p2**2) / 2


def dejmps_full_psi(r, s, p2):
    n = dejmps_psucc_full_psi(r, s, p2)
    a = np.array([
        r[0]*s[0] + r[2]*s[2],
        r[1]*s[1] + r[3]*s[3],
        r[0]*s[2] + r[2]*s[0],
        r[1]*s[3] + r[3]*s[1]
    ])
    return (p2**2 * a + (1 - p2**2) / 8) / n


def theory_dist(nodes, f, p2=1):
    r = np.zeros(4)
    r[1] = f
    r[[0, 2, 3]] = (1 - f) / 3

    level = num2level(nodes)
    psucs = []
    for _ in range(level):
        psucs.append(dejmps_psucc_full_psi(r, r, 1))
        rdist = dejmps_full_psi(r, r, 1)
        r = abstract_entswap_bd_psi(rdist, rdist, p2)
    return [r[1]] + psucs


def theory_swap(nodes, f, p2=1):
    r = np.zeros(4)
    r[1] = f
    r[[0, 2, 3]] = (1 - f) / 3
    s = np.copy(r)

    for _ in range(nodes - 2):
        s = abstract_entswap_bd_psi(r, s, p2)
    return s[1]


def cl(x, xmin, xmax):
    r = (x - xmin) / (xmax - xmin)
    return (r, 0, 1 - r)


MARKERS = ['x', 'o', '^', 's']


def create_colorbar():
    colors = [cl(0, 0, 1), cl(1, 0, 1)]
    cmap_name = 'my_list'
    # Create the colormap
    return LinearSegmentedColormap.from_list(cmap_name, colors, N=100)


def plot(nodes, dist, p2s):
    fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True)
    minp2, maxp2 = np.min(p2s), np.max(p2s)

    levels = num2level(nodes)

    for p2 in p2s:
        f_swap = np.load(f'data/dist_bdcz/{nodes}_200_{p2:.5f}_swap.npy')
        f_dist = np.load(f'data/dist_bdcz/{nodes}_200_{p2:.5f}_dist.npy')
        failed_dist = np.load(f'data/dist_bdcz/{nodes}_200_{p2:.5f}_attempts.npy')

        color = cl(p2, minp2, maxp2)

        th_f_swap = np.zeros(f_swap.shape[0])
        th_f_dist = np.zeros((f_dist.shape[0], 1 + levels))
        for i, f in enumerate(f_swap[:, 0]):
            th_f_swap[i] = theory_swap(nodes, f, p2)
        for i, f in enumerate(f_dist[:, 0]):
            th_f_dist[i, :] = theory_dist(nodes, f, p2)

        ax = axs[0]
        # ax.errorbar(f_dist[:, 0], f_dist[:, 1], yerr=f_dist[:, 2], c=color, ls='None', marker='x')
        # ax.errorbar(f_swap[:, 0], f_swap[:, 1], yerr=f_swap[:, 2], c=color, ls='None', marker='o', alpha=0.5)
        # ax.plot(f_dist[:, 0], th_f_dist[:, 0], c=color)
        # ax.plot(f_swap[:, 0], th_f_swap, c=color, alpha=0.5)
        ax.errorbar(f_dist[:, 0], f_dist[:, 1] - f_swap[:, 1], yerr=np.hypot(f_dist[:, 2], f_swap[:, 2]),
                    c=color, ls='None', marker='x')
        ax.plot(f_dist[:, 0], th_f_dist[:, 0] - th_f_swap, c=color, label=f'{p2:.2f}')
        ax.set_xlabel('Elementary link fidelity')
        ax.set_ylabel('End-to-End fidelity')

        x, u = success_prob_by_levels(nodes, failed_dist[:, 1::2], failed_dist[:, 2::2], 1)

        ax2 = axs[1]
        # for l in range(levels):
        ax2.errorbar(f_dist[:, 0], x[:, levels - 1], yerr=u[:, levels - 1], c=color, marker=MARKERS[0], ls='None')
        ax2.plot(f_dist[:, 0], th_f_dist[:, levels], c=color)
    ax2.set_ylabel('Success probability')
    ax2.set_ylim(0.25, 1.25)
    ax.legend()
    plt.savefig(f'data/dist_bdcz/{nodes}_200_1.pdf')
    plt.show()


if __name__ == "__main__":

    number_of_runs = 100
    nodes = 3

    # for p2 in np.linspace(0.84, 1, 5):
    #     f_swap = []
    #     f_dist = []
    #     failed_dist = []
    #     print(p2)
    #
    #     for f in np.linspace(0.5, 1, 10):
    #         print(f)
    #         input_dic = default_params(nodes, 200)
    #         input_dic['elementary_link_fidelity'] = f
    #         input_dic['swap_fidelity_threshold'] = min(f * 1.01, 1.)
    #         input_dic['swap_quality'] = p2
    #         input_dic['werner'] = False
    #         sim_setup = SimulationSetup.from_parameters(**input_dic)
    #
    #         [statetapper, distill_tapper] = add_default_tappers(sim_setup=sim_setup)
    #
    #         sim_setup.perform_many_runs(number_of_runs=number_of_runs)
    #
    #         # rates = cycle_to_Hz(statetapper.dataframe['time'], input_dic['cycle_time'])
    #         fidelities = statetapper.dataframe['fidelity']
    #         f_dist.append([f, fidelities.mean(), fidelities.std()])
    #         D = distill_tapper.dataframe.mean()
    #         uD = distill_tapper.dataframe.std()
    #         failed_dist.append([f, D['0_1_f'], uD['0_1_f'], D['1_2_f'], uD['1_2_f']])
    #                             # D['2_3_f'], uD['2_3_f'], D['3_4_f'], uD['3_4_f'],
    #                             # D['0_2_f'], uD['0_2_f'], D['2_4_f'], uD['2_4_f']])
    #
    #         sim_setup.reset()
    #         input_dic['use_swap_only'] = True
    #         sim_setup = SimulationSetup.from_parameters(**input_dic)
    #
    #         [statetapper, distill_tapper] = add_default_tappers(sim_setup=sim_setup)
    #
    #         sim_setup.perform_many_runs(number_of_runs=number_of_runs)
    #
    #         # rates = cycle_to_Hz(statetapper.dataframe['time'], input_dic['cycle_time'])
    #         fidelities = statetapper.dataframe['fidelity']
    #         f_swap.append([f, fidelities.mean(), fidelities.std()])
    #
    #     f_swap = np.array(f_swap)
    #     f_dist = np.array(f_dist)
    #     failed_dist = np.array(failed_dist)
    #
    #     np.save(f'data/dist_bdcz/{nodes}_200_r_{p2:.5f}_swap.npy', f_swap)
    #     np.save(f'data/dist_bdcz/{nodes}_200_r_{p2:.5f}_dist.npy', f_dist)
    #     np.save(f'data/dist_bdcz/{nodes}_200_r_{p2:.5f}_attempts.npy', failed_dist)

    plot(nodes, 200, np.linspace(0.84, 1, 5))

    # csv_filename = filebasename + ".csv"
    # dataframe.to_csv(
    #     csv_filename,
    #     columns=['cost', 'elementary_link_fidelity', 'elementary_link_succ_prob', 'swap_quality',
    #              'T1', 'T2', 'number_distill_success', 'rate', 'fidelity', '0_1', '2_1'],
    #     header=False,
    #     index=False
    # )
