import numpy as np
from scipy.optimize import root

from repchain.parameters import fiber_efficiency
from repchain.parameters.nv import NVOptimisticParameters


def swap_asap(f, number_of_nodes):
    if number_of_nodes == 2:
        return f
    L = number_of_nodes - 1
    return 0.25 + 0.75 * np.power((4 * f - 1) / 3, L)


def single_click_r(x, rt, alpha, number_of_nodes):
    internode_distance = x / (number_of_nodes - 1)
    rate_factor = (1 / 2) if number_of_nodes > 2 else 1
    tau = NVOptimisticParameters.cycle_time + 1e9 * internode_distance / NVOptimisticParameters.c
    eta_fibre = fiber_efficiency(internode_distance * 0.5, NVOptimisticParameters.p_loss_length)
    return rate_factor * 2 * alpha * eta_fibre / tau - rt * 1e-9


def single_click_fr(x, rt, ft, number_of_nodes):
    internode_distance = x[1] / (number_of_nodes - 1)
    rate_factor = (1 / 2) if number_of_nodes > 2 else 1
    tau = NVOptimisticParameters.cycle_time + 1e9 * internode_distance / NVOptimisticParameters.c
    eta_fibre = fiber_efficiency(internode_distance * 0.5, NVOptimisticParameters.p_loss_length)
    return [
        rate_factor * 2 * x[0] * eta_fibre / tau - rt * 1e-9,
        swap_asap(1 - x[0], number_of_nodes) - ft
    ]


def double_click_r(x, rt, number_of_nodes):
    internode_distance = x / (number_of_nodes - 1)
    rate_factor = (1 / 2) if number_of_nodes > 2 else 1
    tau = NVOptimisticParameters.cycle_time + 1e9 * internode_distance / NVOptimisticParameters.c
    eta_fibre = fiber_efficiency(internode_distance * 0.5, NVOptimisticParameters.p_loss_length)
    return rate_factor * (eta_fibre**2 / 2) / tau - rt * 1e-9


def double_click_fr(x, rt, ft, number_of_nodes):
    internode_distance = x[1] / (number_of_nodes - 1)
    rate_factor = (1 / 2) if number_of_nodes > 2 else 1
    tau = NVOptimisticParameters.cycle_time + 1e9 * internode_distance / NVOptimisticParameters.c
    eta_fibre = fiber_efficiency(internode_distance * 0.5, NVOptimisticParameters.p_loss_length)
    return [
        rate_factor * (eta_fibre**2 / 2) / tau - rt * 1e-9,
        swap_asap(x[0], number_of_nodes) - ft
    ]


for ft, rt in [(0.8, 1.), (0.9, 0.1)]:
    print(ft, rt)
    print('=' * 50)
    for number_of_nodes in [2, 3, 5, 9]:
        print(number_of_nodes)
        print('-' * 25)
        res = root(single_click_fr, [0.2, 200 / number_of_nodes], args=(rt, ft, number_of_nodes))
        theta = np.arccos(np.sqrt(res.x[0]))
        print(f'SC - FR: {int(res.x[1])}km (alpha = {res.x[0]:.5f}, theta = {theta:.3f})')
        res = root(single_click_r, 200 / number_of_nodes, args=(rt, 0.5, number_of_nodes))
        theta = np.arccos(np.sqrt(0.5))
        print(f'SC - R: {int(res.x)}km (alpha = 0.5, theta = {theta:.3f})')
        print('-' * 10)
        res = root(double_click_fr, [0.92, 200 / number_of_nodes], args=(rt, ft, number_of_nodes))
        theta = np.arccos(np.sqrt(res.x[0]))
        print(f'DC: {int(res.x[1])}km (f = {res.x[0]:.5f})')
        print('-' * 25)
    print('=' * 50)


# fix distance and optimise alpha
for ft, rt in [(0.8, 1.), (0.9, 0.1)]:
    res = root(lambda x: np.sum(single_click_fr([x, 200], rt, ft, 2)), 0.1)
    print(res)
