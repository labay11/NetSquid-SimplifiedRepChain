"""
This script uses the modelling from repchain.validation.nv_distillation_model
to replicate plot 4B of the paper from Kalb et al. (2017)
'Entanglement distillation between solid-state quantum network nodes'
"""

import repchain.validation.plotting_tools_for_nv_distillation_model as plotting_tools
import repchain.validation.nv_distillation_model as model
import numpy as np
import matplotlib.pyplot as plt


if __name__ == "__main__":

    mean_distilled_fidelities = []
    mean_elementary_fidelities = []
    alphas = np.linspace(0.05, 0.5, 6)
    params = model.parameters()
    for alpha in alphas:
        mean_elementary_fidelity, mean_distilled_fidelity = \
            plotting_tools.compute_mean_fidelity(max_number_of_attempts=50,
                                                 alpha=alpha,
                                                 **model.parameters())
        print("alpha={}: dist. fid= {}".format(alpha, mean_distilled_fidelity))
        mean_distilled_fidelities.append(mean_distilled_fidelity)
        mean_elementary_fidelities.append(mean_elementary_fidelity)
    plt.title("Mimicking plot 4B of Kalb et al. (2017), 'Entanglement distillation...' (analytical model)")
    plt.xlabel("Bright state population")
    plt.ylabel("Fidelity")
    plt.plot(alphas, mean_distilled_fidelities)
    plt.plot(alphas, mean_elementary_fidelities)
    plt.show()
