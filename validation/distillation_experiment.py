"""
A script for simulation the distillation experiment from the Hanson group:
    'Entanglement distillation between solid-state quantum network nodes'
    Kalb et al. (2017), Science 356, 928-932
In particular, running this script yields a plot that *should* reproduce
figure 4B from the paper.
"""

import matplotlib.pyplot as plt
import netsquid as ns
from repchain.runtools.simulation_setup import SimulationSetup
from repchain.logic.task import Task
from repchain.runtools.data_collection_tools import StateTapper, WaitingTimeTapper
import netsquid_nv.Delft_NVs.Delft_NV_current_parameters as current_entgen_params
from netsquid_nv.Delft_NVs.Delft_NV_parameters_2017_distillation_experiment import magic_distributor_params
import numpy as np


def set_distillation_logic(sim_setup):
    """
    Replace the default logic by a logic
    that performs the distillation experiment.
    To be precise: replaces the logic of
    each of the two nodes by the following
    operations:
      1. generate entanglement with remote
      2. move electron spin to carbon spin
      3. generate entanglement with remote
      4. perform distillation

    Parameters
    ----------
    sim_setup : :obj:`~repchain.runtools.simulation_setup.SimulationSetup`
    """
    for (node_id, remote_node_id, is_initiator) in [(0, 1, False), (1, 0, True)]:
        umbrellaprot = sim_setup.protocols[node_id]
        entgen_task = Task(topic="ENTGEN", parameters={"free_pos": 0, "remote_node_id": remote_node_id})
        move_task = Task(topic="MOVE", parameters={"old_pos": 0, "new_pos": 1})
        distill_task = Task(topic="DIST", parameters={"pos_keep": 1, "pos_lose": 0, "remote_node_id": remote_node_id})
        tasks = [entgen_task,
                 move_task,
                 entgen_task,
                 distill_task]
        umbrellaprot['logic']._taskexecutor.todolist = tasks
        umbrellaprot['logic']._taskexecutor.continue_performing_tasks()


def get_distillation_experiment(alpha):
    """
    Parameters
    ----------
    alpha : float
        Bright-state parameter
    """
    ns.sim_reset()
    ns.set_qstate_formalism(ns.QFormalism.DM)

    number_of_nodes = 2
    internode_distance = 0.002  # in kilometers

    qmem_config_params = {
        "tot_num_qubits": 2,
        "params": current_entgen_params
    }
#    magic_distributor_params["std_phase_interferometer_drift"] = 14.3 * np.pi / 180
    linenetwork_params = {
        'number_of_nodes': number_of_nodes,
        'internode_distance': internode_distance,
        'qmem_config_params': qmem_config_params,
        'distributor_params': magic_distributor_params,
    }
    cycle_time = magic_distributor_params["cycle_time"]
    logic_params = \
        {"delivery_params": {"alpha": alpha, "cycle_time": cycle_time}}

    # Setup the simulation
    sim_setup = SimulationSetup(linenetwork_params=linenetwork_params,
                                logic_params=logic_params)

    return sim_setup, cycle_time


def add_statetappers(sim_setup):

    # Add data collection tools: recording fidelity once the protocol has finished
    for protocol_name_to_track in ['distilprot', 'entgenprot']:

        protocol_to_track = sim_setup.protocols[1][protocol_name_to_track]

        statetapper = StateTapper(
            memorymanager_A=sim_setup.protocols[1]['memmanager'],
            memorymanager_B=sim_setup.protocols[0]['memmanager'],
            qmemory_A=sim_setup.ln.qnodes[1].qmemory,
            qmemory_B=sim_setup.ln.qnodes[0].qmemory,
            node_index_A=1,
            node_index_B=0,
            wait_params={"event_type": protocol_to_track.evtype_finished,
                         "entity": protocol_to_track},
            apply_Hadamard_if_data_qubit=True)
        sim_setup.add_datatapper(statetapper)

    # Add recorder that records the time every time a subprotocol
    # is finished
    subprotfinishedtimetapper = WaitingTimeTapper(
        wait_params={"event_type": sim_setup.protocols[0]['distilprot'].evtype_finished})
    sim_setup.add_datatapper(subprotfinishedtimetapper)


if __name__ == "__main__":

    number_of_runs = 5000
    # for alpha in [0.5, 0.1]:

    for alpha in list(reversed([0.1 * (x + 1) for x in range(5)])):

        print("-----alpha={}------".format(alpha))

        # setup simulation
        entgen_fidelities = []
        distil_fidelities = []

        sim_setup, cycle_time = get_distillation_experiment(alpha=alpha)
        add_statetappers(sim_setup=sim_setup)
        [distil_statetapper, entgen_statetapper, subprotfinishedtimetapper] = \
            sim_setup.datatappers

        # run simulation
        for run_index in range(number_of_runs):
            sim_setup.reset()
            sim_setup.clear_datatappers()
            set_distillation_logic(sim_setup)

            sim_setup.perform_run()

            if len(distil_statetapper.vals) == 1:
                duration_2nd_generation = (lambda x: x[2] - x[1])(subprotfinishedtimetapper.vals)
                if duration_2nd_generation < 50 * cycle_time:
                    distil_fidelities += [distil_statetapper.vals[0].fidelity]

            assert(len(entgen_statetapper.vals) != 0)
            entgen_fidelities += [entgen_statetapper.vals[0].fidelity]

        # plotting
        for name, fids, color in [("entgen", entgen_fidelities, 'b'),
                                  ("distil", distil_fidelities, 'r')]:
            print('avg {} fidelity: {} pm {} (number of points={})'.format(
                name, np.mean(fids), np.std(fids), len(fids)))
            plt.errorbar([alpha], [np.mean(fids)],
                         yerr=[np.std(fids) / np.sqrt(len(fids))],
                         color=color, fmt='o', label=name)

    plt.xlabel(r"$\alpha = \sin^2(\theta)$")
    plt.ylabel("Bell-state fidelity")
    plt.show()
