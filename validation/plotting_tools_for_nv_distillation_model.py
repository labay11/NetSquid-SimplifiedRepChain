"""
Plotting tools for the analytical model in
:py:mod:`repchain.validation.nv_distillation_model`.
"""
import repchain.validation.nv_distillation_model as model
import netsquid.qubits.qubitapi as qapi
import matplotlib.pyplot as plt
import netsquid as ns

# TODO should not need to import this! remove dependencies!
import netsquid_nv.Delft_NVs.Delft_NV_parameters_2017_distillation_experiment as Delft_2017


def compute_fidelity(alpha, n, p_meas_error_0, p_meas_error_1,
                     delta_phase, pWaitDampingA, phase_per_attempt,
                     tau, initial_phase, pWaitDampingB, p_gate_error):
    """
    Computes the fidelity of the elementary and distilled link according
    to the model as implemented in
    :py:mod:`repchain.validation.nv_distillation_model`.

    Parameters
    ----------
    See :py:mod:`repchain.validation.nv_distillation_model` for
    the meaning of the input parameters.

    Returns
    -------
    float, float
        Fidelity of the elementary link, fidelity of the distilled link
    """
    meas_operator = \
        model._get_measurement_operator(outcome=1,
                                        p_meas_error_0=p_meas_error_0,
                                        p_meas_error_1=p_meas_error_1)
    p_phase_error = \
        model.pLossOfPhase(n=143 + n,
                           phase_per_attempt=delta_phase)
    p_carbon_dephasingA = \
        model.pCombinedGauss(n=n,
                             pWaitDamping=pWaitDampingA,
                             phase_per_attempt=phase_per_attempt,
                             tau=tau,
                             initial_phase=initial_phase)
    p_carbon_dephasingB = \
        model.pCombinedGauss(n=n,
                             pWaitDamping=pWaitDampingB,
                             phase_per_attempt=phase_per_attempt,
                             tau=tau,
                             initial_phase=initial_phase)
    input_state = \
        model.get_input_state(alpha=alpha,
                              **Delft_2017.magic_distributor_params)
    elementary_link_state, distilled_state = \
        model.execute_noisy_distillation_protocol(
            alpha=alpha,
            input_state=input_state,
            p_gate_error=p_gate_error,
            meas_operator=meas_operator,
            p_phase_error=p_phase_error,
            p_carbon_dephasingA=p_carbon_dephasingA,
            p_carbon_dephasingB=p_carbon_dephasingB)

    # compute fidelity of elementary link
    fresh_electrons = qapi.create_qubits(2)
    qapi.assign_qstate(fresh_electrons, elementary_link_state)
    elementary_link_fidelity = qapi.fidelity(fresh_electrons, ns.qubits.ketstates.b01, squared=True)
# compute fidelity of distilled link
    fresh_carbons = qapi.create_qubits(2)
    qapi.assign_qstate(fresh_carbons, distilled_state)
    # rotate first because carbons are in rotated basis
    for carbon_qubit in fresh_carbons:
        qapi.operate(carbon_qubit, ns.qubits.operators.H)
    fidelity = qapi.fidelity(fresh_carbons, ns.qubits.ketstates.b01, squared=True)

    return elementary_link_fidelity, fidelity


def compute_mean_fidelity(max_number_of_attempts=50, **kwargs):
    """
    Computes the mean fidelity of the elementary and distilled link according
    to the model as implemented in
    :py:mod:`repchain.validation.nv_distillation_model`,
    averaged over the number of entanglement attempts needed for generating
    the second link.

    TODO(!) currently takes the uniform average, but this should become
    the geometric distribution average.
    """
    mean_fidelity = 0
    # TODO the article takes the average fidelity over all number of attempts,
    # but this is incorrect! should be weighted average with geometric
    # distribution with probability of an attempt being successful
    for n in [x + 1 for x in range(max_number_of_attempts)]:
        elementary_link_fidelity, fidelity = compute_fidelity(n=n, **kwargs)
        mean_fidelity += fidelity
    mean_fidelity = mean_fidelity / max_number_of_attempts
    return elementary_link_fidelity, mean_fidelity


def produce_bar_plot(xvals, yvals, base_y=0):
    """
    Produces a horizontal bar plot

    Parameters
    ----------
    xvals : list of Any
    yvals : list of float
    base_y : float
        Determines whether the plot is absolute (`base_y=0`)
        or relative (all values in `yvals` divided by `base_y`).

    Returns
    -------
    :obj:`matplotlib.pyplot`
    """
    if len(xvals) != len(yvals):
        raise Exception
    num = len(xvals)
    if base_y == 0:
        yvals_updated = yvals
    else:
        yvals_updated = [y / base_y for y in yvals]
    plt.barh(range(num), yvals_updated)
    plt.yticks(range(num), xvals)
    return plt
