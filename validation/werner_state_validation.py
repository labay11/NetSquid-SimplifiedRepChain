import sys
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt

sys.path.insert(0, str(Path.cwd().parent))
sys.path.insert(0, str(Path.cwd().parent.parent.joinpath('nlblueprint')))

from repchain.runtools.simulation_setup import SimulationSetup  # noqa: 402
from repchain.runtools.data_collection_tools import get_statetapper_between_fixed_nodes, get_waitingtimetapper  # noqa: 402

"""
This script produces a plot of the end-to-end fidelity of a repeater chain against the fidelity of the elementary links,
assuming that these links are Werner states. The plot shows both simulation results and the expected theoretical curve,
taken from arXiv:quant-ph/9808065.

The plots are for the ideal, noiseless, case.

"""


def add_default_tappers(sim_setup):
    # Add data collection tools: recording fidelity once the protocol has finished

    statetapper = get_statetapper_between_fixed_nodes(sim_setup=sim_setup)
    sim_setup.add_datatapper(statetapper)

    # Ensuring the simulation stops as soon as a single link has been created
    node_index_A = 0
    node_index_B = sim_setup.number_of_nodes - 1
    for (nodeX, nodeY) in [(node_index_A, node_index_B), (node_index_B, node_index_A)]:
        sim_setup.protocols[nodeX]["logic"].should_stop_fn = \
            lambda nodeX=nodeX, nodeY=nodeY: len(
                sim_setup.protocols[nodeX]["memmanager"].entangled_positions_with(nodeY)) > 0
    sim_setup.set_automatic_stop()

    # Add recording of timestamps at which the first (between leftmost and middle node)
    # and second elementary link (between middle and rightmost node) were generated
    # waiting_time_tapper = get_waitingtimetapper(collect_postsimulation=False)
    # entity = sim_setup.protocols[node_index_B]['entgenprot']
    # evtype = entity.evtype_finished
    # waiting_time_tapper.collect_on([(entity, evtype)])
    # sim_setup.add_datatapper(waiting_time_tapper)

    return sim_setup.datatappers


def setup_input_dic():
    input_dic = {}
    input_dic['elementary_link_succ_prob'] = 1
    input_dic['tot_num_qubits'] = 10
    input_dic['internode_distance'] = 1
    input_dic['cycle_time'] = 1
    input_dic['p_loss_length'] = 0.
    input_dic['swap_fidelity_threshold'] = 0.
    input_dic['swap_quality'] = 1.
    input_dic['T1'] = 0.
    input_dic['T2'] = 0.
    input_dic['werner'] = True
    input_dic['single_qubit_gate_duration'] = 5
    input_dic['initialization_duration'] = 2000
    input_dic['measurement_duration'] = 3700
    input_dic['two_qubit_gate_duration'] = 1.542e6
    input_dic['use_swap_only'] = True

    return input_dic


def ideal_werner_end_to_end_fidelity(gamma, number_repeaters):
    number_elementary_links = number_repeaters + 1
    return 0.25 * (1 + 3 * ((4 * gamma - 1) / 3)**number_elementary_links)


def cl(x, x_min, x_max):
    r = (x - x_min) / (x_max - x_min)
    return (r, 0, 1 - r)


def plot_sim_results_against_prediction(av_fidelities, err_fidelities, gammas, number_repeaters):
    # plt.rcParams.update({'font.size': 18})
    plt.xlabel('Werner Elementary Link Fidelity')
    plt.ylabel('End-to-end fidelity')
    plt.grid()

    for i, repeater in enumerate(number_repeaters):
        predicted_fidelities = ideal_werner_end_to_end_fidelity(gammas, repeater)
        plt.errorbar(gammas, av_fidelities[i], yerr=err_fidelities[i], linestyle='None', marker='s',
                     c=cl(i, 0, len(number_repeaters)),
                     label='Simulation data points for {} repeaters'.format(repeater))
        plt.plot(gammas, predicted_fidelities, c=cl(i, 0, len(number_repeaters)),
                 label='Theoretical Prediction for {} repeaters'.format(repeater))

    plt.legend()
    plt.savefig('data/werner_state_validation.png')
    plt.show()


gammas = np.linspace(0.5, 1., 100)
input_dic = setup_input_dic()
number_repeaters = [1, 5, 10]
number_of_runs = 2
av_fidelities = []
err_fidelities = []

for repeaters in number_repeaters:
    av_fidelities_rep = []
    err_fidelities_rep = []

    for gamma in gammas:
        input_dic['elementary_link_fidelity'] = gamma
        input_dic['number_of_nodes'] = repeaters + 2
        sim_setup = SimulationSetup.from_parameters(**input_dic)
        [statetapper] = add_default_tappers(sim_setup=sim_setup)
        sim_setup.perform_many_runs(number_of_runs=number_of_runs)
        av_fidelities_rep.append(statetapper.dataframe['fidelity'].mean())
        err_fidelities_rep.append(statetapper.dataframe['fidelity'].std())

    av_fidelities.append(av_fidelities_rep)
    err_fidelities.append(err_fidelities_rep)

plot_sim_results_against_prediction(av_fidelities=av_fidelities, err_fidelities=err_fidelities, gammas=gammas,
                                    number_repeaters=number_repeaters)
