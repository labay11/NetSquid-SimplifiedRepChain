r"""
The model of a distilled state on NVs from Kalb et al. (2017)
'Entanglement distillation between solid-state quantum network nodes'

The distillation protocol is performed by two remote NV-centers in diamond
and consists of the following four operations:

    A. generating an entangled state on the two electron spins
    B. on both nodes: swapping the electron-spin state onto the carbon memory
       qubits.
    C. repeat step A
    D. perform the distillation circuit. The circuit includes a measurement
       of the electron spin state on each side: declare success if both
       measurement outcomes are 0.

We model the state produced by the distillation protocol as follows, lightly
following the modelling of the paper as given in section G.2 of the
supplementary material.

First, assume that :math:`n` rounds of entanglement attempts are needed
for producing the *second* entangled pair on the electrons.

  1. produce the noisy entangled state of the two electron spins as
     specified in section G.1 of the paper.
  2.  On each node:

      1. perform a perfect version of the circuit that swaps ('moves') the
         electron-spin state onto the carbon memory qubits.
      2. Subsequently perform a two-qubit depolarization channel onto the
         electron- and carbon qubit.
      3. Initialize the electron by applying the noisy
         projector :math:`\sqrt{p_0} |0>< 0 | + \sqrt{1 - p_1} |1>< 1|`.

         to it and subsequently normalize the state again. Finish by tracing
         out the electron.

      The state that is left is a (mixed) two-qubit state of the two carbons.

  3. Repeat step 1. Moreover:

      1. apply the single-qubit dephasing channel to *one* of the electrons
      2. to each of the two carbons individually: apply a single-qubit dephasing
         channel.

  4. On each node:

      1. perform a perfect version of the distillation circuit.
      2. perform a two-qubit depolarization channel onto the electron-
         and carbon qubit.
      3. Perform step 2.3

  5. The state on the carbons is now the distilled pair in a rotated basis.
     We rotate it back by performing a perfect Hadamard on each.

"""
import netsquid as ns
from netsquid.components.qprocessor import QuantumProcessor
import netsquid.qubits.qubitapi as qapi
import numpy as np
from netsquid_nv.state_delivery_sampler_factory import NVStateDeliverySamplerFactory
from netsquid.qubits.operators import I, X, Y, Z
from repchain.protocols.moveprotocol import CXDirectionMoveProgram
from repchain.protocols.distillation_protocol import NVDistillationProgram


_single_qubit_paulis = [I, X, Y, Z]
_two_qubit_paulis = [pauli_A ^ pauli_B
                     for pauli_A in _single_qubit_paulis
                     for pauli_B in _single_qubit_paulis]


def parameters():
    """
    """
    # TODO find out whether initial phase and total phase are about
    # the same phase; there is the phase difference between the two generated
    # links and the phase drift of the interferometer and the phase difference
    # between the incoming photons: are these all the same?
    return {
        "p_gate_error": 0.0163,
        "p_meas_error_0": 1. - 0.9962,
        "p_meas_error_1": 1. - 0.9379,
        "phase_per_attempt": 0.314 * np.pi / 180,
        "pWaitDampingA": 1 - 0.039,
        "pWaitDampingB": 1,
        "tau": np.sqrt(2) * 197,
        "delta_phase": 0.073 * np.pi / 180,
        "initial_phase": 24 * np.pi / 180}


def perfect_parameters():
    """
    """
    return {
        "p_gate_error": 0.,
        "p_meas_error_0": 0.,
        "p_meas_error_1": 0.,
        "phase_per_attempt": 0.,
        "pWaitDampingA": 1,
        "pWaitDampingB": 1,
        "tau": 10 ** 9,  # some very high number
        "delta_phase": 0.,
        "initial_phase": 0.}


def _apply_two_qubit_depolarization(qubits, depolar_prob):
    """
    Applies an arbitrary two-qubit Pauli string (other
    than the identity string) to `qubits` with
    probability `depolar_prob` and otherwise leaves their
    state untouched.

    Parameters
    ----------
    qubits : list of :obj:`netsquid.qubits.qubit.Qubit`
    depolar_prob : float

    Returns
    -------
    None
    """
    assert(len(qubits) == 2)
    qapi.combine_qubits(qubits)
    qstate = qubits[0].qstate

    assert(ns.get_qstate_formalism() == ns.QFormalism.DM)
    p_weights = [1 - depolar_prob] + [depolar_prob / 15.] * 15
    qstate.stoch_operate_qubits(qubits,
                                _two_qubit_paulis,
                                p_weights=p_weights)


def _get_measurement_operator(outcome, p_meas_error_0, p_meas_error_1):
    """
    Parameters
    ----------
    outcome : int
        1 or -1
    p_meas_error_0 : float
    p_meas_error_1 : float

    Notes
    -----
    Identical to the mechanism in easysquid.gatenoise.MeasureQuantumNoiseModel.
    """
    if outcome not in [-1, 1]:
        raise Exception
    if outcome == 1:
        observable_op = np.diag([np.sqrt(1 - p_meas_error_0), np.sqrt(p_meas_error_1)])
    else:
        observable_op = np.diag([np.sqrt(p_meas_error_0), np.sqrt(1 - p_meas_error_1)])
    return ns.qubits.operators.Operator("noisy_meas_operator", observable_op)


def _apply_measurement(qubit, meas_operator, discard=False):
    """
    Parameters
    ----------
    qubit: :obj:`netsquid.qubits.qubit.Qubit`
    discard : bool
    """
    assert(ns.get_qstate_formalism() == ns.QFormalism.DM)
    qstate = qubit.qstate
    qstate.operate_qubits(qubits=[qubit], operator=meas_operator)

    # normalize
    qstate.dm = qstate.dm / np.trace(qstate.dm)

    if discard:
        qapi.discard(qubit)


def execute_noisy_move_circuit(qubits, meas_operator, p_gate_error):
    """
    Performs step (2), i.e. on both nodes:

      * the perfect circuit for swapping the electron spin state onto
        the carbon spin state
      * a two-qubit depolarization channel on the electron-carbon pair
      * initialization of the electron by noisy measurement

    Parameters
    ----------
    qubits : list of :obj:`netsquid.qubits.qubit.Qubit`
        Four qubits, in the ordering ElectronNodeA, ElectronNodeB, CarbonNodeA, CarbonNodeB
    p_gate_error : float
    meas_operator : :obj:`netsquid.qubits.operator.Operator`

    Returns
    -------
    qubits : list of :obj:`netsquid.qubits.qubit.Qubit`
        The four qubits in the same ordering as the input.

    Example
    -------

    >>> qubits = qapi.create_qubits(4)
    >>> execute_noisy_move_circuit(qubits=qubits)
    >>> zerozerostate = ns.qubits.ketstates.ket2dm(ket=ns.qubits.ketstates.s00)
    >>> qubits = execute_noisy_circuit(first_input_state=zerozerostate, second_input_state=None)
    >>> f = qapi.fidelity(qubits=qubits, reference_ket=ns.qubits.ketstates.h00, squared=True)
    >>> print(f)
    >>> # should equal 1
    """
    assert(len(qubits) == 4)
    assert(ns.get_qstate_formalism() == ns.QFormalism.DM)

    qmemory = QuantumProcessor(name='x', num_positions=4, fallback_to_nonphysical=True)
    qmemory.put(qubits=[qubits[0], qubits[1]], positions=[0, 1])
    qmemory.put(qubits=[qubits[2], qubits[3]], positions=[2, 3])

    # Apply the entanglement swap
    for positions in [[0, 2], [1, 3]]:
        move_program = CXDirectionMoveProgram()
        qmemory.execute_program(move_program, qubit_mapping=positions)
        ns.sim_run()

    # Apply the noise from the entanglement swap:
    # for each of the two two-qubit gates, apply two-qubit depolarization
    for positions in [[0, 2], [1, 3]]:
        qubits = qmemory.pop(positions=positions)
        _apply_two_qubit_depolarization(qubits=qubits,
                                        depolar_prob=p_gate_error)
        qmemory.put(qubits=qubits, positions=positions)

    # Initialize the electrons; i.e. get the post-measurement state
    # conditioned on outcome 00 (the outcome '0' is the +1-eigenstate of
    # the measurement operator
    for position in [0, 1]:
        [qubit] = qmemory.pop(positions=[position])
        _apply_measurement(qubit, meas_operator=meas_operator)
        qmemory.put(qubits=[qubit], positions=[position])

    return qmemory.pop(positions=[0, 1, 2, 3])


def add_noise_from_second_link_generation(qubits, p_phase_error=0, p_carbon_dephasingA=0, p_carbon_dephasingB=0):
    """
    Performs the noise onto the second generated link as in step (3).

    Parameters
    ----------
        qubits : list of :obj:`netsquid.qubits.qubit.Qubit`
            Four qubits, in the ordering ElectronNodeA, ElectronNodeB, CarbonNodeA, CarbonNodeB
        p_phase_error : float
        p_carbon_dephasingA : float
        p_carbon_dephasingB : float

    Returns
    -------
    None
    """

    [electronA, electronB, carbonA, carbonB] = qubits

    # modify the state on the electrons
    qapi.dephase(qubit=electronA, prob=p_phase_error)

    # the stored state suffers from dephasing
    for (carbon, p_deph) in [(carbonA, p_carbon_dephasingA),
                             (carbonB, p_carbon_dephasingB)]:
        qapi.dephase(qubit=carbon, prob=p_deph)


def execute_noisy_distillation_circuit(qubits, meas_operator, p_gate_error=0):
    """
    Performs step (4).

    Parameters
    ----------
        qubits : list of :obj:`netsquid.qubits.qubit.Qubit`
            Four qubits, in the ordering ElectronNodeA, ElectronNodeB, CarbonNodeA, CarbonNodeB
        meas_operator : :obj:`netsquid.qubits.operator.Operator`

    Returns
    -------
    None
    """

    # apply perfect distillation circuit
    qmemory = QuantumProcessor(name='x', num_positions=4, fallback_to_nonphysical=True)
    qmemory.put(qubits=qubits, positions=[0, 1, 2, 3])
    for positions in [[0, 2], [1, 3]]:
        distillation_program = NVDistillationProgram()
        distillation_program._include_measurement = False
        qmemory.execute_program(distillation_program, qubit_mapping=positions)
        ns.sim_run()

    # apply noise during distillation circuit
    # TODO no need to put qubits back into qprocessor
    for positions in [[0, 2], [1, 3]]:
        qs = qmemory.pop(positions=positions)
        _apply_two_qubit_depolarization(qubits=qs, depolar_prob=p_gate_error)
        qmemory.put(qubits=qs, positions=positions)

    # compute postmeasurement state conditioned on measuring 00 on both qubits
    for electron_position in [0, 1]:
        [q] = qmemory.pop(positions=[electron_position])
        _apply_measurement(qubit=q,
                           meas_operator=meas_operator,
                           discard=True)


def get_input_state(**kwargs):
    """
    Performs the generation of the electron-electron state as in steps (1) and (3).

    Parameters
    ----------
    **kwargs
        Any parameters to this method are directly passed onto the function
        `create_state_delivery_sampler` of the
        :obj:`netsquid_nv.state_delivery_sampler.NVStateDeliverySamplerFactory`

    """
    factory = NVStateDeliverySamplerFactory()
    state_delivery_sampler = factory.create_state_delivery_sampler(**kwargs)

    done = False
    while not done:
        sample = state_delivery_sampler.sample()
        state_first = sample.state
        done = (sample.label[1] != 1)
    # testing: state_first = ns.qubits.kettools.ket2dm(ns.qubits.ketstates.b01)
    return state_first


def execute_noisy_distillation_protocol(alpha, input_state, meas_operator, p_gate_error=0, p_phase_error=0,
                                        p_carbon_dephasingA=0, p_carbon_dephasingB=0):
    """
    Main method in this module. Computes the output state after steps (1) - (4).
    Uses all other methods in this function.

    Parameters
    ----------
    alpha : float
        Bright-state parameter
    input_state : numpy.array
    meas_operator : :obj:`netsquid.qubits.operator.Operator`
    p_gate_error : float
    p_phase_error : float
    p_carbon_dephasingA : float
    p_carbon_dephasingB : float

    Returns
    -------
    Tuple of two numpy.array objects
        Density matrix of the elementary link (named 'raw' state in the paper)
        and the distilled link.
    """

    ns.set_qstate_formalism(ns.QFormalism.DM)
    qubits = qapi.create_qubits(4)
    [electronA, electronB, carbonA, carbonB] = qubits

    # generate first link
    state_first = input_state
    state_second = np.copy(state_first)
    state_copy = np.copy(state_first)

    qapi.assign_qstate([electronA, electronB], state_first)

    # move the link onto the carbons
    [electronA, electronB, carbonA, carbonB] = \
        execute_noisy_move_circuit(qubits=qubits,
                                   p_gate_error=p_gate_error,
                                   meas_operator=meas_operator)

    # adding second link
    qapi.assign_qstate([electronA, electronB], state_second)
    add_noise_from_second_link_generation(qubits=[electronA, electronB, carbonA, carbonB],
                                          p_phase_error=p_phase_error,
                                          p_carbon_dephasingA=p_carbon_dephasingA,
                                          p_carbon_dephasingB=p_carbon_dephasingB)

    # perform the distillation circuit
    execute_noisy_distillation_circuit(qubits=[electronA, electronB, carbonA, carbonB],
                                       p_gate_error=p_gate_error,
                                       meas_operator=meas_operator)

    qapi.combine_qubits([carbonA, carbonB])
    carbon_state = carbonA.qstate.dm

    return state_copy, carbon_state


def pCombinedGauss(n=0, pWaitDamping=0, phase_per_attempt=0, tau=1, initial_phase=0):
    r"""
    Implements
    :math:`\frac{1}{2} \cdot \left(1 - \text{pWaitDamping} \cdot e^{- (\text{phase_per_attempt} * n) ^ 2 / 2 -
    (n / \text{tau}) ^ 2 - \text{initial_phase} ^ 2}\right)`
    """
    first_power = np.exp(-0.5 * (n * phase_per_attempt) ** 2)
    second_power = np.exp(-1 * (n / tau) ** 2)
    third_power = np.exp(-1 * initial_phase ** 2)
    ret = 0.5 * (1 - pWaitDamping * first_power * second_power * third_power)
    return ret


def pLossOfPhase(n=0, phase_per_attempt=0):
    r"""
    Implements :math:`\frac{1}{2} \cdot \left(1 - e ^ { - (n \cdot \text{phase_per_attempt}) ^ 2 / 2}\right)`
    """
    return 0.5 * (1 - np.exp(-0.5 * (n * phase_per_attempt) ** 2))
