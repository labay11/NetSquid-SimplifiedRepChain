"""This script should output a plot to compare the abstract repchain with the full nv repchain.

The idea is to compare both scenarios for different number of nodes, total distance and alpha values
for both, swap-only and distillation. Since the full NV repchain performs the same distillation in all
levels we must do the same for the repchain.

Usage
-----
python3 example_basic.py
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

from repchain.utils.latexify import latexify, savefig

import sys
from pathlib import Path

from compare_netsquid import NetsquidParametersx1, NetsquidParametersx10

MARKERS = ['o', '^', 'x', 'd', 's']
CMAPS = ['Blues', 'Reds', 'Greens', 'Greys', 'Purples']
COLORS = ['b', 'r', 'g', 'y', 'm', 'c', 'o', 'k']
LS = ['--', '-']


def success_probability(distance, param_set):
    return 2 * param_set.alpha * param_set.p_det * np.power(10, - 0.1 * (distance * 0.5) * param_set.p_loss_length)


def sequential_swap_only(nodes, total_dist, param_set):
    internode_distance = total_dist / (nodes - 1)
    p_det_with_fiber = param_set.p_det * np.power(10, - 0.1 * (internode_distance * 0.5) * param_set.p_loss_length)
    p_gen = 2 * param_set.alpha * p_det_with_fiber
    # p_gen = np.array([single_click(alpha, dist / 2.)[1] for dist in internode_distance])
    cycle_with_travel_time = param_set.cycle_time + 1e9 * internode_distance / param_set.c
    if nodes > 2:
        times = (nodes / 2) * cycle_with_travel_time / p_gen
    else:
        times = cycle_with_travel_time / p_gen

    return np.concatenate((total_dist.reshape(-1, 1), 1e9 / times.reshape(-1, 1)), axis=1)


def plot_ppt(params, dark_count_limit):
    latexify(plt, margin_y=0.05)
    fig, axs = plt.subplots(nrows=3, ncols=len(params), sharex=True, sharey='row',
                            gridspec_kw={'height_ratios': [4, 3, 1]})
    plt.subplots_adjust(hspace=0.1, wspace=0.1)

    axs[0, 0].set_ylabel('Fidelity')
    axs[1, 0].set_ylabel('Rate (Hz)')

    for j, nodes in enumerate(params):
        axs[0, j].grid(True)
        axs[1, j].grid(True)
        axs[2, j].grid(True)
        axs[1, j].set_yscale('log')
        axs[2, j].set_yscale('log')
        axs[2, j].set_xlabel('Distance (km)')
        label = f'(b) {nodes - 2} repeaters'
        if nodes == 2:
            label = '(a) No repeater'
        elif nodes == 3:
            label = '(b) 1 repeater'
        axs[0, j].set_title(label)

        if nodes in dark_count_limit:
            for k, v in enumerate(dark_count_limit[nodes]):
                axs[0, j].axvline(v, 0, 1, c='k', ls=LS[k])
                axs[1, j].axvline(v, 0, 1, c='k', ls=LS[k])
                axs[2, j].axvline(v, 0, 1, c='k', ls=LS[k])

        for k, file_list in enumerate(params[nodes]):
            for m, data_file in enumerate(file_list):
                if data_file is None:
                    continue
                if isinstance(data_file, str):
                    data = np.load(data_file)
                else:
                    data = data_file
                has_fid = data.shape[-1] > 2
                has_unc = data.shape[-1] == 5

                cf, cr = (1, 3) if has_unc and has_fid else (1, 2)
                if not has_fid:
                    cf, cr = -1, 1

                if has_fid:
                    axs[0, j].plot(data[:, 0], data[:, cf], ls=LS[m], color=COLORS[k])
                axs[1, j].plot(data[:, 0], data[:, cr], ls=LS[m], color=COLORS[k])
                axs[2, j].plot(data[:, 0], data[:, cr], ls=LS[m], color=COLORS[k])
                if has_unc:
                    if has_fid:
                        axs[0, j].fill_between(data[:, 0], y1=data[:, cf] - data[:, cf + 1],
                                               y2=data[:, cf] + data[:, cf + 1],
                                               color=COLORS[k], alpha=0.2, lw=0)
                    if k == 1:
                        data[:, cr + 1] /= 10
                    axs[1, j].fill_between(data[:, 0], y1=data[:, cr] - data[:, cr + 1],
                                           y2=data[:, cr] + data[:, cr + 1],
                                           color=COLORS[k], alpha=0.2, lw=0)
                    axs[2, j].fill_between(data[:, 0], y1=data[:, cr] - data[:, cr + 1],
                                           y2=data[:, cr] + data[:, cr + 1],
                                           color=COLORS[k], alpha=0.2, lw=0)

        # zoom-in / limit the view to different portions of the data
        axs[1, j].set_ylim(2e-7, 1e3)  # outliers only
        axs[2, j].set_ylim(1e-15, 2e-7)  # most of the data

        # hide the spines between ax and ax2
        axs[1, j].spines['bottom'].set_visible(False)
        axs[2, j].spines['top'].set_visible(False)
        axs[1, j].xaxis.tick_top()
        axs[1, j].tick_params(labeltop=False)  # don't put tick labels at the top
        axs[2, j].xaxis.tick_bottom()

        # Now, let's turn towards the cut-out slanted lines.
        # We create line objects in axes coordinates, in which (0,0), (0,1),
        # (1,0), and (1,1) are the four corners of the axes.
        # The slanted lines themselves are markers at those locations, such that the
        # lines keep their angle and position, independent of the axes size or scale
        # Finally, we need to disable clipping.

        d = .5  # proportion of vertical to horizontal extent of the slanted line
        kwargs = dict(marker=[(-1, -d), (1, d)], markersize=8,
                      linestyle="none", color='k', mec='k', mew=0.7, clip_on=False)
        axs[1, j].plot([0, 1], [0, 0], transform=axs[1, j].transAxes, **kwargs)
        axs[2, j].plot([0, 1], [1, 1], transform=axs[2, j].transAxes, **kwargs)

    return fig


if __name__ == "__main__":
    distance = np.linspace(20, 1500, 100)
    p_gen = success_probability(distance, NetsquidParametersx1)
    index = np.argmax(p_gen < np.power(10., np.ceil(np.log10(NetsquidParametersx1.p_dark_count))))
    dist_dark_count_x1 = (distance[index] + distance[index - 1]) * 0.5
    p_gen = success_probability(distance, NetsquidParametersx10)
    index = np.argmax(p_gen < np.power(10., np.ceil(np.log10(NetsquidParametersx10.p_dark_count))))
    dist_dark_count_x10 = (distance[index] + distance[index - 1]) * 0.5

    print('Limiting distance 2 x1:', dist_dark_count_x1)
    print('Limiting distance 2 x10:', dist_dark_count_x10)

    rate2 = sequential_swap_only(2, distance[distance < dist_dark_count_x1], NetsquidParametersx1)
    rate5 = sequential_swap_only(5, distance, NetsquidParametersx1)
    rate2x10 = sequential_swap_only(2, distance[distance < dist_dark_count_x10], NetsquidParametersx10)
    rate5x10 = sequential_swap_only(5, distance, NetsquidParametersx10)
    # outdir = 'comparison_nice/'
    outdir = 'data_2021-03-20_20.02/'
    outdir = 'data_2021-03-21_11.00/'
    outdir = 'data_2021-03-29_14.30/'
    params = {
        2: [
            [outdir + '2nodes1x.npy', outdir + '2nodes10x.npy'],
            # '/home/adria/Documents/GitLab/netsquid-repchain/validation/comparison_2021-03-07_20.24/output/abs_2_1_better.npy',
            [outdir + 'abs_2_swap_x1_full.npy', outdir + 'abs_2_swap_x10_full.npy'],
            [outdir + 'abs_2_swap_x1_approx.npy', outdir + 'abs_2_swap_x10_approx.npy']
            # [rate2, rate2x10]
        ],
        5: [
            [outdir + '5nodes1x.npy', outdir + '5nodes10x.npy'],
            # '/home/adria/Documents/GitLab/netsquid-repchain/validation/comparison_2021-03-07_20.24/output/abs_5_1_better.npy',
            # '/home/adria/Documents/GitLab/netsquid-repchain/validation/abs_5_1_aaa.npy',
            [outdir + 'abs_5_swap_x1_full.npy', outdir + 'abs_5_swap_x10_full.npy'],
            [outdir + 'abs_5_swap_x1_approx.npy', outdir + 'abs_5_swap_x10_approx.npy']
            # [rate5, rate5x10]
        ]
    }

    fig = plot_ppt(params, {2: [dist_dark_count_x1, dist_dark_count_x10]})
    nodes_legend = fig.legend(
        [Line2D([0], [0], lw=2, color=COLORS[n]) for n in range(3)],
        ['NV', r'Abstract ($\eta_f < 1$)', r'Abstract ($\eta_f = 1$)'],
        title='Model', loc='upper right', ncol=3, bbox_to_anchor=(0.6, 1.)
    )
    plt.gca().add_artist(nodes_legend)
    protocol_legend = fig.legend(
        [Line2D([0], [0], lw=2, color='k', ls=LS[n]) for n in range(2)],
        ['near term', 'x10'],
        title='Hardware Quality', loc='upper left',
        ncol=2,
        bbox_to_anchor=(0.6, 1.)
    )
    # fig.legend(
    #     [
    #         Line2D([0], [0], color='k', ls='None'),
    #         Line2D([0], [0], color=COLORS[0]),
    #         Line2D([0], [0], color=COLORS[1]),
    #         Line2D([0], [0], color=COLORS[2]),
    #         Line2D([0], [0], color='k', ls='None'),
    #         Line2D([0], [0], color='k', ls=LS[0]),
    #         Line2D([0], [0], color='k', ls=LS[1]),
    #      ],
    #     [r'$\mathbf{Model}$', 'NV', r'Abstract ($\eta_f < 1$)', r'Abstract ($\eta_f = 1$)',
    #      r'$\mathbf{Hardware\ quality}$', 'near term', 'x10'],
    #     loc="upper center",   # Position of legend
    #     # borderaxespad=0.1,    # Small spacing around legend box
    #     ncol=2
    # )
    fig.subplots_adjust(top=0.85)
    savefig(plt, outdir + 'validation')
    plt.show()
