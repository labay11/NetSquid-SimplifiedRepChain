#!/bin/bash
#SBATCH --nodes=1
#SBATCH --time=5:00:00
#SBATCH --partition=normal
#SBATCH --mail-user=adria.labay@gmail.com
#SBATCH --mail-type=END,FAIL

echo nodes $1
echo distance $2

currentdir=$PWD
echo $currentdir

# create tmp dir and copy run file
tmp_dir="$(mktemp -d -p /scratch-shared)"
echo $tmp_dir
cp compare_netsquid.py $tmp_dir
cd $tmp_dir
out_dir=$tmp_dir/output
mkdir -p $out_dir

# add repchain to path
export PYTHONPATH="$PYTHONPATH:/home/alabay/nlblueprint"
export PYTHONPATH="$PYTHONPATH:/home/alabay/NetSquid-SimplifiedRepChain"

# init conda and execute script
module purge
module load 2019
module load Miniconda3
source /sw/arch/RedHatEnterpriseServer7/EB_production/2019/software/Miniconda3/4.7.10/etc/profile.d/conda.sh
conda activate netsquid
for nodes in 2 5
do
    for improved in 0 1
    do
        for approx in 0 1
        do
            python3 compare_netsquid.py --nodes $nodes --improved $improved --approx $approx --outputdir $out_dir
        done
    done
done

conda deactivate

# copy the contents in the tmp folder to the output folder in this directory
time_stamp=$(date +'%Y-%m-%d_%H.%M')
folder_name=data_$time_stamp

mkdir -p $currentdir/$folder_name
cp -r $out_dir $currentdir/$folder_name
cd $currentdir
rm -r $tmp_dir
