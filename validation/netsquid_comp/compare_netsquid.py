"""This script should output a plot to compare the abstract repchain with the full nv repchain.

The idea is to compare both scenarios for different number of nodes, total distance and alpha values
for both, swap-only and distillation. Since the full NV repchain performs the same distillation in all
levels we must do the same for the repchain.

Usage
-----
python3 example_basic.py
"""
import os
import gc
from argparse import ArgumentParser

import numpy as np
from joblib import Parallel, delayed

from repchain.runtools.data_analysis_tools import run_many_simulations
from repchain.parameters import AbstractParameterSet, fiber_efficiency


class NetsquidParametersx1(AbstractParameterSet):

    p_loss_length = 0.2
    c = 299792.458 / 1.44  # speed of light in fibre [km/s]

    t_q1gate = 5.  # carbon 20e3ns, electron 5ns
    t_q1init = 2.e3  # 310e3 carbon init vs 2e3 electron init
    t_q1meas = 3.7e3  # electron readout
    t_q2gate = 500.e3  # electron carbon CX
    tot_num_qubits = 10

    T1 = 1. * 3600 * 1.e9
    T2 = 1.e9

    cycle_time = 3.8e3

    p_q1init = 2 * (1. - 0.99)
    p_q1gate = (4. / 3.) * 0.001
    p_q2gate = 0.02
    p_q1meas = (0.05, 0.005)

    werner = False
    respect_NV_structure = False
    use_swap_only = True
    distillation_strategy = []

    # others
    alpha = 0.1
    p_det = 0.0046
    visibility = 0.9
    p_double_exc = 0.06
    std_phase = 0.35
    p_dark_count = 2.5e-8
    eps = 0.

    def __init__(self,
                 number_of_nodes,
                 total_dist,
                 approx=True):
        self.number_of_nodes = number_of_nodes
        self.internode_distance = total_dist / (number_of_nodes - 1)
        if not approx:
            self.elementary_link_fidelity = (1 - self.alpha) * self._state_efficiency()
        else:
            self.elementary_link_fidelity = 1. - self.alpha
        self.elementary_link_succ_prob = 2 * self.alpha * self.p_det\
            * fiber_efficiency(self.internode_distance * 0.5, attenuation=self.p_loss_length)

        super().__init__()

    @classmethod
    def _state_efficiency(cls):
        pd = cls.p_double_exc * 0.5
        ph = 0.5 * (1. - np.exp(-0.5 * cls.std_phase**2))
        return (0.5 * (1. + np.sqrt(cls.visibility))) * (1. - (1.-ph)*pd*(1.-pd) - ph * ((1.-pd)**2 + pd**2))


class NetsquidParametersx10(NetsquidParametersx1):

    T1 = 10 * 3600 * 1.e9
    T2 = 10 * 1.e9

    p_q1init = 2 * (1. - 0.999)
    p_q1gate = (4. / 3.) * 0.0001
    p_q2gate = 0.002
    p_q1meas = (0.005, 0.0005)

    # others
    alpha = 0.1
    p_det = 0.58
    visibility = 0.99
    p_double_exc = 0.003
    std_phase = 0.11
    p_dark_count = 2.5e-9


def run_abstract_repchain(nodes, distance, improved, approx):
    if improved:
        sim_setup_parameters = NetsquidParametersx10(nodes, distance, approx).to_dict()
    else:
        sim_setup_parameters = NetsquidParametersx1(nodes, distance, approx).to_dict()

    # Run simulation(s) with the above parameters and print the mean fidelity
    # of the produced end-to-end link to the console
    try:
        fid, rate = run_many_simulations(sim_setup_parameters, number_of_runs=1000, minimal=True)
        res = (distance, fid[0], fid[1], rate[0], rate[1])
    except Exception:
        res = (distance, np.nan, np.nan, np.nan, np.nan)
    # time.sleep(1)
    gc.collect()
    return res


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument('--improved', type=int)
    parser.add_argument('--approx', type=int)
    parser.add_argument('--nodes', type=int)
    parser.add_argument('--outputdir', type=str)

    args = parser.parse_args()

    improved = args.improved == 1
    approx = args.approx == 1

    distances = np.linspace(20, 1500, 8 * 24)

    res_abstract = Parallel(n_jobs=-1)(
        delayed(run_abstract_repchain)(args.nodes, distance, improved, approx)
        for distance in distances
    )

    suffix = 'approx' if approx else 'full'
    improvement = '10' if improved else '1'

    np.save(os.path.join(args.outputdir, f'abs_{args.nodes}_swap_x{improvement}_{suffix}.npy'),
            np.array(res_abstract))
