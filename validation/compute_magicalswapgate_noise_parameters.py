"""
2019 April 08

In this script, we compute the noise parameter of the "magical swap gate" between electron spin
and carbon nuclear spin in an NV center. This gate simply exchanges the
two qubits.

We test its noise parameter by simulating the circuit in http://doi.org/10.1126/science.aan0070
( "Entanglement distillation between solid-state quantum network nodes", Kalb et al., 2016)
to verify that the fidelity of the state that the circuit outputs is 0.96 (which is the
value found in the article).

(see also ~Easysquid.util.compute_CXDirection_noise_parameter.py for the version
where we use the real circuit)

"""
import sys
from pathlib import Path

import netsquid as ns
import netsquid.qubits.qubitapi as qapi
from netsquid.qubits.ketstates import b00, b01, b10, b11

sys.path.insert(0, str(Path.cwd().parent))
sys.path.insert(0, str(Path.cwd().parent.parent.joinpath('nlblueprint')))

from repchain.protocols.entswapProtocol import AbstractSwapProgram
from repchain.network.abstract_node import AbstractNode
from repchain.runtools.parameters import netsquid_params_nv, default_params_nv
from repchain.utils.tools import _add_Bell_pair, compute_fidelity

ns.set_console_debug()

BELLS = {0: b00, 1: b01, 2: b10, 3: b11}


def peek_state(qproc, pgrm):
    qubits = qproc.peek(positions=[0, 3])
    dm = ns.qubits.qubitapi.reduced_dm(qubits)

    fid = compute_fidelity(dm, reference_ket=BELLS[pgrm.get_outcome_as_bell_index], squared=True)
    print('Fidelity', fid)


if __name__ == "__main__":

    ns.set_qstate_formalism(ns.QFormalism.DM)
    params = default_params_nv(2, 20)
    qproc = AbstractNode(
        name="Abstractnode", ID=0,
        num_positions=4,
        T1=params['T1'],
        T2=params['T2'],
        errors={
           k: v
           for k, v in params.items()
           if k.startswith('p_')
        },
        durations={
            k: v
            for k, v in params.items()
            if k.startswith('t_')
        },
        noiseless=False)

    _add_Bell_pair(qmemA=qproc.qmemory,
                   qmemB=qproc.qmemory,
                   posA=0,
                   posB=1,
                   bell_index=1)
    _add_Bell_pair(qmemA=qproc.qmemory,
                   qmemB=qproc.qmemory,
                   posA=2,
                   posB=3,
                   bell_index=1)

    pgrm = AbstractSwapProgram()
    qproc.qmemory.set_program_done_callback(callback=peek_state, qproc=qproc.qmemory, pgrm=pgrm)
    qproc.qmemory.execute_program(pgrm, qubit_mapping=[1, 2])

    ns.sim_run()
