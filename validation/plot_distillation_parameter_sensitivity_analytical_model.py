"""
This script analyses how variations in the parameters in the modelling
from repchain.validation.nv_distillation_model change the measured fidelity
"""

import repchain.validation.nv_distillation_model as model
import repchain.validation.plotting_tools_for_nv_distillation_model as plotting_tools
import numpy as np


if __name__ == "__main__":

    alpha = 0.5
    improvement_percentage = 50

    # initialize
    parameter_names = list(model.parameters().keys())
    mean_fidelities = {}
    mean_elementary_fidelities = {}
    names = {}

    # compute baseline: i.e. where no parameter is changed
    base_elementary_link_fidelity, base_fidelity = plotting_tools.compute_mean_fidelity(
        alpha=alpha, **model.parameters())
    mean_fidelities["no change"] = base_fidelity
    names["no change"] = "no change"

    # iterate over all parameters:
    #   * improve single parameter
    #   * compute fidelity with the improved parameter
    for param_name in parameter_names:

        # get fresh instance of the parameters in
        # order to be sure not to keep multiple
        # improved parameters
        parameters = model.parameters()

        # increase a single parameter by 10%
        parameters[param_name] = \
            parameters[param_name] * (1 - improvement_percentage / 100) + \
            model.perfect_parameters()[param_name] * improvement_percentage / 100

        # compute fidelity
        elementary_link_fidelity, mean_fidelity = \
            plotting_tools.compute_mean_fidelity(alpha=alpha, **parameters)

        # store
        mean_elementary_fidelities[param_name] = elementary_link_fidelity
        mean_fidelities[param_name] = mean_fidelity
        names[param_name] = param_name + "\n(if unimproved:{})".format(np.round(model.parameters()[param_name], 3))
        print("Fidelity of distilled state {} (with parameter {} improved)"
              .format(mean_fidelity, param_name))

    # plot
    for base_y in [0, base_fidelity]:
        plt = plotting_tools.produce_bar_plot(xvals=list(names.values()), yvals=list(mean_fidelities.values()),
                                              base_y=base_y)
        plt.xlim(0.45, 0.55)
        relative = (base_y == 0)
        title = "Distillation experiment (analytical model):\n"
        title += "Mean fidelity of distilled state at alpha={}\n".format(alpha)
        title += "while improving parameters by {}% (relative plot={})".format(improvement_percentage, relative)
        plt.title(title)
        plt.show()
        plt.clf()
