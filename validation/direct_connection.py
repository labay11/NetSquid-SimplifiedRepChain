import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import root, fsolve

import sys
from pathlib import Path

sys.path.insert(0, str(Path.cwd().parent))
sys.path.insert(0, str(Path.cwd().parent.parent.joinpath('nlblueprint')))

from repchain.runtools.data_analysis_tools import log_param_cost
from repchain.parameters.nv import NVRestrictedParameters
from repchain.parameters import fiber_efficiency


def detector_efficiency(alpha, tau, r_t, b_det):
    return min(max(tau * r_t / alpha, b_det), 1.)


def state_efficiency(alpha, f_t, b_state):
    return min(max(f_t / (1 - alpha), b_state), 1.)


def f(alpha, b_det, b_state, tau, f_t, r_t):
    det = detector_efficiency(alpha, tau, r_t, b_det)
    state = state_efficiency(alpha, f_t, b_state)
    A = alpha * log_param_cost(b_state, state) * np.log(det)
    B = (1 - alpha) * log_param_cost(b_det, det) * np.log(state)
    return (A / B) - 1.


def line_search(f, bounds, N, args):
    h = (bounds[1] - bounds[0]) / N
    alpha = bounds[0]
    best_x, best_f = np.inf, np.inf
    while alpha < bounds[1]:
        y = f(alpha, *args)
        if abs(y) < abs(best_f):
            best_x = alpha
            best_f = y
        alpha += h
    return (2 * best_x + h) * 0.5, best_f


ft, rt = 0.8, 1.0
ft, rt = 0.9, 0.1

n_points = 20
distances = np.linspace(25, 500, n_points)

sols = np.zeros((n_points, 4))

for k in range(n_points):
    distance = distances[k]
    nv = NVRestrictedParameters(2, distance, 0.1)
    b_state = nv._state_efficiency()
    b_det = nv.p_det
    tau = (nv.cycle_time * 1e-9 + distance / nv.c) / (2 * fiber_efficiency(distance, nv.p_loss_length))
    sol = line_search(f, [1e-8, 0.4], 5000, args=(b_det, b_state, tau, ft, rt))
    optimal_alpha = sol[0]

    det = detector_efficiency(optimal_alpha, tau, rt, b_det)
    state = state_efficiency(optimal_alpha, ft, b_state)
    print(k, det, state)
    cost = log_param_cost(b_det, det) + log_param_cost(b_state, state)
    print(optimal_alpha, det, state, cost)

    sols[k] = [optimal_alpha, det, state, cost]

fig, ax = plt.subplots(2, 1, sharex=True)
ax[0].set_yscale('log')
ax[0].plot(distances, sols[:, -1])
ax[1].plot(distances, sols[:, 0])
plt.show()
