from pathlib import Path
import re

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

fig_width_pt = 426.0  # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0 / 72.27               # Convert pt to inch
golden_mean = (np.sqrt(5) - 1.0) / 2.0         # Aesthetic ratio
fig_width = fig_width_pt*inches_per_pt  # width in inches
fig_height = 0.5 * fig_width*golden_mean      # height in inches
fig_size = [fig_width, fig_height]
params = {
    'backend': 'ps',
    'axes.labelsize': 6,
    'font.size': 6,
    'legend.fontsize': 6,
    'xtick.labelsize': 5,
    'ytick.labelsize': 5,
    'axes.xmargin': 0.01,
    'axes.ymargin': 0.,
    'lines.linewidth': 0.7,
    'text.usetex': True,
    'figure.figsize': fig_size,
    'axes.grid': True,
    'axes.linewidth': 0.9,
    'grid.linewidth': 0.2
}
plt.rcParams.update(params)

FOLDER_RE = re.compile(r'(\d)_(\d)_')


outdir = Path.cwd() / 'output' / 'exploit_variable'

fig, ax = plt.subplots(ncols=1, nrows=1)
ax.set_xlabel('Generation')
ax.set_ylabel('Best fitness')
# ax.set_yscale('log')
# ax2 = ax.twinx()
# ax2.set_ylabel('Avg. fitness')
# ax2.set_yscale('log')

generations = 100
X = np.arange(generations)

COLORS = ['b', 'r', 'g', 'tab:orange']
LS = ['-', '--']

DIRS = [
    '1_1_2021-03-14_14.18',
    '0_1_2021-03-14_12.22',
    '1_0_2021-03-14_14.06',
    '0_0_2021-03-14_12.21'
]

c = 0
for dir in DIRS:
    if match := FOLDER_RE.match(dir):
        exploit = match.group(1) == '1'
        variable = match.group(2) == '1'
        print(dir, exploit, variable)

        data = np.zeros(generations)

        outpath = outdir / dir

        df = pd.read_csv(outpath / 'best_result.csv')

        for npy_file in outpath.iterdir():
            if npy_file.name.endswith('.npy'):
                gen = int(npy_file.stem)
                if gen < generations:
                    gen_data = np.load(npy_file)

                    data[gen] = gen_data[:, 0].min()

        print(data.argmin(), data[data.argmin()], data[50:].mean(), data[50:].std())

        label = ''
        label += 'Exploit' if exploit else 'No exploit'
        label += ' - '
        label += 'Variable' if variable else 'Constant'

        ax.plot(X, data, label=label, c=COLORS[0 if exploit else 1], ls=LS[0 if variable else 1])
        # ax2.plot(X, data[:, 1], c=COLORS[c], ls='--')
        c += 1

ax.legend(loc="upper center", ncol=2)
fig.savefig('ga_val.pdf', bbox_inches='tight')
