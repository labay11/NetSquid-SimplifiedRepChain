import numpy as np


def p_dejmps(f1, f2, p2):
    return p2**2 * (f1*f2 + (1/3)*(f1*(1-f2) + f2*(1-f1)) + (5/9)*(1-f1)*(1-f2)) + (1 - p2**2) / 2


def dejmps(f1, f2, p2):
    b = p_dejmps(f1, f2, p2)
    a = (f1*f2 + ((1-f1)*(1-f2)/9)) * p2**2 + (1 - p2**2) / 8
    return a / b, b


def entswap(f, p2):
    return 0.25 + 0.25 * p2 * ((4 * f - 1) / 3)**2


def decohere(f, t, T2):
    return f * np.exp(- t / T2)


def waiting_time_entgen(p_elem, cycle_time, communication_time):
    return (cycle_time + communication_time) / p_elem


def estimate(f_elem, p_elem, p2, T2, cycle_time, distance, c):
    T_com = 1e9 * distance / c
    T_entgen = waiting_time_entgen(p_elem, cycle_time, T_com)
    f1 = decohere(f_elem, T_com, T2)
    f2 = decohere(f1, T_com + T_entgen, T2)

    # distill
    f_dist, p_dist = dejmps(f1, f2, p2)
    T_dist = (2 * T_entgen + T_com) / p_dist

    f_dist_wait = decohere(f_dist, T_com, T2)

    # ent swap
    f_swap = entswap(f_dist_wait, p2)
    f_swap_wait = decohere(f_swap, T_com, T2)
    T_total = T_dist + T_com

    return f_swap_wait, T_total


if __name__ == '__main__':
    f_elem = np.linspace(0.5, 1, 10)
    p_elem = np.linspace(1e-6, 0.5, 10)
    f_elem, p_elem = np.meshgrid(f_elem, p_elem)
    p2 = 0.99
    T2 = 1e10
    cycle_time = 3.5e3
    distance = 100
    c = 3e5 / 1.44
    f, t = estimate(f_elem, p_elem, p2, T2, cycle_time, distance, c)
    print(f)
    print(t / 1e9)
