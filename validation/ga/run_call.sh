#!/bin/bash
for nodes in 0 1
do
    for dist in 0 1
    do
        name=validation_$protocol\_$nodes\_$dist
        outfile=slurm_validation_$nodes\_$dist\_%j.out

        echo $name
        sbatch --output=$outfile --job-name=$name run.sh $nodes $dist exploit_variable
    done
done
