"""
This is an example file for how to run simulations using this repeater chain code and write relevant metrics to a file.
"""
import sys
from pathlib import Path
from itertools import permutations
from pprint import pprint

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


sys.path.insert(0, str(Path.cwd().parent))
sys.path.insert(0, str(Path.cwd().parent.parent.joinpath('nlblueprint')))

from repchain.runtools.simulation_setup import SimulationSetup  # noqa E402
from repchain.runtools.data_analysis_tools import run_many_simulations # noqa E402
from repchain.utils.bcdzfunctions import num2level  # noqa E402


def run(sim_params, number_of_runs):
    # Run simulation and perform data collection
    fid, rate = run_many_simulations(sim_params, number_of_runs=number_of_runs, duration=None, minimal=True)

    return fid, rate


def plot_distill_attempts(distill_attempts):
    nnodes = len(distill_attempts)
    attemps = np.zeros((nnodes, nnodes))
    total = np.zeros((nnodes, nnodes), dtype=int)

    for node in range(nnodes):
        for remote in distill_attempts[node]:
            a = distill_attempts[node][remote]
            attemps[node, remote] = a['s'] / (a['s'] + a['f'])
            total[node, remote] = (a['s'] + a['f'])

    fig, ax = plt.subplots()
    plt.title('Fraction of successful distillation attempts')
    cs = ax.imshow(attemps, cmap='Blues', origin='lower')

    for node, remote in permutations(range(nnodes), 2):
        t = total[node, remote]
        a = int(attemps[node, remote] * t)

        ax.text(node, remote, f'{a:d}/{t}', va='center', ha='center',
                c='black' if attemps[node, remote] < 0.5 else 'white')

    fig.colorbar(cs)
    plt.show()


def plot(file_name,
         x_col, y_col,
         secondary_y_col=None, yerr_col=None, secondary_yerr_col=None,
         style={}, secondary_style={},
         label_x=None, label_y=None, label_secondary_y=None, title=None):
    data = pd.read_csv(file_name)
    fig, ax = plt.subplots()
    plt.grid(True)
    line1, = ax.plot(data[x_col], data[y_col], label=y_col, **style)
    ax.fill_between(data[x_col],
                    y1=data[y_col] - data[yerr_col],
                    y2=data[y_col] + data[yerr_col],
                    **style,
                    alpha=0.2)
    ax.yaxis.get_label().set_color(line1.get_color())
    if secondary_y_col:
        ax_right = ax.twinx()
        line2, = ax_right.plot(data[x_col], data[secondary_y_col], label=secondary_y_col, **secondary_style)
        if secondary_yerr_col:
            ax_right.fill_between(data[x_col],
                                  y1=data[secondary_y_col] - data[secondary_yerr_col],
                                  y2=data[secondary_y_col] + data[secondary_yerr_col],
                                  **secondary_style,
                                  alpha=0.2)
        ax_right.set_ylabel(label_secondary_y if label_secondary_y else secondary_y_col)
        ax_right.yaxis.get_label().set_color(line2.get_color())

    ax.set_ylabel(label_y if label_y else y_col)
    ax.set_xlabel(label_x if label_x else x_col)
    if title:
        plt.title(title)
    plt.savefig(file_name.rsplit('.', 1)[0] + '.png')


def setup_simulation(param, options, default_params=None, number_of_runs=100):
    data = {}
    default_params = default_params if default_params else default_sim_params()

    for i, v in enumerate(options):
        sim_params = default_params.copy()
        sim_params[param] = v

        print('Params:')
        for key, val in sim_params.items():
            print(f'\t{key}: {val}')
        print('')

        fidelity, rate = run(sim_params, number_of_runs)
        print(param, v, fidelity, rate)

        sim_params['fidelity'] = fidelity[0]
        sim_params['fidelity_unc'] = fidelity[1]
        sim_params['rate'] = rate[0]
        sim_params['rate_unc'] = rate[1]

        data[i] = sim_params

    df = pd.DataFrame(data)
    df = df.transpose().set_index(param)
    return df


def parameter_to_distillation_strategy(nodes, param):
    if param == 0 or nodes == 2:
        # swap only
        return True, None

    levels = num2level(nodes)
    strategy = []
    for i in range(levels + 1):
        mask = 1  # get the first bit
        protocol = 'EPL' if mask & param else 'DEJMPS'
        param >>= 1    # divide by two
        mask = 3  # get the next two bits
        n = mask & param
        strategy.append((protocol, n if protocol == 'DEJMPS' else min(n, 1)))
        param >>= 2

    return False, strategy


def sequential_swap_only(nodes, total_dist, cycle_time, alpha, p_det,
                         fiber_attenuation=0.22, c=206753.41931034482, p2=0.96, T2=1e9):
    internode_distance = total_dist / (nodes - 1)
    p_det_with_fiber = p_det * np.power(10, - 0.1 * (internode_distance * 0.5) * fiber_attenuation)
    p_gen = alpha * p_det_with_fiber * 2.
    # p_gen = np.array([single_click(alpha, dist / 2.)[1] for dist in internode_distance])
    cycle_with_travel_time = cycle_time + 1e9 * internode_distance / c

    if nodes > 2:
        time = ((nodes - 1) / 2) * cycle_with_travel_time / p_gen
    else:
        time = cycle_with_travel_time / p_gen

    fid = (1 - alpha) * np.exp(- cycle_with_travel_time / p_gen / T2)
    fid = 0.25 + 0.75 * np.power(0.999 * p2 * (4 * 0.99**2 - 1) / 3, nodes - 2)\
        * np.power((4 * (1 - alpha) - 1) / 3, (nodes - 1)/2) * np.power((4 * fid - 1) / 3, (nodes - 1)/4)
    return time, fid


if __name__ == "__main__":
    number_of_runs = 200
    param = 'number_of_nodes'

    # sim_params = full_params_nv_optimistic(
    #     3, 200, alpha=0.2380742455936796,
    #     distillation_strategy=3, uniform_strategy=False,
    #     p_det=0.06774268453778423, T1=np.power(10, 13.55), T2=np.power(10, 9.515),
    #     ec_gate_depolar_prob=1 - 0.9840868821962806)
    # pprint(sim_params)
    sim_params = {
        'T1': 3600000000000.004,
        'T2': 50164029340.196,
        'c': 208189.20694444445,
        'cycle_time': 3500.0,
        'distillation_strategy': [None, None, None, None],
        'elementary_link_fidelity': 0.989823809446500,
        'elementary_link_succ_prob': 0.002917546760876413,
        'eps': 0.0,
        'internode_distance': 100.0,
        'number_of_nodes': 9,
        'p_loss_length': 0.22,
        'p_q1gate': 0.0013333333333333333,
        'p_q1init': 0.006,
        'p_q1meas': (0.01, 0.005),
        'p_q2gate': 0.003265874235198,
        'respect_NV_structure': False,
        # 'result': {
        #     'cost': {'T1': 1.0,
        #              'T2': 50.022938397378908,
        #              'elementary_link_fidelity': 8.194541422576382,
        #              'detector_efficiency': 13.954427104644214,
        #              'p_q2gate': 6.175904008297986,
        #              'total': 78.34687253551859},
        #     'fidelity': 0.8099999999999998,
        #     'fidelity_unc': 0.027582676086639994,
        #     'rate': 1.586840230912947,
        #     'rate_unc': 0.05031201745483418},
        't_q1gate': 20000.0,
        't_q1init': 310000.0,
        't_q1meas': 3700.0,
        't_q2gate': 500000.0,
        'tot_num_qubits': 10,
        'use_swap_only': True,
        'werner': False}

    print(run(sim_params, number_of_runs))

    # if not sim_params['use_swap_only']:
    #     options = [5]
    #     output_file_name = "data/distill/{:s}_{:d}.csv".format(param, number_of_runs)
    #     sim_params['swap_fidelity_threshold'] = 1.
    # else:
    #     options = [9]
    #     output_file_name = "data/swap_only/{:s}_{:d}.csv".format(param, number_of_runs)
    #
    # data = setup_simulation(param, options, default_params=sim_params, number_of_runs=number_of_runs)
    # time, fid = sequential_swap_only(9, 200, sim_params['cycle_time'], alpha, p_det,
    #                                  p2=1-sim_params['p_q2gate'],
    #                                  T2=sim_params['T2'])
    # cost = lambda x, b: np.log(b) / np.log(x)
    # print(fid, 1e9 / time, cost(p_det, 0.0046) + cost(1 - sim_params['p_q2gate'], 0.96) + cost(1 - 1 / sim_params['T2'], 1 - 1e-9) + cost(1 - 1 / sim_params['T1'], 1 - 1 / 10**13.55))

    # data.to_csv(output_file_name)
    # plot(output_file_name,
    #      param, 'fidelity',
    #      secondary_y_col='rate', yerr_col='fidelity_unc', secondary_yerr_col='rate_unc',
    #      label_x='Number of repeaters', label_y='Fidelity', label_secondary_y='Rate (Hz)',
    #      style={'color': 'b'}, secondary_style={'color': 'r'},
    #      title='Entanglement between 2 end nodes separated by N repeaters')
