import copy
import numpy as np
import sys
from pathlib import Path
import csv
import matplotlib.pyplot as plt

from netsquid_nv.nv_parameter_set import compute_product_tau_decay_delta_w_from_nodephasing_number
from netsquid_nv.delft_nvs.delft_nv_2019_optimistic import NVParameterSet2019Optimistic, NVParameterSet2019
from netsquid_magic.state_delivery_sampler import SingleClickDeliverySamplerFactory
import netsquid_nv.nv_state_delivery_model as nv_model
from netsquid.qubits.ketstates import b01

sys.path.insert(0, str(Path.cwd().parent))
sys.path.insert(0, str(Path.cwd().parent.parent.joinpath('nlblueprint')))

from nlblueprint.nv_abstract_mapping import NVParameters


def convert_NV_to_abstract(NV_parameters, internode_distance, alpha, T2_induced_dephasing):
    """Convert NV parameters to respective abstract parameters."""
    # Compute necessary parameters such as dephasing probability
    dic_to_convert = copy.deepcopy(NV_parameters)
    dic_to_convert['alpha'] = alpha
    cycle_time = dic_to_convert['photon_emission_delay']
    p_dephase = (1 - dic_to_convert['alpha']) / 2 * (1 - np.exp(-dic_to_convert['product_tau_decay_delta_w']**2 / 2))
    # print(dic_to_convert['product_tau_decay_delta_w'])
    photon_travel_distance = internode_distance / 2.0
    factor = np.power(10, -1. * photon_travel_distance * 0.22 / 10.0)
    dic_to_convert['p_det'] = dic_to_convert['prob_detect_excl_transmission_with_conversion_with_cavities'] * factor
    dic_to_convert['p_dark_count'] = dic_to_convert.pop('prob_dark_count')
    dic_to_convert['electron_measure_error_prob'] = dic_to_convert.pop('prob_error_1')

    # Remove unnecessary parameters. TODO: Rewrite nv_to_abstract.py such that this is not required

    dic_to_convert.pop('photon_emission_delay')
    dic_to_convert.pop('product_tau_decay_delta_w')
    dic_to_convert.pop('prob_error_0')
    dic_to_convert.pop('carbon_init_depolar_prob')
    dic_to_convert.pop('electron_T1')
    dic_to_convert.pop('electron_T2')
    dic_to_convert.pop('prob_detect_excl_transmission_with_conversion_with_cavities')
    dic_to_convert.pop('prob_detect_excl_transmission_no_conversion_no_cavities')
    dic_to_convert.pop('p_loss_lengths_with_conversion')
    dic_to_convert.pop('p_loss_lengths_no_conversion')
    dic_to_convert.pop('initial_nuclear_phase')
    dic_to_convert['time_window'] = None
    dic_to_convert['dark_count_rate'] = None

    NV_mapping = NVParameters(**dic_to_convert)
    abstract_params = {}
    abstract_params['elementary_link_fidelity'] = NV_mapping.elementary_link_fidelity
    abstract_params['elementary_link_succ_prob'] = NV_mapping.elementary_link_succ_prob
    abstract_params['swap_quality'] = NV_mapping.swap_quality
    abstract_params['T1'] = NV_mapping.t1
    abstract_params['p_det'] = dic_to_convert['p_det']

    if T2_induced_dephasing:
        abstract_params['T2'] = 1 / (1 / (2 * NV_mapping.t1) - np.log(1 - 2 * p_dephase) / cycle_time)
    else:
        abstract_params['T2'] = NV_mapping.t2

    return abstract_params


# print(compute_product_tau_decay_delta_w_from_nodephasing_number(2042, 0.05))
# alphas = np.linspace(0, 0.5, 100)
# Ts = np.array([compute_product_tau_decay_delta_w_from_nodephasing_number(2042, alpha) for alpha in alphas])
# plt.plot(alphas, Ts)
# plt.show()


NVParams = NVParameterSet2019Optimistic()
nv_params_dict = NVParams.to_dict()
T2_induced_dephasing = False
cycle_time = 3.5e3
num_steps = 1000
total_dist = 800
file_name = f'baseline_values_{total_dist}km.csv'
baseline_values = []
#
# alphas = np.linspace(0, 0.5, num_steps)
# distances = [25, 50, 100, 200, 400, 800]
#
# Fs = np.zeros((len(distances), num_steps, 2))
# Ps = np.zeros((len(distances), num_steps, 2))
#
# for i, internode_distance in enumerate(distances):
#     for j, alpha in enumerate(alphas):
#         abstract_params = convert_NV_to_abstract(NV_parameters=nv_params_dict,
#                                                  internode_distance=internode_distance,
#                                                  alpha=alpha,
#                                                  T2_induced_dephasing=T2_induced_dephasing)
#
#         factor = np.power(10, -1. * (internode_distance / 2.0) * nv_params_dict["p_loss_lengths_with_conversion"] / 10)
#         Fs[i, j, :] = [abstract_params["elementary_link_fidelity"], (1 - alpha)]
#         Ps[i, j, :] = [abstract_params["elementary_link_succ_prob"], 2 * alpha * abstract_params['p_det']]
#
#
# def cl(x, xmin, xmax):
#     r = (x - xmin) / (xmax - xmin)
#     return (r, 0, 1 - r)
#
# import matplotlib.pyplot as plt
#
#
# fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True)
# axs[1].set_xlabel('Alpha')
# axs[0].set_ylabel('Fidelity')
# axs[1].set_ylabel('p success')
# axs[1].set_yscale('log')
# for i, internode_distance in enumerate(distances):
#     color = cl(np.log(internode_distance), np.log(distances[0]), np.log(distances[-1]))
#     axs[0].plot(alphas, Fs[i, :, 0], c=color, ls='None', marker='x')
#     axs[0].plot(alphas, Fs[i, :, 1], c=color, label=f'{internode_distance}km')
#
#     axs[1].plot(alphas, Ps[i, :, 0], c=color, ls='None', marker='x')
#     axs[1].plot(alphas, Ps[i, :, 1], c=color)
# axs[0].legend(title='internode distance')
# plt.show()

# for internode_distance in [25, 50, 100, 200, 400, 800]:
#     best_fidelity = (0., 0.)
#     best_psuc = (0., 0.)
#     alpha = 0.
#
#     for i in range(0, num_steps):
#         abstract_params = convert_NV_to_abstract(NV_parameters=nv_params_dict,
#                                                  internode_distance=internode_distance,
#                                                  alpha=alpha,
#                                                  T2_induced_dephasing=T2_induced_dephasing)
#
#         if abstract_params["elementary_link_fidelity"] > best_fidelity[0]:
#             best_fidelity = (abstract_params["elementary_link_fidelity"], alpha)
#
#         if abstract_params["elementary_link_succ_prob"] > best_psuc[0]:
#             best_psuc = (abstract_params["elementary_link_succ_prob"], alpha)
#
#         alpha += 0.5 / (num_steps - 1)
#
#     T1, T2 = abstract_params['T1'], abstract_params['T2']
#
#     baseline_values.append((internode_distance, best_fidelity[0], best_fidelity[1], best_psuc[0],
#                             best_psuc[1], abstract_params["swap_quality"], T1, T2))
#
#
# with open('internode_distance_baselines.csv', 'w') as file:
#     writer = csv.writer(file, delimiter=',')
#     writer.writerow(('internode_distance', 'el_fidelity', 'alpha', 'el_psuc', 'alpha', 'sq', 'T1', 'T2'))
#     for row in baseline_values:
#         writer.writerow(row)


def single_click(alpha, length, improvement_factor=1.):
    # F = 2 * (1 - alpha) * (1 + np.sqrt(V)) / (4 + alpha * p_det * (V - 3))
    improved = improvement_factor > 9
    if improved:
        p_det = 0.58
    else:
        p_det = 0.0046
    p_det_with_fiber = p_det * np.power(10., -0.1 * length * 0.2)

    if improved:
        visibility = 0.99
        p_double_exc = 0.003
        std_phase = 0.11
    else:
        visibility = 0.9
        p_double_exc = 0.06
        std_phase = 0.35

    dark_count = 2.5e-8 / improvement_factor

    F = 0.0
    for _ in range(150):
        dm_states = nv_model.successful_density_matrices(
            alpha,
            p_dark_count=dark_count,
            p_det=p_det_with_fiber,
            visibility=visibility,
            p_double_exc=p_double_exc,
            p_fail_class_corr=0.,
            coherent_phase=0.,
            std_phase_interferometric_drift=std_phase
        )

        F += np.real(np.dot(b01.T, np.dot(dm_states[0][0], b01)))[0, 0]
    F /= 150.0

    p = SingleClickDeliverySamplerFactory._get_success_probability(
        alpha,
        p_dark_count=dark_count,
        p_det=p_det_with_fiber,
        visibility=visibility
    )

    F3, p3 = NVParameters._compute_elementary_link(
        alpha, dark_count, p_det_with_fiber, visibility, None, None, 0., 0., std_phase
    )

    pd = p_double_exc / 2
    ph = 0.5 * (1. - np.exp(-0.5 * std_phase**2))
    q = 2 * (1 - ph) * pd * (1 - pd) + ph * ((1 - pd)**2 + pd**2)
    eff = 0.5 * (1 + np.sqrt(visibility)) * (1 - q)

    p2 = alpha * p_det_with_fiber * (2. + 0.5 * alpha * p_det_with_fiber * (visibility - 3))
    F2 = (1 - alpha) * eff

    return [F, p, F2, p2]


alphas = np.linspace(0, 0.5, 100)
R = np.zeros((100, 4))
for j in range(100):
    R[j, :] = single_click(alphas[j], 25)

fig, ax = plt.subplots(2, 1)
ax[0].plot(alphas, np.clip(R[:, 0], 0, 1), label='Full')
ax[0].plot(alphas, R[:, 2], label='Approx')
ax[1].plot(alphas, R[:, 1], label='Full')
ax[1].plot(alphas, R[:, 3], label='Approx')
plt.legend()
plt.show()
