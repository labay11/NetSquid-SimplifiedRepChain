#!/bin/bash

for targets in 0 1
do
    for nodes in 3 5 9 17
    do
        for dist in 200 400 600 800 1200 1600
        do
            name=doubleclick_$targets\_$nodes\_$dist
            outfile=slurm_$targets\_$nodes\_$dist\_%j.out

            echo $name
            sbatch --output=$outfile --job-name=$name run.sh $nodes $dist $targets
        done
    done
done
