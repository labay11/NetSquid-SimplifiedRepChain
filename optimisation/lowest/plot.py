import pathlib

from run import restricted_parameters
from repchain.opt.plot import parse_folder_and_plot


cwd = pathlib.Path.cwd()

parameters = restricted_parameters(5, 100)
parse_folder_and_plot(
    cwd, parameters, show_legend=[True, False]
)
