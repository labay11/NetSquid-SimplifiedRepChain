import os
from argparse import ArgumentParser
from pprint import pprint
import pickle

import numpy as np

from repchain.runtools.data_analysis_tools import run_many_simulations, compute_rate_from_time, cost, penalty
from repchain.parameters import BaselineParameter, identity_map,\
    coherence_time_ga_to_prob, coherence_time_ga_from_prob
from repchain.parameters.nv import NVOptimisticParameters, distillation_strategy_range
from repchain.opt.run_file import compute_generation, final_generation


POP_SIZE = 5 * 24
NUMBER_SELECTION = 24
NUMBER_CROSSOVER = 3 * 24


def constraint_T1T2(x):
    return x[..., 3] > x[..., 4]  # T1 > T2


def _psucc_to_prob(x):
    return x * 2.


def _psuc_from_prob(x):
    return x * 0.5


class NVUniformUnrestricted(NVOptimisticParameters):
    """Restricted abstract model with different parameters used in optimization."""

    def __init__(self, nodes, distance,
                 elementary_link_fidelity, elementary_link_succ_prob, p2, T1, T2, distillation_strategy):
        super().__init__(nodes, distance,
                         elementary_link_fidelity, elementary_link_succ_prob,
                         distillation_strategy, uniform_strategy=True, T1=T1, T2=T2, p_q2gate=p2)


def unrestricted_parameters(nodes, distance, uniform=True):
    internode_distance = int(distance / (nodes - 1))
    if internode_distance == 25:
        f = 0.9681426996181608
        p = 0.0024434370529679973
    elif internode_distance == 50:
        f = 0.9658630458353884
        p = 0.0012975783426089724
    elif internode_distance == 100:
        f = 0.9584928231741393
        p = 0.0003658284201232597
    elif internode_distance == 200:
        f = 0.9204163287274016
        p = 2.910725898198144e-05
    elif internode_distance == 400:
        f = 0.6019725647609528
        p = 2.3334023133431288e-07
    elif internode_distance == 800:
        f = 0.4871708245126285
        p = 5.000729713056162e-08
    else:
        raise ValueError('Baseline value not known for internode distance {}.'.format(internode_distance))

    max_value = distillation_strategy_range(nodes, True)
    # compute baseline values for T1 and T2 in microseconds
    baseline_T1 = np.log10(NVOptimisticParameters.T1) - 6.
    baseline_T2 = np.log10(NVOptimisticParameters.T2) - 6.

    params = [
        BaselineParameter('elementary_link_fidelity', float, f, 1. - 1.e-8, identity_map, identity_map),
        BaselineParameter('elementary_link_succ_prob', float, p, 0.5 - 1.e-8, _psucc_to_prob, _psuc_from_prob),
        BaselineParameter('p_q2gate', float,
                          1. - NVOptimisticParameters.p_q2gate,  1. - 1.e-8,
                          identity_map, identity_map),
        BaselineParameter('T1', float,
                          baseline_T1, baseline_T1 + 3.,
                          coherence_time_ga_to_prob, coherence_time_ga_from_prob),
        BaselineParameter('T2', float,
                          baseline_T2, baseline_T2 + 5.,
                          coherence_time_ga_to_prob, coherence_time_ga_from_prob)
    ]

    if nodes > 2:
        params.append(BaselineParameter('distillation_strategy', int, 0, max_value, None, None))

    return params


def fitness(param_cost, penalty):
    return param_cost + penalty * 1e9


def f(x, parameters, nodes, distance, target_fidelity, target_rate, number_of_runs):
    input_dic = NVUniformUnrestricted(nodes, distance,
                                      x[0], x[1], 1. - x[2],
                                      np.power(10., x[3] + 6), np.power(10., x[4] + 6),
                                      int(x[5]) if nodes > 2 else 0).to_dict()
    max_sim_time = 10 * 1.e9 / target_rate

    if nodes == 9 and input_dic['use_swap_only']:
        # increase number of runs since we are working with kets
        number_of_runs *= 2

    fid, rate = run_many_simulations(input_dic, duration=max_sim_time, number_of_runs=number_of_runs, minimal=True)

    P = penalty(fidelity=fid[0], target_fidelity=target_fidelity, rate=rate[0], target_rate=target_rate)
    C = np.sum(cost(parameters, x))

    return fitness(C, P)


def final_f(x, parameters, nodes, distance, target_fidelity, target_rate, outputdir, number_of_runs):
    input_dic = NVUniformUnrestricted(nodes, distance,
                                      x[0], x[1], 1. - x[2],
                                      np.power(10., x[3] + 6), np.power(10., x[4] + 6),
                                      int(x[5]) if nodes > 2 else 0).to_dict()

    df = run_many_simulations(input_dic, duration=None, number_of_runs=number_of_runs, minimal=False)

    fidelity = df['fidelity'].mean()
    uf = df['fidelity'].std() / np.sqrt(len(df['fidelity']))
    rate, rate_unc = compute_rate_from_time(df['time'] / 1.e9)
    P = penalty(fidelity=fidelity, target_fidelity=target_fidelity, rate=rate, target_rate=target_rate)
    C = cost(parameters, x)
    fit = fitness(np.sum(C), P)
    df['cost'] = fit
    df['rate'] = rate

    df.to_csv(
        os.path.join(outputdir, "best_result.csv"),
        index=False
    )

    for i, param in enumerate(parameters):
        if param.name not in input_dic:
            input_dic[param.name] = x[i]

    input_dic['result'] = {
        'fidelity': fidelity,
        'fidelity_unc': uf,
        'rate': rate,
        'rate_unc': rate_unc,
        'cost': {
            'total': fit,
            **{
                param.name: k for param, k in zip(parameters, C) if k > 0
            }
        }
    }

    with open(os.path.join(outputdir, "best_result_params.pkl"), 'wb') as params_file:
        pickle.dump(input_dic, params_file)

    print('Best result:')
    print('=' * 50)
    pprint(input_dic)
    print('=' * 50)
    pprint(df.mean().to_dict())

    return fit


if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument('--generation', type=int)
    parser.add_argument('--distance', type=int)
    parser.add_argument('--nodes', type=int)
    parser.add_argument('--targets', type=int)
    parser.add_argument('--outputdir', type=str)

    args = parser.parse_args()

    if args.targets == 1:
        target_fidelity, target_rate = 0.9, 0.1
    else:
        target_fidelity, target_rate = 0.8, 1.

    parameters = unrestricted_parameters(args.nodes, args.distance)

    number_of_runs = 100 if args.nodes > 3 else 200

    if args.generation >= 0:
        x, c = compute_generation(
            f,
            parameters,
            POP_SIZE,
            args.generation,
            NUMBER_SELECTION,
            NUMBER_CROSSOVER,
            args.outputdir,
            init_mode='lhs',
            p_mutation=1. / len(parameters),
            constraints=constraint_T1T2,
            exploit=None,
            kargs=(parameters, args.nodes, args.distance, target_fidelity, target_rate, number_of_runs)
        )
        print(args.generation, x, c)
    else:
        final_generation(
            f,
            final_f,
            parameters,
            args.outputdir,
            constraints=constraint_T1T2,
            exploit_iters=10,
            kargs=(parameters, args.nodes, args.distance, target_fidelity, target_rate, number_of_runs)
        )
