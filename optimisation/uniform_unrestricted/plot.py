import pathlib

from run import unrestricted_parameters
from repchain.opt.plot import parse_folder_and_plot


cwd = pathlib.Path.cwd()

parameters = unrestricted_parameters(5, 100)
parse_folder_and_plot(
    cwd, parameters, include_strategy='Uniform Strategy'
)
