import os
from argparse import ArgumentParser
from pprint import pprint
import pickle

import numpy as np

from repchain.runtools.data_analysis_tools import run_many_simulations, compute_rate_from_time, cost, penalty
from repchain.parameters import BaselineParameter, identity_map, coherence_time_ga_to_prob, coherence_time_ga_from_prob
from repchain.parameters.nv import NVRestrictedParameters, distillation_strategy_range
from repchain.opt.run_file import compute_generation, final_generation


POP_SIZE = 5 * 24
NUMBER_SELECTION = 24
NUMBER_CROSSOVER = 3 * 24


def constraint_T1T2(x):
    return x[..., 3] > x[..., 4]  # T1 > T2


class NVUniformRestricted(NVRestrictedParameters):
    """Restricted abstract model with different parameters used in optimization."""

    def __init__(self, nodes, distance,
                 alpha, detector_efficiency, p2, T1, T2, distillation_strategy):
        self.p_det = detector_efficiency
        super().__init__(nodes, distance, alpha,
                         distillation_strategy, uniform_strategy=True, T1=T1, T2=T2, p_q2gate=p2)

    def _state_efficiency(self):
        return 1.


def restricted_parameters(nodes, distance):
    baselines = NVRestrictedParameters(2, 1., 1.)
    max_value = distillation_strategy_range(nodes, True)
    # compute baseline values for T1 and T2 in microseconds
    baseline_T1 = np.log10(NVRestrictedParameters.T1) - 6.
    baseline_T2 = np.log10(NVRestrictedParameters.T2) - 6.
    params = [
        BaselineParameter('alpha', float, 1.e-5, 0.5, None, None),
        BaselineParameter('detector_efficiency', float,
                          baselines.p_det,
                          1. - 1.e-8, identity_map, identity_map),
        BaselineParameter('p_q2gate', float,
                          1. - NVRestrictedParameters.p_q2gate, 1. - 1.e-8, identity_map, identity_map),
        BaselineParameter('T1', float,
                          baseline_T1, baseline_T1 + 3., coherence_time_ga_to_prob, coherence_time_ga_from_prob),
        BaselineParameter('T2', float,
                          baseline_T2, baseline_T2 + 5., coherence_time_ga_to_prob, coherence_time_ga_from_prob)
    ]

    if nodes > 2:
        params.append(BaselineParameter('distillation_strategy', int, 0, max_value, None, None))

    return params


def fitness(param_cost, penalty):
    return param_cost + penalty * 1e9


def f(x, parameters, nodes, distance, target_fidelity, target_rate, number_of_runs):
    input_dic = NVUniformRestricted(nodes, distance,
                                    x[0], x[1], 1. - x[2],
                                    np.power(10., x[3] + 6), np.power(10., x[4] + 6),
                                    int(x[5]) if nodes > 2 else 0).to_dict()
    max_sim_time = 10 * 1.e9 / target_rate

    if nodes == 9 and input_dic['use_swap_only']:
        # increase number of runs since we are working with kets
        number_of_runs *= 2

    fid, rate = run_many_simulations(input_dic, duration=max_sim_time, number_of_runs=number_of_runs, minimal=True)

    P = penalty(fidelity=fid[0], target_fidelity=target_fidelity, rate=rate[0], target_rate=target_rate)
    C = np.sum(cost(parameters, x))

    return fitness(C, P)


def final_f(x, parameters, nodes, distance, target_fidelity, target_rate, outputdir, number_of_runs):
    input_dic = NVUniformRestricted(nodes, distance,
                                    x[0], x[1], 1. - x[2],
                                    np.power(10., x[3] + 6), np.power(10., x[4] + 6),
                                    int(x[5]) if nodes > 2 else 0).to_dict()

    df = run_many_simulations(input_dic, duration=None, number_of_runs=number_of_runs, minimal=False)

    fidelity = df['fidelity'].mean()
    uf = df['fidelity'].std() / np.sqrt(len(df['fidelity']))
    rate, rate_unc = compute_rate_from_time(df['time'] / 1.e9)
    P = penalty(fidelity=fidelity, target_fidelity=target_fidelity, rate=rate, target_rate=target_rate)
    C = cost(parameters, x)
    fit = fitness(np.sum(C), P)
    df['cost'] = fit
    df['rate'] = rate

    df.to_csv(
        os.path.join(outputdir, "best_result.csv"),
        index=False
    )

    for i, param in enumerate(parameters):
        if param.name not in input_dic:
            input_dic[param.name] = x[i]

    input_dic['result'] = {
        'fidelity': fidelity,
        'fidelity_unc': uf,
        'rate': rate,
        'rate_unc': rate_unc,
        'cost': {
            'total': fit,
            **{
                param.name: k for param, k in zip(parameters, C) if k > 0
            }
        }
    }

    with open(os.path.join(outputdir, "best_result_params.pkl"), 'wb') as params_file:
        pickle.dump(input_dic, params_file)

    print('Best result:')
    print('=' * 50)
    pprint(input_dic)
    print('=' * 50)
    pprint(df.mean().to_dict())

    return fit


if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument('--generation', type=int)
    parser.add_argument('--distance', type=int)
    parser.add_argument('--nodes', type=int)
    parser.add_argument('--targets', type=int)
    parser.add_argument('--outputdir', type=str)

    args = parser.parse_args()

    if args.targets == 2:
        target_fidelity, target_rate = 0.88, 1.
    elif args.targets == 1:
        target_fidelity, target_rate = 0.9, 0.1
    else:
        target_fidelity, target_rate = 0.8, 1.

    parameters = restricted_parameters(args.nodes, args.distance)

    number_of_runs = 100 if args.nodes > 3 else 200

    if args.generation >= 0:
        x, c = compute_generation(
            f,
            parameters,
            POP_SIZE,
            args.generation,
            NUMBER_SELECTION,
            NUMBER_CROSSOVER,
            args.outputdir,
            init_mode='lhs',
            p_mutation=1. / len(parameters),
            constraints=constraint_T1T2,
            exploit=None,
            kargs=(parameters, args.nodes, args.distance, target_fidelity, target_rate, number_of_runs)
        )
        print(args.generation, x, c)
    else:
        final_generation(
            f,
            final_f,
            parameters,
            args.outputdir,
            constraints=constraint_T1T2,
            exploit_iters=10,
            kargs=(parameters, args.nodes, args.distance, target_fidelity, target_rate, number_of_runs)
        )
