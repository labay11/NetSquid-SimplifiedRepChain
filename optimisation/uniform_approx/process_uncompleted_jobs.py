import os
from pathlib import Path
import shutil
import re

SLURM_RE = re.compile(r'slurm_(\d)_(\d)_(\d{3})_(\d*)\.out')


def find_folder(dir, name):
    for d in dir.iterdir():
        if d.is_dir() and d.name.startswith(name):
            return d

    return None


def find_scratch_dir(slurm_file, max_lines=10):
    tmp_dir = None
    with open(slurm_file, 'r') as slurm:
        i = 0
        for line in slurm.readlines():
            if line.startswith('/scratch-shared/'):
                tmp_dir = line.strip()

            i += 1
            if i > max_lines:
                break
    return tmp_dir


cwd = Path.cwd()

outdir = cwd / 'output'


for f in cwd.iterdir():
    if f.is_file() and f.name.startswith('slurm'):
        print(f.name, end=' ')
        match = SLURM_RE.match(f.name)
        if match:
            targets = match.group(1)
            nodes = int(match.group(2))
            dist = int(match.group(3))
            jobid = int(match.group(4))

            protocol_folder = outdir / targets

            if not find_folder(protocol_folder, f'{nodes}_{dist}'):
                tmpdir = find_scratch_dir(f)
                if tmpdir:
                    print('Scratch', tmpdir, end=' ')
                    tmpout = Path(tmpdir) / 'output'
                    new_folder = protocol_folder / f'{nodes}_{dist}_{jobid}'
                    try:
                        # tmpout.rename(new_folder)
                        shutil.copytree(tmpout, new_folder)
                        print('Correctly copied into', new_folder)
                    except Exception as e:
                        print('ERROR:', str(e))
                else:
                    print('ERROR: tmp dir not found')
            else:
                print('Job already finished')
        else:
            print('ERROR: no match')
