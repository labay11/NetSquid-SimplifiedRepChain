#!/bin/bash

for targets in 0
do
    for nodes in 2 3 5 9
    do
        for dist in 200 400 800
        do
            name=approx_$targets\_$nodes\_$dist
            outfile=slurm_$targets\_$nodes\_$dist\_%j.out

            echo $name
            sbatch --output=$outfile --job-name=$name run.sh $nodes $dist $targets
        done
    done
done
