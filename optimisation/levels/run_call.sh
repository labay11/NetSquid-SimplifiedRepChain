#!/bin/bash

for targets in 0 1
do
    for nodes in 2
    do
        for dist in 200 400 800
        do
            name=levels_$targets\_$nodes\_$dist
            outfile=slurm_$targets\_$nodes\_$dist\_%j.out

            echo $name
            sbatch --output=$outfile --job-name=$name run.sh $nodes $dist $targets
        done
    done
done
