#!/bin/bash
#SBATCH --nodes=1
#SBATCH --time=2:00:00
#SBATCH --partition=normal
#SBATCH --mail-user=adria.labay@gmail.com
#SBATCH --mail-type=END,FAIL

echo nodes $1
echo distance $2
echo targets $3

currentdir=$PWD
echo $currentdir

# create tmp dir and copy run file
tmp_dir="$(mktemp -d -p /scratch-shared)"
echo $tmp_dir
cp run.py $tmp_dir
cd $tmp_dir
out_dir=$tmp_dir/output
mkdir -p $out_dir

# add repchain to path
export PYTHONPATH="$PYTHONPATH:/home/alabay/nlblueprint"
export PYTHONPATH="$PYTHONPATH:/home/alabay/NetSquid-SimplifiedRepChain"

# init conda and execute script
module purge
module load 2019
module load Miniconda3
source /sw/arch/RedHatEnterpriseServer7/EB_production/2019/software/Miniconda3/4.7.10/etc/profile.d/conda.sh
conda activate netsquid

generations=500

for ((gen=0; gen < generations; gen++))
do
    echo "Running generation $gen"
    python3 run.py --nodes $1 --distance $2 --outputdir $out_dir --generation $gen --targets $3
    if [ $? -ne 0 ]; then
        echo "GENERATION $gen FALIED! STOPPING"
        break
    fi
done
python3 run.py --nodes $1 --distance $2 --outputdir $out_dir --generation -1 --targets $3
conda deactivate

# copy the contents in the tmp folder to the output folder in this directory
time_stamp=$(date +'%Y-%m-%d_%H.%M')
folder_name=$currentdir/output/$3/$1\_$2\_$time_stamp

mkdir -p $folder_name
mv $out_dir/* $folder_name
echo "File saved in $folder_name"

cd $currentdir
# mv slurm_$1\_$2\_*.out $currentdir/output/$folder_name/
rm -r $tmp_dir
