"""File that should produce a plot with the optimal number of nodes and protocol to be used per distance.

The optimal solution is interpolated from the results of the GA. If two points using different strategies are joined,
what do we do? One option is to run the simulation for those values and take the strategy that satisfies the targets
(if any) (Note: no need to create a parameter class, directly modify the input_dic read from the pickle object).

This plot must have the distance vs. the total cost (probably considering also double click but there are not enough
axes :( maybe use two markers? up/down for double click but a bit ugly).
"""
from argparse import ArgumentParser
import pickle
import pathlib

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

from scipy.interpolate import lagrange as interpolate
from scipy.optimize import curve_fit
from scipy.stats import linregress, chisquare
from joblib import Parallel, delayed
from uncertainties import ufloat
from uncvalue import Value, val, unc

from repchain.opt.plot import SLURM_RE, NODES_TO_COLOR_DC, NODES_TO_COLOR_SC, PROTOCOL_TO_MARKER
from repchain.runtools.data_analysis_tools import run_many_simulations, penalty
from repchain.utils.latexify import latexify, savefig


TARGETS = [(0.8, 1.), (0.9, 0.1)]

# COMMON_BASELINE_VALUES = {  # define as params to compute the cost
#     'elementary_link_fidelity': NVDoubleClickParameters.elementary_link_fidelity,  # dc only
#     'state_efficiency': NVDoubleClickParameters(3, 100)._state_efficiency(),
#     'detector_efficiency': NVDoubleClickParameters.p_det,
#     'p_q2gate': 1. - NVDoubleClickParameters.p_q2gate,
#     'T1': NVDoubleClickParameters.T1 / 1e3,
#     'T2': NVDoubleClickParameters.T2 / 1e3
# }


def exponention_cost_growth(distance, C0, D0):
    return C0 * np.exp(distance / D0)


def scan_best_solution_per_distance(directory, target):
    """Return a dictionary with the best parameters per distance."""
    RESULTS = {}

    outdir = pathlib.Path(directory) / 'output' / str(target)
    if outdir.exists():
        for simdir in outdir.iterdir():
            if match := SLURM_RE.match(simdir.name):
                nodes = int(match.group(1))
                dist = int(match.group(2))

                if dist in [1000, 1400]:
                	continue

                # best_file = simdir / 'best_result.csv'
                best_file_params = simdir / 'best_result_params.pkl'

                with open(best_file_params, 'rb') as f:
                    params = pickle.load(f)

                if params['result']['cost']['total'] > 1e9:
                    continue

                if dist in RESULTS:
                    params2 = RESULTS[dist]
                    if params['result']['cost']['total'] < params2['result']['cost']['total']:
                        RESULTS[dist] = params
                else:
                    RESULTS[dist] = params

    return RESULTS


def total_distance(p):
    return p['internode_distance'] * (p['number_of_nodes'] - 1)


TITLES = [r'$(i)$ $F_t = 0.8$ and $R_t = 1$Hz', r'$(ii)$ $F_t = 0.9$ and $R_t = 0.1$Hz']
markersize = 5


def add_legend_to_fig(fig, show_as_repeaters, adjust):
    nodes_keys = [2, 2, 3, 3, 5, 5, 9, 9, 17, 17]
    if show_as_repeaters:
        nodes_names = map(lambda x: x - 2, nodes_keys.copy())
    else:
        nodes_names = nodes_keys.copy()

    lines_nodes = [
        Line2D([0], [0], color='None', marker='None'),
        Line2D([0], [0], color='None', marker='None'),
        *[
            Line2D([0], [0], lw=2, color=(NODES_TO_COLOR_DC if j % 2 else NODES_TO_COLOR_SC)[n])
            for j, n in enumerate(nodes_keys)
        ]
    ]
    labels_nodes = [
        'SC',
        'DC',
        *list(map(str, nodes_names)),
    ]
    nodes_legend = fig.legend(
        lines_nodes, labels_nodes,
        title='Number of repeaters', loc='upper right', ncol=6, bbox_to_anchor=(0.55, 1.)
    )
    plt.gca().add_artist(nodes_legend)
    markers = [
        Line2D([0], [0], color='k', ls='None', marker=PROTOCOL_TO_MARKER[n], markersize=markersize)
        for n in range(len(PROTOCOL_TO_MARKER) - 1)
    ]
    protocol_legend = fig.legend(
        markers, ['SWAP-ASAP', 'EPL', 'DEJMPS (1)', 'DEJMPS (2)'],
        title='Strategies', loc='upper left', ncol=2, bbox_to_anchor=(0.58, 1.)
    )

    fig.subplots_adjust(top=adjust)


def just_plot_best(directories, show_as_repeaters=True):
    results = []
    for target in range(2):
        bests_to_plot = {}
        for directory in directories:
            strategy_results = scan_best_solution_per_distance(directory, target)
            for dist, params in strategy_results.items():
                if dist in bests_to_plot:
                    # params['result']['cost']['total'] *= (params['number_of_nodes'] - 2) if params['number_of_nodes'] > 2 else 1
                    params2 = bests_to_plot[dist]
                    if params['result']['cost']['total'] < params2['result']['cost']['total']:
                        bests_to_plot[dist] = params.copy()
                else:
                    bests_to_plot[dist] = params.copy()

        results.append(bests_to_plot)

    show_legend = 'levels' in directories

    latexify(plt, fract=0.65 if show_legend else 0.5, margin_x=0.025, margin_y=0.05)

    fig, axs = plt.subplots(nrows=1, ncols=2, sharey=True, gridspec_kw={'wspace': 0.1})

    axs[0].set_ylabel('Parameter cost')

    for target, bests in enumerate(results):
        axs[target].set_xlabel('Total distance (km)')
        axs[target].set_title(TITLES[target])
        # axs[target].set_yscale('log')

        print('\n' * 2, directories, target, '\n' + '='*20)

        fit_vals = []
        for distance, params in bests.items():
            if 'distillation_strategy' not in params:
                protocol = 0
            else:
                strategy = params['distillation_strategy']
                if not strategy[0] or strategy[0][1] == 0:
                    protocol = 0
                else:
                    # even strategy implies EPL (1) and odd strategy DEJMPS (2,3,4)
                    protocol = 1 if strategy[0][0] == 'EPL' else strategy[0][1] + 1

            mk = PROTOCOL_TO_MARKER[protocol]
            if np.isclose(params['eps'], 0.):  # sc
                color = NODES_TO_COLOR_SC[params['number_of_nodes']]
            else:
                color = NODES_TO_COLOR_DC[params['number_of_nodes']]

            axs[target].plot(distance, params['result']['cost']['total'], c=color, marker=mk, markersize=markersize)
            fit_vals.append((distance, params['result']['cost']['total']))

        fit_vals = sorted(fit_vals, key=lambda x: x[0])
        print(fit_vals)
        x_vals, y_vals = list(zip(*fit_vals))
        x_vals = np.array(x_vals)
        y_vals = np.array(y_vals)
        res = linregress(x_vals, np.log(y_vals))
        chi_exp, p_exp = chisquare(y_vals, np.exp(res.intercept + x_vals * res.slope)) 
        res2 = linregress(x_vals, y_vals)
        chi_ln, p_ln = chisquare(y_vals, res2.intercept + x_vals * res2.slope)
        print(res.rvalue, res2.rvalue, chi_exp, p_exp, chi_ln, p_ln)
        k_base = 9 if 'gates' in directories else 5
        if res.rvalue > res2.rvalue:
            fit_type = 'Exponential fit:'
            fit = np.exp(res.intercept), 1 / res.slope
            sigmas = fit[0] * res.intercept_stderr, res.stderr * fit[1]**2
            r2 = res.rvalue
            X = np.linspace(min(x_vals), max(x_vals), 50)
            kappa = Value(fit[0], sigmas[0])
            delta = Value(fit[1], sigmas[1])
            C = exponention_cost_growth(X, kappa, delta)
            d_max = delta * np.log(k_base / kappa)
        else:
            fit_type = 'Linear fit:'
            res = res2
            fit = res.intercept, 1 / res.slope
            sigmas = res.intercept_stderr, res.stderr * fit[1]**2
            r2 = res.rvalue
            X = np.linspace(min(x_vals), max(x_vals), 50)
            kappa = Value(fit[0], sigmas[0])
            delta = Value(fit[1], sigmas[1])
            C = kappa + X / delta
            d_max = delta * (k_base - kappa)
        # axs[target].plot(X, exponention_cost_growth(X, fit[0], fit[1]), color='gray')
        v, u = val(C), unc(C)
        # axs[target].fill_between(X, y1=v + u, y2=v - u, color='lightgrey')
        axs[target].plot(X, v, color='k', ls='--' if 'Exp' in fit_type else '-')

        C0 = ufloat(fit[0], sigmas[0])
        D0 = ufloat(fit[1], sigmas[1])
        DM = ufloat(d_max.x, d_max.ux)
        print(f'Dist max: {DM:.2uS}')
        box_pos = (0.6, 0.3) if 'gates' in directories and False else (0.04, 0.95)
        suffix = 'exp' if 'Exp' in fit_type else 'ln'
        axs[target].text(*box_pos,
                         fit_type + '\n$\kappa_{' + suffix + '} = ' + f'{C0:.2uS}' + '$\n'
                         r'$\Delta_{' + suffix + '} = ' + f'{D0:.2uSL}' + # '$km\n$d_{max} = ' + f'{DM:.2uSL}'
                         '$km\n$r^2 = ' + f'{r2:.5f}$',
                         fontsize=6,
                         horizontalalignment='left', verticalalignment='top', transform=axs[target].transAxes,
                         bbox=dict(fc="whitesmoke", boxstyle='Round'))

    if show_legend:
        add_legend_to_fig(fig, show_as_repeaters, 0.7)

    savefig(fig, '_'.join(directories))


PARAMS = [
    ('elementary_link_fidelity', r'$f_{elem}$', lambda x: x, False, None),
    # ('elementary_link_succ_prob', r'$p_{elem}$', lambda x: x, True),
    ('detector_efficiency', r'$\eta_{lm}$ (\%)', lambda x: x * 100, True, 0.0046 * 100),
    ('p_q2gate', r'$p_{2,gate}$ (\%)', lambda x: x * 100, True, 0.02 * 100),
    ('T1', r'$T_1$ (h)', lambda x: x / (3600 * 1e9), False, 1),
    ('T2', r'$T_2$ (s)', lambda x: x / 1e9, True, 1)
]


def just_plot_params(directories, show_as_repeaters=True):
    results = []
    for target in range(2):
        bests_to_plot = {}
        for directory in directories:
            strategy_results = scan_best_solution_per_distance(directory, target)
            for dist, params in strategy_results.items():
                if dist in bests_to_plot:
                    params2 = bests_to_plot[dist]
                    if params['result']['cost']['total'] < params2['result']['cost']['total']:
                        bests_to_plot[dist] = params.copy()
                else:
                    bests_to_plot[dist] = params.copy()

        results.append(bests_to_plot)

    width, height = 426.79135, 674.33032 - 74.33
    golden_mean = (np.sqrt(5) - 1.0) / 2.0  # Aesthetic ratio

    latexify(plt, fract=height / (width * golden_mean), margin_x=0.05, margin_y=0.06)

    nrows = len(PARAMS)
    fig, axs = plt.subplots(nrows=nrows, ncols=2, sharey='row', sharex=True, gridspec_kw={'wspace': 0.1, 'hspace': 0.1})

    markersize = 5

    titles = [r'\textbf{a.} $F_t = 0.8$ and $R_t = 1$Hz', r'\textbf{b.} $F_t = 0.9$ and $R_t = 0.1$Hz']
    roman = [r'$(i)$', r'$(ii)$', r'$(iii)$', r'$(iv)$', r'$(v)$', r'$(vi)$']

    for target, bests in enumerate(results):
        axs[nrows - 1, target].set_xlabel('Total distance (km)')
        axs[0, target].set_title(titles[target])

        for distance, params in bests.items():
            if 'distillation_strategy' not in params:
                protocol = 0
            else:
                strategy = params['distillation_strategy']
                if not strategy[0] or strategy[0][1] == 0:
                    protocol = 0
                else:
                    # even strategy implies EPL (1) and odd strategy DEJMPS (2,3,4)
                    protocol = 1 if strategy[0][0] == 'EPL' else strategy[0][1] + 1

            mk = PROTOCOL_TO_MARKER[protocol]
            if np.isclose(params['eps'], 0.):  # sc
                color = NODES_TO_COLOR_SC[params['number_of_nodes']]
            else:
                color = NODES_TO_COLOR_DC[params['number_of_nodes']]

            for j, param in enumerate(PARAMS):
                axs[j, target].plot(distance, param[2](params[param[0]]), c=color, marker=mk, markersize=markersize)
                if target == 0:
                    axs[j, target].set_ylabel(param[1])
                    if param[3]:
                        axs[j, target].set_yscale('log')
                axs[j, target].text(0.5, 0.9, roman[j], fontsize=8, horizontalalignment='center', verticalalignment='top', 
                	transform=axs[j, target].transAxes)
                if param[-1]:
                    axs[j, target].axhline(param[-1], color='k')

    add_legend_to_fig(fig, show_as_repeaters, 0.92)

    savefig(fig, '_'.join(directories) + '_params')


if __name__ == '__main__':
    # parser = ArgumentParser(description='Process some integers.')
    # parser.add_argument('directories', type=str, nargs='*', help='directories to compare')
    # # parser.add_argument(['target', '-t'], type=int, help='target', default=0)
    # # parser.add_argument('dx', type=int, help='increase in distance', default=50)
    # args = parser.parse_args()

    # compare(args.directories, args.target, args.dx)
    just_plot_best(['levels', 'lowest', 'doubleclick'])
    just_plot_best(['gates', 'doubleclick_gates'])
    just_plot_params(['levels', 'lowest', 'doubleclick'])
    just_plot_params(['gates', 'doubleclick_gates'])
