# Quantum Repeater Chain in NetSquid


Code based on https://gitlab.com/FranciscoHS/NetSquid-SimplifiedRepChain used in the production of the paper "Reducing hardware requirements for entanglement distribution via joint hardware-protocol optimization" by Adrià Labay Mora Francisco Ferreira da Silva and Stephanie Wehner.

**Abstract**

    We conduct a numerical investigation of fiber-based entanglement distribution 
    over distances of up to 1600km using a chain of processing-node quantum repeaters. 
    We determine minimal hardware requirements while simultaneously optimizing over 
    protocols for entanglement generation and entanglement purification, as well as 
    over strategies for entanglement swapping. Notably, we discover that through an 
    adequate choice of protocols the hardware improvement cost scales linearly with 
    the distance covered. Our results highlight the crucial role of good protocol choices 
    in significantly reducing hardware requirements, such as employing purification 
    to meet high-fidelity targets and adopting a SWAP-ASAP policy for faster rates. 
    To carry out this analysis, we employ an extensive simulation framework implemented 
    with NetSquid, a discrete-event-based quantum-network simulator, and a 
    genetic-algorithm-based optimization methodology to determine minimal hardware requirements.


## Usage

First make sure Python can find this package using
    
`export PYTHONPATH=$PYTHONPATH:/path/to/the/directory/Repchain`

Then run all unit tests by navigating to the main directory (the one where `Makefile` is located) and using

`make tests`


## A brief introduction to the RepChain code structure

### What is a repeater chain?

A repeater chain is a collection of nodes on a line which generate, store, manipulate and readout entangled quantum bits in order to generate entanglement between the two end nodes of the chain. The nodes performs three different kinds of actions:
  * *generate entanglement* with a neighbour node
  * *distill* or purify an entangled pair of qubits by consuming another pair. Distillation is probabilistic; if it fails, then all involved entanglement is destroyed.
  * *perform an entanglement swap*, which could intuitively be thought of as "glueing" two entangled links together to create entanglement on a longer distance.

An introduction to quantum repeater chains without any prerequisitie knowledge of quantum physics can be found in this [paper](https://ieeexplore.ieee.org/abstract/document/6576340/).

### What does the code in the NetSquid-Repchain repository do?

Using the code in this repository, one can set up a quantum repeater chain where the nodes hold a simplified model of quantum processors and subsequently run a repeater-chain-protocol (based on [BCDZ](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.81.5932)) on the chain.

## Code structure

### repchain

  * **logic**: task management of actions that a node performs during the simulation. Note that nodes can only perform a single action at a time.

  * **network**: utility model used to create a network from the configuration parameters which include hardware (errors, number of nodes, distance...) and protocol (purification, entanglement generation...) parameters.

  * **opt**: optimisation files with the genetic algorithm and a example run file which can be reused for different configurations. Includes also the deterministic protocol that optimises the best individual found by the genetic algorithm.

  * **parameters**: hardware parameters characterising the chain and nodes for different optimisation strategies.

  * **physical**: contains a magic entanglement distribution protocol that mimics real probabilitic protocols but saves resources by not computing all failed links. Instead, an entangled state is generated between two nodes according to the probability distribution of the protocols used.

  * **protocols**: contains the three subprotocols for entanglement generation, distillation and swapping. Moreover holds the **umbrellaprotocol**, which is the class containing both the node's logic and its three subprotocols and thus constitutes the 'full' protocol that is run on a node.

  * **runtools**: tools for running a simulation and datagathering during simulation runs.

  * **utils**: contains standalone utility classes, such as **memorymanager** for a node to track entanglement, the file **message** where a message format is defined, and some standalone tools **tools.py** and **timlogging.py** which are mainly used for testing and logging.

### optimisation

Contains the run files for each optimisation round. The `run.py` file is called from `run.sh` for each generation. Each call of `run.py` saves an `.npy` files with a list of individuals (made out of hardware and protocol parameters) and its corresponding cost to be used for the next generation. 

Results are saved in a subfolder depending on the target values and the total distance between end nodes.

# License

    Copyright 2023 labay11

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
