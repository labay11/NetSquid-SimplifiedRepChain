from abc import ABCMeta

import numpy as np

from netsquid.components.qprogram import QuantumProgram
from netsquid.components.instructions import INSTR_CNOT, INSTR_CXDIR,  INSTR_H, INSTR_ROT, INSTR_SWAP, INSTR_INIT
from netsquid_physlayer.quantum_program_library import move_using_CNOTs, move_using_CXDirections

from repchain.protocols.subprotocol import SubProtocol

# TODO: replace all classes by a single one `MoveProgram` that accepts a move function as parameter.


def move_using_three_CNOTs(q_program, control=0, target=1):
    q_program.apply(INSTR_CNOT, [control, target])
    q_program.apply(INSTR_CNOT, [target, control])
    q_program.apply(INSTR_CNOT, [control, target])


def reverse_move_using_CXDirections(q_program, control=0, target=1):
    """Swaps the qubits states using gates typical of NV Centers.

    The reverse of the circuit defined in :class:`easysquid.qProgramLibrary.move_using_CXDirections`.
    """
    q_program.apply(INSTR_ROT, control, angle=np.pi / 2, axis=[0, 1, 0])
    q_program.apply(INSTR_CXDIR, [control, target], angle=-np.pi / 2)
    q_program.apply(INSTR_ROT, control, angle=-np.pi / 2, axis=[1, 0, 0])
    # TODO I should still compute why the ROT_Z gates here
    # rotate over +pi/2 before the CXDIR and -pi/2 after
    # instead of the other way around
    q_program.apply(INSTR_ROT, target, angle=np.pi / 2, axis=[0, 0, 1])
    q_program.apply(INSTR_CXDIR, [control, target], angle=np.pi / 2)
    q_program.apply(INSTR_ROT, target, angle=-np.pi / 2, axis=[0, 0, 1])


def magical_move(q_program, control=0, target=1):
    """Magically swap the qubit states.

    This function uses a swap instruction (`INSTR_SWAP`) to swap the control and target qubits.

    Notes
    -----
    Most real quantum processor do not support this gate so make use of this function only for testing purpose.
    Real use scenarios should implement `move_using_three_CNOTs` that uses CNOT only which are available in
    some devices or `reverse_move_using_CXDirections` that uses gates available in NV center computers.
    """
    q_program.apply(INSTR_SWAP, [control, target])


def magical_move_rotated_target(q_program, control=0, target=1):
    """ maps electron -> carbon"""
    physical = False  # TODO should be 'false'
    q_program.apply(INSTR_H, target, physical=physical)
    q_program.apply(INSTR_SWAP, [control, target])
    q_program.apply(INSTR_H, target, physical=physical)


class MoveProgram(QuantumProgram, metaclass=ABCMeta):
    """Implementation of a Swap gate as a Quantum Program.

    Attributes
    ----------
    num_qubits : int, optional
        the number of qubits, default to 2
    parallel : bool, optional
        parallel, default to True
    """

    MOVE_FUNCTION = None
    """
    func
        function that implements the move, must take as input three positional parameters:

        * qprogram : QuantumProgram, the quantum program
        * control : int, the control qubit
        * target : int, the target qubit
    """

    def __init__(self, num_qubits=2, parallel=True):
        super().__init__(num_qubits=num_qubits, parallel=parallel)
        self.reset()
        self.initialize_target_before = False

    def program(self):
        control, target = self.get_qubit_indices(2)
        if self.initialize_target_before:
            self.apply(INSTR_INIT, [target])
        self.MOVE_FUNCTION(control=control, target=target)
        yield self.run()


class ThreeCNOTMoveProgram(MoveProgram):
    MOVE_FUNCTION = move_using_three_CNOTs


class TwoCNOTMoveProgram(MoveProgram):
    """Maps the target (carbon) state onto the control (electron)"""

    MOVE_FUNCTION = move_using_CNOTs


class CXDirectionMoveProgram(MoveProgram):
    """Maps the control (electron) state onto the target (carbon), modulo a Hadamard gate on the target (carbon)"""

    MOVE_FUNCTION = move_using_CXDirections


class ReverseCXDirectionMoveProgram(MoveProgram):
    MOVE_FUNCTION = reverse_move_using_CXDirections


class MagicalMoveProgram(MoveProgram):
    MOVE_FUNCTION = magical_move


class MagicalMoveRotatedTargetProgram(MoveProgram):
    """Maps the control (electron) state onto the target (carbon) and vice versa, modulo a Hadamard gate on
    the target (carbon)"""

    MOVE_FUNCTION = magical_move_rotated_target


class ReverseMagicalMoveRotatedTargetProgram(MoveProgram):
    MOVE_FUNCTION = magical_move_rotated_target

    def program(self):
        control, target = self.get_qubit_indices(2)
        if self.initialize_target_before:
            self.apply(INSTR_INIT, [control])
        q_program = self
        q_program.MOVE_FUNCTION(control=control,
                                target=target)
        yield self.run()


class MoveProtocol(SubProtocol):
    """Performs a swap.

    Attributes
    ----------
    respect_NV_structure : bool
        See `is_move_magical`.
    is_move_magical : bool
        Determines, together with `respect_NV_structure`, which quantum circuit
        to use for swapping qubits from position.
    """

    def __init__(self, index, qmemory, memmanager, respect_NV_structure=True, is_move_magical=True):
        super().__init__(name="MOVE",
                         index=index,
                         memmanager=memmanager,
                         class_channels=None)
        self.qmemory = qmemory
        self.respect_NV_structure = respect_NV_structure

        # choose the correct quantum circuits
        if self.respect_NV_structure and False:
            if is_move_magical:
                self.prgm = MagicalMoveRotatedTargetProgram()
                self.reverse_prgm = ReverseMagicalMoveRotatedTargetProgram()
            else:
                self.prgm = CXDirectionMoveProgram()
                self.reverse_prgm = ReverseCXDirectionMoveProgram()
        else:
            self.prgm = MagicalMoveProgram()
            self.reverse_prgm = self.prgm

    def trigger(self, old_pos, new_pos):
        """
        Performs a MOVE-operation between memory positions; i.e. the qubit on position `old_pos` is swapped with
        the qubit on `new_pos`.

        Notes
        -----
        If `respect_NV_structure` is set to True while `is_move_magical`
        is set to False, then the underlying quantum registers are merged
        in this process, which might result in a memory overflow error.
        """
        self.qmemory.set_program_done_callback(
            callback=lambda: self._schedule_now(self.evtype_finished),
            once=True)
        self.memmanager.swap(posA=old_pos, posB=new_pos)
        if old_pos == self.memmanager.communication_position:
            prgm = self.prgm
            control = old_pos
            target = new_pos
        else:
            if self.respect_NV_structure:
                assert(new_pos == self.memmanager.communication_position)
            prgm = self.reverse_prgm
            control = new_pos
            target = old_pos
#            control = old_pos
#            target = new_pos
        prgm.initialize_target_before = self.respect_NV_structure
        self.qmemory.execute_program(prgm, qubit_mapping=[control, target])
