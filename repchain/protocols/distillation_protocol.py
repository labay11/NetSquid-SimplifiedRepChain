"""
This file contains the following classes:

  * `IDistillationProgram`, which represents the interface for
    a quantum program (a circuit) which implements the local operations
    for DEJMPS distillation (http://doi.org/10.1103/PhysRevLett.77.2818)
  * `UnrestrictedDistillationProgram`: the quantum program that
    performs a CNOT, followed by a measurement in Z. This is the
    most straightforward implementation of the local operations
    for the DEJMPS distillation protocol.
  * `RestrictedDistillationProgram`: the quantum program which
    performs a more complex sequence of operations which respect
    the constraints of the NV center.
    Circuit is explained in http://doi.org/10.1126/science.aan0070
  * `LocalDistillationProtocol`:
"""
import abc
import logging

import numpy as np

import netsquid as ns
from netsquid.components.instructions import INSTR_CNOT, INSTR_MEASURE, INSTR_ROT, INSTR_CXDIR
from netsquid.components.qprogram import QuantumProgram

from repchain.protocols.subprotocol import SubProtocol
from repchain.utils.message import Message
from repchain.utils.tools import index_of_Pauli_product, does_precisely_one_commute_with_Z
from repchain.utils.bcdzfunctions import get_level


class IDistillationProgram(QuantumProgram, metaclass=abc.ABCMeta):
    """
    Abstract base class for the quantum circuit that implements the local operations for entanglement distillation.
    The two qubits that the local operations act upon are called 'keep' and 'lose', indicating whether the qubit
    is measured ('lose') or not ('keep').

    The local operations consists of (in this order):

      * pre distillation: applying a correction Pauli to the 'keep' qubit
      * performing a fixed sequence of gates and measurements

    Using the class method `get_correction_Pauli_keep`, one can find out
    the classically-tracked Pauli operator belonging to the 'keep' qubit
    after this protocol has finished.

    Attributes
    ----------
    num_qubits : int
        number of qubits needed for distillation
    parallel : bool, optional
        whether the distillation can be done in parallel, default to True
    outcome : int
        gives the outcome of the measurement after distillation has been done.
    pre_correction_Pauli : int
        Index of the single-qubit Pauli operator that will be applied to qubit 'lose' before the local distillation
        operations (0=Id, 1=X, 2=Y, 3=Z).

    Notes
    -----
    Takes two qubits.
    Convention: the *first* qubit is always the one that is measured (the 'lose' qubit).
    """
    PROGRAM_NAME = None
    default_num_qubits = 2
    _MEASUREMENT_OUTCOME_NAME = "distillation_outcome"
    _KEEP_MEASURED_QUBIT = False  # for testing purposes

    def __init__(self, num_qubits=None, parallel=True):
        super().__init__(num_qubits=num_qubits, parallel=parallel)
        self.reset()
        self._pauli_correction_keep = 0
        self._pauli_correction_lose = 0

    def clear(self):
        self._pauli_correction_keep = 0
        self._pauli_correction_lose = 0

    @abc.abstractmethod
    def pauli_correction(self, pauli_keep, pauli_lose):
        pass

    @property
    def outcome(self):
        """Measurement outcome of the local distillation operations."""
        return self.output[self._MEASUREMENT_OUTCOME_NAME][0]

    def program(self):
        super().program()
        q_lose, q_keep = self.get_qubit_indices(2)
        if self._pauli_correction_keep > 0:
            self.apply(
                INSTR_ROT,
                [q_keep],
                angle=np.pi,
                axis=[(1 if i == self._pauli_correction_keep else 0) for i in range(1, 4)]
            )
        if self._pauli_correction_lose > 0:
            self.apply(
                INSTR_ROT,
                [q_lose],
                angle=np.pi,
                axis=[(1 if i == self._pauli_correction_lose else 0) for i in range(1, 4)]
            )
        return q_lose, q_keep

    @classmethod
    def was_distillation_successful(cls, local_outcome, remote_outcome):
        """Check if the distillation protocol was successful.

        Parameters
        ----------
        local_outcome : int
            the result of the local measurement
        remote_outcome : int
            the result of the remote measurement

        Returns
        -------
        bool
            Whether the two outcomes correspond to a successfull entanglement distillation attempt.
        """
        pass

    @classmethod
    @abc.abstractmethod
    def get_correction_pauli_keep(cls,
                                  is_master,
                                  correction_Pauli_local,
                                  correction_Pauli_remote):
        """Compute the correction Pauli for the 'keep' qubit after the distillation experiment.

        Parameters
        ----------
        is_master : bool
            Whether the current node is the 'master' of the two nodes involved in the distillation.
        correction_Pauli_local : int
            Correction Pauli operator of the 'keep' before start of the distillation protocol of the node 'self'.
        correction_Pauli_remote : int
            Correction Pauli operator of the 'keep' before start of the distillation protocol of the remote node.

        Returns
        -------
        int
            Index of the Pauli operator (0=Id, 1=X, 2=Y, 3=Z).
        """
        # check input
        if not isinstance(is_master, bool):
            raise TypeError("Parameter `is_master` determines if the calling node\
                             is the `master` or `slave` of the two and should thus be a bool")
        for correction_Pauli in [correction_Pauli_local,  correction_Pauli_remote]:
            if correction_Pauli not in [0, 1, 2, 3]:
                raise TypeError("Parameters for correction Paulis should indicate a Pauli matrix (0=Id, 1=X, 2=Y, 3=Z)")
        pass


class DEJMPSProgram(IDistillationProgram):
    """
    Quantum program that performs a CNOT, followed by a measurement in Z. This is the
    most straightforward implementation of the local operations
    for the DEJMPS distillation protocol.
    """
    PROGRAM_NAME = 'DEJMPS'

    def pauli_correction(self, pauli_keep, pauli_lose):
        self._pauli_correction_lose = index_of_Pauli_product(pauli_keep, pauli_lose)

    def program(self):
        q_lose, q_keep = super().program()
        self.apply(INSTR_CNOT, [q_keep, q_lose])
        self.apply(instruction=INSTR_MEASURE,
                   qubit_indices=q_lose,
                   output_key=self._MEASUREMENT_OUTCOME_NAME,
                   inplace=self._KEEP_MEASURED_QUBIT)
        yield self.run()

    @classmethod
    def was_distillation_successful(cls, local_outcome, remote_outcome):
        return local_outcome == remote_outcome

    @classmethod
    def get_correction_pauli_keep(cls, is_master, correction_Pauli_local, correction_Pauli_remote):
        super().get_correction_pauli_keep(
            is_master=is_master,
            correction_Pauli_local=correction_Pauli_local,
            correction_Pauli_remote=correction_Pauli_remote)
        if is_master and does_precisely_one_commute_with_Z(correction_Pauli_local, correction_Pauli_remote):
            return 1  # the X Pauli matrix
        return 0  # the Identity matrix


class EPLProgram(DEJMPSProgram):
    """Implementation of the distillation protocol for `R` states.

    This is essentially the same as the DEJMPS protocol but takes into account the
    special form of the states using the information stored in non-diagonal elements.

    More information can be fount in section IV.3.C (page 9) of DOI: 10.1103/PhysRevA.97.062333.
    """
    PROGRAM_NAME = 'EPL'

    # def pauli_correction(self, pauli_keep, pauli_lose):
    #     self._pauli_correction_lose = pauli_lose
    #     self._pauli_correction_keep = pauli_keep

    @classmethod
    def was_distillation_successful(cls, local_outcome, remote_outcome):
        """Distillation is successful if both outcomes are 1."""
        return local_outcome == 1 and remote_outcome == 1

    # @classmethod
    # def get_correction_pauli_keep(cls, is_master, correction_Pauli_local, correction_Pauli_remote):
    #     """No correction is necessary, the state is directly projected to the Psi_+ state."""
    #     return 0  # the Identity matrix


class NVDEJMPSProgram(IDistillationProgram):
    """
    The set of gates for distillation as found in
    Fig. 1c, circuit 4 (purple) in http://doi.org/10.1126/science.aan0070
    """

    _include_measurement = True

    def pauli_correction(self, pauli_keep, pauli_lose):
        self._pauli_correction_lose = index_of_Pauli_product(pauli_keep, pauli_lose)

    def program(self):
        q_lose, q_keep = super().program()
        electron = q_lose
        carbon = q_keep
        self.apply(INSTR_ROT, electron, angle=np.pi / 2, axis=[0, 1, 0])
        self.apply(INSTR_CXDIR, [electron, carbon], angle=-np.pi / 2)
        self.apply(INSTR_ROT, electron, angle=np.pi / 2, axis=[1, 0, 0])
        if self._include_measurement:
            self.apply(instruction=INSTR_MEASURE,
                       qubit_indices=electron,
                       output_key=self._MEASUREMENT_OUTCOME_NAME,
                       inplace=self._KEEP_MEASURED_QUBIT)
        yield self.run()

    @classmethod
    def was_distillation_successful(cls, local_outcome, remote_outcome):
        return local_outcome == 0 and remote_outcome == 0

    @classmethod
    def get_correction_pauli_keep(cls,
                                  is_master,
                                  correction_Pauli_local,
                                  correction_Pauli_remote):
        super().get_correction_pauli_keep(
            is_master=is_master,
            correction_Pauli_local=correction_Pauli_local,
            correction_Pauli_remote=correction_Pauli_remote)
        if is_master and does_precisely_one_commute_with_Z(correction_Pauli_local, correction_Pauli_remote):
            return 2  # the Y Pauli matrix
        return 0  # the Identity matrix


DIST_PROGRAMS = {
    DEJMPSProgram.PROGRAM_NAME: DEJMPSProgram,
    EPLProgram.PROGRAM_NAME: EPLProgram
}


class LocalDistillationProtocol(SubProtocol):
    """
    Performs the DEJMPS distillation protocol locally. That is:

        1. first performs the local operations needed for DEJMPS;

        2. the local operations include a measurement. The measurement
        outcome is sent to the other node performing the distillation;

        3. receives the remote measurement outcome and compares it to
        the measurement outcome of the node itself to determine whether the distillation was successful

    Note: operation (3) might occur before, after or simultaneously with (1) or (2).

    This protocol does not actively listen to messages on the classical channel:
    the remote measurement outcome must be passed on to this protocol manually by
    using the function `process_incoming_message`.

    When the protocol has finished (i.e. when steps 1-3 above have finished,
    regardless of whether the distillation succeeded), it updates the memorymanager,
    removes the 'keep' qubits from the QuantumProcessor in case of failure,
    and signals the `evtype_finished` event type.

    Attributes
    ----------
    index : int
        Node ID of the node performing this local protocol.
    qmemory : :class:`netsquid.components.qprocessor.QuantumProcessor`
    memmanager : :class:`repchain.utils.memory_manager.MemoryManager`
    class_channels : dict if (str, :class:`netsquid.components.cchannel.ClassicalChannel`)
        Classical channels that the measurement outcome will be sent over where the keys are the directions "L" or "R".
        Note: for sending; the protocol doesn't pick up anything that it receives from it!
    respect_NV_structure : bool, optional
        Determines whether a generic (CNOT-measure) quantum circuit is used
        for the local operations of distillation, or one that is tailored
        to the NV center (see :class:`repchain.protocols.distillation_protocol.NVDistillationProgram`).
    swap_fidelity_threshold : float, optional
        the fidelity needed for a link to the ready for swap, default to 0.
    distillation_strategy : dict of int and (str, int) or (str, int) or None
        this controls how links are distilled per level, the dictionary keys run from 0 to `l` where `2^l + 1` is the
        number of nodes in the chain and the value at a particular level has information about the program
        used (see :class:`repchain.protocols.distillation_protocol.IDistillationProgram`) and
        the number of time to distill a link at that given level. If None, no distillation is used but the
        nesting bdcz strategy remain and if a single pair of (str, int) is specified then the same strategy is used
        at all the levels. Default to `('DEJMPS', 1)`.

    Notes
    -----
    The original DEJMPS distillation protocol was presented in:
    http://doi.org/10.1103/PhysRevLett.77.2818
    """

    def __init__(self,
                 index,
                 qmemory,
                 memmanager,
                 class_channels,
                 respect_NV_structure=False,
                 swap_fidelity_threshold=0.):
        super().__init__(name="DIST",
                         index=index,
                         memmanager=memmanager,
                         class_channels=class_channels)
        self._qmemory = qmemory
        self._swap_fidelity_threshold = swap_fidelity_threshold
        self._prgm = None

        if respect_NV_structure:
            logging.info('respect_NV_structure is set to True but right now this is not supported, '
                         'falling back to abstract.')

        self.distill_attempts = {}

        self.reset()

    def reset(self):
        super().reset()
        self._myoutcome = None
        self._otheroutcome = None
        self._remote_node_id = None
        self._pos_keep = None
        self._pos_lose = None
        self._link_keep = None
        self._remote_pauli_index_keep = None
        self._strategy = None
        if self._prgm:
            self._prgm.clear()
            self._prgm = None

    def trigger(self, pos_lose, pos_keep, strategy):
        """
        Perform local DEJMPS distillation steps on qubits in position `pos_lose` and `pos_keep`,
        including sending the measurement outcome to the remote node.
        Here, `pos_lose` is the qubit that is measured during the distillation protocol and thus lost.

        Parameters
        ----------
        pos_lose : int
            Memory position of the qubit that will be measured
            and thus will be lost.
        pos_keep : int
            Memory position of the qubit that will *not* be
            measured and will thus *not* be lost.
        """
        if pos_lose == pos_keep:
            raise ValueError("Needs at least two links for distillation")
        # setting variables
        self._pos_keep = pos_keep
        self._pos_lose = pos_lose
        self._remote_node_id = self.memmanager.remote_node_at(pos_lose)
        logging.info("{}:DIST->{}:(pos:{})".format(self.index, self._remote_node_id, (pos_lose, pos_keep)))

        # Ensuring that pauli_lose * pauli_keep will be applied to the `lose`-qubit.
        virtual_pauli_index_lose = self.memmanager.get_link(pos=self._pos_lose).cor_Pauli
        self._link_keep = self.memmanager.get_link(pos=self._pos_keep)
        virtual_pauli_index_keep = self._link_keep.cor_Pauli

        self._level = get_level(self.index, self._remote_node_id)
        if not strategy or (isinstance(strategy, dict) and not strategy[self._level]):
            raise ValueError('No distillation should be happing at this level: {}.'.format(strategy))

        self._strategy = strategy[self._level] if isinstance(strategy, dict) else strategy
        self._prgm = DIST_PROGRAMS[self._strategy[0]]()
        self._prgm.pauli_correction(virtual_pauli_index_keep, virtual_pauli_index_lose)

        self._qmemory.set_program_done_callback(callback=self._send_info_to_peer)
        self._qmemory.execute_program(self._prgm, qubit_mapping=[pos_lose, pos_keep])

    def _send_info_to_peer(self):
        """Send a message to the remote node with the distillation outcome."""
        direction = "L" if self._remote_node_id < self.index else "R"
        self._myoutcome = self._prgm.outcome
        info = (self._myoutcome, self._link_keep.cor_Pauli)
        msg = Message.distill(sender=self.index,
                              receiver=self._remote_node_id,
                              subject=Message.SUBJECT_DIST_OUTCOME,
                              content=info)
        self.class_channels[direction].send(items=msg)
        self._check_finished()

    def process_incoming_message(self, msg):
        """Parse the message containing the distillation outcome from the remote node.

        Parameters
        ----------
        msg : :class:`repchain.utils.message.Message`
            Message containing the distillation outcome.
        """
        assert msg.has_header(Message.ACTION_DIST, Message.SUBJECT_DIST_OUTCOME)
        self._otheroutcome, self._remote_pauli_index_keep = msg.content
        self._check_finished()

    def _check_finished(self):
        if self._myoutcome is not None and self._otheroutcome is not None:
            # data collection
            self._last_handled_pair = (ns.sim_time(), self._remote_node_id, self._pos_keep)
            self._schedule_now(self.EVT_COLLECT_DATA)

            if self._remote_node_id not in self.distill_attempts:
                self.distill_attempts[self._remote_node_id] = {'s': 0, 'f': 0}
            # update the memorymanager
            if self._prgm.was_distillation_successful(self._myoutcome, self._otheroutcome):
                self._update_estimated_fidelity(pos=self._pos_keep)
                self._update_correction_Pauli_keep()
                self.distill_attempts[self._remote_node_id]['s'] += 1
            else:
                self.memmanager.discard(pos=self._pos_keep)
                self.distill_attempts[self._remote_node_id]['f'] += 1

            self.memmanager.discard(pos=self._pos_lose)

            # "close" this run of the protocol
            self.reset()
            self._schedule_now(self.evtype_finished)

    def _update_correction_Pauli_keep(self):
        self._link_keep.cor_Pauli = \
            self._prgm.get_correction_pauli_keep(
                is_master=self.index > self._remote_node_id,
                correction_Pauli_local=self._link_keep.cor_Pauli,
                correction_Pauli_remote=self._remote_pauli_index_keep)

    def _update_estimated_fidelity(self, pos):
        # Avoid fidelity decay over time is also applied during the operation
        # TODO Apply imperfect gate fidelities
        link = self.memmanager.get_link(pos)
        level = get_level(self.index, link.remote_node_id)
        # the filtering protocol yields a perfect Phi_+ state
        prgm, n = self._strategy
        f = link.estimated_fidelity + (self._swap_fidelity_threshold[level]-self.memmanager.elementary_link_fidelity)/n

        link.update_fidelity(f)
