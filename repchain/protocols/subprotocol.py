from abc import ABCMeta

import netsquid as ns


class SubProtocol(ns.Entity, metaclass=ABCMeta):
    """Entity class

    Attributes
    ----------
    name : str
        name of the protocol
    index : int
        index of the node the protocol acts on
    memmanager : :class:`repchain.utils.memorymanager.MemoryManager`
        memory manager of the links
    class_channels : dict of (str, :class:`ClassicalChannel`)
        classical channels connecting the node to its Neighbours in both directions ('L' and 'R').
    """

    EVT_COLLECT_DATA = ns.EventType("COLLECT_DATA", "Data should be collected")
    """EventType that tells when the data can be collected"""

    def __init__(self,
                 name,
                 index,
                 memmanager,
                 class_channels=None):
        self.name = name  # just used for logging purposes
        self.index = index
        self.memmanager = memmanager
        self.evtype_finished = ns.EventType("FIN", "Subprotocol has finished")
        if class_channels is not None:
            self.class_channels = class_channels

    @property
    def last_handled_pair(self):
        """
        Returns a tuple (time, remote node ID, position of the 'keep' qubit).
        This variable is not used by the protocol and is only present for logging purposes.
        """
        return self._last_handled_pair

    def reset(self):
        # for data collection purposes only
        self._last_handled_pair = None

    def trigger(self, **kwargs):
        pass

    def process_incoming_message(self, msg):
        pass
