from netsquid_magic.magic_distributor import MagicDistributor
from netsquid_nv.magic_distributor import NVMagicDistributor
from repchain.physical.simplified_delivery_model import SimplifiedStateDeliverySamplerFactory


class SimplifiedMagicDistributor(MagicDistributor):
    """Magic distributor of NV-type states with fidelity given by the parametter gamma, being generated with probability
    psuc, an attempt happening every cycle_time. The cycle_time is divided by itself so as to be used as the base time
    unit of the simulations.

    Attributes
    ----------
    nodes : int
        number of nodes in the chain
    component : :obj:`~netsquid.components.component.Component` or None
        Component from which physical parameters will be extracted.
        If None, parameters will be extracted from the specified nodes.
    """
    def __init__(self, nodes, component=None, **kwargs):
        super().__init__(delivery_sampler_factory=SimplifiedStateDeliverySamplerFactory(),
                         nodes=nodes,
                         component=component,
                         state_delay=1.e-10,
                         **kwargs)
        self._callbacks = [{}]  # every MD has a dictionary of callbacks

    def add_callback(self, callback, node_id, md_index=0):
        """
        Parameters
        ----------
        callback : callback function with delivery_id as argument
        node_id : ID of the node who adds the callback
        md_index : int (default 0 for when magic distributor has not been merged yet)
            Index of original magic distributor and its callbacks.

        Notes
        -----
        Each callback gets called at every individual node delivery. Thus, for every time
        :meth:`~MagicDistributor.add_delivery` is called, each callback is called once for each node in
        :prop:`~MagicDistributor.nodes`.
        """
        self._callbacks[md_index][node_id] = callback

    def clear_all_callbacks(self):
        """Removes all set callback functions."""
        self._callbacks = [{}]

    def _handle_state_delivery(self, node_delivery, event):
        # BEGIN PART THAT IS DIFFERENT FROM super()._handle_state_delivery ###
        # let the calling node know that a state was created, only perform callbacks for that original MD
        delivery = node_delivery.delivery
        callbacks = self._callbacks[delivery.parameters['md_index']]
        if node_delivery.node_id in callbacks:
            callbacks[node_delivery.node_id](event=event)
        # END PART THAT IS DIFFERENT FROM super()._handle_state_delivery ###

        self._schedule_label_delivery_event(node_delivery=node_delivery)

        # ensure the same delivery will not be performed multiple times
        self._pop_state_delivery(event)


class NVMagicDistributorWithCallbacks(NVMagicDistributor):
    """
    Subclass of :obj:`~netsquid_nv.magic_distributor.NVMagicDistributor`
    with the changed functionality that callbacks are now node-dependent.
    That is, when entanglement is magically delivered, only the nodes
    involved in the entanglement will be notified.

    Notes
    -----
    Only the methods in :obj:`~netsquid_nv.magic_distributor.NVMagicDistributor`
    in which the callbacks occur are altered in this class.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._callbacks = [{}]  # every MD has a dictionary of callbacks

    def add_callback(self, callback, node_id, md_index=0):
        """
        Parameters
        ----------
        callback : callback function with delivery_id as argument
        node_id : ID of the node who adds the callback
        md_index : int (default 0 for when magic distributor has not been merged yet)
            Index of original magic distributor and its callbacks.

        Notes
        -----
        Each callback gets called at every individual node delivery. Thus, for every time
        :meth:`~MagicDistributor.add_delivery` is called, each callback is called once for each node in
        :prop:`~MagicDistributor.nodes`.
        """
        self._callbacks[md_index][node_id] = callback

    def clear_all_callbacks(self):
        """Removes all set callback functions."""
        self._callbacks = [{}]

    def _handle_state_delivery(self, node_delivery, event):
        # BEGIN PART THAT IS DIFFERENT FROM super()._handle_state_delivery ###
        # let the calling node know that a state was created, only perform callbacks for that original MD
        delivery = node_delivery.delivery
        callbacks = self._callbacks[delivery.parameters['md_index']]
        if node_delivery.node_id in callbacks:
            callbacks[node_delivery.node_id](event=event)
        # END PART THAT IS DIFFERENT FROM super()._handle_state_delivery ###

        self._schedule_label_delivery_event(node_delivery=node_delivery)

        # ensure the same delivery will not be performed multiple times
        self._pop_state_delivery(event)
