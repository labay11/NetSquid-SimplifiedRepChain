import numpy as np

from netsquid.qubits.dmtools import DMState
from netsquid.qubits.qubit import Qubit
from netsquid.qubits.qubitapi import dephase
from netsquid_magic.state_delivery_sampler import HeraldedStateDeliverySamplerFactory


DM_UPUP = np.array([[1, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]],
                   dtype=np.complex)

DM_DOWNDOWN = np.array([[0, 0, 0, 0],
                        [0, 0, 0, 0],
                        [0, 0, 0, 0],
                        [0, 0, 0, 1]],
                       dtype=np.complex)

DM_PSI_PLUS = np.array([[0, 0, 0, 0],
                        [0, 0.5, 0.5, 0],
                        [0, 0.5, 0.5, 0],
                        [0, 0, 0, 0]],
                       dtype=np.complex)

DM_PSI_MINUS = np.array([[0, 0, 0, 0],
                         [0, 0.5, -0.5, 0],
                         [0, -0.5, 0.5, 0],
                         [0, 0, 0, 0]],
                        dtype=np.complex)

DM_PHI_PLUS = np.array([[0.5, 0, 0, 0.5],
                        [0, 0, 0, 0],
                        [0, 0, 0, 0],
                        [0.5, 0, 0, 0.5]],
                       dtype=np.complex)

DM_PHI_MINUS = np.array([[0.5, 0, 0, -0.5],
                         [0, 0, 0, 0],
                         [0, 0, 0, 0],
                         [-0.5, 0, 0, 0.5]],
                        dtype=np.complex)

ID4 = np.identity(4, dtype=np.complex)


def get_states(gamma, werner=False, eps=0):
    """Compute the density matrix of a state with fidelity `gamma` with respect to the perfect Bell state Φ+.

    Parameters
    ----------
    gamma : float
        Fidelity of generated state.
    werner: bool
        If true, generated state is in the Werner form.
    eps: float
        Perturbation to Werner state.

    Returns
    -------
    list of two :class:`netsquid.qubits.dmtools.DMState`
    """
    if not werner:
        dm_plus = gamma * DM_PSI_PLUS + (1 - gamma) * ((1 - eps) * DM_UPUP + eps * DM_DOWNDOWN)
        dm_minus = gamma * DM_PSI_MINUS + (1 - gamma) * ((1 - eps) * DM_UPUP + eps * DM_DOWNDOWN)

        return [dm_plus, dm_minus], [0.5, 0.5]

    # dm_other = np.identity(4, dtype=np.complex) - DM_PHI_PLUS
    # dm_out = (4*gamma-1) * dm_bell_psi_plus/3 + ((1-gamma)/3)*identity
    dm_out = (1 - eps) * (gamma * DM_PSI_PLUS + ((1 - gamma) / 3) * (ID4 - DM_PSI_PLUS)) + eps * DM_UPUP
    # dm_out = (1 - eps) * (gamma * DM_PHI_PLUS + (1 - gamma) / 3 * dm_other) + eps * DM_UPUP
    return [dm_out, dm_out], [1., 0.]


def successful_density_matrices(elementary_link_fidelity, **kwargs):
    """
    In this model we return two possible states, depending on the truth value of parameter 'werner', whose default value
    is False. In this case, the returned state is of the type
    :math:`\\rho = \\gamma \\Psi_+ + (1-\\gamma)|11><11|`,
    which is intended to be a 1-parameter approximation of the type of states generated with NV centers.
    If the `werner` is true, the returned state is a Werner state.

    Parameters
    ----------
    elementary_link_fidelity : float
        Fidelity of returned state.
    werner : bool
        If true, returned state is a Werner state.
    eps : float
        Quantifies divergence from Werner state.

    Returns
    -------
    list of two :class:`netsquid.qubits.dmtools.DMState` objects
    """
    werner = kwargs['werner'] if 'werner' in kwargs else False
    eps = kwargs['eps'] if 'eps' in kwargs else 0

    dm_states, _ = get_states(elementary_link_fidelity, werner, eps)
    for i, dm in enumerate(dm_states):
        # putting the density matrix onto two freshly generated qubits
        qubits = [Qubit("A"), Qubit("B")]
        DMState(qubits=qubits, dm=dm)
        for qubit in qubits:
            dephase(qubit, 0)

        dm_states[i] = qubit.qstate.dm

    return dm_states, ([1., 0.] if werner else [0.5, 0.5])


def success_probability(elementary_link_succ_prob, **kwargs):
    """
    In this model the success probability of establishing an elementary link is understood to be a parameter, not a
    computed quantity, so this function directly returns the user-defined success probability.

    Parameters
    ----------
    elementary_link_succ_prob : float

    Returns
    -------
    psuc : float
    """
    return elementary_link_succ_prob


class SimplifiedDeliverySamplerFactory(HeraldedStateDeliverySamplerFactory):
    """
    A simple delivery sampler factory for entanglement generation. Can deliver either Werner states or
    Bell states with an infidelity corresponding to both spins being up, parametrized by the elementary
    link fidelity parameter.
    """
    def __init__(self):
        super().__init__(func_delivery=get_states,
                         func_success_probability=success_probability)


class SimplifiedStateDeliverySamplerFactory(HeraldedStateDeliverySamplerFactory):
    """
    Abstract base class for a factory that produces
    :obj:`~repchain.state_delivery_factory.SimplifiedDeliverySamplerFactory`
    objects for two-qubit entangled states between two abstract processing nodes.
    """
    def __init__(self):
        super().__init__(func_delivery=successful_density_matrices,
                         func_success_probability=success_probability)
