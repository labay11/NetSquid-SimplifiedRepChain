from abc import ABCMeta

import numpy as np
from netsquid.qubits.state_sampler import StateSampler
from netsquid.magic_distributor.state_delivery_sampler import IStateDeliverySamplerFactory, StateDeliverySampler


class DeterministicHeraldedStateDeliverySamplerFactory(IStateDeliverySamplerFactory, metaclass=ABCMeta):
    r"""
    Abstract base class for a factory that produces
    :obj:`~netsquid_magic state_delivery_factory.StateDeliverySampler`
    objects for which the underlying tree is of the form

    .. code-block:: text

            +-------+
            |  root |
            +-------+
            //      \\
           //        \\
          //          \\
         //            \\
        Id/n            \\
                         \\
                 +------------------+
                 |      success     |
                 +------------------+
                //   ||    \\       \\
               //    ||     \\       \\
              //     ||      \\       \\
             State0  State1  State2  ....

    Example usage for delivering:

      * the mixed state :math:`0.25 |0\rangle\langle 0| + 0.75 |1\rangle\langle1|`
      * which is generated in attempts, each of which succeeds with probability 0.3
      * the cycle time (time of a single attempt) is 10

    .. code-block:: python

        from netsquid.qubits.ketstates import s0, s1, ket2dm
        from netsquid_magic.state_sampler.state_delivery_sampler import HeraldedStateDeliverySamplerFactory

        # Case A: the mixed state is in ket form

        def func_delivery_ket(p, **kwargs):
            kets = [s0, s1]
            probabilities = [p, 1 - p]
            return kets, probabilities

        # Case B: the mixed state is in density matrix form

        def func_delivery_dm(p, **kwargs):
            dm = p * ket2dm(s0) + (1 - p) * ket2dm(s1)
            return [dm], [1.]

        # Creating a DeliverySamplerFactory with either of the two functions

        def func_success_probability(**kwargs):
            return 0.3

        for func_delivery in [func_delivery_ket, func_delivery_dm]:
            sampler_factory = HeraldedStateDeliverySamplerFactory(func_delivery, func_success_probability)

            parameters = {'p': 0.25, 'cycle_time': 10}
            print(sampler_factory.create_state_delivery_sampler(**parameters).sample())
        # DeliverySample(state=array([[0.+0.j],
        #     [1.+0.j]]), delivery_duration=10.0, label=('success', 1))
        # DeliverySample(state=array([[0.25+0.j, 0.  +0.j],
        #     [0.  +0.j, 0.75+0.j]]), delivery_duration=50.0, label=('success', 0))
    """

    LABELS = ["success", "fail"]

    def __init__(self, func_delivery, func_success_probability, failure_state=None):
        """
        Parameters
        ----------
        func_delivery : function which outputs a tuple `(states, probabilities)`
                        where `states` is a list of :obj:`~netsquid.qubits.qstate.QState`
                        objects and `probabilities` is a list of float of the same
                        length.
            Outputs the list `[State0, State1, ...]` together with their probability of
            occurence.
        func_success_probability : function which outputs a float
        failure_state : numpy.ndarray
            the state to deliver in cas of failure
        """
        self._func_delivery = func_delivery
        self._func_success_probability = func_success_probability
        self._failure_state = failure_state

    def create_state_delivery_sampler(self, cycle_time, **parameters):
        """
        Parameters
        ----------
        paramaters : dictionary with str as keys and Any as values
            Parameters that will be inputted in the delivery function.

        Returns
        -------
        :obj:`~netsquid_magic.state_delivery_factory.StateDeliverySampler`
            The labels on the edges are 0, 1, 2, ... up to the number
            of states/probabilities as outputted by `func_delivery`
            (which is a parameter upon initialization).
        """
        qstates, probabilities_if_success = self._func_delivery(**parameters)

        if len(qstates) != len(probabilities_if_success):
            raise ValueError("Cannot create StateSampler from differently-sized lists")

        if not np.isclose(sum(probabilities_if_success), 1.):
            raise ValueError("Probabilities do not sum up to 1")

        number_of_success_leaves = len(qstates)

        success_probability = self._func_success_probability(**parameters)

        # Default to integer labels if no custom labels in **parameters
        labels = parameters.get('custom_labels', list(range(number_of_success_leaves)))
        assert (len(labels) == number_of_success_leaves), \
            "Incorrect number of custom labels for heralded success"

        successful_state_sampler = \
            StateSampler(qs_reprs=qstates,
                         probabilities=probabilities_if_success,
                         labels=labels)
        full_state_sampler = \
            StateSampler(qs_reprs=[successful_state_sampler, self._failure_state],
                         probabilities=[success_probability, 1. - success_probability],
                         labels=self.LABELS)
        return StateDeliverySampler(state_sampler=full_state_sampler,
                                    cycle_time=cycle_time)
