import netsquid as ns

from repchain.utils.tools import neighbour_index, get_possible_directions, is_outermost_node, DIRECTIONS, LEFT, RIGHT
from repchain.utils.bcdzfunctions import get_height, get_ngbh, get_level
from repchain.logic.distillation_scheduling import TimedTopDownScheduler
from repchain.logic.action import Action
from repchain.utils.memorymanager import ELECTRON_POSITION

DEFAULT_ACTION_ORDERING = [Action.SWAP, Action.DIST, "MSG", Action.ENTGEN]
DEFAULT_ACTION_ORDERING_OUTERMOST = [Action.DIST, "MSG"]


class IActionGenerator(ns.Entity):
    """
    Abstract base class for objects that determines the next :class:`repchain.logic.action.Action`
    that a node should perform.

    Main functionality:

    * the method `get_action` returns the next action

    * the attributes `has_stopped` and `should_stop_fn` can be used to
    determine whether the node has stopped, which implies that the action `None` should be returned.
    """

    EVT_FINISHED = ns.EventType("FIN", "No more actions will be returned")

    def __init__(self):
        self._should_stop_fn = None
        self.start()

    @property
    def should_stop_fn(self):
        """Should stop function.

        This property determines if the node has succeded in realising all its duties and can finally RIP.
        The function takes no input parameter and returns a boolean, True if it should stop and False Otherwise.
        """
        return self._should_stop_fn

    @should_stop_fn.setter
    def should_stop_fn(self, val):
        self._should_stop_fn = val

    @property
    def has_stopped(self):
        """Checks if the node has stopped.

        If the node is not stopped it checks with `should_stop_fn` whether it should be or not,
        in case it returns True an event is scheduled with type :obj:`IActionGenerator.EVT_FINISHED`.

        Returns
        -------
        bool
            wether the antion generator is stopped
        """
        if self._stopped:
            return True

        if self._should_stop_fn and self._should_stop_fn():
            self._schedule_now(IActionGenerator.EVT_FINISHED)
            self._stopped = True
            return True

        return False

    def get_action(self):
        """Abstract method to retrieve the next action.

        Returns
        -------
        :obj:`~repchain.logic.action.Action` or None
        """
        return None

    def start(self):
        """Starts the action generator."""
        self._stopped = False


class SingleNVActionGenerator(IActionGenerator):
    """
    Decides which of the actions to perform next:

    * entanglement generation ("ENTGEN")

    * distillation ("DIST")

    * swapping ("SWAP")

    Assumes that the node has a single NV center as quantum processor.

    Parameters
    ----------
    number_of_nodes : int
        the number of nodes in the chain
    index : int
        index of the node in the chain
    distillation_strategy : dict of int and (str, int) or (str, int) or None
        this controls how links are distilled per level, the dictionary keys run from 0 to `l` where `2^l + 1` is the
        number of nodes in the chain and the value at a particular level has information about the program
        used (see :class:`repchain.protocols.distillation_protocol.IDistillationProgram`) and
        the number of time to distill a link at that given level. If None, no distillation is used but the
        nesting bdcz strategy remain and if a single pair of (str, int) is specified then the same strategy is used
        at all the levels. Default to `('DEJMPS', 1)`.
    distillation_scheduler : :class:`repchain.logic.distillation_scheduling.DistillationScheduler` or 'default',
        optional distillation strategy to use, default value to `default` which uses
        :class:`~repchain.logic.distillation_scheduling.GreedyTopDownScheduler`.
    swap_fidelity_threshold: float, optional
        if two links reach an estimated fidelity of this threshold, they are eligible for swapping, default to 0.8
    action_ordering: list of str
        Determines the order in which swapping/distilling/entanglement
        generation/checking the mailbox of messages is performed.
        Example: if `action_ordering = ["SWAP", "MSG", "DIST", "ENTGEN"]`, then first,
        it is checked whether it is possible to perform a swap ("SWAP") operation, and if so,
        the corresponding action is returned by the `get_action` function. If not, then it is checked
        whether there are old messages ("MSG") which result in an action, and if so,
        then the corresponding action is returned. If not, then consider "DIST", followed by considering "ENTGEN".
    """

    def __init__(self, number_of_nodes, index,
                 memorymanager,
                 distillation_strategy=('DEJMPS', 1),
                 distillation_scheduler='default',
                 swap_fidelity_threshold=0.8,
                 action_ordering='default'):
        super().__init__()
        self._number_of_nodes = number_of_nodes
        self._index = index
        self._memorymanager = memorymanager
        self.distillation_scheduler = (
            TimedTopDownScheduler
            if distillation_scheduler == 'default' else distillation_scheduler
        )

        # levels = num2level(number_of_nodes)
        # if not distillation_strategy:
        #     # when no distillation is specified then the swap fidelity threshold is set to 0 to swap links
        #     # as soon as possible
        #     self.swap_fidelity_threshold = {level: 0. for level in range(levels + 1)}
        #     self.distillation_strategy = {level: None for level in range(levels + 1)}
        # else:
        #     if isinstance(distillation_strategy, tuple):
        #         distillation_strategy = {level: distillation_strategy for level in range(levels)}
        #     elif not isinstance(distillation_strategy, dict):
        #         raise ValueError('Invalid type passed to distillation_strategy, '
        #                          'valid types are tuple or dict of tuples.')
        #
        #     self.distillation_strategy = {}
        #     self.swap_fidelity_threshold = {}  # TODO: allow swap to be a dict
        #     for level in range(levels + 1):
        #         if level in distillation_strategy\
        #          and distillation_strategy[level] and distillation_strategy[level][1] > 0:
        #             self.distillation_strategy[level] = distillation_strategy[level]
        #             self.swap_fidelity_threshold[level] = swap_fidelity_threshold
        #         else:
        #             self.distillation_strategy[level] = None
        #             self.swap_fidelity_threshold[level] = 0.
        self.distillation_strategy = distillation_strategy
        self.swap_fidelity_threshold = swap_fidelity_threshold

        is_outermost = is_outermost_node(number_of_nodes=number_of_nodes, index=index)
        if action_ordering == 'default':
            self._action_ordering = DEFAULT_ACTION_ORDERING_OUTERMOST if is_outermost else DEFAULT_ACTION_ORDERING
        else:
            self._action_ordering = action_ordering

        directions = get_possible_directions(number_of_nodes=self._number_of_nodes, index=self._index)
        self._ngbhs = {
            direction: neighbour_index(number_of_nodes=self._number_of_nodes, index=self._index, direction=direction)
            for direction in directions
        }

        # height of its table
        _height = self._get_height(number_of_nodes=number_of_nodes, index=index)

        self._is_initiator = (_height == 0)

    def _get_height(self, number_of_nodes, index):

        height = get_height(number_of_nodes=number_of_nodes, index=index)

        # the other nodes this node should watch out for
        self._toplevelngbhs = {
            direction: get_ngbh(number_of_nodes=self._number_of_nodes,
                                index=self._index,
                                level=height,
                                direction=direction,
                                nonevalue=-1)
            for direction in DIRECTIONS
        }

        return height

    def get_action(self, first_msg_on_inbox):
        """Determine the next action to be performed. Should only be called if the node is idle.

        Parameters
        ----------
        first_msg_on_inbox : :class:`repchain.utils.message.Message` or None
            the first message on the queue to be processed.
            If None, then this is ignored. See more information on message format below.

        Returns
        -------
        action : None or :obj:`~repchain.logic.action.Action`
            the action to perform
        is_message_used : bool
            wether the input message `first_msg_on_inbox` has been consumed or not

        Notes
        -----
        A DIST-ASK message should look like:

            * "action" : :obj:`repchain.utils.message.Message.ACTION_DIST`

            * "sender" : the sender of the message

            * "intended receiver" : the intended receiver of the message

            * "subject" : :obj:`repchain.utils.message.Message.SUBJECT_ASK`

            * "additional content" : list of two link IDs of the links that should be distilled:
              first the one that will be measured and thus lost, and the second which is 'lost' only if distillation
              fails.

        An ENTGEN-ASK message should look like:

            * "action" : :obj:`repchain.utils.message.Message.ACTION_ENTGEN`

            * "sender" : the sender of the message

            * "intended receiver" : the intended receiver of the message

            * "subject" : :obj:`repchain.utils.message.Message.SUBJECT_ASK`
        """
        if self.has_stopped:
            return None, False

        for topic in self._action_ordering:
            action, is_message_used = self._get_next_action_from(topic, first_msg_on_inbox)
            if action is not None:
                return action, is_message_used

        return None, False

    def _get_next_action_from(self, topic, first_msg_on_inbox):
        """Return the next action for the given topic.

        Parameters
        ----------
        topic : str
            Among "SWAP", "DIST", "ENTGEN", "MSG",
            here "MSG" is only used internally for ask messages for `ENTGEN` or `DIST` received from another node

        Returns
        -------
        tuple (None or :class:`repchain.logic.action.Action`, bool)
            The action together with a boolean stating whether the input message was used.
        """
        action = None
        is_message_used = False

        if not self._memorymanager.has_free_positions(number=2):  # len(self._memorymanager.free_positions()) < 2:
            action = None
        elif topic == "MSG":
            # Check if there are messages on the agenda and if so, return the corresponding action
            if first_msg_on_inbox is not None:
                # We know that all messages on the message list are ASK-messages,
                # so we just perform the action that it states
                is_message_used = True
                action = self._get_next_action_from_ask_message(msg=first_msg_on_inbox)
        else:
            if topic == Action.SWAP:
                # check whether can swap at the highest level  and if possible,
                # return the electron as the first of the two memory positions at which
                # a swap is possible
                swap_positions = self._can_swap(preferred_control_position=ELECTRON_POSITION)

                if swap_positions is not False:
                    action = Action.swap(swap_positions[0], swap_positions[1])
            elif topic == Action.DIST:
                # check whether can distill at the highest level
                dist = self._can_distill()
                if dist is not False:
                    action = Action.distill(remote_node_id=dist[2],
                                            pos_lose=dist[0], pos_keep=dist[1], strategy=dist[3],
                                            is_initiator=True)
            elif topic == Action.ENTGEN:
                # check whether we should create entanglement
                node_id = self._can_entangle()
                if node_id is not False:
                    action = Action.generate_entanglement(node_id, is_initiator=True)
            else:
                raise ValueError(f'Topic {topic} does not match any of ["SWAP", "DIST", "ENTGEN", "MSG"]')

        return action, is_message_used

    def _get_next_action_from_ask_message(self, msg):
        remote_node_id = msg.sender
        if msg.has_action(Action.ENTGEN):
            # TODO this goes wrong if the node does not have any free memories left
            return Action.generate_entanglement(remote_node_id, is_initiator=False)
        elif msg.has_action(Action.DIST):
            # We identify the corresponding qubit positions of `self`
            # that the distillation request from a remote node is about;
            # the link IDs of the remote node can be found in the message
            link_id_lose, link_id_keep, strategy = msg.content
            pos_lose = self._memorymanager.find_pos(remote_node_id=remote_node_id, link_id=link_id_lose)
            pos_keep = self._memorymanager.find_pos(remote_node_id=remote_node_id, link_id=link_id_keep)
            return Action.distill(remote_node_id, pos_lose, pos_keep, strategy, is_initiator=False)

        raise Exception('No action found for message {}'.format(msg))

    def _can_swap(self, preferred_control_position=None):
        """Check if the given node can swap two links.

        Parameters
        ----------
        preferred_control_position : int or None
            Position that will be returned as the first element
            of the tuple, provided that a swap can be performed
            on this position. Will be ignored if None.

        Returns
        -------
        False or tuple (int, int)
            False in case swapping cannot be done. If swapping *can*
            be done, then returns the tuple, which consists of the two
            qubit memory positions on which the Bell state measurement
            has to be performed.
        """
        left = self._exists_swappable_link(direction=LEFT, preferred_position=preferred_control_position)
        if left is not False:
            right = self._exists_swappable_link(direction=RIGHT, preferred_position=preferred_control_position)
            if right is not False:
                ret = sorted([left, right])
                if preferred_control_position is not None and ret[1] is preferred_control_position:
                    return ret[1], ret[0]
                else:
                    return tuple(ret)
        return False

    def _can_distill(self):
        """Check if the given node can distill a link with any of the two connected nodes.

        Returns
        -------
        False or tuple (int, int, int, int)
            Returns false is distillation is not possible at the highest level.
            Otherwise return (qubit position, qubit position, remote node_id,
            level at which this distillation occurs).
        """
        left = self._can_distill_with(direction=LEFT)
        return self._can_distill_with(direction=RIGHT) if left is False else left

    def _can_entangle(self):
        """Check whether entanglement needs to and can be created.

        Returns
        -------
        False or int.
            Return false in case entanglement cannot or should not be
            created. Otherwise returns the ID of the remote node with
            whom to start entangling.
        """
        left = self._can_entangle_with(direction=LEFT)
        return self._can_entangle_with(direction=RIGHT) if left is False else left

    def _can_entangle_with(self, direction):
        """Return false if it is not an initiator or there are no swappable links in the given direction."""
        if self._is_initiator and direction in self._ngbhs:
            remote_node_id = self._ngbhs[direction]

            level = get_level(self._index, remote_node_id)

            # Check if there already exists entanglement of sufficient quality in this direction:
            has_entanglement_with = self._memorymanager.number_of_links_with_exceeds(
                remote_node_id,
                min_links=0,
                fidelity_range=(self.swap_fidelity_threshold[level], 1.)
            )
            if not has_entanglement_with:
                return remote_node_id
        return False

    def _exists_swappable_link(self, direction, fidelity_range="default",
                               remote_node_id="default",
                               preferred_position=None):
        """Check if a link on the highest level in the given direction has sufficiently large fidelity.

        Parameters
        ----------
        preferred_position : int or None
            Position that will be returned if a swappable link exists at this position.
            If None, then will be ignored.

        Returns
        -------
        False or int
        """
        if remote_node_id == "default":
            remote_node_id = self._toplevelngbhs[direction]
        if remote_node_id == -1:
            return False

        if fidelity_range == "default":
            level = get_level(self._index, remote_node_id)
            fidelity_range = [self.swap_fidelity_threshold[level], 1.]

        # has_entanglement_with = self._memorymanager.number_of_links_with_exceeds(
        #     remote_node_id,
        #     min_links=0,
        #     fidelity_range=fidelity_range
        # )
        positions = self._memorymanager.entangled_positions_with(remote_node_id, fidelity_range)
        if positions:
            if preferred_position is not None and preferred_position in positions:
                return preferred_position
            else:
                return positions[0]

        return False

    def _can_distill_with(self, direction):
        """Auxillary method for `can_distill()`.

        # TODO: why all nodes except first can distill???

        Parameters
        ----------
        direction: `LEFT` or `RIGHT`
            indicates the direction to check if the node can connect to

        Returns
        -------
        pos_lose : int
            position of the qubit entangled with the remote node that is to be lost
        pos_keep : int
            postion of the qubit entangled with the remote node that is to be kept in case distillation succeeds
        remote_node_id : int
            the index of the remote node
        is_initiator : bool
            wether the current node initiates the action.
            This is always true for all nodes in all levels except for the first one, in which is always false
            because in the last level, when the two outermost nodes share 2 links the right most node will be
            the one executing the connection.
        """
        remote_node_id = self._toplevelngbhs[direction]
        if remote_node_id == -1 or self._index == 0:
            return False

        level = get_level(self._index, remote_node_id)
        if not self.distillation_strategy[level]:
            return False

        links = self._memorymanager.entangled_links_with(remote_node_id,
                                                         fidelity_range=(0., self.swap_fidelity_threshold[level]))
        if any(link.estimated_fidelity >= self.swap_fidelity_threshold[level] for link in links):
            # there is already a link with sufficient fidelity, do not bother to distill another one
            return False

        links_to_distill = self.distillation_scheduler().choose_links_to_distill(links)

        if len(links_to_distill) > 0:
            distill_positions = [self._memorymanager.link2pos(link) for link in links_to_distill]
            # WARNING: we are using top-bottom greedy strategy which returns the best link first, the one to keep
            return distill_positions[1], distill_positions[0], remote_node_id, self.distillation_strategy[level]

        return False
