from repchain.logic.actiongenerator import SingleNVActionGenerator
from repchain.logic.action import Action
from repchain.utils.tools import is_outermost_node, LEFT


class SwapAsapActionGenerator(SingleNVActionGenerator):
    """Extension of `SingleNVActionGenerator` implementing the SWAP as soons a possible protocol.

    A node remains idle until it is triggered to check wether it should perform an action, this can
    happend under 3 circumstances:

    *  when the simulation starts,

    *  when a message is received,

    *  after an action has been completed

    A simulation is considered to have finished successfully when the two end nodes share an entangled state.

    For this protocol, the sequence of operations depends on the index of the node:

    * If the node is even, it sends a message to generate entanglement to its node on the left and
      starts the operation as soons as it has received the confirmation message.
      Once entalgement has been achieved, it repeats the process for its right neighbourg.

    * If the node is odd, it waits until a generate entalgement message is received, it releases the
      necessary qubits, responds back to the initiator node, sleeps for the duration of the message and
      finally generates entanglement.

    * If the node is the first one, it behaves like an odd node but without swap.

    * If the node is the last one, it behaves as an odd node but without swap if odd or like an even node
      but just generates enganglement with the left node.

    Here it comes the particularity of this protocol, once the node of interest has generated a link
    with both its neighbourgs, it runs an entaglement swap protocol to entangle the neighbourgs
    and release the current node. Once this happend, the node becomes idle.

    Notes
    -----
    Uses the default distillation strategy, a swap threshold of 0 and an action ordering of: 'SWAP', 'MSG' and 'ENTGEN'.
    """

    def __init__(self, number_of_nodes, index, memorymanager):
        super().__init__(number_of_nodes=number_of_nodes,
                         index=index,
                         memorymanager=memorymanager,
                         distillation_strategy=None,
                         swap_fidelity_threshold=[0.0] * number_of_nodes,
                         action_ordering=[Action.SWAP, 'MSG', Action.ENTGEN]
                         )
        self._nodes_on_the_left = list(range(0, index))[::-1]
        self._nodes_on_the_right = list(range(index + 1, number_of_nodes))

    def get_action(self, first_msg_on_inbox):
        """Determine the next action of does nothing if the link has successfully swapped."""
        if self._has_swapped:
            return None, False

        action, was_message_used = super().get_action(first_msg_on_inbox=first_msg_on_inbox)
        if action and action.topic == Action.SWAP:
            self._has_swapped = True

        return action, was_message_used

    def _get_height(self, number_of_nodes, index):
        # return 1 if is_outermost_node(number_of_nodes, index) else 0
        return (index + 1) % 2

    def _exists_swappable_link(self, direction, fidelity_range="default", preferred_position=None):
        nodes = self._nodes_on_the_left if direction == LEFT else self._nodes_on_the_right
        for remote_node_id in nodes:
            exists = super()._exists_swappable_link(
                direction=direction,
                fidelity_range=(0., 1.),
                remote_node_id=remote_node_id,
                preferred_position=preferred_position)
            if exists is not False:
                return exists
        return False

    def start(self):
        super().start()
        self._has_swapped = False
