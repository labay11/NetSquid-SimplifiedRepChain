from repchain.logic.swap_asap_action_generator import SwapAsapActionGenerator
from repchain.logic.task import Action
from repchain.utils.memorymanager import ELECTRON_POSITION


class OddInitiateSwapAsapActionGenerator(SwapAsapActionGenerator):

    def __init__(self, number_of_nodes, index, memorymanager):
        super().__init__(number_of_nodes=number_of_nodes,
                         index=index,
                         memorymanager=memorymanager)
        self._is_rightmost_node = (number_of_nodes - 1) == self._index
        self._odd_index = (self._index % 2) == 1
        self._even_number_of_nodes = (self._number_of_nodes % 2) == 0

    def _get_next_action_from(self, topic, first_msg_on_inbox):
        if topic != Action.ENTGEN or (not self._is_rightmost_node and self._odd_index):
            return super()._get_next_action_from(topic=topic, first_msg_on_inbox=first_msg_on_inbox)
        elif self._is_rightmost_node and self._even_number_of_nodes and self._memorymanager.is_free(ELECTRON_POSITION):
            return Action.generate_entanglement(self._index - 1, is_initiator=True), False
        else:
            return None, False
