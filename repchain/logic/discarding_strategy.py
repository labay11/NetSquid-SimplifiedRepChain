"""This module contains possible link distarding strategies to apply with other protocols."""
from abc import ABCMeta
import netsquid as ns
from repchain.utils.bcdzfunctions import get_level


class DiscardingStrategy(metaclass=ABCMeta):
    """Abstact class that implements the `should_discard` method telling if a link has to be discarded ot not."""

    def should_discard(self, link):
        """Check if the given links has to be discarded.

        Parameters
        ----------
        link : :class:`repchain.utils.memorymanager.Link`

        Returns
        -------
        bool
            if true, the link should be discarded
        """
        pass


class FixedCutoffTimeDiscardingStrategy(DiscardingStrategy):
    """Discading strategy by cut-off time.

    If the time between generation and current time is bigger than the cut-off time then the link is discarded.

    Attributes
    ----------
    cutoff_time : int
        max time before link is discarded in ns.
    """

    def __init__(self, cutoff_time):
        self.cutoff_time = cutoff_time

    def should_discard(self, link):
        """returns true if the differnce between current time and creation time of the link exceed the cut-off time"""
        return self._current_time() - link.creation_time > self.cutoff_time

    def _current_time(self):
        return ns.sim_time()


class LevelDependentFidelityBasedDiscardingStrategy(DiscardingStrategy):
    """Discarding fidelity based on a fidelity threshold by level.

    This strategy is implemented on the `BDCZ` protocol and discards a link if the fidelity
    of a link in the j-th nesting level exceed the threshold for that level.

    Attributes
    ----------
    node_index : int
        index of the current node
    level_fidelity_thresholds : list of float
        If a link has fidelity smaller than the fidelity threshold for the level it lives in, it should be discarded.
        The first element (index 0) of the list holds the level fidelity for neighbouring nodes (i.e. level 1).
    """

    def __init__(self, node_index, level_fidelity_thresholds):
        self.node_index = node_index
        self.level_fidelity_thresholds = level_fidelity_thresholds

    def should_discard(self, link):
        level = get_level(index=self.node_index, remote_index=link.remote_node_id)
        # if nodes are neighbours, then their level is 1, so:
        return link.estimated_fidelity < self.level_fidelity_thresholds[level]
