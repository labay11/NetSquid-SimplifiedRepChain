class Task:
    """Represrents a single task to be done by the nodes.

    Generally, an Action is divided into multiple tasks, the list of possible tasks is:

      * Task.SWAP : swap entanglement between two qubits
      * Task.DIST : distill a link
      * Task.ENTGEN : generate entanglement between two nodes
      * Task.SLEEP : pause a node for a certain amount of time
      * Task.SEND_MSG : send a message to a different node
      * Task.MOVE : move a state between qubits

    The parameter `halt_until_confirm` indicates that the task, or any following
    task on the todo list, should not be executed until a CONFIRM-message is received
    or until the expiry time (as indicated in `halt_expires_after`) has passed, whichever
    comes first.

    In case `halt_until_confirm` is True, the parameter `halt_expires_after` indicates the time
    after which the "halting until a confirm message comes in" should be overruled by an expiry time:
    after that time, the task is thrown away and not performed. Its default value is None, which
    indicates an infinite expiry time, i.e. the "halting" never expires.

    Attributes
    ----------
    topic : str
        the topic of the action, must be one of repchain.logic.task.Task.TOPICS
    parameters : dict if (str, Any)
        the parameters needed to perform the action, see the Notes section for more information.
    halt_until_confirm : bool, optional
        indicates that the task, or any following
        task on the todo list, should not be executed until a CONFIRM-message is received
        or until the expiry time (as indicated in `halt_expires_after`) has passed, whichever
        comes first, default to False.
    halt_expires_after : int or None, optional
        indicates the time after which the "halting until a confirm message comes in" should be
        overruled by an expiry time: after that time, the task is thrown away and not performed.
        Its default value is None, which indicates an infinite expiry time, i.e. the "halting" never expires.
        If not `None` then `halt_until_confirm` must be set to `True`.

    See Also
    --------
    repchain.logic.action.Action

    Notes
    -----
    It is strongly recommended to initialise this class via one of the available class methods:

     * :class:`repchain.logic.task.Task.swap`
     * :class:`repchain.logic.task.Task.distill`
     * :class:`repchain.logic.task.Task.generate_entanglement`
     * :class:`repchain.logic.task.Task.sleep`
     * :class:`repchain.logic.task.Task.message`
     * :class:`repchain.logic.task.Task.move`

    See the documentation of any of the previous methods for more details.
    """

    SWAP = 'SWAP'
    DIST = 'DIST'
    ENTGEN = 'ENTGEN'
    SLEEP = 'SLEEP'
    SEND_MSG = 'SENDMSG'
    MOVE = 'MOVE'

    TOPICS = [SWAP, DIST, ENTGEN, SLEEP, SEND_MSG, MOVE]

    def __init__(self, topic, parameters, halt_until_confirm=False, halt_expires_after=None):
        self.topic = Task.check_topic(topic)
        self.parameters = parameters
        self.halt_until_confirm = halt_until_confirm
        self.halt_expires_after = halt_expires_after
        self.ID = id(self)

        # some sanity checks
        # if topic == Task.SEND_MSG and halt_until_confirm:
        #     assert parameters["subject"] == 'ASK'
        # if "request_expires_after" in self.parameters:
        #     if parameters["content_type"] == "ASK":
        #         assert(parameters["request_expires_after"] is not None)
        #     elif parameters["content_type"] == "CONFIRM":
        #         assert(parameters["request_expires_after"] is None)
        if halt_expires_after is not None:
            assert halt_until_confirm

    def similarTo(self, other):
        return self.topic == other.topic and\
            self.parameters == other.parameters and\
            self.halt_until_confirm == other.halt_until_confirm

    def __getitem__(self, key):
        return self.parameters[key]

    def __eq__(self, other):
        return self.ID == other.ID

    def __neq__(self, other):
        return not self == other

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "{}: {}, (halt={})".format(self.topic, self.parameters, self.halt_until_confirm)

    @staticmethod
    def check_topic(topic):
        """Return the topic if it belongs to the list of valid topics or raises."""
        if topic not in Task.TOPICS:
            raise ValueError('Topic {} is not one of: {}'.format(topic, ', '.join(Task.TOPICS)))

        return topic

    @classmethod
    def swap(cls, posA, posB):
        """Initialise an entanglement swap acion with the two qubit positions to be swapped.

        Parameters
        ----------
        posA : int
            the first position, key: *qmem_posA*
        posB : int
            the second position, key: *qmem_posB*

        See Also
        --------
        repchain.logic.action.Action.swap
        """
        return cls(Task.SWAP, {
            'qmem_posA': posA,
            'qmem_posB': posB
        })

    @classmethod
    def distill(cls, remote_node_id, pos_lose, pos_keep, strategy, halt_until_confirm=False, halt_expires_after=None):
        """Initialise a distillation acion with the remote node id.

        Parameters
        ----------
        remote_node_id : int
            the id of the remote node to distill with, key: *remote_node_id*
        pos_lose : int
            the position of the qubit to measure, key: *pos_lose*
        pos_keep : int
            the position of the qubit to keep if the result is successfull, key: *pos_keep*
        strategy : tuple of str and int
            the distillation strategy to use, where str names the program to be used and ìnt the number of times to
            distill a link.
        """
        return cls(Task.DIST, {
            'remote_node_id': remote_node_id,
            'pos_lose': pos_lose,
            'pos_keep': pos_keep,
            'strategy': strategy
        }, halt_until_confirm, halt_expires_after)

    @classmethod
    def generate_entanglement(cls, remote_node_id, free_pos, halt_until_confirm=False, halt_expires_after=None):
        """Initialise an entanglement generator acion with the given remote node id.

        Parameters
        ----------
        remote_node_id : int
            the id of the remote node to entangle with, key: *remote_node_id*
        free_pos : int
            qubit position of the remote qubit used for entanglement, key: *free_pos*
        """
        return cls(Task.ENTGEN, {
            'remote_node_id': remote_node_id,
            'free_pos': free_pos
        }, halt_until_confirm, halt_expires_after)

    @classmethod
    def move(cls, old_position, new_position):
        """Initialise a qubit to a new position.

        Parameters
        ----------
        old_position : int
            the old position, key: *old_pos*
        new_position : int
            the new position, key: *new_pos*
        """
        return cls(Task.MOVE, {'old_pos': old_position, 'new_pos': new_position})

    @classmethod
    def message(cls, action, subject, receiver_id, content=None, expires_after=None,
                halt_until_confirm=False, halt_expires_after=None):
        """Initialise a task that sends a classical message to the receiver with the given action and subject.

        Parameters
        ----------
        action : str
            Indicates the topic of the message: entanglement generation/distillation/swapping.
            Should be among one of the following: Action.SWAP, Action.DIST or Action.ENTGEN.
            Key: *action*
        subject : str
            Indicates the subject of the message. The list of available subjects can be found at
            repchain.utils.message.Message.SUBJECTS but note that not all of them are available for all the actions,
            to see the list of subjects per actions take a look at
            repchain.utils.message.Message.ALLOWED_SUBJECTS_BY_ACTION.
            Key: *subject*
        receiver_id : int
            the id of the receiver node
            Key: *receiver_id*
        content : Any, optional
            Can be used to store any addional data, default to None.
            Only for DIST messages, in which case the value is a list of two link IDs.
            Key: *content*
        expires_after : int or None, optional
            amount of time before the message expires in nanoseconds, default to None (never expires).
            Should only not equal None for "ASK"-messages.
            Key: *expires_after*

        See Also
        --------
        repchain.utils.message.Message
        """
        return cls(Task.SEND_MSG, {
            'action': action,
            'subject': subject,
            'content': content,
            'receiver_id': receiver_id,
            'expires_after': expires_after
        }, halt_until_confirm, halt_expires_after)

    @classmethod
    def sleep(cls, duration):
        """Initialise a task that pauses a node for a certain amount of time.

        Parameters
        ----------
        duration : int
            amount of time to sleep in nanoseconds, key: *duration*
        """
        return cls(Task.SLEEP, {'duration': duration})
