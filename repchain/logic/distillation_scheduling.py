from abc import ABCMeta, abstractmethod


class IDistillationScheduler(metaclass=ABCMeta):
    """Class that represents a Scheduler to decide which two or more links to use for distillation.

    Usage: initiate and use the method `choose_links_to_distill`.
    """

    @abstractmethod
    def choose_links_to_distill(self, links):
        """Chooses which links to distill.

        The passed links are filtered according to the fidelity range, if the fidelity of a link
        is lower than `lower_threshold` or higher than `upper_threshold` it is discarded.

        Parameters
        ----------
        links : list of :class:`repchain.utils.memorymanager.Link`
            List of links from which two are chosen for distillation.

        Returns
        -------
        list of links
            The two links that should be used for distillation. The number
            of items in this list is two for e.g. the DEJMPS protocol, but might be more.

        Notes
        -----
            It is assumed that all links are links between the same nodes (this is not checked!)
        """
        pass


class DistillationThresholdScheduler(IDistillationScheduler):
    """Chooses links according to a fidelity threshold.

    Usage: initiate and use the method `choose_links_to_distill`.

    Attributes
    ----------
    lower_threshold : float, optional
        The fidelity threshold below which links are considered
        of such bad quality, that they are not worth any further
        processing. These links will not be selected for distillation.
        Default to 0.5.
    upper_threshold : float, optional
        The fidelity threshold above which links no longer need distillation.
        Default to 0.8.
    """

    def __init__(self, lower_threshold=0., upper_threshold=1.):
        self.lower_threshold = lower_threshold
        self.upper_threshold = upper_threshold

    def choose_links_to_distill(self, links):
        """Chooses which links to distill.

        The passed links are filtered according to the fidelity range, if the fidelity of a link
        is lower than `lower_threshold` or higher than `upper_threshold` it is discarded.

        Parameters
        ----------
        links : list of :class:`repchain.utils.memorymanager.Link`
            List of links from which two are chosen for distillation.

        Returns
        -------
        list of links
            The two links that should be used for distillation. The number
            of items in this list is two for e.g. the DEJMPS protocol, but might be more.

        Notes
        -----
            It is assumed that all links are links between the same nodes (this is not checked!)
        """
        return [
            link
            for link in links
            if self.lower_threshold <= link.estimated_fidelity < self.upper_threshold
        ]


class GreedyDistillationScheduler(DistillationThresholdScheduler, metaclass=ABCMeta):
    """Links are choosen accoding to a majorization function."""

    @abstractmethod
    def _majorizes(self, link):
        pass

    def choose_links_to_distill(self, links):
        interesting_links = super().choose_links_to_distill(links)
        if len(interesting_links) < 2:
            return []

        return sorted(interesting_links, key=self._majorizes)[:2]


class GreedyTopDownScheduler(GreedyDistillationScheduler):
    """The links are selected in order, where the first two links with higher fidelity are choosen."""

    def _majorizes(self, link):
        return -link.estimated_fidelity


class GreedyBottomUpScheduler(GreedyDistillationScheduler):
    """The links are selected in reverse order, where the last two links with higher fidelity are choosen."""

    def _majorizes(self, link):
        return link.estimated_fidelity


class BandedScheduler(IDistillationScheduler):
    """Chooses two links that both fall within one of the bands.

    Attributes
    ----------
    band_boundaries : list of float
        the boundaries of the bands, all within the interval [0, 1].

    Examples
    --------
    A list of band boundaries like [0.1, 0.3, 0.8] will create 4 bands [(0, 0.1), (0.1, 0.3), (0.3, 0.8), (0.8, 1)]
    where the first and last band are automatically added.

    Nodes
    -----
    The bands are checked in order with both ends included so if the fidelity of a link fall right in one
    of the boundaries then the band with lowest fidelity is choosen.
    """

    def __init__(self, band_boundaries):
        assert len(band_boundaries) > 0

        boundaries = [0.] + band_boundaries + [1.]
        self._bands = [(boundaries[index], boundaries[index + 1]) for index in range(len(boundaries) - 1)]

    def _within_band(self, fidelityA, fidelityB):
        for lower, upper in self._bands:
            if lower <= fidelityA <= upper and lower <= fidelityB <= upper:
                return True
        return False

    def choose_links_to_distill(self, links):
        """Chooses two links that both fall within one of the bands.

        Parameters
        ----------
        links : list of :class:`repchain.utils.memorymanager.Link`

        Returns
        -------
        list of :class:`repchain.utils.memorymanager.Link`
            if there are two links in the same band then a list is returned with those links,
            otherwise an empty list is returned
        """
        if len(links) < 2:
            return []

        for indexA, linkA in enumerate(links):
            for indexB, linkB in enumerate(links):
                if indexA > indexB and\
                   self._within_band(fidelityA=linkA.estimated_fidelity,
                                     fidelityB=linkB.estimated_fidelity):
                    return [linkA, linkB]
        return []


class TimeScheduler(GreedyDistillationScheduler):
    """Links are ordered by their creation time.

    Since qubits dephase with time, it is expected that younger links will have a higher fidelity than old ones.
    """

    def _majorizes(self, link):
        return -link.creation_time  # links with larger creation time (younger) will go first


class TimedTopDownScheduler(GreedyDistillationScheduler):
    """Links are ordered by their fidelity first and then by their creation time.

    This is usefull when we don't know the fidelity of the link but we arbitrarily estimated it. Thus, even though
    links are estimated to have the same fidelity we know that is not true as they dephase with time. Therefore,
    the ordering will now put newest links before oldest ones. But it still keeps the fidelity ordering first to
    be able to select first those links that already went through a distillation step. Assuming that distillation
    increases the fidelity, those links should have a higher fidelity in reality than new links.
    """

    def _majorizes(self, link):
        return -link.estimated_fidelity, -link.creation_time
