from .task import Task
from repchain.utils.message import Message


class Action:
    """
    Interface representing an action to be done by one of the repeteras at a time.
    The list of possible topics is:

      * Action.SWAP
      * Action.DIST
      * Action.ENTGEN

    Attributes
    ----------
    topic : str
        the topic of the action, must be on of the previous.
    parameters : dict of (str, Any)
        the parameters needed to perform the action, see the Notes section for more information.

    Notes
    -----
    It is strongly recommended to initialise this class via one of the available class methods:

     * :class:`repchain.logic.action.Action.swap`
     * :class:`repchain.logic.action.Action.distill`
     * :class:`repchain.logic.action.Action.generate_entanglement`

    See the documentation of any of the previous methods for more details.
    """

    SWAP = 'SWAP'
    DIST = 'DIST'
    ENTGEN = 'ENTGEN'

    def __init__(self, topic, parameters):
        self.topic = topic
        self.parameters = parameters

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "{}: {}".format(self.topic, self.parameters)

    def __getitem__(self, key):
        return self.parameters[key]

    @classmethod
    def swap(cls, posA, posB):
        """
        Initialises an entanglement swap acion with the two qubit positions to be swapped.

        Parameters
        ----------
        posA : int
            the first position, key: *qmem_posA*
        posB : int
            the second position, key: *qmem_posB*
        """
        return cls(Action.SWAP, {
            'qmem_posA': posA,
            'qmem_posB': posB
        })

    @classmethod
    def distill(cls, remote_node_id, pos_lose, pos_keep, strategy, is_initiator):
        """
        Initialises a distillation acion with the remote node id.

        Parameters
        ----------
        remote_node_id : int
            the id of the remote node to distill with, key: *remote_node_id*
        pos_lose : int
            the position of the qubit to measure, key: *pos_lose*
        pos_keep : int
            the position of the qubit to keep if the result is successfull, key: *pos_keep*
        strategy : tuple of str and int
            the distillation strategy to use, where str names the program to be used and ìnt the number of times to
            distill a link.
        is_initiator : bool
            wether the `remote_node_id` is an initiatior of the action, key: *is_initiator*
        """
        return cls(Action.DIST, {
            'remote_node_id': remote_node_id,
            'pos_lose': pos_lose,
            'pos_keep': pos_keep,
            'strategy': strategy,
            'is_initiator': is_initiator
        })

    @classmethod
    def generate_entanglement(cls, remote_node_id, is_initiator):
        """
        Initialises an entanglement generator acion with the given remote node id.

        Parameters
        ----------
        remote_node_id : int
            the id of the remote node to entangle with, key: *remote_node_id*
        is_initiator : bool
            wether the `remote_node_id` is an initiatior of the action, key: *is_initiator*
        """
        return cls(Action.ENTGEN, {
            'remote_node_id': remote_node_id,
            'is_initiator': is_initiator
        })

    def to_tasks(self, memorymanager, sleep_delay=None, respect_NV_structure=False):
        """Divides the action into individual tasks.

        Parameters
        ----------
        memorymanager : :class:`repchain.utils.memorymanager.MemoryManager`
            the memory containing all entangled links of a node
        sleep_delay : float, optional
            how much time to wait before starting to generate entanglement, default to None.
            Only used in `ENTGEN` actions.
        respect_NV_structure : bool, optional
            wether to respect the memory positions as if it was an NV center, default to False.

        Returns
        -------
        list of :class:`repchain.logic.task.Task`

        See Also
        --------
        :class:`repchain.logic.action.divide_swap_action_into_tasks`
        :class:`repchain.logic.action.divide_entgen_action_into_tasks`
        :class:`repchain.logic.action.divide_dist_action_into_tasks`
        """
        if self.topic == Action.ENTGEN:
            return divide_entgen_action_into_tasks(
                remote_node_id=self['remote_node_id'],
                is_initiator=self['is_initiator'],
                memorymanager=memorymanager,
                sleep_delay=sleep_delay,
                respect_NV_structure=respect_NV_structure
            )
        elif self.topic == Action.DIST:
            return divide_dist_action_into_tasks(
                remote_node_id=self["remote_node_id"],
                is_initiator=self["is_initiator"],
                position_keep=self["pos_keep"],
                position_lose=self["pos_lose"],
                strategy=self['strategy'],
                memorymanager=memorymanager,
                sleep_delay=sleep_delay,
                respect_NV_structure=respect_NV_structure
            )
        elif self.topic == Action.SWAP:
            return divide_swap_action_into_tasks(
                qmem_posA=self["qmem_posA"],
                qmem_posB=self["qmem_posB"],
                memorymanager=memorymanager,
                sleep_delay=sleep_delay,
                respect_NV_structure=respect_NV_structure
            )

        raise Exception('No action found for the topic {}'.format(self.topic))


def divide_entgen_action_into_tasks(remote_node_id, is_initiator,
                                    memorymanager, sleep_delay, respect_NV_structure=False):
    """A `ENTGEN` action is divided into a list of task for a node.

    The sequence of tasks is:

    1. Looks for a position ready to generate entaglement and a free position in the memory and moves the
    state (if any) to the free position.

    2. Sends a message to the `remote_node_id` asking for confirmation if this node is an initiator
    or to confirm a generate entanglement request by the `remote_node_id`.

    3. If this node is not an initiator wait for `sleep_delay` until the initiator node sends the qubit.

    4. Finally, creates an entanglement generation task with the remote node.

    Parameters
    ----------
    remote_node_id : int
        the other node
    is_initiator : bool
        wether the node initiates the entaglement
    memorymanager : :class:`repchain.utils.memorymanager.MemoryManager`
        the memory containing all entangled links of a node
    sleep_delay : float or None
        how much time to wait before starting to generate entanglement,
        ignore if this is an initiator
    respect_NV_structure : bool, optional
        wether to respect the memory positions as if it was an NV center, default to False.

    Returns
    -------
    list of :class:`repchain.logic.task.Task`

    See Also
    --------
    :class:`repchain.logic.task.Task.generate_entanglement`

    Notes
    -----
    If `free_position` equals `entgen_position`, then no MOVE task is added; otherwise, a MOVE task
    is added that makes the existing qubit on `entgen_position` move to `free_position`.
    """
    entgen_position, free_position = _determine_positions_involved_in_entgen(
        memorymanager=memorymanager,
        respect_NV_structure=respect_NV_structure
    )
    # is_initiator = (sleep_delay is None)
    expiry_time = None  # TODO add expiry time if this is set
    # 1. possibly add a MOVE task
    tasks = _get_move_tasks(old_positions=[entgen_position], new_positions=[free_position])
    # 2. send a message
    tasks.append(Task.message(
        action=Action.ENTGEN,
        subject=Message.SUBJECT_ASK if is_initiator else Message.SUBJECT_CONFIRM,
        receiver_id=remote_node_id,
        expires_after=expiry_time
    ))

    # 3. if the node is a responder: wait for a bit so that photon emission is synchronized
    if not is_initiator:
        tasks.append(Task.sleep(sleep_delay))

    # 4. start entangling
    tasks.append(Task.generate_entanglement(remote_node_id,
                                            entgen_position,
                                            halt_until_confirm=is_initiator,
                                            halt_expires_after=expiry_time))
    return tasks


def divide_dist_action_into_tasks(remote_node_id, is_initiator, position_lose, position_keep, strategy,
                                  memorymanager, sleep_delay=None, respect_NV_structure=False):
    """A `DIST` action is divided into a list of task for a node.

    The sequence of tasks is:
      1. Looks for a communication position (fixed to be the electron position in NV centers) and
      a free position in the memory, the free position will be used to store the state previously
      in the communication position if any and then the state in `position_lose` will be moved to the
      communication position.

      2. Sends a message to the `remote_node_id` asking for confirmation if this node is an initiator
      or to confirm a generate entanglement request by the `remote_node_id`. The content of this message
      contains a tuple of (`link_id_lose`, `link_id_keep`) which correspond to the links associated to the
      positions `position_lose` and `position_keep` respectively.

      3. Finally, creates an distillation task with the remote node, if this node is an initiator then it waits
      for confirmation before executing the task.

    Parameters
    ----------
    remote_node_id : int
        the other node to distill with
    is_initiator : bool
        wether the node initiates the distillation
    position_keep : int
        the qubit to keep after distillation
    position_lose : int
        the qubit to measure and therefore lose after distillation
    strategy : tuple of str and int
        the distillation strategy to use, where str names the program to be used and ìnt the number of times to
        distill a link.
    memorymanager : :class:`repchain.utils.memorymanager.MemoryManager`
        the memory containing all entangled links of a node
    sleep_delay : float or None
        ignored
    respect_NV_structure : bool, optional
        wether to respect the memory positions as if it was an NV center, default to False.

    Returns
    -------
    list of :class:`repchain.logic.task.Task`

    See Also
    --------
    :class:`repchain.logic.task.Task.distill`

    Notes
    -----
    Maps `communication_position` onto `free_position` followed by mapping `position_lose` onto `communication_position`
    (unless the corresponding memory positions are already equal).
    """
    expiry_time = None  # TODO add expiry time if this is set
    # possibly: move
    communication_position, free_position = _determine_positions_involved_in_moving(
        pos=position_lose,
        memorymanager=memorymanager,
        respect_NV_structure=respect_NV_structure
    )
    move_tasks = _get_move_tasks(old_positions=[communication_position, position_lose],
                                 new_positions=[free_position, communication_position])

    link_id_keep = memorymanager.get_link(position_keep).link_id
    link_id_lose = memorymanager.get_link(position_lose).link_id
    # send a message
    sendmsgtask = Task.message(
        Action.DIST,
        Message.SUBJECT_ASK if is_initiator else Message.SUBJECT_CONFIRM,
        remote_node_id,
        content=(link_id_lose, link_id_keep, strategy),
        expires_after=expiry_time
    )

    # 1. & 2. if initiator, then first MOVE, then send a message (vice versa if responder)
    # tasks = move_tasks + [sendmsgtask] if is_initiator else [sendmsgtask] + move_tasks
    tasks = move_tasks + [sendmsgtask]
    # (no need to wait)
    # track where the position_keep went (it might have been at communication_position)
    for move_task in move_tasks:
        if move_task.parameters["old_pos"] == position_keep:
            position_keep = move_task.parameters["new_pos"]
    # 3. start distillation
    distill_task = Task.distill(remote_node_id,
                                pos_lose=communication_position,
                                pos_keep=position_keep,
                                strategy=strategy,
                                halt_until_confirm=is_initiator,
                                halt_expires_after=expiry_time)
    # NOTE: on purpose, we don't add `level` as parameter, because I think it is not needed
    return tasks + [distill_task]


def divide_swap_action_into_tasks(qmem_posA, qmem_posB,
                                  memorymanager, sleep_delay=None, respect_NV_structure=False):
    """A `SWAP` action is divided into a list of task for a node.

    The sequence of tasks is:

      1. The first memory position is mapped onto a position that can be used for communication (in the case of
      NV centers this is the electron position), any state in that position is moved to a free position and
      the state in `qmem_posA` is moved to the communication position.

      2. Checks for the new position of `qmem_posB` after the previous move tasks and creats a swap task from
      `communication_position` to `qmem_posB`.

    Parameters
    ----------
    qmem_posA : int
        the first qubit to use in entaglement swapping
    qmem_posB : int
        the second qubit
    memorymanager : :class:`repchain.utils.memorymanager.MemoryManager`
        the memory containing all entangled links of a node
    sleep_delay : float or None
        ignored
    respect_NV_structure : bool, optional
        wether to respect the memory positions as if it was an NV center, default to False.

    Returns
    -------
    list of :class:`repchain.logic.task.Task`
        move tasks that happen internally in the node and entaglement swapping task between
        the communication position of this node and `qmem_posB`.

    See Also
    --------
    :class:`repchain.logic.task.Task.swap`

    Notes
    -----
    Maps `communication_position` onto `free_position` followed by mapping `qmem_posA` onto `communication_position`
    (unless the corresponding memory positions are already equal).
    """
    communication_position, free_position = _determine_positions_involved_in_moving(
        pos=qmem_posA,
        memorymanager=memorymanager,
        respect_NV_structure=respect_NV_structure
    )
    # possibly: move
    move_tasks = _get_move_tasks(old_positions=[communication_position, qmem_posA],
                                 new_positions=[free_position, communication_position])
    # track where the other position went (it might have been at communication_position)
    for move_task in move_tasks:
        if move_task.parameters["old_pos"] == qmem_posB:
            qmem_posB = move_task.parameters["new_pos"]
    # start swapping
    swap_task = Task.swap(communication_position, qmem_posB)
    return move_tasks + [swap_task]


def _determine_positions_involved_in_entgen(memorymanager, respect_NV_structure):
    """Determines a possition that can be used for entanglement and a free memory position.

    There are two cases:

    * `respect_NV_structure = False`: both positions are the same and correspond to the first free
    memory position in the memory manager.

    * `respect_NV_structure = True`: the communication position is determined bu the communication position
    of the memory manager and the free position is just the first empty position excluding that one if
    the electron is in use or the electron itself if it is free.

    Parameters
    ----------
    memorymanager : :class:`repchain.utils.memorymanager.MemoryManager`
        the memory containing all entangled links
    respect_NV_structure : bool
        wether to respect the NV structure or not, if true then the communication position
        (the `ELECTRON_POSITION`) is excluded from the list of possible move positions

    Returns
    -------
    tuple of (int, int)
        Tuple of memory position that will be used for entanglement generation,
        and a free memory position, not necessarily different: these two are
        the same precisely if `respect_NV_structure` is set to False, otherwise
        the free memory position is the position to which the qubit that is
        currently on the entanglement-generation position should be mapped onto.
    """
    entgen_position = memorymanager.communication_position
    if respect_NV_structure and not memorymanager.is_free(entgen_position):
        # the electron is in use so use another
        free_position = memorymanager.first_free_positions(num=1, excluded=[entgen_position])
    else:
        entgen_position = free_position = memorymanager.first_free_positions(num=1, excluded=[])

    return entgen_position, free_position


def _determine_positions_involved_in_moving(pos, memorymanager, respect_NV_structure):
    """Determines the qubit to use for communitation and a qubit free qubit where to move the state in `pos` if any.

    There are two possibilities:

    * `respect_NV_structure = False`: in such case all memory positions are available for moving,
    both the communication position and the free position are equal to `pos`.

    * `respect_NV_structure = True`: the communication position is determined bu the communication position
    of the memory manager and the free position is just the first empty position excluding that one if the
    electron is in use or the electron itself if it is free.

    Parameters
    ----------
    pos : int
        the qubit position involved in moving
    memorymanager : :class:`repchain.utils.memorymanager.MemoryManager`
        the memory containing all entangled links
    respect_NV_structure : bool
        wether to respect the NV structure or not, if true then the communication position
        (the `ELECTRON_POSITION`) is excluded from the list of possible move positions

    Returns
    -------
    tuple of (int, int)
        Communication position, free memory position
        Free memory position is a real free memory position unless ....
    """
    comm_position = memorymanager.communication_position
    if respect_NV_structure:
        if pos != comm_position and not memorymanager.is_free(comm_position):
            # and the electron is already is use
            free_position = memorymanager.first_free_positions(num=1, excluded=[comm_position])
        else:
            free_position = comm_position
    else:
        comm_position = free_position = pos

    return comm_position, free_position


def _get_move_tasks(old_positions, new_positions):
    """
    Example:
    >>> old_positions = [1, 2, 3]
    >>> new_positions = [4, 5, 6]
    >>> #Now 1 will be mapped to 4, followed by 2 onto 5, followed by 3 onto 6

    Note
    ----
    If two positions are the same, then no MOVE task is returned
    Example:
    >>> old_positions = [1, 2]
    >>> new_positions = [1, 3]
    >>> # will only return a MOVE task of 2 onto 3
    """
    assert len(old_positions) == len(new_positions)

    return [
        Task.move(old_position, new_positions[index])
        for index, old_position in enumerate(old_positions)
        if old_position != new_positions[index]
    ]


# Currently unused function
# def _get_tasks_move_onto_communication_position(memorymanager, desired_qubit_at_comm_pos):
#    """
#    Parameters
#    ----------
#    desired_qubit_at_comm_pos : int
#        Memory position that one would like to have at the communication position
#        (Note: these two positions are not necessarily distinct!)
#    """
#    comm_pos = memorymanager.communication_position
#    if comm_pos == desired_qubit_at_comm_pos:
#        return []
#    else:
#        move_task = _get_single_move_task(old_position=desired_qubit_at_comm_pos,
#                                           new_position=comm_pos)
#        if memorymanager.is_free(pos=comm_pos):
#            return [move_task]
#        else:
#            free_position = memorymanager.random_positions(num=1, excluded=[comm_pos])
#            first_move_task = _get_single_move_task(old_position=comm_pos,
#                                                     new_position=free_position
#            return [first_move_task, move_task]
