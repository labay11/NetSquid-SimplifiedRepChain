from netsquid import Entity, EventType, EventHandler

from repchain.logic.task import Task
from repchain.utils.message import Message


class TaskExecutor(Entity):
    """Holds a list of tasks (a “to-do-list”) which it performs sequentially.

    Usage:

    * append tasks to taskexecutor.todolist
    * taskexecutor.start()
    * call taskexecutor.continue_performing_tasks()

    Attributes
    ----------
    index : int
        the index of the repeater in the chain
    entswapprot : :class:`repchain.protocols.entswapProtocol.EntSwapProtocol`
        the entanglement swap protocol
    distilprot : :class:`repchain.protocols.distillation_protocol.LocalDistillationProtocol`
        the distillation protocol
    entgenprot : :class:`repchain.protocols.magic_entgen_protocol.MagicEntGenProtocol`
        the entanglement generation protocol
    moveprot : :class:`repchain.protocols.moveprotocol.MoveProtocol`
        the protocol that moves a state from the electron to the carbon qubit
    class_channels : dict if (str, :class:`netsquid.components.cchannel.ClassicalChannel`)
        the classical channel that connects the node with its neighbours at the left ("L") and right ("R") direction
    """

    EVT_TODOLIST_EMPTY = EventType("TODOLIST_EMPTY", "Todo list is empty")
    _EVT_CHECK_DISCARD_PENDING_TASK = EventType("DISCARD_PENDING", "Check if pending task should be discarded")
    STATUS_IDLE = 0
    STATUS_BUSY = 1

    def __init__(self, index, entswapprot, distilprot, entgenprot, moveprot, class_channels):
        self._index = index
        self._entswapprot = entswapprot
        self._distilprot = distilprot
        self._entgenprot = entgenprot
        self._moveprot = moveprot
        self._class_channels = class_channels
        self._evhandler_treat_next_todo = EventHandler(lambda event: self._treat_next_todo())

        # glue the event types together, so that the taskexecutor
        # is triggered to start a next task every time the previous one is done
        self._EVT_PREVIOUS_TASK_DONE = EventType("PREV_DONE", "Previous task finished")
        self._subprotocols = [self._entswapprot, self._distilprot, self._entgenprot, self._moveprot]
        for subprotocol in self._subprotocols:
            subprotocol.evtype_finished = self._EVT_PREVIOUS_TASK_DONE
        self._reset_parameters()

    def _reset_parameters(self):
        # only for logging purposes
        self._current_task = None
        # the list of tasks that should still be executed
        self.todolist = []
        self._pending_task = None
        self._confirmed_task = None
        # Setting the event handlers that deal with executing tasks, one after another:
        # self._dismiss(self._evhandler_treat_next_todo)

    def start(self):
        """
        Sets the status to 'idle' and moreover makes the TaskExecutor wait for the finishing of the four
        subprotocols, and treats the next task on the to-do-list
        when the "finishing event type" is signalled.
        """
        self._wait(self._evhandler_treat_next_todo, event_type=self._EVT_PREVIOUS_TASK_DONE)
        self._status = self.STATUS_IDLE

    def stop(self):
        """Reset parameters, set status to 'idle' and dismiss event handlers."""
        self._reset_parameters()
        self._dismiss(self._evhandler_treat_next_todo)
        self._status = self.STATUS_IDLE

    def reset(self):
        """`stop()` and `start()`"""
        self.stop()
        self.start()

    @property
    def status(self):
        """Whether the TaskExecutor is 'idle' or 'busy'."""
        return self._status

    def _check_discard_pending_task(self, taskID):
        # whether the pending task should be discarded:
        if self._pending_task and taskID == self._pending_task.ID:
            # logging.debug("{}: {}: discarding pending task {}".format(sim_time(), self._index, self._pending_task))
            self._pending_task = None
            self._schedule_now(self._EVT_PREVIOUS_TASK_DONE)

    def _set_expiry_handlers(self, task):
        expires_after = task.halt_expires_after
        if expires_after is not None:
            evhandler = EventHandler(lambda event, taskID=task.ID: self._check_discard_pending_task(taskID=taskID))
            self._wait_once(evhandler,
                            entity=self,
                            event_type=self._EVT_CHECK_DISCARD_PENDING_TASK)
            self._schedule_after(expires_after,
                                 self._EVT_CHECK_DISCARD_PENDING_TASK)

    def continue_performing_tasks(self):
        """Sets status to 'busy' and perform the tasks on the to-do-list."""
        self._status = self.STATUS_BUSY
        self._treat_next_todo()

    def _treat_next_todo(self):
        if self.status == self.STATUS_IDLE:
            raise Exception("Node {} has an idle taskexecutor".format(self._index))

        if len(self.todolist) != 0:
            task = self.todolist.pop(0)
            self._current_task = task
            if task.halt_until_confirm:
                # wait until we get a confirmation (so we *don't* set the status back to BUSY)
                self._pending_task = task
                # ... or until the task has expired
                self._set_expiry_handlers(task=task)
            else:
                self._perform_task_now(task=task)
        else:
            # logging.debug("{}: {}: taskexecutor's todo-list is empty".format(sim_time(), self._index))
            self._status = self.STATUS_IDLE
            self._current_task = None
            self._schedule_now(self.EVT_TODOLIST_EMPTY)

    def _perform_task_now(self, task):
        if task.topic == Task.SWAP:
            self._entswapprot.trigger(qmem_posA=task["qmem_posA"],
                                      qmem_posB=task["qmem_posB"])
        elif task.topic == Task.DIST:
            self._distilprot.trigger(pos_keep=task["pos_keep"],
                                     pos_lose=task["pos_lose"],
                                     strategy=task['strategy'])
        elif task.topic == Task.ENTGEN:
            self._entgenprot.trigger(remoteID=task["remote_node_id"],
                                     free_qmem_pos=task["free_pos"])
        elif task.topic == Task.MOVE:
            self._moveprot.trigger(old_pos=task["old_pos"],
                                   new_pos=task["new_pos"])
        elif task.topic == Task.SLEEP:
            self._schedule_after(task["duration"], self._EVT_PREVIOUS_TASK_DONE)
        elif task.topic == Task.SEND_MSG:
            # create an appropriate message
            msg = Message.from_task(self._index, task)
            direction = "L" if task["receiver_id"] < self._index else "R"
            self._class_channels[direction].send(items=msg)
            # immediately after sending a message, we can perform any remaining tasks:
            # We could call the following:
            self._schedule_now(self._EVT_PREVIOUS_TASK_DONE)
            # But this is faster:
            # self._treat_next_todo()
        else:
            # should not come here
            raise Exception("Received unknown action type {}".format(task.topic))

    def process_msgs(self, direction, msglist):
        """Forward the incoming messages to remote nodes or to the correct subprotocol."""
        for msg in msglist:
            self._treat_msg(direction, msg)

    def _treat_msg(self, direction, msg):
        # logging.debug("{}: {}: taskexecutor receives msg {}".format(sim_time(), self._index, msg))
        if msg.intended_receiver != self._index:
            # message is not meant for this node, so forward it
            forwarddirection = "L" if direction == "R" else "R"
            self._class_channels[forwarddirection].send(items=msg)
        else:
            if msg.has_subject(Message.SUBJECT_CONFIRM):
                # assert this is a confirmation to a message we sent out earlier
                assert msg.action == self._pending_task.topic
                self._confirmed_task = self._pending_task
                self._pending_task = None
                self._perform_task_now(task=self._confirmed_task)

            elif msg.has_subject(Message.SUBJECT_DIST_OUTCOME):
                # message contains outcome of a measurement
                # that is part of the distillation protocol
                self._distilprot.process_incoming_message(msg)
            elif msg.has_action(Message.ACTION_SWAP):
                # message is a swap update
                self._entswapprot.process_incoming_message(msg=msg)
            else:
                # code should not reach here
                raise Exception("Unknown message {}".format(msg))
