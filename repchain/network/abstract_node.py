import numpy as np
from netsquid.qubits.operators import Operator
from netsquid.nodes.node import Node
from netsquid.components.models.qerrormodels import T1T2NoiseModel, DepolarNoiseModel
from netsquid.components.qprocessor import QuantumProcessor, PhysicalInstruction
from netsquid.components.instructions import INSTR_MEASURE, INSTR_INIT, INSTR_ROT, \
    INSTR_H, INSTR_I, INSTR_CNOT, INSTR_SWAP, INSTR_ROT_Z


class AbstractNode(Node):
    """Node with :class:`netsquid_netconf.abstract_processor.AbstractProcessor` as its QuantumProcessor.

    Parameters
    ----------
    name : str
      Name for this node.
    num_positions : int
      Number of qubits in the Processor.
    swap_quality : float, optional
      Parametrizes the depolarizing noise introduced by the node's processor during an entanglement swap
    T1 : float, optional
      Relaxation time of the node's qubits
    T2 : float, optional
      Dephasing time of the node's qubits
    initialization_duration : float, optional
      Time in ns needed to initialise a qubit in the memory.
    single_qubit_gate_duration : float, optional
      Time in ns needed to perform a single qubit gate in the memory.
    two_qubit_gate_duration : float, optional
      Time in ns needed to perform a two qubit gate in the memory.
    measurement_duration : float, optional
      Time in ns needed to measure a qubit in the memory.
    noiseless : bool, optional
      If True, all operations are noiseless.
    instantaneous : bool, optional
      If True, all operations are instantaneous.
    ID : int, optional
      ID for this node.
    port_names : list of str or None, optional
        Names of additional ports to add to this component.
    """

    def __init__(self, name, num_positions,
                 T1=3.6e18, T2=1e15,
                 errors=None,
                 durations=None,
                 noiseless=False,
                 ID=None, port_names=None):
        abstract_processor = AbstractProcessor(
            num_positions=num_positions,
            T1=T1, T2=T2,
            errors=errors,
            durations=durations,
            noiseless=noiseless)
        super().__init__(name=name, qmemory=abstract_processor, ID=ID, port_names=None)


class AbstractProcessor(QuantumProcessor):
    """Abstract QuantumProcessor.

    This is a simple model for a QuantumProcessor. Qubits are fully connected and all suffer from T1,T2
    memory decoherence. Operation noise is depolarizing and quantified by a swap_quality parameter. This noise
    is applied only through an identity gate, whose duration is set to 0. All other operations are noiseless, so
    one should include identity gates whenever noise modelling is desired.

    This processor supports arbitrary single-qubit rotations, Hadamard, CNOT and SWAP gates on all of its qubits.
    All qubits can also be initialized and measured with no restrictions.

    Default properties were obtained by mapping from NV center parameters achievable in 2020 to this abstract model.
    For details see appendices of "Optimizing Entanglement Generation and Distribution using Genetic Algorithms."
    Default operation times are also set to 0, but it is strongly recommend overwriting this to ensure realistic
    entanglement generation times.

    Parameters
    ----------
    num_positions : int
        Number of qubits in the processor.
    swap_quality : float, optional
      Parametrizes the depolarizing noise introduced by the node's processor during an entanglement swap
    T1 : float, optional
      Relaxation time of the node's qubits
    T2 : float, optional
      Dephasing time of the node's qubits
    initialization_duration : float, optional
      Time in ns needed to initialise a qubit in the memory.
    single_qubit_gate_duration : float, optional
      Time in ns needed to perform a single qubit gate in the memory.
    two_qubit_gate_duration : float, optional
      Time in ns needed to perform a two qubit gate in the memory.
    measurement_duration : float, optional
      Time in ns needed to measure a qubit in the memory.
    noiseless : bool, optional
      If True, all operations are noiseless.
    instantaneous : bool, optional
      If True, all operations are instantaneous.
    """

    NOISELESS_PARAMS = {
        "T1": 0.,
        "T2": 0.,
        'p_q1gate': 0.,
        'p_q2gate': 0.,
        'p_q1meas': (0., 0.),
        'p_q1init': 0.
    }

    INSTANTANEOUS_PARAMS = {
        't_q1gate': 0.,
        't_q2gate': 0.,
        't_q1meas': 0.,
        't_q1init': 0.
    }

    def __init__(self, num_positions,
                 T1=3.6e18, T2=1e15,
                 errors=None,
                 durations=None,
                 noiseless=False):
        params = self.NOISELESS_PARAMS.copy()
        if not noiseless:
            if T1:
                params['T1'] = T1
            if T2:
                params['T2'] = T2
        if errors:
            params.update(errors)
            if isinstance(params['p_q1meas'], float):
                params['p_q1meas'] = (params['p_q1meas'], params['p_q1meas'])

        params.update(self.INSTANTANEOUS_PARAMS)
        if durations:
            params.update(durations)

        for property, value in params.items():
            self.add_property(name=property, value=value, mutable=False)

        super().__init__(name="abstract_processor",
                         num_positions=num_positions,
                         mem_noise_models=T1T2NoiseModel(T1=params["T1"], T2=params["T2"]))

        self._set_physical_instructions()

    def _set_physical_instructions(self):
        """Function that initializes the physical instructions of abstract processor.

        Physical instructions are initialized by this function using noise parameters
        obtained from self.properties (which are set in __init__()).
        """
        p0 = self.properties['p_q1meas'][0]
        p1 = self.properties['p_q1meas'][1]
        M0 = Operator("M0", np.diag([np.sqrt(1 - p0), np.sqrt(p1)]))
        M1 = Operator("M1", np.diag([np.sqrt(p0), np.sqrt(1 - p1)]))

        # hack to set imperfect measurements
        INSTR_MEASURE._meas_operators = [M0, M1]

        physical_measurement = PhysicalInstruction(instruction=INSTR_MEASURE,
                                                   duration=self.properties["t_q1meas"],
                                                   q_noise_model=None,
                                                   parallel=False)

        physical_init = PhysicalInstruction(
            instruction=INSTR_INIT,
            duration=self.properties["t_q1init"],
            q_noise_model=DepolarNoiseModel(self.properties["p_q1init"], time_independent=True),
            c_noise_model=None,
            parallel=True,
            apply_q_noise_after=True)

        dep1q = DepolarNoiseModel(self.properties["p_q1gate"], time_independent=True)
        rotation_gate = PhysicalInstruction(instruction=INSTR_ROT,
                                            duration=self.properties["t_q1gate"],
                                            q_noise_model=dep1q,
                                            c_noise_model=None,
                                            parallel=True,
                                            apply_q_noise_after=True)

        z_rotation_gate = PhysicalInstruction(instruction=INSTR_ROT_Z,
                                              duration=self.properties["t_q1gate"],
                                              q_noise_model=dep1q,
                                              c_noise_model=None,
                                              parallel=True,
                                              apply_q_noise_after=True)

        h_gate = PhysicalInstruction(instruction=INSTR_H,
                                     duration=self.properties["t_q1gate"],
                                     q_noise_model=dep1q,
                                     c_noise_model=None,
                                     parallel=True,
                                     apply_q_noise_after=True)

        i_gate = PhysicalInstruction(instruction=INSTR_I,
                                     duration=self.properties["t_q1gate"],
                                     q_noise_model=dep1q,
                                     c_noise_model=None,
                                     parallel=True,
                                     apply_q_noise_after=True)

        dep2q = DepolarNoiseModel(self.properties["p_q2gate"], time_independent=True)
        cnot_gate = PhysicalInstruction(instruction=INSTR_CNOT,
                                        duration=self.properties["t_q2gate"],
                                        q_noise_model=dep2q,
                                        c_noise_model=None,
                                        parallel=True,
                                        apply_q_noise_after=True)

        magical_swap_gate = PhysicalInstruction(instruction=INSTR_SWAP,
                                                duration=self.properties["t_q2gate"],
                                                q_noise_model=dep2q,
                                                c_noise_model=None,
                                                parallel=True,
                                                apply_q_noise_after=True)

        self.add_physical_instruction(physical_measurement)
        self.add_physical_instruction(physical_init)
        self.add_physical_instruction(rotation_gate)
        self.add_physical_instruction(z_rotation_gate)
        self.add_physical_instruction(h_gate)
        self.add_physical_instruction(i_gate)
        self.add_physical_instruction(cnot_gate)
        self.add_physical_instruction(magical_swap_gate)
