from collections import namedtuple

from netsquid.nodes.network import Network
from netsquid.components.cchannel import ClassicalChannel

from repchain.physical.magic_distributor import SimplifiedMagicDistributor
from repchain.network.abstract_node import AbstractNode


AbstractNetworkFactory = namedtuple('AbstractNetworkFactory', [
    "network",
    "number_of_nodes",
    "indices",
    "delivery_params",
    "class_channels_send",
    "class_channels_receive",
    "distributors",
])


def create_network_from_dic(network_params, logic_params):
    """Create network from dictionary of parameters.

    Initialises the network as `AbstractNetwork` and adds all the nodes (determined by `number_of_nodes`)
    to it, each node is an `AbstractNode`, has an id equal to the index in the chain,
    an amount of qubits given by `tot_num_qubits` and quantum memory properties given by `qmem_config_params`.

    Then, the classical channels connecting a node to its left and right neighbours (except end nodes of course),
    those are :class:`netsquid.components.cchannel.ClassicalChannel` with name 'Channel{node1}>{node2}' and delay
    calculated as the internode distance divided by the speed of light in fiber (in units of cycle time).

    Next, the quantum connections are creates as :class:`netsquid_physlayer.easyfibre.MiddleHeraldedFibreConnection`.

    Finally, a magic distributor of states is created for each pair of nearest-neighbour nodes.

    Parameters
    ----------
    network_params : dict of (str, Any)
        contains information about the network parameters like the number of nodes, distance between them...
    logic_params : dict of (str, Any)
        contains information about the protocol parameters like the delivery parameters

    Returns
    -------
    :class:`repchain.network.abstract_network_factory.AbstractNetworkFactory`
    """
    distributor_params = network_params["distributor_params"]
    number_of_nodes = network_params["number_of_nodes"]
    indices = range(number_of_nodes)

    internode_distance = network_params["internode_distance"]
    # delay computed in units of cycle time
    delay = 1e9 * internode_distance / network_params['c']
    distributor_params['delay'] = delay

    delivery_params = []
    logic_params['delivery_params']['cycle_time'] += delay
    for index in indices:
        delivery_params.append(logic_params["delivery_params"])

    qmem_config_params = network_params["qmem_config_params"]

    network = Network('AbstractNetwork')

    qnodes = [AbstractNode(name="Abstractnode{}".format(index), ID=index,
                           num_positions=qmem_config_params["tot_num_qubits"],
                           T1=qmem_config_params['T1'],
                           T2=qmem_config_params['T2'],
                           errors={
                               k: v
                               for k, v in qmem_config_params.items()
                               if k[0] == 'p'
                           },
                           durations={
                                k: v
                                for k, v in qmem_config_params.items()
                                if k[0] == 't'
                           },
                           noiseless=False)
              for index in indices]
    network.add_nodes(qnodes)

    # the following is a dictionary with as keys node indices and
    # values { direction : connection }
    class_channels_send = {index: {} for index in indices}
    for index in range(0, number_of_nodes - 1):
        # all indices expect for the last one
        class_channels_send[index]["R"] = ClassicalChannel(
            name="Channel{}>{}".format(index, index + 1),
            delay=delay)
    # add a dictionary for the last node
    for index in range(1, number_of_nodes):
        # all indices expect for the first one
        class_channels_send[index]["L"] = ClassicalChannel(
            name="Channel{}<{}".format(index - 1, index),
            delay=delay)
    class_channels_receive = {index: {} for index in indices}
    for index in range(0, number_of_nodes - 1):
        class_channels_receive[index]["R"] = class_channels_send[index + 1]["L"]
    for index in range(1, number_of_nodes):
        class_channels_receive[index]["L"] = class_channels_send[index - 1]["R"]

    distributors = [
        SimplifiedMagicDistributor(nodes=[network.get_node(name="Abstractnode{}".format(node_index)),
                                          network.get_node(name="Abstractnode{}".format(node_index + 1))],
                                   component=None,
                                   **distributor_params)
        for node_index in range(number_of_nodes - 1)
    ]

    return AbstractNetworkFactory(
        network,
        number_of_nodes,
        indices,
        delivery_params,
        class_channels_send,
        class_channels_receive,
        distributors
    )
