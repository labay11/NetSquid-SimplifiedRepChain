"""Abstract parameter set to be used with this model."""
from typing import NamedTuple, Any

import numpy as np

from netsquid_simulationtools.parameter_set import ParameterSet, Parameter


def coherence_time_to_prob(x):
    return 1. - 1. / x


def coherence_time_from_prob(x):
    return 1. / (1. - x)


def coherence_time_ga_to_prob(x):
    return coherence_time_to_prob(np.power(10., x))


def coherence_time_ga_from_prob(x):
    return np.log10(coherence_time_from_prob(x))


def duration_to_prob(x):
    return 1. / (1. + x)


def duration_from_prob(x):
    return (1. / x) - 1.


def psucc_to_prob(x):
    return x * 2.


def psuc_from_prob(x):
    return x * 0.5


def p_error_to_from_prob(x):
    return 1. - x


def identity_map(x):
    return x


FIBER_ATTENUATION = 0.22  # db/km, p_loss_lengths_with_conversion, mean value according to cisco


def fiber_efficiency(distance, attenuation=FIBER_ATTENUATION):
    """Compute the efficiency of the fiber for a given distance and attenuation length in db/km."""
    return np.power(10.0, -0.1 * distance * attenuation)


class BaselineParameter(NamedTuple):
    """Container class for a parameter.

    Parameters
    ----------
    name : str
        Name of the parameter.
    dtype
        Type that the parameter should be. Examples: `int` or `float`.
    perfect_value : Any
        The value that the parameter would have if it were free of any errors,
        e.g. 0.0 for probabilities of error or `numpy.inf` for coherence times.
        Should be of type `type`.
    baseline_value : Any
        The state-of-the.art value that the parameter have, must be less than or equal to perfect value.
        Should be of type `type`.
    convert_to_prob_fn : function that takes value of type `type` and returns float.
        Function that converts the parameter value into a probability of no-error.
        Optional.
        Example: if :math:`T_1` is the coherence time, then `convert_to_prob_fn(T_1=100)`
        returns `numpy.exp(-1/100)`.
    convert_from_prob_fn : function that takes value of type float and returns `type`.
        The inverse of `convert_to_prob_fn`. Optional.

    Notes
    -----
    It might be necessary to define a parameter which has no perfect or baseline value,
    for instance protocol parameters. In those cases, you can just use the `perfect_value` and `baseline_value`
    as the maximum and minimum value allowed for that value. In case the value is an integer, the maximum
    should be one above the actual maximum.
    """

    name: str
    dtype: Any
    baseline_value: Any
    perfect_value: Any
    convert_to_prob_fn: Any = None
    convert_from_prob_fn: Any = None

    def cost(self, x):
        """Calculate the cost of increasing the parameter from the baseline value."""
        if not self.convert_to_prob_fn:
            raise ValueError('Cannot convert parameter to probability: ' + self.name)
        if np.isclose(x, self.perfect_value, atol=1.e-14):
            return np.inf
        return np.log(self.convert_to_prob_fn(self.baseline_value)) / np.log(self.convert_to_prob_fn(x))

    def improve(self, k):
        """Improve the parameter by a cost `k` from its baseline value."""
        if not self.convert_to_prob_fn or not self.convert_from_prob_fn:
            raise ValueError('Cannot convert parameter to or from probability: ' + self.name)
        if np.isinf(k):
            return self.perfect_value
        if np.isclose(k, 1.):
            return self.baseline_value
        return self.convert_from_prob_fn(np.power(self.convert_to_prob_fn(self.baseline_value), 1. / k))


class AbstractParameterSet(ParameterSet):
    """Parameter set for the abstract model."""

    _REQUIRED_PARAMETERS = [
        Parameter(name='elementary_link_fidelity', units='', type=float, perfect_value=1.,
                  convert_to_prob_fn=identity_map, convert_from_prob_fn=identity_map),
        Parameter(name='elementary_link_succ_prob', units='', type=float, perfect_value=1.,
                  convert_to_prob_fn=identity_map, convert_from_prob_fn=identity_map),
        Parameter(name='p_q1init', units='', type=float, perfect_value=0.,
                  convert_to_prob_fn=p_error_to_from_prob, convert_from_prob_fn=p_error_to_from_prob),
        Parameter(name='p_q1gate', units='', type=float, perfect_value=0.,
                  convert_to_prob_fn=p_error_to_from_prob, convert_from_prob_fn=p_error_to_from_prob),
        Parameter(name='p_q1meas', units='', type=tuple, perfect_value=(0., 0.),
                  convert_to_prob_fn=p_error_to_from_prob, convert_from_prob_fn=p_error_to_from_prob),
        Parameter(name='p_q2gate', units='', type=float, perfect_value=0.,
                  convert_to_prob_fn=p_error_to_from_prob, convert_from_prob_fn=p_error_to_from_prob),
        Parameter(name='T1', type=float, perfect_value=np.inf, units='ns',
                  convert_to_prob_fn=coherence_time_to_prob, convert_from_prob_fn=coherence_time_from_prob),
        Parameter(name='T2', type=float, perfect_value=np.inf, units='ns',
                  convert_to_prob_fn=coherence_time_to_prob, convert_from_prob_fn=coherence_time_from_prob),
        Parameter(name='cycle_time', type=float, perfect_value=0., units='ns',
                  convert_to_prob_fn=duration_to_prob, convert_from_prob_fn=duration_from_prob),
        Parameter(name='t_q1init', type=float, perfect_value=0., units='ns',
                  convert_to_prob_fn=duration_to_prob, convert_from_prob_fn=duration_from_prob),
        Parameter(name='t_q1gate', type=float, perfect_value=0., units='ns',
                  convert_to_prob_fn=duration_to_prob, convert_from_prob_fn=duration_from_prob),
        Parameter(name='t_q1meas', type=float, perfect_value=0., units='ns',
                  convert_to_prob_fn=duration_to_prob, convert_from_prob_fn=duration_from_prob),
        Parameter(name='t_q2gate', type=float, perfect_value=0., units='ns',
                  convert_to_prob_fn=duration_to_prob, convert_from_prob_fn=duration_from_prob),
        Parameter(name='tot_num_qubits', type=int, units='', perfect_value=None),
        Parameter(name='number_of_nodes', type=int, units='', perfect_value=None),
        Parameter(name='internode_distance', type=float, units='km', perfect_value=None),
        Parameter(name='c', type=float, units='km/s', perfect_value=3.e8),
        Parameter(name='p_loss_length', type=float, units='dB/km', perfect_value=0.18),
        Parameter(name='use_swap_only', type=bool, units='', perfect_value=None),
        Parameter(name='respect_NV_structure', type=bool, units='', perfect_value=None),
        Parameter(name='werner', type=bool, units='', perfect_value=None),
        Parameter(name='eps', type=float, units='', perfect_value=0.,
                  convert_to_prob_fn=p_error_to_from_prob, convert_from_prob_fn=p_error_to_from_prob),
        Parameter(name='distillation_strategy', units='', type=list, perfect_value=[])
    ]
