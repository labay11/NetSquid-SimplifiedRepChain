"""NV to abstract mapping."""
import numpy as np

# from netsquid.qubits.ketstates import b01
# from netsquid_nv.delft_nvs.delft_nv_2019_optimistic import NVParameterSet2019Optimistic
# from netsquid_magic.state_delivery_sampler import DoubleClickDeliverySamplerFactory

# from repchain.utils.tools import compute_fidelity
from repchain.parameters import AbstractParameterSet, fiber_efficiency
from repchain.utils.bcdzfunctions import num2level


_LEVEL_STRATEGY = {
    0: None,
    1: ('EPL', 1),
    2: ('DEJMPS', 1),
    3: ('DEJMPS', 2),
    4: ('DEJMPS', 3)
}


def distillation_strategy_range(nodes, uniform=False):
    """Compute the maximum number accepted as distillation strategy."""
    if uniform:
        return len(_LEVEL_STRATEGY)
    else:
        lv = num2level(nodes)
        return int(len(_LEVEL_STRATEGY) ** lv)


class NVOptimisticParameters(AbstractParameterSet):
    """Abstract parameter set modeling NV centers with no trade-off between fidelity and success probability."""

    p_loss_length = 0.22
    c = 299792.458 / 1.44  # speed of light in fibre [km/s]

    t_q1gate = 20.e3  # carbon 20e3ns, electron 5ns
    t_q1init = 310.e3  # 310e3 carbon init vs 2e3 electron init
    t_q1meas = 3.7e3  # electron readout
    t_q2gate = 500.e3  # electron carbon CX
    tot_num_qubits = 10

    T1 = 1. * 3600 * 1e9
    T2 = 1.e9

    cycle_time = 3.5e3

    p_q1init = 0.006  # was proben in 10 qubits
    p_q1gate = (4. / 3.) * 0.001
    p_q2gate = 0.02  # 0.012 was proben in 10 qubits
    p_q1meas = (0.01, 0.005)

    werner = False
    respect_NV_structure = False
    eps = 0.

    def __init__(self,
                 number_of_nodes,
                 total_dist,
                 elementary_link_fidelity,
                 elementary_link_succ_prob,
                 distillation_strategy=0,
                 uniform_strategy=False,
                 **kwargs):
        self.number_of_nodes = number_of_nodes
        self.internode_distance = total_dist / (number_of_nodes - 1)

        self.elementary_link_fidelity = elementary_link_fidelity
        self.elementary_link_succ_prob = elementary_link_succ_prob

        if distillation_strategy >= distillation_strategy_range(number_of_nodes, uniform_strategy):
            raise ValueError('Distillation strategy out of range: {}'.format(distillation_strategy))

        swap_only, strategy = self._distillation_strategy(num2level(number_of_nodes),
                                                          distillation_strategy,
                                                          uniform_strategy)
        self.use_swap_only = swap_only
        self.distillation_strategy = strategy

        if kwargs:
            # override any inital value
            for label, value in kwargs.items():
                setattr(self, label, value)

        super().__init__()

    @classmethod
    def _distillation_strategy(cls, levels, parameter, uniform):
        """Convert a distillation strategy parameter into an actual strategy valid for the abstract chain.

        Returns
        -------
        use_swap_only : bool
            in case no distillation is used, fallback to swap-asap
        distillation_strategy : list of tuple or None
            the strategy to use per level, the number of items in the list equals the number of levels plus 1.
            At each level, a tuple with the protocol name (`DEJMPS` or `EPL`) and the number of distillations is
            specified or None if the number of distillations is 0 for that level.
        """
        if levels == 0:
            return True, [None]

        if uniform:
            strategy = _LEVEL_STRATEGY[parameter]
            return strategy is None, [strategy] * levels + [None]
        else:
            if not parameter or parameter < 1:
                # no distillation at all
                return True, [None] * (levels + 1)

            param = parameter
            n_strategies = len(_LEVEL_STRATEGY)
            strategy = []
            for i in range(levels + 1):
                strategy.append(_LEVEL_STRATEGY[param % n_strategies])
                param //= n_strategies

            return False, strategy


class NVRestrictedParameters(NVOptimisticParameters):
    """Abstract parameter set modeling NV centers."""

    visibility = 0.9
    p_double_exc = 0.06
    std_phase = 0.35
    p_det = 0.0046

    def __init__(self,
                 number_of_nodes,
                 total_dist,
                 alpha,
                 distillation_strategy=0,
                 uniform_strategy=False,
                 **kwargs):
        f, p = self._single_click_mapping(
            alpha,
            total_dist / (number_of_nodes - 1),
            self.p_det,
            self._state_efficiency())

        super().__init__(number_of_nodes, total_dist, f, p,
                         distillation_strategy, uniform_strategy, **kwargs)

    def _single_click_mapping(self, alpha, internode_distance, p_det, state_efficiency):
        p = 2 * alpha * p_det * fiber_efficiency(internode_distance * 0.5, attenuation=self.p_loss_length)
        F = (1 - alpha) * state_efficiency
        return F, p

    def _state_efficiency(self):
        pd = self.p_double_exc * 0.5
        ph = 0.5 * (1. - np.exp(-0.5 * self.std_phase**2))
        p_ph = (1. - ph) * pd * (1. - pd) + ph * ((1. - pd)**2 + pd**2)
        return 0.5 * (1. + np.sqrt(self.visibility)) * (1 - p_ph)


class NVDoubleClickParameters(NVOptimisticParameters):
    """Abstract parameter set modeling NV centers."""

    # visibility = NVParameterSet2019Optimistic.visibility
    p_det = 0.0046
    elementary_link_fidelity = 0.92  # doi:10.1038/nature15759
    eps = 0.5
    # https://static-content.springer.com/esm/art%3A10.1038%2Fnature15759/MediaObjects/41586_2015_BFnature15759_MOESM113_ESM.pdf

    def __init__(self,
                 number_of_nodes,
                 total_dist,
                 distillation_strategy=0,
                 uniform_strategy=False,
                 **kwargs):
        internode_distance = total_dist / (number_of_nodes - 1)
        super().__init__(number_of_nodes, total_dist,
                         self.elementary_link_fidelity,
                         self._elementary_link_succ_prob(internode_distance),
                         distillation_strategy, uniform_strategy, **kwargs)

    # def _elementary_link_fidelity(self, internode_distance):
    #     states, _ = DoubleClickDeliverySamplerFactory._func_delivery(
    #         internode_distance, internode_distance, self.p_loss_length, self.p_loss_length,
    #         0., 0., 1., 1., self.p_det, 0.)
    #     return compute_fidelity(states[0], b01)

    def _elementary_link_succ_prob(self, internode_distance):
        # p = DoubleClickDeliverySamplerFactory._func_success_probability(
        #     internode_distance, internode_distance, self.p_loss_length, self.p_loss_length,
        #     0., 0., self.p_det, 0.)
        return 0.5 * (self.p_det * fiber_efficiency(internode_distance * 0.5, self.p_loss_length))**2


class NVApproxRestrictedParameters(NVRestrictedParameters):
    """Similar to `NVRestrictedParameters` but uses the simple approximation of `1-alpha` for the fidelity."""

    def __init__(self,
                 number_of_nodes,
                 total_dist,
                 alpha,
                 distillation_strategy=None,
                 uniform_strategy=False,
                 **kwargs):
        super().__init__(number_of_nodes, total_dist, alpha,
                         distillation_strategy, uniform_strategy, **kwargs)

    def _state_efficiency(self, visibility, p_double_exc, std_phase):
        return 1.
