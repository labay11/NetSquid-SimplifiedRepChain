import pathlib
import pickle
import re

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, RegularPolygon
from matplotlib.path import Path
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection
from matplotlib.spines import Spine
from matplotlib.transforms import Affine2D
from matplotlib.lines import Line2D
from matplotlib.gridspec import GridSpec
from uncertainties import ufloat

from repchain.utils.latexify import latexify, savefig

SLURM_RE = re.compile(r'^(\d*)_(\d{3,4})_')


def radar_factory(num_vars, frame='circle', radius=0.5):
    """
    Create a radar chart with `num_vars` axes.

    This function creates a RadarAxes projection and registers it.

    Parameters
    ----------
    num_vars : int
        Number of variables for radar chart.
    frame : {'circle', 'polygon'}
        Shape of frame surrounding axes.

    """
    # calculate evenly-spaced axis angles
    theta = np.linspace(0, 2 * np.pi, num_vars, endpoint=False)

    class RadarAxes(PolarAxes):

        name = 'radar'
        # use 1 line segment to connect specified points
        RESOLUTION = 1

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            # rotate plot such that the first axis is at the top
            self.set_theta_zero_location('N')
            self.set_theta_direction(-1)
            self.ymin = np.inf
            self.ymax = -np.inf

            super().plot(np.linspace(0, 2 * np.pi, 180), np.full(180, 0.), ls='-', c='k', marker='None')

        def fill(self, *args, closed=True, **kwargs):
            """Override fill so that line is closed by default."""
            return super().fill(closed=closed, *args, **kwargs)

        def plot(self, x, y, *args, **kwargs):
            """Override plot so that line is closed by default."""
            self.ymin = min(self.ymin, np.amin(y))
            self.ymax = max(self.ymax, np.amax(y))
            y = np.log10(y)
            lines = super().plot(x, y, *args, **kwargs)
            for line in lines:
                self._close_line(line)

        def set_rticks(self):
            min10 = np.log10(self.ymin)
            max10 = np.log10(self.ymax) + 0.1
            if np.isinf(max10) or np.isinf(min10):
                return
            lp = np.arange(np.floor(min10), max10)
            self.set_rlabel_position(-45)
            super().set_rticks(lp - min10)
            self.set_yticklabels([r"$10^%d$" % x if x > 0 else '1' % x for x in lp])
            self.set_rlim(min10 - 0.1, max10)

            raxis_theta = self.get_rlabel_position() * np.pi / 180
            super().plot([raxis_theta, raxis_theta], [min10, max10], ls='-', c='k')

            self.text(raxis_theta, max10, 'Cost', rotation=self.get_rlabel_position(),
                      horizontalalignment='right', verticalalignment='bottom')

            if abs(lp[-1] - max10) > 0.2 * max10:
                self.text(raxis_theta + 0.1 / max10, max10, rf'{int(np.ceil(self.ymax))}',
                          horizontalalignment='left', verticalalignment='top')

            # plot mini ticks
            for x in lp:
                ticks = np.log10(np.linspace(10**x, 10**(x + 1), 10)[1:-2])
                for tick in ticks:
                    offset = 0.05 / tick
                    super().plot([raxis_theta - offset, raxis_theta + offset], [tick, tick],
                                 c='#b0b0b0', lw=0.8)  # grid color

        def _close_line(self, line):
            x, y = line.get_data()
            # FIXME: markers at x[0], y[0] get doubled-up
            if x[0] != x[-1]:
                x = np.append(x, x[0])
                y = np.append(y, y[0])
                line.set_data(x, y)

        def set_varlabels(self, labels):
            self.set_thetagrids(np.degrees(theta), labels)

        def _gen_axes_patch(self):
            # The Axes patch must be centered at (0.5, 0.5) and of radius 0.5
            # in axes coordinates.
            if frame == 'circle':
                return Circle((0.5, 0.5), radius)
            elif frame == 'polygon':
                return RegularPolygon((0.5, 0.5), num_vars, radius=radius, edgecolor="k")
            else:
                raise ValueError("Unknown value for 'frame': %s" % frame)

        def _gen_axes_spines(self):
            if frame == 'circle':
                return super()._gen_axes_spines()
            elif frame == 'polygon':
                # spine_type must be 'left'/'right'/'top'/'bottom'/'circle'.
                spine = Spine(axes=self,
                              spine_type='circle',
                              path=Path.unit_regular_polygon(num_vars))
                # unit_regular_polygon gives a polygon of radius 1 centered at
                # (0, 0) but we want a polygon of radius 0.5 centered at (0.5,
                # 0.5) in axes coordinates.
                spine.set_transform(Affine2D().scale(radius).translate(.5, .5)
                                    + self.transAxes)
                return {'polar': spine}
            else:
                raise ValueError("Unknown value for 'frame': %s" % frame)

    register_projection(RadarAxes)
    return theta


def _nodes_to_str(n, show_as_repeaters):
    if show_as_repeaters:
        return f'{n - 2}'
    else:
        return f'{n}'


def _strategy_to_str(strategy):
    to_join = []
    for s in strategy:
        if s is None:
            to_join.append('SWAP')
        else:
            prt, n = s
            if prt == 'EPL':
                to_join.append(prt)
            else:
                to_join.append(f'{prt} ({n})')
    del to_join[-1]  # remove last element
    if len(to_join) == 0:
        return np.nan  # 2 nodes
    elif len(to_join) == 1:
        return to_join[0]

    identical = len(set(to_join)) <= 1
    only_lowest = all(to_join[j] == 'SWAP' for j in range(1, len(to_join)))
    if identical or only_lowest:
        return to_join[0]
    return ', '.join(to_join)


def _param_format(name, x):
    if name == 'T1':
        x /= (3600 * 1e9)
    elif name == 'T2':
        x *= 1e-9
    elif name == 'cost':
        return f'{x:.4g}' if x < 1e9 else 'x'
    # elif name == 'k_gates':
    #     print('ads', x)
    #     x /= 1
    if np.isclose(int(x), x):
        return str(int(x))
    if x > 1:
        a = f'{x:.4g}'
        return '$\num{' + a + '}$'
    return f'{x:.4f}'


SUBTABLE = '\begin{subtable}{\linewidth}{}\end{subtable}'


def create_params_latex_table(results, params, distances, outdir, prefix='', include_strategy=False,
                              show_as_repeaters=True, include_targets=True, aliases=None):
    """Create a table for each distance where the value of the parameters are plotted."""
    all_nodes = sorted(results.keys())
    files = {}
    for distance in distances:
        table = {}
        for nodes in all_nodes:
            data = results[nodes]
            key = _nodes_to_str(nodes, show_as_repeaters)
            if distance not in data:
                table[key] = {aliases[p]: np.nan for p in params}
            else:
                table[key] = {}
                values = data[distance]
                for p in params:
                    table[key][aliases[p]] = _param_format(p, values[p]) if p in values else np.nan

                if include_strategy:
                    col = include_strategy if isinstance(include_strategy, str) else 'Strategy'
                    table[key][col] = _strategy_to_str(values['distillation_strategy'])

                if include_targets:
                    cost = values['result']
                    table[key][aliases['cost']] = _param_format('cost', cost['cost']['total'])
                    f = ufloat(cost['fidelity'], cost['fidelity_unc'])
                    table[key][aliases['fidelity']] = '$\num{' + f'{f:.2uS}' + '}$'
                    r = ufloat(cost['rate'], cost['rate_unc'])
                    table[key][aliases['rate']] = '$\num{' + f'{r:.2uS}' + '}$'  # L

        # df = pd.DataFrame(table).transpose()
        # cols = params + ([col] if include_strategy else []) + (['cost', 'fidelity', 'rate'] if include_targets else [])
        # align = 'c|' + ('c' * len(params)) + ('|c' if include_strategy else '') + ('|c|cc' if include_targets else '')
        # df.to_latex(outdir / f'{prefix}_{distance}.txt',
        #             columns=cols,
        #             header=[aliases[c] if c != col else col for c in cols],
        #             na_rep='--', escape=False, column_format=align, label=f'tab:{prefix}_{distance}', caption='')
        files[distance] = f'{prefix}_{distance}.txt'
        df = pd.DataFrame(table)
        align = 'c|' + 'c' * len(all_nodes)
        df.to_latex(outdir / f'{prefix}_{distance}.txt',
                    columns=[_nodes_to_str(n, show_as_repeaters) for n in all_nodes],
                    na_rep='--', escape=False, column_format=align, label=f'tab:{prefix}_{distance}', caption='')

    all_distances = sorted(files.keys())

    parent_table = "\\begin{table}\n\\centering\n\\caption{}\n\\label{tab:" + prefix + '}\n'
    for distance in all_distances:
        filename = files[distance]
        with open(outdir / filename, 'r') as infile:
            lines = infile.readlines()
        subtable = '\\begin{subtable}{\\linewidth}\n\t\\centering\n\t\\caption{' + str(distance) + 'km}\n\t'\
            + '\t'.join(lines[2:-1]) + '\\end{subtable}\n'
        parent_table += subtable
    parent_table += '\\end{table}'

    with open(outdir / f'{prefix}.txt', 'w') as outf:
        outf.write(parent_table)


def create_dist_strategy_table(results, outdir, prefix='',
                               strategy_param='distillation_strategy', show_as_repeaters=True):
    table = {
        _nodes_to_str(nodes, show_as_repeaters): {
            distance: _strategy_to_str(results[nodes][distance][strategy_param])
            for distance in results[nodes]
        }
        for nodes in results if nodes > 2
    }

    df = pd.DataFrame(table)
    align = 'c|' + 'c' * (len(df.columns) - 1)
    df.to_latex(outdir / f'{prefix}_strategies.txt',
                na_rep='--', escape=False, column_format=align,
                label=f'tab:{prefix}_strategies', caption='')


TARGETS_DIRS = {'0': (0.8, 1.0), '1': (0.9, 0.1)}

RESULTS = [{2: {}, 3: {}, 5: {}, 9: {}} for _ in range(len(TARGETS_DIRS))]

NODES_TO_COLOR = {2: 'm', 3: 'b', 5: 'r', 9: 'g', 17: 'orange'}
NODES_TO_COLOR_SC = {2: 'fuchsia', 3: 'lightblue', 5: 'lightcoral', 9: 'lime', 17: 'moccasin'}
NODES_TO_COLOR_DC = {2: 'darkmagenta', 3: 'darkblue', 5: 'red', 9: 'darkgreen', 17: 'darkorange'}

PROTOCOL_TO_MARKER = {0: 'x', 1: 'o', 2: '^', 3: 's', 4: 'p'}
# DISTANCES_TO_LS = {200: '--', 400: (0, (3, 1, 1, 1, 1, 1)), 800: ':'}
DISTANCES_TO_LS = {200: '-', 400: '--', 800: ':'}

PARAM_LABELS = {
    'elementary_link_fidelity': r'$f_{elem}$',
    'elementary_link_succ_prob': r'$p_{elem}$',
    'p_q2gate': r'$p_{2,gate}$',
    'T1': r'$T_1$',
    'T2': r'$T_2$',
    'alpha': r'$\alpha$',
    'detector_efficiency': r'$\eta_{lm}$',
    'state_efficiency': r'$\eta_{f}$',
    'fidelity': 'F',
    'rate': 'R',
    'cost': 'Cost',
    'k_gates': r'$k_{gates}$'
}

TABLE_ALIASES = {
    'elementary_link_fidelity': r'$f_{elem}$',
    'elementary_link_succ_prob': r'$p_{elem}$',
    'p_q2gate': r'$p_{2,gate}$',
    'T1': r'$T_1$ (h)',
    'T2': r'$T_2$ (s)',
    'alpha': r'$\alpha$',
    'detector_efficiency': r'$\eta_{lm}$',
    'state_efficiency': r'$\eta_{f}$',
    'fidelity': 'Fidelity',
    'rate': 'Rate',
    'cost': 'Cost',
    'k_gates': r'$k_{gates}$'
}


def plot_optimisation_results(
    basedir, parameters, target_results,
    show_as_repeaters=True, markersize=2.5, show_legend=True, radar_frame='circle', force_linear_axes=False,
    distinguish_eg=False, include_strategy=False
):
    cwd = pathlib.Path(basedir)
    prefix = cwd.name
    outdir = cwd / 'output'

    has_alpha = False
    params_with_cost = []
    for param in parameters:
        if param.name == 'alpha':
            has_alpha = True
        if param.name == 'k_gates' or param.convert_to_prob_fn:
            params_with_cost.append(param.name)

    figsize = latexify(plt, fract=0.7 if show_legend else 0.65, margin_x=0.05, margin_y=0.05)
    theta = radar_factory(len(params_with_cost), frame=radar_frame)

    if isinstance(show_legend, bool):
        show_legend = [show_legend] * len(target_results)

    if isinstance(force_linear_axes, bool):
        force_linear_axes = [force_linear_axes] * len(target_results)

    for target, results in enumerate(target_results):
        if not results:
            continue
        fig = plt.figure(figsize=figsize)

        gs = GridSpec(2, 3, figure=fig, hspace=0.1, wspace=0.3)

        if has_alpha:
            ax_f = fig.add_subplot(gs[0, 0])
            ax_r = fig.add_subplot(gs[1, 0])
            ax_cost = fig.add_subplot(gs[0, 1])
            ax_alpha = fig.add_subplot(gs[1, 1])
            ax_radar = fig.add_subplot(gs[:, 2], projection='radar')

            ax_alpha.set_xlabel('Total distance (km)')
            ax_alpha.set_ylabel(r'$\alpha$', labelpad=-0.1)
            ax_alpha.set_title(r'$(v)$', y=1.0, pad=-8)
        else:
            ax_f = fig.add_subplot(gs[0, 0])
            ax_r = fig.add_subplot(gs[1, 0])
            ax_cost = fig.add_subplot(gs[:, 1])
            ax_radar = fig.add_subplot(gs[:, 2], projection='radar')

        ax_f.set_ylabel('Fidelity')
        ax_f.set_title(r'$(i)$', y=1.0, pad=-8)
        plt.setp(ax_f.get_xticklabels(), visible=False)
        ax_r.set_xlabel('Total distance (km)')
        ax_r.set_title(r'$(ii)$', y=1.0, pad=-8)
        ax_r.set_ylabel('Rate (Hz)')
        ax_r.set_yscale('log')

        if has_alpha:
            plt.setp(ax_cost.get_xticklabels(), visible=False)
        else:
            ax_cost.set_xlabel('Total distance (km)')
        ax_cost.set_title(r'$(iii)$', y=1.0, pad=-8)
        ax_cost.set_ylabel('Parameter cost', labelpad=-0.1)
        ax_radar.set_title(r'$(iv)$', x=-0.1, y=1.1, pad=-8)
        ax_radar.set_xticks(theta)
        ax_radar.set_xticklabels([PARAM_LABELS[p] for p in params_with_cost])
        ax_radar.tick_params('x', width=0., labelsize=8., pad=2., length=2.)
        ax_radar.set_label('Cost')

        best_cost_per_distance = {}

        cost_min, cost_max = np.inf, -np.inf

        for nodes in results:
            color = NODES_TO_COLOR[nodes]
            for distance in results[nodes]:
                df = results[nodes][distance]
                targets = df['result']
                if targets['cost']['total'] > 1e9:
                    continue
                f, uf = targets['fidelity'], targets['fidelity_unc']
                r, ur = targets['rate'], targets['rate_unc']

                if 'distillation_strategy' not in df:
                    protocol = 0
                else:
                    strategy = df['distillation_strategy']
                    if not strategy[0] or strategy[0][1] == 0:
                        protocol = 0
                    else:
                        # even strategy implies EPL (1) and odd strategy DEJMPS (2,3,4)
                        protocol = 1 if strategy[0][0] == 'EPL' else strategy[0][1] + 1
                        if strategy[0][1] > 1:
                            print('WARNING! # distillation larger than one:')
                        print(nodes, distance, strategy)

                pos_x = distance  # + 5 * (-1 if nodes == 2 or nodes == 5 else 1)

                mk = PROTOCOL_TO_MARKER[protocol]
                if distinguish_eg:
                    if np.isclose(df['eps'], 0.):  # sc
                        color = NODES_TO_COLOR_SC[nodes]
                    else:
                        color = NODES_TO_COLOR_DC[nodes]
                ax_f.errorbar(pos_x, f, yerr=uf, c=color, marker=mk, markersize=markersize)
                ax_r.errorbar(pos_x, r, yerr=ur, c=color, marker=mk, markersize=markersize)

                cost_min = min(cost_min, targets['cost']['total'])
                cost_max = max(cost_max, targets['cost']['total'])

                ax_cost.plot(pos_x, targets['cost']['total'], c=color, marker=mk, markersize=markersize)
                if has_alpha:
                    ax_alpha.plot(pos_x, df['alpha'], c=color, marker=mk, markersize=markersize)

                if distance not in best_cost_per_distance\
                        or best_cost_per_distance[distance]['total'] > targets['cost']['total']:
                    targets['cost']['style'] = {
                        'color': color,
                        'marker': mk,
                        'ls': DISTANCES_TO_LS[distance],
                        'markersize': markersize
                    }
                    best_cost_per_distance[distance] = targets['cost']

        for _, costs in best_cost_per_distance.items():
            ys = [costs[param] / 5 if param == 'k_gates' else costs[param] for param in params_with_cost]
            ax_radar.plot(theta, ys, **costs['style'])

        target_fidelity, target_rate = TARGETS_DIRS[str(target)]
        ax_f.axhline(target_fidelity, color='k')
        ax_r.axhline(target_rate, color='k')
        ax_radar.set_rticks()

        cost_max = np.floor(np.log10(cost_max))
        cost_min = np.floor(np.log10(cost_min))

        if not force_linear_axes[target] and abs(cost_max - cost_min) >= 1.:
            ax_cost.set_yscale('log')

        if show_legend[target]:
            markers = [
                Line2D([0], [0], color='k', ls='None', marker=PROTOCOL_TO_MARKER[n], markersize=markersize)
                for n in range(len(PROTOCOL_TO_MARKER))
            ]

            if distinguish_eg:
                lines_nodes = [
                    Line2D([0], [0], color='None', marker='None'),
                    Line2D([0], [0], color='None', marker='None'),
                    *[
                        Line2D([0], [0], lw=2, color=(NODES_TO_COLOR_DC if j % 2 else NODES_TO_COLOR_SC)[n])
                        for j, n in enumerate([2, 2, 3, 3, 5, 5, 9, 9])
                    ]
                ]
                labels_nodes = [
                    'SC',
                    'DC',
                    *list(map(str, [0, 0, 1, 1, 3, 3, 7, 7] if show_as_repeaters else [2, 2, 3, 3, 5, 5, 9, 9])),
                ]
                nodes_legend = fig.legend(
                    lines_nodes, labels_nodes,
                    title='Number of repeaters', loc='upper center', ncol=5, bbox_to_anchor=(0.25, 1.)
                )
            else:
                lines_nodes = [Line2D([0], [0], lw=2, color=NODES_TO_COLOR[n]) for n in [2, 3, 5, 9]]
                nodes_legend = fig.legend(
                    lines_nodes, list(map(str, [0, 1, 3, 7] if show_as_repeaters else [2, 3, 5, 9])),
                    title='Number of repeaters', loc='upper center', ncol=4, bbox_to_anchor=(0.2, 1.)
                )
            plt.gca().add_artist(nodes_legend)
            protocol_legend = fig.legend(
                markers, ['SWAP-ASAP', 'EPL', 'DEJMPS (1)', 'DEJMPS (2)', 'DEJMPS (3)'],
                title='Strategies', loc='upper center',
                ncol=3 if distinguish_eg else len(PROTOCOL_TO_MARKER),
                bbox_to_anchor=(0.7, 1.)
            )

            if distinguish_eg:
                fig.subplots_adjust(top=0.8)
            else:
                fig.subplots_adjust(top=0.85)

        ax_radar.legend([
            Line2D([0], [0], color='k', ls=DISTANCES_TO_LS[n]) for n in [200, 400, 800]
        ], [f'{d}km' for d in [200, 400, 800]], loc='upper right', bbox_to_anchor=(1.5, 1.2))

        savefig(fig, outdir / str(target) / f'{prefix}_{target}_results')

        cols = [p.name for p in parameters if p.name != 'distillation_strategy']
        create_params_latex_table(results,
                                  cols,
                                  [200, 400, 800],
                                  outdir / str(target),
                                  prefix=prefix + '_' + str(target),
                                  show_as_repeaters=show_as_repeaters,
                                  include_targets=True,
                                  include_strategy=include_strategy,
                                  aliases=TABLE_ALIASES)

        create_dist_strategy_table(results, outdir / str(target),
                                   prefix=prefix + '_' + str(target),
                                   show_as_repeaters=show_as_repeaters)


def parse_folder_and_plot(
    basedir, parameters, include_strategy=False,
    show_as_repeaters=True, markersize=4, show_legend=True, radar_frame='circle', force_linear_axes=False
):
    RESULTS = [{2: {}, 3: {}, 5: {}, 9: {}} for _ in range(2)]

    outdir = basedir / 'output'

    for target in range(2):
        outdir_target = outdir / str(target)
        if outdir_target.exists():
            for simdir in outdir_target.iterdir():
                if match := SLURM_RE.match(simdir.name):
                    nodes = int(match.group(1))
                    dist = int(match.group(2))

                    if dist not in [200, 400, 800]:
                        continue

                    # best_file = simdir / 'best_result.csv'
                    best_file_params = simdir / 'best_result_params.pkl'

                    with open(best_file_params, 'rb') as f:
                        params = pickle.load(f)

                    if nodes == 2:
                        # add the cost of not optimising p2, T1 and T2
                        params['result']['cost']['total'] += 3.

                    if dist in RESULTS[target][nodes]:
                        params2 = RESULTS[target][nodes][dist]
                        if params['result']['cost']['total'] < params2['result']['cost']['total']:
                            RESULTS[target][nodes][dist] = params
                    else:
                        RESULTS[target][nodes][dist] = params
        else:
            RESULTS[target] = None

    plot_optimisation_results(
        basedir, parameters, RESULTS,
        show_as_repeaters=show_as_repeaters, markersize=markersize, include_strategy=include_strategy,
        show_legend=show_legend, radar_frame=radar_frame, force_linear_axes=force_linear_axes
    )
