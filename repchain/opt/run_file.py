"""
This script is run by inputting a 2-column parameter file directly in the console, i.e.:

python3 simple_example.py file_name

The parameters are `number_of_runs` and in addition all parameters
that should be inputted in the function
`repchain.runtools.run_simulation.run_many_simulations`
"""
import os

import numpy as np
from joblib import Parallel, delayed

from repchain.opt.ga import initial_population, next_generation
from repchain.opt.exploit import exploit_minimum


def find_best_per_generation(outputdir):
    generation = 0
    bests = []
    while os.path.exists(os.path.join(outputdir, f'{generation}.npy')):
        data = np.load(os.path.join(outputdir, f'{generation}.npy'))
        index = data[:, 0].argmin()
        bests.append(data[index])
        generation += 1

    data = np.array(bests)
    return data[:, 1:], data[:, 0]


def final_generation(
    exploit_func,
    final_func,
    params,
    outputdir,
    constraints=None,
    exploit_iters=5,
    kargs=()
):
    """Run a final step to find the best of the best individual.

    Due to the probabilistic nature of the problem, the best individual might not be found in the last generation
    as the cost is recomputed. This implies that, with the same parameters, we might be lucky and achieve low fitnesses
    while in the next ones this does not hold. For this reason we also pick `number_best` individuals of all
    generations to recompute its fitness.

    Parameters
    ----------
    fitness_func : func
        the function to minimise, must accept at least one parameter that will be the individual as a 1d array
        with the parameter values ordered as in `params`.
    params : list of `class:repchain.runtools.parameters.BaselineParameter`
        the parameters to modify.
    outputdir : path-like object
        the path where the generation files are saved.
    constraints : func or None, optional
        function that determines if a given individual satisfyes the constraints, returns `True` if it does.
        This must accept one parameter that will be a numpy 1d or 2d array where each row is an individual an
        the columns correspond with the `params`. Default to `None`.

        In order to take profit of the number implementation use numpy comparisons like `x[..., 2] > x[..., 4]`
        which checks that the values in the 2nd column are larger than those in the 4th. If `x` is a 1d arry then
        the ellipsis ensure you don't get an `IndexOutOfBounds` error.
    exploit_iters : int, optional
        the maximum number of iters to exploit the individual, default to 0 (no exploitation).
    kargs : tuple, optional
        the parameters to pass to the `fitness_func`

    Returns
    -------
    best_individual : numpy.ndarray
        the best individual found at this generation
    fitness : float
        the fitness for the previous individual
    """
    individuals, fitnesses = find_best_per_generation(outputdir)
    individuals, indices = np.unique(individuals, axis=0, return_index=True)
    fitnesses = fitnesses[indices]
    sorted = fitnesses.argsort().flatten()
    for index in sorted:
        if fitnesses[index] < 1e9:
            newx, cost = exploit_minimum(exploit_func,
                                         individuals[index], fitnesses[index],
                                         params, constraints, exploit_iters, kargs)
            print('Reduced cost to', fitnesses[index], cost)
        else:
            newx, cost = individuals[index], fitnesses[index]
        print(index, newx, cost)
        iter = 0
        while iter < 5:
            c = final_func(newx, *kargs)

            iter += 1
            if (fitnesses[index] < 1e9 and c < 1e9) or fitnesses[index] > 1e9:
                # if the fitnesss is already large, the solution can't be smaller but if it is low
                # then the solution can also be made small
                break
        if fitnesses[index] < 1e9 and c < 1e9 or fitnesses[index] > 1e9:
            break

    return newx, cost


def compute_generation(
    fitness_func,
    params,
    popsize,
    generation,
    number_selection,
    number_crossover,
    outputdir,
    init_mode='lhs',
    p_mutation=0.5,
    constraints=None,
    exploit=None,
    kargs=(),
    seed=None
):
    """Compute the n-th generation of a GA.

    The GA stores all data in the `outputdir` folder, each file is named as `n.npy` where `n` is the generation number.
    To open those files use the `np.load` function, this will import the 2d array that has a first column the
    fitness for the parameters of the next columns (ordered as in `params`).

    Parameters
    ----------
    fitness_func : func
        the function to minimise, must accept at least one parameter that will be the individual as a 1d array
        with the parameter values ordered as in `params`.
    params : list of `class:repchain.runtools.parameters.BaselineParameter`
        the parameters to modify.
    popsize : int
        the population size, note that the size of the first generation will be twice this number.
    generation : int
        the generation to evaluate.
        If 0 then an initial population is created using the method specified.
        If > 0 then the population is loaded from the file corresponding to the previous generation in outputdir.
    number_selection : int
        the number of parent individuals selected for the next generation.
    number_crossover : int
        the number of children individuals created through recombination of the parent individuals.
    outputdir : path-like object
        the path where to save the files, if it doesn't exist it is created in the first generation.
    init_mode : str, optional
        the way to initialise the first generation (0), this can be `random` or `lhs`. Default to `lhs`.
    p_mutation : float, optional
        the probability that a gen of an individual is mutated (this does not apply to the elitist individual).
        Default to `0.5`.
    constraints : func or None, optional
        function that determines if a given individual satisfyes the constraints, returns `True` if it does.
        This must accept one parameter that will be a numpy 1d or 2d array where each row is an individual an
        the columns correspond with the `params`. Default to `None`.

        In order to take profit of the number implementation use numpy comparisons like `x[..., 2] > x[..., 4]`
        which checks that the values in the 2nd column are larger than those in the 4th. If `x` is a 1d arry then
        the ellipsis ensure you don't get an `IndexOutOfBounds` error.
    exploit : dict or None, optional
        wether to use local search intervealed with GA. This parameter specifies the parameters to pass to the
        exploitation function as well as the period. The required parameters are:
        - `period`: number of generations after which exploitation is applied.
        - `number_best_individuals`: number of individuals to minimise, order from smaller to larger fitness.
        - `maxiter`: maximum number of iters to exploit.
    kargs : tuple, optional
        the parameters to pass to the `fitness_func`
    seed : int or None, optional
        the seed to use to initialise the `np.random.default_rng`, default to `None` (see `numpy` docs).

    Returns
    -------
    best_individual : numpy.ndarray
        the best individual found at this generation
    fitness : float
        the fitness for the previous individual
    """
    rng = np.random.default_rng(seed)

    if generation == 0:
        os.makedirs(outputdir, exist_ok=True)

        population, fitness = initial_population(fitness_func, params, constraints, 2 * popsize, popsize,
                                                 init_mode, rng, kargs)
    else:
        data = np.load(os.path.join(outputdir, f'{generation - 1}.npy'))
        scale_factor = np.exp(-generation / 200.)
        population = next_generation(rng, data[:, 1:], data[:, 0], params, constraints,
                                     popsize, number_selection, number_crossover, p_mutation, scale_factor)

        fitness = Parallel(n_jobs=-1)(
            delayed(fitness_func)(population[individual, :], *kargs)
            for individual in range(popsize)
        )

        fitness = np.array(fitness).reshape(-1)

    if exploit and (generation % exploit['period'] == 0):
        to_exploit = fitness.argsort()[:exploit['number_best_individuals']]
        for individual in to_exploit:
            newx, cost = exploit_minimum(fitness_func,
                                         population[individual, :], fitness[individual],
                                         params, constraints, exploit['maxiters'], kargs)

            if cost < fitness[individual]:
                fitness[individual] = cost
                population[individual, :] = newx[:]

    np.save(os.path.join(outputdir, f'{generation}.npy'),
            np.concatenate((fitness.reshape(-1, 1), population), axis=1))

    best_individual = fitness.argmin()

    return population[best_individual], fitness[best_individual]
