"""Implmentation of genetic algorithm for N-parameters."""
import os
import gc
import time
from itertools import product

import numpy as np
from joblib import Parallel, delayed


def get_parents(data, fitness, number_best_candidates, rng):
    """Select the N=number_best_candidates data points from the data based on their fitness."""
    # argsort orders from small to big, we are looking for the minimum so take the n first elements
    best = fitness.argsort()[:number_best_candidates]
    return data[best], fitness[best]


def get_parents_roulette(data, fitness, number_selection, rng):
    """Select N=number_best_candidates data points from data based on roulette wheel selection."""
    # convert fitness to probability, where smallest fitnesses have higher probability
    # prob = np.max(fitness) - 2 * np.min(fitness) - fitness
    prob = np.exp(-(fitness - np.min(fitness)))
    prob = prob / np.sum(prob)
    best_candidates = rng.choice(data, axis=0, size=number_selection, p=prob)
    return best_candidates


def mutate(data, p_mutation, scale_factor, params, constraints, rng):
    """Generate small changes in the data.

    The mutation probability is determined by the fitness of the data point. Parameters in the data points
    are muated randomly. The mutations size of each parameter is determined by the parameter scale_factor.

    Returns
    -------
    mutated_points : list (arrays)
        Mutated data points.
    """
    n_points, n_params = data.shape

    for i, param in enumerate(params):
        p = rng.random(n_points) < p_mutation
        count = np.count_nonzero(p)
        if count == 0:
            continue

        full_range = rng.random() < p_mutation
        if param.dtype == int:
            while count > 0:
                if full_range or True:
                    data[p, i] = rng.integers(param.baseline_value, param.perfect_value, size=count)
                else:
                    data[p, i] += (rng.integers(2, size=count) * 2 - 1)
                    data[data[:, i] < param.baseline_value, i] = param.perfect_value - 1
                    data[data[:, i] >= param.perfect_value, i] = param.baseline_value
                valid_points = constraints(data) if constraints else np.full(n_points, True)
                p &= (~valid_points)
                count = np.count_nonzero(p)
                full_range = True
        else:
            while count > 0:
                if full_range:
                    rnd = rng.random(count)
                    data[p, i] = rnd * (param.perfect_value - param.baseline_value) + param.baseline_value
                else:
                    rnd = rng.uniform(-scale_factor, scale_factor, count) * (param.perfect_value - param.baseline_value)
                    data[p, i] += rnd
                    data[p, i] = np.clip(data[p, i], param.baseline_value, param.perfect_value)

                valid_points = constraints(data) if constraints else np.full(n_points, True)
                p &= (~valid_points)
                count = np.count_nonzero(p)
                full_range = True

    return data


def crossover(data, number_crossover, constraints, rng):
    """Swap parameter values between the data points in data.

    Parameters
    ----------
    data : list (arrays)
        Array with the data points corresponding to the fittest and their
        mutations.

    Returns
    -------
    crossover_points: list (arrays)
        List with data points after crossover. If no population size defined, it
        creates N=number_best_candidates*number_parameters new points. The new
        points are created by randomly swapping parameters of the data points.
    """
    parents, n_params = data.shape

    crossover_points = np.zeros((number_crossover, n_params))

    i = 0
    while i < number_crossover:
        i1, i2 = rng.choice(parents, size=2, replace=False)
        crossover_location = rng.integers(1, n_params)
        crossover_points[i, :crossover_location] = data[i1, :crossover_location]
        crossover_points[i, crossover_location:] = data[i2, crossover_location:]
        if not constraints or constraints(crossover_points[i]):
            i += 1

    return crossover_points


def migration(number_points, params, constraints, rng):
    """Make a fully random sample of points.

    Constraints are evaluated to ensure that the returned array has the desired size
    and all points satisfy the constraints.

    Parameters
    ----------
    set_params : smartstopos.utils.SetParameters object

    Returns
    -------
    list_points
    """
    population = []
    while len(population) < number_points:
        points = np.zeros((number_points, len(params)))
        for index, param in enumerate(params):
            if param.dtype == int:
                points[:, index] = rng.integers(param.baseline_value, param.perfect_value, size=number_points)
            else:
                rnd = rng.random(number_points)
                points[:, index] = rnd * (param.perfect_value - param.baseline_value) + param.baseline_value

        valid = constraints(points) if constraints else np.full(number_points, True)
        population.extend(points[valid, :].tolist())

        del points

    population = np.array(population)
    if len(population) > number_points:
        population = rng.choice(population, number_points, axis=0)

    return population


def next_generation(rng, population, fitness, params, constraints,
                    popsize, number_selection, number_crossover,
                    p_mutation=0.5, scale_factor=-1):
    """population shape is 1 + # params where first position is the fitness, which is non-negative."""
    valid = ~(np.isnan(fitness) | np.isinf(fitness))
    population = population[valid]
    fitness = fitness[valid]

    if len(fitness) == 0:
        population = migration(popsize, params, constraints, rng)
    elif len(fitness) < number_selection:
        population = np.concatenate((population, migration(popsize - len(fitness), params, constraints, rng)), axis=0)
    else:
        if scale_factor < 0:
            scale_factor = rng.random()

        parents = get_parents_roulette(population, fitness, number_selection, rng)
        children = crossover(parents, number_crossover, constraints, rng)
        mutated = mutate(np.concatenate((parents, children), axis=0),
                         p_mutation, scale_factor, params, constraints, rng)
        if mutated.shape[0] + 1 < popsize:
            newpop = np.concatenate((mutated,
                                     migration(popsize - mutated.shape[0] - 1, params,  constraints, rng)))
        else:
            newpop = mutated[:popsize - 1, :]

        best = fitness.argmin()
        population = np.concatenate((population[best, :].reshape(1, len(params)), newpop), axis=0)

        del newpop
        del children
        del parents

    del fitness

    return population


def initial_population_random(rng, params, constraint, popsize, children, fitness_func, kargs):
    population = migration(popsize, params, constraint, rng)

    r = Parallel(n_jobs=-1)(
        delayed(fitness_func)(population[i], *kargs)
        for i in range(popsize)
    )

    fitness = np.array(r).reshape(-1)
    return get_parents(population, fitness, children, rng)


def initial_population_lhs(rng, params, constraints, popsize, children, fitness_func, kargs):
    def __fitness_func(x, args):
        if not constraints or constraints(x):
            return fitness_func(x, *args), x
        else:
            return np.nan, x

    n_params = len(params)
    points_per_param = max(2, int(np.log(popsize) / np.log(n_params)))

    points = np.repeat(np.linspace(0., 1., points_per_param, endpoint=True), n_params, axis=0)\
        .reshape(points_per_param, n_params)

    # slightly mutate the original points to make it less deterministic and try
    # different individuals after different rounds within 5% diff
    points += rng.uniform(-0.1, 0.1, size=points.shape)
    np.clip(points, 0.0, 1.0, points)

    for i, param in enumerate(params):
        if param.dtype == int:
            # max int value is not included
            points[:, i] *= (param.perfect_value - 1 - param.baseline_value)
            points[:, i] = np.floor(points[:, i])
        else:
            points[:, i] *= (param.perfect_value - param.baseline_value)

        points[:, i] += param.baseline_value

    index_params = list(range(n_params))

    r = Parallel(n_jobs=-1)(
        delayed(__fitness_func)(points[indices, index_params], kargs)
        for indices in product(list(range(points_per_param)), repeat=n_params)
    )

    fitness, population = zip(*r)
    population, fitness = np.array(population), np.array(fitness)
    valid = np.argwhere(~np.isnan(fitness)).reshape(-1)

    if np.count_nonzero(valid) < children:
        valid = rng.choice(valid, size=children)

    return get_parents(population[valid], fitness[valid], children, rng)


def initial_population(fitness_func, params, constraints, popsize, children, mode, rng, kargs):
    if mode == 'lhs':
        return initial_population_lhs(rng, params, constraints, popsize, children, fitness_func, kargs)
    else:
        return initial_population_random(rng, params, constraints, popsize, children, fitness_func, kargs)


def genetic_algorithm(fitness_func,
                      params,
                      popsize,
                      generations,
                      number_selection,
                      number_crossover,
                      init_mode='lhs',
                      p_mutation=0.5,
                      constraints=None,
                      kargs=(),
                      seed=None,
                      outputdir=None):
    """Create a new generation of data points based on genetic algorithms.

    Parameters
    ----------
    data : list
        Data from output file
    set_param : smartstopos object
        Set parameters to be explored
    sim_param : dict
        Simulation information, parameters for algorithm
    opt_step : int
        The current generation being generated (step number)

    Returns
    -------
    new_generation : list of arrays
        New set of data points to be explored. Each data point is a set of
        parameters values.
    """
    if outputdir:
        os.makedirs(outputdir, exist_ok=True)

    rng = np.random.default_rng(seed)

    if init_mode == 'lhs':
        population, fitness = initial_population_lhs(rng, params, constraints, 2 * popsize, popsize,
                                                     fitness_func, kargs)
    else:
        population, fitness = initial_population_random(rng, params, constraints, 2 * popsize, popsize,
                                                        fitness_func, kargs)

    gc.collect()

    best_fitness = fitness.min()
    extinction_prob = 0
    extinction_period = 20

    for gen in range(generations):
        if rng.integers(extinction_period) < extinction_prob:
            # we are kinda stuck, create a lot of new individuals to explore the space
            print('An extinction occurred')
            next_generation(rng, population, fitness, params, constraints,
                            popsize, 10, 10, p_mutation)
            extinction_prob = 0
        else:
            next_generation(rng, population, fitness, params, constraints,
                            popsize, number_selection, number_crossover, p_mutation)

        fitness = Parallel(n_jobs=-1)(
            delayed(fitness_func)(population[individual, :], *kargs)
            for individual in range(popsize)
        )

        fitness = np.array(fitness).reshape(-1)

        new_best = fitness.argmin()
        if np.isclose(fitness[new_best], best_fitness, rtol=1.e-3, atol=1.e-4):
            extinction_prob += 1

        best_fitness = new_best

        print(gen, fitness[new_best], population[new_best], np.mean(fitness), np.std(fitness))

        if outputdir:
            np.save(os.path.join(outputdir, f'{gen}.npy'),
                    np.concatenate((fitness.reshape(-1, 1), population), axis=1))

        # sleep for 20 seconds and then collect the rubish
        time.sleep(20)
        gc.collect()

    best = fitness.argmin()

    return population[best], fitness[best]
