"""This file includes tools to visualise and analyse optimisation results obtained through smartstopos.

It can plot the value of the cost function of the best parameter set for each generation,
as well as the corresponding fidelity and entanglement generation rate.

TODO Maybe import all these simulation related functions from elsewhere instead of defining them here.
"""
import numpy as np
import pandas as pd

from repchain.runtools.simulation_setup import SimulationSetup
from repchain.runtools.data_collection_tools import get_statetapper_between_fixed_nodes, get_distill_links_tapper


def add_default_tappers(sim_setup, fidelity_range=(0, 1), minimal=True):
    """Add the default data tappers to the given simulation instance and the stopping condition.

    By default the tappers added include the link between the end nodes and the number of distillations if
    `use_swap_only` is set to False.

    Parameters
    ----------
    sim_setup : `:class:repchain.runtools.simulation_setup.SimulationSetup`
        the simulation where to add the tappers
    fidelity_range : tuple of (float and float)
        the minimum and maximum fidelity of the end link to consider that the simulation has to stop.

    Returns
    -------
    list of tappers
        this list is identical to the list returned by sim_setup.datatappers
    """
    # Add data collection tools: recording fidelity once the protocol has finished
    # fidelity_range = (0, 1)  # [sim_setup.logic_params['swap_fidelity_threshold'], 1]
    statetapper = get_statetapper_between_fixed_nodes(
        sim_setup=sim_setup,
        fidelity_range=fidelity_range,
        apply_Hadamard_if_data_qubit=sim_setup.logic_params['respect_NV_structure'] and False,
        minimal=minimal)
    sim_setup.add_datatapper(statetapper)

    # Ensuring the simulation stops when end-to-end entanglement has been established
    node_index_A = 0
    node_index_B = sim_setup.number_of_nodes - 1
    for (nodeX, nodeY) in [(node_index_A, node_index_B), (node_index_B, node_index_A)]:
        sim_setup.protocols[nodeX]["logic"].should_stop_fn = \
            lambda a=nodeX, b=nodeY:\
            sim_setup.protocols[a]["memmanager"].number_of_links_with_exceeds(b, 0, fidelity_range)
    sim_setup.set_automatic_stop()

    distill_tapper = get_distill_links_tapper(sim_setup)
    sim_setup.add_datatapper(distill_tapper)

    return sim_setup.datatappers


def compute_rate_from_time(times):
    """Compute the rate based on a time array.

    Parameters
    ----------
    times : numpy.ndarray or pandas.DataFrame
        1 dimensional array with time information

    Returns
    -------
    rate : float
        the average rate computed as 1 / E[T]
    uncertainty : float
        the standard deviation on the rate, computed as V[T]/E[T]^2.
    """
    r = 1. / times.mean()
    return r, r**2 * times.std() / np.sqrt(len(times))


def run_many_simulations(input_dic, number_of_runs=200, duration=None, minimal=True):
    """Run a repeater chain simulation and compute basic statistics.

    Parameters
    ----------
    input_dic : dict
        the parameters to the based to the `SimulationSetup.from_parameters` function.
    number_of_runs : int, optional
        number of time to run the simulation, default to 200.
    duration : float, optional
        maximum simulation time, default to None (run until there are no more events).
    minimal : bool, optional
        determines the information to return, default to True.

    Returns
    -------
    `minimal = True`: tuple(float, float), tuple(float, float)
        only returns the expected fidelity and rate (in seconds) as (E[fidelity], V[fidelity]), tuple(E[rate], V[rate])
    `minimal = False`: pandas.DataFrame
        returns all information about each individual run

    See Also
    --------
    `class:repchain.runtools.simulation_setup.SimulationSetup`
    `method:repchain.runtools.datan_analysis_tools.add_default_tappers`
    """
    sim_setup = SimulationSetup.from_parameters(**input_dic)

    [statetapper, distill_tapper] = add_default_tappers(sim_setup, minimal=minimal)

    if number_of_runs > 1:
        sim_setup.perform_many_runs(
            number_of_runs,
            duration=duration
        )
    else:
        sim_setup.perform_run(duration)

    if minimal:
        fidelities = statetapper.dataframe['fidelity']
        times = statetapper.dataframe['time_stamp'] / 1.e9
        return (fidelities.mean(), fidelities.std() / np.sqrt(len(fidelities))), compute_rate_from_time(times)
    else:
        return pd.concat([statetapper.dataframe, distill_tapper.dataframe], axis=1)


def penalty(fidelity, target_fidelity, rate, target_rate):
    """
    Returns the cost function defined in "What are the worst repeaters we can get away with"
    for a set of parameters defined in input_dic with respect to a set of parameters defined in baseline_dic.

    Parameters
    ----------
    fidelity : float
        Fidelity achieved by parameter set.
    target_fidelity : float
        Minimum required fidelity.
    rate : float
        Rate achieved by parameter set.
    target_rate : float
        Minimum required rate.

    Returns
    -------
    float
        Value of cost function for the given parameters.
    """
    if np.isnan(fidelity) or np.isnan(rate):
        return np.inf

    constraints = (1 + (fidelity - target_fidelity)**2) * np.heaviside(target_fidelity - fidelity, 0)\
        + (1 + (target_rate - rate)**2) * np.heaviside(target_rate - rate, 0)

    return constraints


def log_param_cost(baseline, param):
    """Compute the cost associated to a hardware improvent from its baseline value."""
    return np.log(baseline) / np.log(param)


def cost(parameters, x):
    """Compute the cost associated to the passed parameters if possible.

    Parameters
    ----------
    params : list of `class:repchain.runtools.parameters.BaselineParameter`
        the list of parameters for which to calculate the cost
    baseline_values : list of float
        the baseline values for the previous parameters
    x : list of floats
        the list of parameters values, note that for each param `p` `x[p] > baseline_values[p]`

    Returns
    -------
    cost : list of float
        the cost associated to improving each parameter from its baseline value.
        If not `convert_to_prob_fn` is specified then the cost is `NaN`.
    """
    k = [0.0] * len(parameters)
    for i, param in enumerate(parameters):
        if param.convert_to_prob_fn:
            k[i] = param.cost(x[i])
    return k
