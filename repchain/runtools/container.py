from inspect import getfullargspec


class Container:
    """
    Class that acts as a "container", a "basket" where objects are collected and find values for the parameters they
    need. It acts like a node and might thus be superfluous.

    There are two types of objects that can be added to the Container: parameters (simple objects) and class objects
    (advanced objects). Parameters are for example "dark_count_probability" and "index_in_the_chain",
    where class objects are e.g. a :class:`repchain.utils.memorymanager.MemoryManager` or
    :class:`repchain.protocol.distillation_protocol.LocalDistillationProtocol`. The difference is, is that
    class objects need parameters as input, which they find in the `Container`.

    Additional functionality: adding callbacks that are called when `Container.start()` is called.

    Attributes
    ----------
    items : dict of str and Any or None
        the parameters or classes to add to the container

    Notes
    -----
    Be careful to pass all the required parameters before the classes, otherwise they won't completely
    initialised if some parameter is missing.

    Example
    -------

    >>> class MyClass:
    >>>
    >>>     def __init__(self, myparameter):
    >>>         self.myparameter = myparameter
    >>>
    >>> container = Container()
    >>> self.container.add_simple_item(name='myparameter', value=3)
    >>> self.container.add_cls(name='myclass', cls=MyClass)
    >>> myclass_object = self.container['myclass']
    >>> myclass_object.myparameter
    3
    """

    def __init__(self, items=None):
        if items is not None:
            self._items = items
        else:
            self._items = {}

        self._callbacks = []

    def add_callback_during_start(self, callback_fn):
        """Functions to execute when the container starts.

        Parameters
        ----------
        callback_fn : func
            function that does not take any arguments
        """
        self._callbacks.append(callback_fn)

    def remove_item(self, name):
        """removes a simple or class item

        Parameters
        ----------
        name : str
            name of the item to delete

        Raises
        ------
        KeyNotFoundError
            if the item does not belong to the container
        """
        del self._items[name]

    def __setitem__(self, name, value):
        if name in self._items:
            raise ValueError("Name {} already among the items".format(name))
        self._items[name] = value

    def __getitem__(self, name):
        return self._items[name]

    def add_simple_item(self, name, value):
        """Add a parameter to the container

        Parameters
        ----------
        name : str
            parameter name
        value : Any
            value of the parameter
        """
        self[name] = value

    def add_multiple_simple_items(self, items):
        """Add multiple simple parameters to the container

        Parameters
        ----------
        items : dict of str and Any
        """
        for name, value in items.items():
            self[name] = value

    def add_cls(self, name, cls):
        """Add a class to the container

        Parameters
        ----------
        name : str
            parameter name
        cls : class
            class that it is initialised with all the parameters that it accepts

        Raises
        ------
        KeyNotFoundError
            if one of the required parameters by `cls` is not inside the container
        """

        # find names of the input parameters
        param_names = getfullargspec(cls.__init__).args

        # find values for input parameters
        params = {
            param_name: self._items[param_name]
            for param_name in param_names
            if param_name != 'self'
        }

        # initialize
        obj = cls(**params)
        self.add_simple_item(name=name, value=obj)

    def _perform_function_of_all(self, fn_name):
        for name in self._items:
            try:
                method = getattr(self._items[name], fn_name)
                method()
            except Exception:
                pass

    def start(self):
        """Starts the container, runs all the `start` functions of the classes and runs the callback functions"""
        self._perform_function_of_all(fn_name='start')
        for callback_fn in self._callbacks:
            callback_fn()

    def stop(self):
        """Stops the container by running all the `stop` functions of the classes"""
        self._perform_function_of_all(fn_name='stop')

    def reset(self):
        """resets the container by calling `stop` and `start`"""
        self.stop()
        self.start()
