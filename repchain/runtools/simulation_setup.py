import logging
import traceback

import netsquid as ns

from repchain.runtools.umbrellaprotocol import create_umbrellaprot
from repchain.utils.tools import index_of_Pauli_product
import repchain.network.abstract_network_factory as network_factory


DEFAULT_NETWORK_PARAMS = {
    "number_of_nodes": 3,
    "tot_num_qubits": 4,
    "qmem_config_params": None,
    "distributor_params": None,
    "internode_distance": 1,
    "c": 3e5 / 1.44
}

DEFAULT_LOGIC_PARAMS = {
    "action_ordering": "default",
    "is_move_magical": True,
    "swap_fidelity_threshold": 0.8,
    "delivery_params": {
        "alpha": 1.,
        "cycle_time": 3.5e3,
        "elementary_link_fidelity": 1.,
        "elementary_link_succ_prob": 1.
    },
    "use_two_node_NV": False,
    "use_swap_only": True,
    "distillation_scheduler": "default",
    "distillation_strategy": None,
    "respect_NV_structure": False,
    'distillation_program': 'DEJMPS'
}


class SimulationSetup(ns.Entity):
    """
    Class that supports easy setup and running of simulations.

    Attributes
    ----------
    network : :obj:`~netsquid.nodes.network`
        network component that handles the nodes and connections between them
    number_of_nodes : int
        number of repeaters in the chain
    indices : list of ints
        indices of the repeaters
    delivery_params : dict of (str, float)
        contains information about the elementary link generation. Necessary keys:
        *  cycle_time : delay between link generation trials
        *  elementary_link_fidelity : fidelity of the generated link
        *  elementary_link_succ_prob : probability of succefully realising entanglement
    class_channels_send : dict of (int, dict of (str, :obj:`~netsquid.components.cchannel.ClassicalChannel`))
        classical channel used to send information for each repeater and direction,
        the first key identifies the repeater index and the second the direction (one of "L" or "R").
    class_channels_receive : dict of (int, dict of (str, :obj:`~netsquid.components.cchannel.ClassicalChannel`))
        classical channel used to receive information for each repeater and direction,
        the first key identifies the repeater index and the second the direction (one of "L" or "R").
    distributors : list of :obj:`~repchain.physical.magic_distributor.MagicDistributor`
        magic distributors of entanglement per node
    respect_NV_structure : bool
        if set to True, uses the quantum processor specially designed for NV centers,
        otherwise a general processor is used
    """
    def __init__(
        self,
        network,
        number_of_nodes,
        indices,
        delivery_params,
        class_channels_send,
        class_channels_receive,
        distributors,
        logic_params=None,
        network_params=None
    ):
        if number_of_nodes >= 9 and logic_params['use_swap_only']:
            ns.set_qstate_formalism(ns.QFormalism.KET)
        else:
            ns.set_qstate_formalism(ns.QFormalism.DM)

        ns.sim_reset()

        self.network = network

        self.number_of_nodes = number_of_nodes
        self.indices = indices
        self.delivery_params = delivery_params
        self.class_channels_send = class_channels_send
        self.class_channels_receive = class_channels_receive
        self.distributors = distributors

        self.logic_params = DEFAULT_LOGIC_PARAMS
        if logic_params:
            self.logic_params.update(logic_params)
        self.network_params = DEFAULT_NETWORK_PARAMS
        if network_params:
            self.network_params.update(network_params)

        self.protocols = [
            create_umbrellaprot(
                index=index,
                network=self.network,
                delivery_params=self.delivery_params[index],
                number_of_nodes=self.number_of_nodes,
                class_channels_send=self.class_channels_send,
                class_channels_receive=self.class_channels_receive,
                distributors=self.distributors,
                logic_params=self.logic_params,
                network_params=self.network_params
            ) for index in self.indices
        ]

        self.datatappers = []
        self._stop_handler = ns.EventHandler(lambda event: self._check_stop())
        self._automatic_stop = False
        self.reset()
        self.simulation_start_times = []

    def reset(self):
        """
        Calls the `reset()` function of:

          * abstractnetwork
          * umbrellaprotocols
        """
        self.network.reset()
        all_channels = list(self.class_channels_send.values()) + list(self.class_channels_receive.values())
        for channel_dictionary in all_channels:
            for __, channel in channel_dictionary.items():
                channel.reset()
        for distributor in self.distributors:
            distributor.reset()
        for index in self.indices:
            self.protocols[index].reset()
            self.protocols[index]['distilprot'].distill_attempts = {}
            node = self.network.get_node(name="Abstractnode{}".format(index))
            node.qmemory.reset()
        for datatapper in self.datatappers:
            datatapper.reset()
        if self._automatic_stop:
            self._register_stop_handler()
        self._number_of_stopped_end_nodes = 0

    def _post_data_gathering(self):
        if self.logic_params['use_swap_only']:
            n = self.number_of_nodes - 1
            for node_id, neighbour_id, other_id in [(0, 1, n), (n, n - 1, 0)]:
                prot = self.protocols[node_id]
                memorymanager = prot['memmanager']
                links = memorymanager.entangled_links_with(neighbour_id)
                if len(links) > 0:
                    link = links[0]
                    link.remote_node_id = other_id
                    entswapprot = prot['entswapprot']
                    link.cor_Pauli = index_of_Pauli_product(entswapprot.cor_pauli, link.cor_Pauli)
        else:
            self.distill_attempts = {
                node: self.protocols[node]['distilprot'].distill_attempts
                for node in self.indices
            }

        for datatapper in self.datatappers:
            datatapper.perform_postsimulation_collection()

    def _check_stop(self):
        """Checks that both end nodes have triggered an `EVT_FINISHED` event, if that is true the simulation stops."""
        self._number_of_stopped_end_nodes += 1
        if self._number_of_stopped_end_nodes == 2:
            logging.info(f'{ns.sim_time()} stopping on purpose')
            ns.sim_stop()

    def add_datatapper(self, datatapper):
        self.datatappers.append(datatapper)

    def clear_datatappers(self):
        for datatapper in self.datatappers:
            datatapper.reset()

    def set_automatic_stop(self):
        """
        Makes the node stop automatically once the two end nodes
        in the chain both have stopped (i.e. their `have_stopped`
        properties are both True).
        """
        self._automatic_stop = True
        self._register_stop_handler()

    def _register_stop_handler(self):
        self._wait(self._stop_handler, event_type=self.protocols[0]['logic'].EVT_FINISHED)

    def perform_run(self, duration=None):
        """
        Calls `netsquid.sim_run()`. After NetSquid has finished running, call the `perform_postsimulation_collection`
        method of all :obj:`~repchain.runtools.data_collection_tools.DataTapper` objects
        which were added using `add_datatapper`.
        """
        stats = ns.sim_run(duration=duration)
        self._post_data_gathering()
        return stats

    def perform_many_runs(self, number_of_runs, duration=None):
        """Call `perform_run` many times and resets both NetSquid and itself afterwards."""
        succefull_runs = 0
        times_target_reached = 0
        # self.reset()
        # ns.sim_reset()
        stats = None
        for run_index in range(number_of_runs):
            logging.info(f'{ns.sim_time()} Simulation run {run_index}/{number_of_runs}')
            self.simulation_start_times.append(ns.sim_time())
            try:
                stats = self.perform_run(duration=duration)
                succefull_runs += 1
                if duration and (ns.sim_time() - duration) < 0.001:
                    times_target_reached += 1
            except Exception as e:
                logging.debug(f'Silumation number {run_index} failed: ' + str(e))
                # traceback.print_exc()
            if times_target_reached > 0.4 * number_of_runs:
                break
            if run_index < number_of_runs:
                self._remove_all_qubits()
                self.reset()
                ns.sim_reset()
                self.reset()
        return succefull_runs, stats

    def _remove_all_qubits(self):
        # This function is only here because
        # NetSquid's sim_reset() function
        # resets all components, but while
        # resetting the quantum processor,
        # first applies T1T2 noise to all qubits,
        # which yields an error since the
        # last_accessed time of those qubits
        # is later than 0, the time to which
        # the netsquid simulator is reset.
        # In order to avoid this, we remove
        # all qubits manually
        for index in self.indices:
            node = self.network.get_node(name="Abstractnode{}".format(index))
            for mem_position in node.qmemory.mem_positions:
                mem_position.busy = False
                mem_position.get_qubit(remove=True, skip_noise=True)

    @classmethod
    def from_factory(cls, factory, logic_params=None, network_params=None):
        """Create a SimulationSetup object from a `AbstractNetworkFactory` object.

        Parameters
        ----------
        factory : :class:`repchain.network.abstract_network_factory.AbstractNetworkFactory`

        Returns
        -------
        SimulationSetup

        See Also
        --------
        repchain.network.abstract_network_factory
        """
        return cls(
            network=factory.network,
            number_of_nodes=factory.number_of_nodes,
            indices=factory.indices,
            delivery_params=factory.delivery_params,
            class_channels_send=factory.class_channels_send,
            class_channels_receive=factory.class_channels_receive,
            distributors=factory.distributors,
            logic_params=logic_params,
            network_params=network_params
        )

    @staticmethod
    def from_config_file(cls, network_config_file):
        """Create a SimulationSetup object from a network configuration file.

        Parameters
        ----------
        network_config_file : str
            path to the configuration file

        Returns
        -------
        SimulationSetup

        See Also
        --------
        repchain.network.abstract_network_factory.create_network_from_file
        """
        factory = network_factory.create_network_from_file(network_config_file)
        return SimulationSetup.from_factory(factory)

    @staticmethod
    def from_parameters(
        number_of_nodes=3,
        tot_num_qubits=4,
        internode_distance=200,
        T1=3.6e18,
        T2=1e15,
        p_q1init=1.,
        p_q1gate=0.9986,
        p_q2gate=0.96,
        p_q1meas=0.99,
        cycle_time=3.5e3,
        elementary_link_fidelity=1.,
        p_loss_length=0.,
        elementary_link_succ_prob=1.,
        t_q1gate=0.,
        t_q1init=0.,
        t_q1meas=0.,
        t_q2gate=0.,
        werner=False,
        eps=0.,
        use_swap_only=True,
        swap_fidelity_threshold=0.,
        c=206753.41931034482,
        distillation_scheduler="default",
        respect_NV_structure=False,
        action_ordering="default",
        is_move_magical=True,
        distillation_strategy=None
    ):
        """Create a SimulationSetup object from the network parameters.

        Parameters
        ----------
        number_of_nodes : int, optional
            number of repeaters in the chain, default to 3.
        tot_num_qubits : int, optional
            number of qubits per repeater, default to 4.
        internode_distance : float, optional
            distance between nodes in m, defalut to 0.002.
        T1 : float, optional
            dephasing time in ns, default to 0.
        T2 : float, optional
            dephasing time in ns, default to 0.
        swap_quality : float, optional
            swap fidelity, default to 0.
        cycle_time : float, optional
            time between link generation in ns, default to 3500.
        elementary_link_fidelity : float, optional
            fidelity of the elementary link, defalut to 1.
        p_loss_length : float, optional
            probability of lossing a photon during transmission, default to 0.
        elementary_link_succ_prob : float, optional
            probability of succefully generating entanglement, default to 1.
        single_qubit_gate_duration : float, optional
            duration of single qubit gates in ns, default to 0.
        initialization_duration : float, optional
            duration of qubit initialization in ns, default to 0.
        measurement_duration : float, optional
            duration of a qubit measurement in ns, default to 0.
        two_qubit_gate_duration : float, optional
            duration of two qubit gates in ns, default to 0.
        werner : bool, optional
            use a Werner state in the simulation, default to False,
        eps : float, optional
            Quantifies divergence from Werner state, default to 0.
        use_swap_only : bool, optional
            wether to use swap only, default to True.
        swap_fidelity_threshold : float, optional
            minimum fidelity to consider a swap succefull, default to 0.
        c : float, optional
            light velocity in m/s, default to 206753.41931034482 (light velocity in optical fiber).
        distillation_scheduler : :obj:`~repchain.logic.distillation_strategy.DistillationStrategy` or "default"
            distillation strategy to use, default to :obj:`~repchain.logic.distillation_strategy.GreedyTopDownStrategy`
        respect_NV_structure : bool, optional
            Determines whether a generic (CNOT-measure) quantum circuit is used for the local operations
            of distillation, or one that is tailored to the NV center (see
            :class:`repchain.protocols.distillation_protocol.NVDistillationProgram`), default to False
        action_ordering : list of str or "default", optional
            determines the order in which swapping/distilling/entanglement generation/checking
            the mailbox of messages is performed.

            Example: `["SWAP", "MSG", "DIST", "ENTGEN"]`, then first, it is checked whether it is possible to perform a
            swap ("SWAP") operation, and if so, the corresponding action is returned by the `get_action` function.
            If not, then it is checked whether there are old messages ("MSG") which result in an action,
            and if so, then the corresponding action is returned. If not, then consider "DIST",
            followed by considering "ENTGEN".
        is_move_magical : bool, optional
            Determines, together with `respect_NV_structure`, which quantum circuit to use
            for moving the state from the electron to the carbon, default to True.
            See: :obj:`~repchain.protocols.moveprotocol`
        distillation_strategy : dict of int and (str, int) or (str, int) or None
            this controls how links are distilled per level, the dictionary keys run from 0 to `l` where `2^l + 1`
            is the number of nodes in the chain and the value at a particular level has information about the program
            used (see :class:`repchain.protocols.distillation_protocol.IDistillationProgram`) and
            the number of time to distill a link at that given level. If None, no distillation is used but the
            nesting bdcz strategy remain and if a single pair of (str, int) is specified then the same strategy is used
            at all the levels. Default to `('DEJMPS', 1)`.

        Returns
        -------
        SimulationSetup

        See Also
        --------
        repchain.network.abstract_network_factory.create_network_from_dic
        """
        network_params = {
            "number_of_nodes": number_of_nodes,
            "internode_distance": internode_distance,
            "qmem_config_params": {
                "p_q1gate": p_q1gate,
                "p_q1init": p_q1init,
                "p_q1meas": p_q1meas,
                "p_q2gate": p_q2gate,
                "T1": T1,
                "T2": T2,
                "tot_num_qubits": tot_num_qubits,
                "t_q1gate": t_q1gate,
                "t_q1init": t_q1init,
                "t_q1meas": t_q1meas,
                "t_q2gate": t_q2gate
            },
            "distributor_params": {
                "p_loss_length": p_loss_length,
                "elementary_link_fidelity": elementary_link_fidelity,
                "elementary_link_succ_prob": elementary_link_succ_prob,
                "werner": werner,
                "eps": eps,
            },
            "c": c
        }

        logic_params = {
            "respect_NV_structure": respect_NV_structure,
            "delivery_params": {
                "elementary_link_fidelity": elementary_link_fidelity,
                "elementary_link_succ_prob": elementary_link_succ_prob,
                "cycle_time": cycle_time,
                "c": c
            },
            "distillation_strategy": distillation_strategy,
            "use_swap_only": use_swap_only or distillation_strategy is None,
            "swap_fidelity_threshold": swap_fidelity_threshold,
            "action_ordering": action_ordering,
            "is_move_magical": is_move_magical,
            'distillation_scheduler': distillation_scheduler
        }

        return SimulationSetup.from_dict(network_params, logic_params)

    @staticmethod
    def from_dict(network_params, logic_params):
        """Create a SimulationSetup object from two dictionaries containing the network and logic parameters.

        Parameters
        ----------
        network_params : dict of keys=str and values=Any
            Dictionary of parameters that will be passed on as input
            to the :obj:`~repchain.network.abstractnetwork.AbstractNetwork`
        logic_params : dict of keys=str and values=Any
            Dictionary of parameters that will be passed on to the
            method `~repchain.runtools.umbrellaprototocol.create_umbrellaprot`

        Returns
        -------
        SimulationSetup

        See Also
        --------
        repchain.network.abstract_network_factory.create_network_from_dic
        """
        return SimulationSetup.from_factory(
            network_factory.create_network_from_dic(network_params, logic_params),
            logic_params, network_params
        )
