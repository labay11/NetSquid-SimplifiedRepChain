"""
Main function in this file is 'create_umbrellaprot', which returns
a :obj:`~repchain.runtools.container.Container` object which is a basket
where subprotocols and logic are connected to the physical network and
to each other.
"""

# the container which will hold everything
from repchain.runtools.container import Container

# the four subprotocols
from repchain.protocols.entswapProtocol import EntSwapProtocol
from repchain.protocols.magic_entgen_protocol import MagicEntGenProtocol
from repchain.protocols.distillation_protocol import LocalDistillationProtocol
from repchain.protocols.moveprotocol import MoveProtocol

# the decision logic
from repchain.logic.umbrellalogic import UmbrellaLogic

# memory tracking
from repchain.utils.memorymanager import MemoryManager

# miscellaneous
from repchain.utils.timlogging import logging
from repchain.utils.tools import get_possible_directions, contains_multiple_messages
from repchain.utils.bcdzfunctions import num2level


def _get_messages(class_channel, direction):
    """
    Returns
    -------
    list of objects send over the classical channel
        (usually, these objects are Message objects)
    """
    channel_content = class_channel.receive()
    msgs = []
    if contains_multiple_messages(channel_content):
        msgs = list(channel_content[0])
    else:
        # (msg, deltaT) = channel_content
        msg, __ = channel_content
        msgs = [msg]
    for msg in msgs[0]:
        logging.debug("class channel {} receiving {} in direction {}".format(class_channel, msg, direction))
    # in NetSquid, for some reason its Channel.get method
    # returns an extra list around it. We remove this outer layer
    # and just return a list of the objects that were sent
    return msgs[0]


def _register_classical_handler(container, direction, class_channel):
    # TODO clean up
    def callback_fn(event, class_channel=class_channel, direction=direction):
        return container['logic'].process_msgs(direction=direction, msglist=_get_messages(class_channel, direction))
    class_channel.register_handler(callback_function=callback_fn)


def _normalise_distillation_strategy(distillation_strategy, swap_fidelity_threshold, number_of_nodes):
    levels = num2level(number_of_nodes)
    if not distillation_strategy:
        # when no distillation is specified then the swap fidelity threshold is set to 0 to swap links
        # as soon as possible
        swap_fidelity_threshold = [0.] * (levels + 1)  # {level: 0. for level in range(levels + 1)}
        distillation_strategy = [None] * (levels + 1)  # {level: None for level in range(levels + 1)}
        return distillation_strategy, swap_fidelity_threshold
    else:
        if isinstance(distillation_strategy, tuple):
            distillation_strategy = [distillation_strategy] * (levels + 1)
        elif not isinstance(distillation_strategy, list):
            raise ValueError('Invalid type passed to distillation_strategy, '
                             'valid types are tuple or dict of tuples.')
        # if isinstance(swap_fidelity_threshold, float):
        #     swap_fidelity_threshold = [swap_fidelity_threshold] * (levels + 1)

        if len(distillation_strategy) != levels + 1:
            raise ValueError(f'Length of distillation_strategy ({len(distillation_strategy)}) '
                             f'is different than number of levels ({levels + 1}).')

        distill = []
        swap = []
        for level in range(levels + 1):
            if distillation_strategy[level] and distillation_strategy[level][1] > 0:
                distill.append(distillation_strategy[level])
                swap.append(1.)
            else:
                distill.append(None)
                swap.append(0.)

        return distill, swap


def create_umbrellaprot(
    index,
    network,
    number_of_nodes,
    class_channels_send,
    class_channels_receive,
    distributors,
    delivery_params,
    logic_params,
    network_params
):
    """
    Creates a container with all the protocols for a given repeater in the network.

    Returns
    -------
    :obj:`~repchain.runtools.container.Container`

    See Also
    --------
    :obj:`~repchain.runtools.container.simulation_setup.SimulationSetup`
    """

    container = Container()

    # setting 'elementary' parameters
    container.add_simple_item('index', index)
    container.add_simple_item('number_of_nodes', number_of_nodes)
    container.add_simple_item('qmemory', network.get_node(name="Abstractnode{}".format(index)).qmemory)
    container.add_simple_item('procDev', container['qmemory'])
    container.add_simple_item('num_positions', container['qmemory'].num_positions)
    container.add_simple_item('class_channels', class_channels_send[index])

    # get an arbitrary channel in order to find the delay between sending and receiving of a message
    cchannel = list(container['class_channels'].values())[0]
    container.add_simple_item('sleep_delay', cchannel.delay_mean)
    distributor_L = distributors[index - 1] if index != 0 else None
    container.add_simple_item('magic_distributor_L', distributor_L)
    distributor_R = distributors[index] if index != container['number_of_nodes'] - 1 else None
    container.add_simple_item('magic_distributor_R', distributor_R)
    container.add_simple_item('respect_NV_structure', logic_params['respect_NV_structure'])
    container.add_simple_item('is_move_magical', logic_params['is_move_magical'])

    use_swap_only = logic_params['use_swap_only']
    container.add_simple_item('use_swap_only', use_swap_only)
    container.add_simple_item('swap_update_receivers', [0, number_of_nodes - 1] if use_swap_only else None)

    # set logical choices
    container.add_simple_item('discarding_strategy', None)
    container.add_simple_item('distillation_scheduler', logic_params['distillation_scheduler'])
    container.add_simple_item('action_ordering', logic_params['action_ordering'])
    container.add_simple_item('delivery_params', delivery_params)
    container.add_simple_item('elementary_link_fidelity', delivery_params['elementary_link_fidelity'])

    distill_strategy, swap_threshold = _normalise_distillation_strategy(logic_params['distillation_strategy'],
                                                                        logic_params['swap_fidelity_threshold'],
                                                                        number_of_nodes)
    container.add_simple_item('distillation_strategy', distill_strategy)
    container.add_simple_item('swap_fidelity_threshold', swap_threshold)

    # add memorymanager
    container.add_cls('memorymanager', MemoryManager)
    container.add_simple_item('memmanager', container['memorymanager'])
    container.add_simple_item('midpoint_outcome_to_cor_Pauli_fn', None)

    # add subprotocols
    container.add_cls('entswapprot', EntSwapProtocol)
    container.add_cls('moveprot', MoveProtocol)
    container.add_cls('distilprot', LocalDistillationProtocol)
    container.add_cls('entgenprot', MagicEntGenProtocol)

    # add logic
    container.add_cls('logic', UmbrellaLogic)
    # container.add_callback_during_start(container['logic'].trigger)

    # ensure the items received on the channels will be
    # forwarded to the logic
    # TODO clean up
    container.add_simple_item('class_channels_receive', class_channels_receive[index])
    for direction in get_possible_directions(number_of_nodes=container['number_of_nodes'], index=index):
        cchannel = container['class_channels_receive'][direction]
        if cchannel is not None:
            def fn(container=container, direction=direction, class_channel=cchannel):
                return _register_classical_handler(container, direction, class_channel)
            container.add_callback_during_start(callback_fn=fn)
    return container
