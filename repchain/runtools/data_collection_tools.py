from collections import namedtuple
from collections.abc import MutableMapping

import numpy as np
import netsquid as ns

from repchain.utils.tools import index_of_Pauli_product,\
    apply_Pauli_to_one_side_of_bell_state,\
    bell_index_to_ket_state,\
    compute_fidelity, RIGHT
from repchain.utils.memorymanager import ELECTRON_POSITION
from repchain.utils.bcdzfunctions import get_table


class DataTapper(ns.util.datacollector.DataCollector):
    """
    Extension to :obj:`netsquid.util.datacollector.DataCollector`
    which is able to collect information when the simulation
    has finished.

    Parameters
    ----------
    get_data_function : function
        Function that has 'event' as first parameter and returns a dict.
        Identical to input of :obj:`netsquid.util.datacollector.DataCollector`.
    collect_postsimulation : bool
        Whether also should collect data once the simulation has finished.
        If set to `False`, then identical to the
        :obj:`netsquid.util.datacollector.DataCollector`.
    """

    def __init__(self, get_data_function, name, collect_postsimulation=True):
        super().__init__(get_data_function=get_data_function)
        self.priority = 1
        self.name = name
        self._collect_postsimulation = collect_postsimulation

    def reset(self):
        pass

    def perform_postsimulation_collection(self):
        if self._collect_postsimulation:
            # NOTE the code below is directly copied from
            # netsquid.util.datacollector.DataCollector.callback
            # The reason for not calling this function is that
            # it takes an event as input which should have a source
            data = self._get_data_function(event=None)
            source_name = "entity"
            if data is None:
                return
            elif isinstance(data, MutableMapping):
                if self._include_entity_name:
                    data[self.column_name_entity_name] = source_name
                if self._include_time_stamp:
                    data[self.column_name_time_stamp] = ns.sim_time()
                if len(data) > 0:
                    self._data_buffer.append(data)
            # end copy of code

            # TODO does this work in general, or do we need a
            # general EVT_SIMULATION_FINISHED event type?
            # a hack would be:
            #   event = self._schedule_now(EVT_SIMULATION_FINISHED)
            #   self.get_data_function(event=event)


###############
# "Factories" #
###############


def get_simstoptapper(check_fn):
    """
    Factory method for a datatapper which stops the simulation
    (i.e. it calls `netsquid.sim_stop()` when the evaluation
    of `check_fn` yields True.

    **Note:** the method `collect_on` should still be used in order
    to determine when `check_fn` will be called.

    Parameters
    ----------
    check_fn : function that takes no arguments and returns a bool

    Returns
    -------
    :obj:`repchain.runtools.data_collection_tools.DataTapper`
    """
    def _stop_simulation(event, check_fn=check_fn):
        if check_fn():
            ns.sim_stop
        return {}

    return DataTapper(get_data_function=_stop_simulation,
                      name="SimStopTapper",
                      collect_postsimulation=False)


def get_waitingtimetapper(collect_postsimulation=True):
    """
    Factory method for a datatapper which records timestamps.

    Returns
    -------
    :obj:`repchain.runtools.data_collection_tools.DataTapper`
    """
    return DataTapper(get_data_function=lambda event: {"time": ns.sim_time()},
                      name="WaitingTimeTapper",
                      collect_postsimulation=collect_postsimulation)


def get_distill_links_tapper(sim_setup):
    def func(event):
        if sim_setup.logic_params['use_swap_only']:
            attempts = {}
        else:
            attempts = sim_setup.distill_attempts

        out = {}
        for i in range(sim_setup.number_of_nodes):
            table = get_table(sim_setup.number_of_nodes, i)[RIGHT]
            for _, nghb in table.items():
                if nghb == -1:
                    continue
                key = f'{i}_{nghb}'

                if i in attempts and nghb in attempts[i]:
                    out[key + '_s'] = attempts[i][nghb]['s']
                    out[key + '_f'] = attempts[i][nghb]['f']
                else:
                    out[key + '_s'] = 0
                    out[key + '_f'] = 0
        return out

    return DataTapper(
        get_data_function=func,
        name='DistillLinksTapper',
        collect_postsimulation=True)


def get_dmtapper(memorymanager_A, memorymanager_B,
                 qmemory_A, qmemory_B,
                 node_index_A, node_index_B,
                 collect_postsimulation,
                 apply_Hadamard_if_data_qubit=True):
    """
    Factory method for a datatapper which records the density
    matrix for each bipartite two-qubit state of which one qubit
    lives in `qemory_A` and the other in `qmemory_B`.

    Parameters
    ----------
    memorymanager_A : :obj:`repchain.utils.memorymanager.MemoryManager`
    memorymanager_B : :obj:`repchain.utils.memorymanager.MemoryManager`
    qmemory_A : :obj:`netsquid.components.qprocessor.QuantumProcessor`
    qmemory_B : :obj:`netsquid.components.qprocessor.QuantumProcessor`
    node_index_A : int
    node_index_B : int
    collect_postsimulation : bool
    apply_Hadamard_if_data_qubit : bool

    Returns
    -------
    :obj:`repchain.runtools.data_collection_tools.DataTapper`
    """
    def get_dm(event):
        lambda event: _DMTapperTools._get_states(
            memorymanager_A=memorymanager_A,
            memorymanager_B=memorymanager_B,
            node_index_A=node_index_A,
            node_index_B=node_index_B,
            qmemory_A=qmemory_A,
            qmemory_B=qmemory_B,
            apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit)
    return DataTapper(get_data_function=get_dm,
                      name="DMTapper",
                      collect_postsimulation=collect_postsimulation)


def get_statetapper(memorymanager_A, memorymanager_B,
                    qmemory_A, qmemory_B,
                    node_index_A, node_index_B,
                    collect_postsimulation,
                    apply_Hadamard_if_data_qubit=True,
                    fidelity_range=None,
                    minimal=True):
    """
    DataTapper for collecting not only the density matrix of all links
    shared between two nodes, but also:

      * their memory positions in their respective quantum memories
      * the corresponding Pauli correction operations to make the
        state (in the absence of all noise) become :math:`Psi^+`
      * the time at which the state was created
      * the fidelity of the state with :math:`Psi^+`, after
        applying the Pauli corrections and performing
        the Hadamard rotation on the data qubits
        (the latter only when `apply_Hadamard_if_data_qubit` is `True`).

    Parameters
    ----------
    memorymanager_A : :obj:`repchain.utils.memorymanager.MemoryManager`
    memorymanager_B : :obj:`repchain.utils.memorymanager.MemoryManager`
    qmemory_A : :obj:`netsquid.components.qprocessor.QuantumProcessor`
    qmemory_B : :obj:`netsquid.components.qprocessor.QuantumProcessor`
    node_index_A : int
    node_index_B : int
    collect_postsimulation : bool
    apply_Hadamard_if_data_qubit : bool

    Returns
    -------
    :obj:`repchain.runtools.data_collection_tools.DataTapper`
    """
    def get_state(event):
        infos = _StateTapperTools._get_infos(
            memorymanager_A=memorymanager_A,
            memorymanager_B=memorymanager_B,
            node_index_A=node_index_A,
            node_index_B=node_index_B,
            qmemory_A=qmemory_A,
            qmemory_B=qmemory_B,
            apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit,
            fidelity_range=fidelity_range,
            minimal=minimal)
        return infos
    return DataTapper(get_data_function=get_state,
                      name="StateTapper",
                      collect_postsimulation=collect_postsimulation)


def get_statetapper_between_fixed_nodes(sim_setup,
                                        node_indices=None,
                                        collect_postsimulation=True,
                                        apply_Hadamard_if_data_qubit=True,
                                        fidelity_range=(0, 1),
                                        minimal=True):
    """
    A statetapper that taps the state of all freshly generated links
    between the nodes with indices specified in `node_indices`.
    The statetapper is not yet  "activated", i.e. its function
    `collect_on` has not yet been called.

    Parameters
    ----------
    sim_setup : :obj:`repchain.simulation_setup.SimulationSetup`
    node_indices : list of int
    collect_postsimulation : bool
    apply_Hadamard_if_data_qubit : bool

    Returns
    -------
    :obj:`repchain.runtools.data_collection_tools.DataTapper`
    """

    # input checks
    if node_indices is None:
        node_indices = [0, sim_setup.number_of_nodes - 1]

    if len(node_indices) != 2:
        raise ValueError
    for node_index in node_indices:
        if not isinstance(node_index, int) or node_index < 0:
            raise ValueError

    # set up the statetapper
    A = node_indices[0]
    B = node_indices[1]

    node1 = sim_setup.network.get_node(name="Abstractnode{}".format(A))
    node2 = sim_setup.network.get_node(name="Abstractnode{}".format(B))

    statetapper = get_statetapper(
        memorymanager_A=sim_setup.protocols[A]['memmanager'],
        memorymanager_B=sim_setup.protocols[B]['memmanager'],
        qmemory_A=node1.qmemory,
        qmemory_B=node2.qmemory,
        node_index_A=A,
        node_index_B=B,
        collect_postsimulation=collect_postsimulation,
        apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit,
        fidelity_range=fidelity_range,
        minimal=minimal)

    # NOTE we do *not* "activate" the statetapper, i.e.
    # we do not calls its function `collect_on`

    return statetapper


class _DMTapperTools:
    """
    Tools for collecting the density matrix between two nodes.
    """

    @staticmethod
    def _get_states(memorymanager_A, memorymanager_B,
                    node_index_A, node_index_B, qmemory_A,
                    qmemory_B, apply_Hadamard_if_data_qubit,
                    fidelity_range):
        position_tuples = \
            _DMTapperTools._identify_positions(
                memorymanager_A=memorymanager_A,
                memorymanager_B=memorymanager_B,
                node_index_A=node_index_A,
                node_index_B=node_index_B,
                fidelity_range=fidelity_range)
        states = []
        for (pos_A, pos_B) in position_tuples:
            state = \
                _DMTapperTools._position_tuple_to_state(
                    pos_A=pos_A,
                    pos_B=pos_B,
                    qmemory_A=qmemory_A,
                    qmemory_B=qmemory_B,
                    apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit)
            states.append(state)
        return {"states": states}

    @staticmethod
    def _identify_positions(memorymanager_A, node_index_A,
                            memorymanager_B, node_index_B,
                            fidelity_range=None):
        """
        Returns
        -------
        list of tuples (int, int)
            Each tuple `(pos_A, pos_B)` represents a single two-qubit entangled
            link, where `pos_A` (`pos_B`) is the qubit position
            at node A (B).
        """
        positions_at_A = \
            memorymanager_A.entangled_positions_with(remote_node_id=node_index_B, fidelity_range=fidelity_range)
        positions_at_B = []

        # identify the corresponding memory position on Bs side
        for pos_at_A in positions_at_A:
            link_at_A = memorymanager_A.get_link(pos=pos_at_A)
            pos = memorymanager_B.find_pos(remote_node_id=node_index_A, link_id=link_at_A.link_id)
            positions_at_B.append(pos)
        return list(zip(positions_at_A, positions_at_B))

    @staticmethod
    def _position_tuple_to_state(pos_A, qmemory_A, pos_B, qmemory_B,
                                 apply_Hadamard_if_data_qubit):
        """
        Returns
        -------
        reduced density matrix
        """
        [qubit_A] = qmemory_A.peek(positions=[pos_A])
        [qubit_B] = qmemory_B.peek(positions=[pos_B])
        reduced_dm = ns.qubits.qubitapi.reduced_dm([qubit_A, qubit_B])

        # TODO should also make deep copy of reduced_dm in order to avoid
        # acting on the original state?

        # make fresh qubits in order not to change the QState
        # of the qubits mid-simulation
        [qA, qB] = ns.qubits.qubitapi.create_qubits(2)
        ns.qubits.qubitapi.assign_qstate(qubits=[qA, qB], qs_repr=reduced_dm)
        if apply_Hadamard_if_data_qubit:
            for (pos, qubit) in [(pos_A, qA), (pos_B, qB)]:
                if pos != ELECTRON_POSITION:
                    ns.qubits.qubitapi.operate(qubit, ns.qubits.operators.H)
        return ns.qubits.qubitapi.reduced_dm([qA, qB])


def _dm_to_dict(dm, split_imaginary=False, prefix='dm', out=None):
    """Convert a matrix into a dict.

    Parameters
    ----------
    dm : numpy.ndarray of dimension (M, N)
        the matrix to convert.
    split_imaginary : bool, optional
        if True, the real and imaginary parts will be divided into two keys ending with `r` and `i` respectively.
        Otherwise, each element will have the same type as the input matrix. Default to False.
    prefix : str, optional
        how to call each item in the dict, the keys are formatted as {prefix}{i}{j} where `i` and `j` are the
        components of the element in the matrix: i for rows and j for columns. Default to `dm`.
    out : dict, optional
        the dict where to add the components of the dm. If None, a new one is created and returned. Default to None.

    Returns
    -------
    dict
        a dictionary that cointains the M*N elements of the density matrix indexed as {prefix}{i}{j}.
        If `split_imaginary = True`, then the dictionary contains 2*M*N items of float type indexed as {prefix}{i}{j}r
        for the real part and {prefix}{i}{j}i for the imaginary part.
    """
    if not out:
        out = {}
    M, N = dm.shape  # will raise if dm has a dimension different than 2
    for i in range(M):
        for j in range(N):
            if split_imaginary:
                out['{}{}{}r'.format(prefix, i, j)] = np.real(dm[i, j])
                out['{}{}{}i'.format(prefix, i, j)] = np.imag(dm[i, j])
            else:
                out['{}{}{}'.format(prefix, i, j)] = dm[i, j]
    return out


class _StateTapperTools(_DMTapperTools):

    @staticmethod
    def _get_infos(memorymanager_A, memorymanager_B, node_index_A,
                   node_index_B, qmemory_A, qmemory_B,
                   apply_Hadamard_if_data_qubit,
                   fidelity_range, minimal):
        """
        Returns
        -------
        dict with state info
        """
        position_tuples = \
            _DMTapperTools._identify_positions(
                memorymanager_A=memorymanager_A,
                node_index_A=node_index_A,
                memorymanager_B=memorymanager_B,
                node_index_B=node_index_B,
                fidelity_range=fidelity_range)

        stateinfo = None
        for pos_A, pos_B in position_tuples:
            info = \
                _StateTapperTools._get_info(
                    pos_A=pos_A,
                    memorymanager_A=memorymanager_A,
                    qmemory_A=qmemory_A,
                    pos_B=pos_B,
                    memorymanager_B=memorymanager_B,
                    qmemory_B=qmemory_B,
                    apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit)
            stateinfo = {
                'time': ns.sim_time(),
                'node_index_A': node_index_A,
                'node_index_B': node_index_B,
                'memory_position_A': info[0],
                'memory_position_B': info[1],
                'correction_Pauli_A': info[2],
                'correction_Pauli_B': info[3],
                'fidelity': info[5]
            }
            if not minimal:
                _dm_to_dict(info[4], prefix='dm', split_imaginary=True, out=stateinfo)
            break

        first_stateinfo = stateinfo if stateinfo else {
            'time': ns.sim_time() * 100,
            'node_index_A': node_index_A,
            'node_index_B': node_index_B,
            'memory_position_A': np.nan,
            'memory_position_B': np.nan,
            'correction_Pauli_A': np.nan,
            'correction_Pauli_B': np.nan,
            'fidelity': 0.0
        }

        if minimal:
            return {
                'time': first_stateinfo['time'],
                'fidelity': first_stateinfo['fidelity']
            }

        return first_stateinfo

    @staticmethod
    def _get_info(pos_A, memorymanager_A, qmemory_A,
                  pos_B, memorymanager_B, qmemory_B,
                  apply_Hadamard_if_data_qubit):
        """
        Returns
        -------
        tuple (int, int, numpy array, int, float)
            Tuple is (memory position at A, memory position at B,
                      correction pauli at A, correction pauli at B,
                      density matrix,
                      fidelity)
        """
        # get density matrix
        dm = _DMTapperTools._position_tuple_to_state(
                pos_A=pos_A,
                qmemory_A=qmemory_A,
                pos_B=pos_B,
                qmemory_B=qmemory_B,
                apply_Hadamard_if_data_qubit=apply_Hadamard_if_data_qubit)

        # get correction Paulis
        pauli_A = memorymanager_A.get_link(pos=pos_A).cor_Pauli
        pauli_B = memorymanager_B.get_link(pos=pos_B).cor_Pauli

        # get product of correction Paulis
        pauli_product = index_of_Pauli_product(pauli_A, pauli_B)

        # get reference bell index from correction Paulis
        reference_bell_index = \
            apply_Pauli_to_one_side_of_bell_state(
                bell_index=1,
                pauli_index=pauli_product)

        # compute fidelity
        reference_ket = \
            bell_index_to_ket_state(bell_index=reference_bell_index)
        fidelity = \
            compute_fidelity(
                dm=dm,
                reference_ket=reference_ket,
                squared=True)

        # if fidelity < 0.01:
        #     reference_bell_index = apply_Pauli_to_one_side_of_bell_state(reference_bell_index, 2)
        #     reference_ket = bell_index_to_ket_state(reference_bell_index)
        #     fidelity = compute_fidelity(dm, reference_ket, squared=True)

        return (pos_A, pos_B, pauli_A, pauli_B, dm, fidelity)
