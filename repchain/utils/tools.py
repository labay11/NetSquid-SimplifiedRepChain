from netsquid.qubits.qubitapi import create_qubits, operate
from netsquid.qubits.operators import Operator, CNOT, H, I, X, Y, Z
from netsquid.components.instructions import INSTR_X, INSTR_Y, INSTR_Z
from netsquid import pydynaa
from netsquid.qubits.dmtools import DMState
from netsquid.qubits.ketstates import b00, b01, b10, b11
import numpy as np


###################
# Bell state tools
###################

COR_PAULI_TO_OPERATOR = {1: X, 2: Y, 3: Z}  # the identity gate is omitted on purpose
COR_PAULI_TO_INSTRUCTION = {1: INSTR_X, 2: INSTR_Y, 3: INSTR_Z}
COR_PAULI_TO_PSIPLUS = {0: 1, 1: 0, 2: 2, 3: 3}

LEFT = 'L'
RIGHT = 'R'

DIRECTIONS = [LEFT, RIGHT]


def is_valid_pauli(pauli):
    """Checks if the given pauli index is valid.

    Parameters
    ----------
    pauli : int
        the pauli index, between 0 and 3 being 0->I, 1->X, 2->Y, 3->Z.

    Raises
    ------
    ValueError
        if pauli is not an interger or dis not within 0 and 3 (both included)
    """
    if not (isinstance(pauli, int) and 0 <= pauli <= 3):
        raise ValueError("Pauli index {} not among 0, 1, 2, 3".format(pauli))
    return pauli


def is_valid_bell_index(bell_index):
    """Checks if the given pauli index is valid.

    Parameters
    ----------
    bell_index : int
        the bell index index, between 0 and 3 being 0->Φ+, 1->Ψ+, 2->Φ-, 3->Ψ-.

    Raises
    ------
    ValueError
        if pauli is not an interger or dis not within 0 and 3 (both included)
    """
    if not (isinstance(bell_index, int) and 0 <= bell_index <= 3):
        raise ValueError("Bell index {} not among 0, 1, 2, 3".format(bell_index))
    return bell_index


def commutes_with_Z(pauli_index):
    return pauli_index == 0 or pauli_index == 3


def commutes_with_X(pauli_index):
    return pauli_index == 0 or pauli_index == 1


def conjugate_with_hadamard(pauli_index):
    is_valid_pauli(pauli_index)

    if pauli_index == 0 or pauli_index == 2:
        return pauli_index
    elif pauli_index == 1:
        return 3
    else:  # 3
        return 1


def bell_index_to_stabilizer_coefficients(bell_index):
    r"""
    Each of the four Bell states can be written using two generators of their
    stabilizer group: :math:`a \cdot X \otimes X` and :math:`b \cdot Z \otimes Z`
    where :math:`X` and :math:`Z` are the X- and Z-pauli-matrices, respectively,
    and :math:`a, b` take values {+1, -1}.

    Parameters
    ----------
    bell_index : int
        Translation: 0=Phi^+, 1=Psi^+, 2=Phi^-, 3=Psi^-

    Returns
    -------
    a : int
        coefficient of the stabilizer :math:`X\otimes X`
    b : int
        coefficient of the stabilizer :math:`Z\otimes Z`
    """
    bell_index = is_valid_bell_index(bell_index)

    if bell_index == 0:
        return (1, 1)
    elif bell_index == 1:
        return (1, -1)
    elif bell_index == 2:
        return (-1, 1)
    else:  # 3
        return (-1, -1)


def stabilizer_coefficients_to_bell_index(stabilizer_coefficients):
    r"""
    Each of the four Bell states can be written using two generators of their
    stabilizer group: :math:`a \cdot X \otimes X` and :math:`b \cdot Z \otimes Z`
    where :math:`X` and :math:`Z` are the X- and Z-pauli-matrices, respectively,
    and :math:`a, b` take values {+1, -1}.

    Parameters
    ----------
    stabilizer_coefficients: (int, int)
        The coefficients :math:`(a, b)`.


    Returns
    -------
    bell_index : int
        Translation: 0=Phi^+, 1=Psi^+, 2=Phi^-, 3=Psi^-
    """
    if not isinstance(stabilizer_coefficients, tuple) or len(stabilizer_coefficients) != 2:
        raise TypeError("Parameter `stabilizer_coefficients` should be a tuple of length two.")
    (x, z) = stabilizer_coefficients
    if not (x in [-1, 1] and z in [-1, 1]):
        raise ValueError("Both coefficients should be either +1 or -1")
    else:
        if x == 1:
            return 0 if z == 1 else 1
        else:
            return 2 if z == 1 else 3


def apply_Hadamard_on_each_qubit(bell_index):
    (c_X, c_Z) = bell_index_to_stabilizer_coefficients(bell_index)
    # apply hadamards means flipping the coefficients
    return stabilizer_coefficients_to_bell_index((c_Z, c_X))


def apply_Pauli_to_one_side_of_bell_state(bell_index, pauli_index):
    (c_X, c_Z) = bell_index_to_stabilizer_coefficients(bell_index)
    if not commutes_with_X(pauli_index):
        c_X *= -1
    if not commutes_with_Z(pauli_index):
        c_Z *= -1
    return stabilizer_coefficients_to_bell_index((c_X, c_Z))


def bell_index_to_ket_state(bell_index):
    """
    Parameters
    ----------
    bell_index : int
        Translation: 0=Phi^+, 1=Psi^+, 2=Phi^-, 3=Psi^-

    Returns
    -------
    :obj:`~netsquid.qubits.kettools.KetState`
    """
    is_valid_bell_index(bell_index)
    if bell_index == 0:
        return b00
    elif bell_index == 1:
        return b01
    elif bell_index == 2:
        return b10
    else:  # 3
        return b11


def index_of_Pauli_product(Pauli_index_A, Pauli_index_B):
    """
    Returns the index of the product of two single-qubit
    Pauli operators, while ignoring their phase. The
    translation is made as 0=Id, 1=X, 2=Y, 3=Z.

    Example
    -------
    Since X.Y = iZ (with i the imaginary unit),
    calling
    >>> index_of_Pauli_product(1, 2)
    >>> # returns 3

    Parameters
    ----------
    Pauli_index_A : int
    Pauli_index_B : int

    Returns
    -------
    int
    """
    Pauli_index_A = is_valid_pauli(Pauli_index_A)
    Pauli_index_B = is_valid_pauli(Pauli_index_B)

    if Pauli_index_A == 0:
        # A = Id so A * B = B
        return Pauli_index_B
    elif Pauli_index_B == 0:
        # B = Id so A * B = A
        return Pauli_index_A
    elif Pauli_index_A == Pauli_index_B:
        # for every Pauli P, we have P^2 = Id
        return 0
    elif Pauli_index_A == 1:
        # A = X so:
        return 3 if Pauli_index_B == 2 else 2
    elif Pauli_index_A == 2 and Pauli_index_B == 3:
        return 1

    return index_of_Pauli_product(Pauli_index_B, Pauli_index_A)


def pauli_index_to_get_to_psiplus(bell_index):
    return COR_PAULI_TO_PSIPLUS[is_valid_bell_index(bell_index)]


def does_precisely_one_commute_with_Z(pauli_index_A, pauli_index_B):
    """Checks if only one of the pauli matrices specified commutes with `Z`.

    Parameters
    ----------
    pauli_index_A : int
        first pauli index, between 0 and 3.
    pauli_index_B : int
        second pauli index, between 0 and 3.

    Returns
    -------
    bool
        if only one of the two matrices commutes with `Z`.

    Raises
    -------
    ValueError
        if any of the two indices is not a pauli matrix
    """
    does_A_commute = commutes_with_Z(pauli_index_A)
    does_B_commute = commutes_with_Z(pauli_index_B)
    # return (does_A_commute and not does_B_commute) or (does_B_commute and not does_A_commute)
    return does_A_commute ^ does_B_commute


# def whosontheotherend(myindex, connection):
#    remote_index = connection.idA
#    return remote_index if myindex != remote_index else connection.idB

# def _check_canStart(qmem_posA, qmem_posB):
#    assert(qmem_posA is not None)
#    assert(qmem_posB is not None)
#    assert(qmem_posA != qmem_posB)

def stop_simulation():
    pydynaa.DynAASim().stop()


def is_valid_direction(direction):
    if direction in ['L', 'l', -1]:
        return LEFT
    elif direction in ['R', 'r', 1]:
        return RIGHT

    raise TypeError('Direction {} is not one of "L/R", "l/r" or "1/-1".'.format(direction))


def is_outermost_node(number_of_nodes, index, direction=None):
    if direction is None:
        return np.any([
            is_outermost_node(number_of_nodes, index, direction)
            for direction in DIRECTIONS
        ])

    return index == 0 if is_valid_direction(direction) == LEFT else index == number_of_nodes - 1


def neighbour_index(number_of_nodes, index, direction=LEFT):
    pm = 1 if is_valid_direction(direction) == RIGHT else -1
    return None if is_outermost_node(number_of_nodes, index, direction) else index + pm


def get_possible_directions(number_of_nodes, index):
    return [
        direction
        for direction in DIRECTIONS
        if not is_outermost_node(number_of_nodes, index, direction)
    ]


def _add_Bell_pair(qmemA, qmemB, posA, posB, bell_index=0):
    """
    bell_index : int
        0=Phi_plus, 1=Psi_plus, 2=Phi_minus, 3=Psi_minus
    """
    # TODO refer to _add_noisy_Bell_pair
    # putting a Bell pairs (Phi_plus) in the node's memories by
    # - let each node have a qubits in state |0>
    # - nodeA applies H to its qubit
    # - we apply a nonlocal CNOT
    for (qmem, pos) in [(qmemA, posA), (qmemB, posB)]:
        qubits = create_qubits(1)
        qmem.put(qubits=qubits, positions=[pos])

    [qA] = qmemA.peek(positions=posA)
    [qB] = qmemB.peek(positions=posB)

    operate(qubits=[qA], operator=H)
    operate(qubits=[qA, qB], operator=CNOT)

    bellindex2operator = {0: I,
                          1: X,
                          2: Z,
                          3: Y}
    operate(qubits=[qB], operator=bellindex2operator[bell_index])


_oneoneprojector = \
    np.array([[0., 0., 0., 0.],
              [0., 0., 0., 0.],
              [0., 0., 0., 0.],
              [0., 0., 0., 1.]])
_oneoneprojop = Operator(name="oneoneproj_unnormalized", matrix=_oneoneprojector)


def _add_noisy_Bell_pair(qmemA, qmemB, posA, posB, p, bell_index=1):
    """
    Outputs the state F * |b> + (1-F) * |11><11|
    where |b> is a bell state,
    where F^2 = p.

    Parameters
    ----------
    bell_index : int
        Either 0, 1, 2 or 3, where:
          * 0 : Phi^+
          * 1 : Psi^+
          * 2 : Phi^-
          * 3 : Psi^-
    """
    F = np.sqrt(p)
    qubits = {}
    for qmem, pos, name in [(qmemA, posA, "A"), (qmemB, posB, "B")]:
        qmem.create_qubits_at(positions=[pos])
        [qubits[name]] = qmem.peek(positions=pos)

    if bell_index >= 2:
        operate(qubits=[qubits["A"]], operator=X)
    if bell_index % 2 == 1:
        operate(qubits=[qubits["B"]], operator=X)
    operate(qubits=[qubits["A"]], operator=H)
    operate(qubits=[qubits["A"], qubits["B"]], operator=CNOT)
    bell_state_dm = qubits["A"].qstate.dm
    DMState(qubits=[qubits["A"], qubits["B"]], dm=F * bell_state_dm + (1 - F) * _oneoneprojector)

    assert np.isclose(np.trace(qubits["A"].qstate.dm), 1.)


def reverse_direction(direction):
    """returns the opposite direction to the given one, `L <-> R`"""
    return LEFT if is_valid_direction(direction) == RIGHT else RIGHT


def contains_multiple_messages(connection_content):
    return isinstance(connection_content[0], tuple)


def compute_fidelity(dm, reference_ket, squared=False):
    """Computes the fidelity between a density matrix and a ket.

    Squared: :math:`F = |<\\psi|\\rho|\\psi>|^2`.

    Not Squared: :math:`F = |<\\psi|\\rho|\\psi>|`.


    Parameters
    ----------
    dm : numpy.ndarray
        Density matrix.
    reference_ket : numpy.ndarray
        ket
    squared : bool, optional
        if false, the returned fidelity is the square root of the element :math:`<\\psi|\\rho|\\psi>`.
        Default to False.

    Notes
    -----
    See also :class:`netsquid.qubits.qubitapi.fidelity`.
    """
    fid = (reference_ket.conj().T @ dm @ reference_ket)[0, 0]
    return np.real(fid if squared else np.sqrt(fid))
