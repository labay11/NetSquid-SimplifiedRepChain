import logging
import inspect

import netsquid as ns

# set netsquid debug logging
# uncomment the following line if you want netsquid-type logging to be off
# from netsquid import logger
# logger.setLevel(logging.DEBUG)
# logging.basicConfig(level=logging.DEBUG)

_previous_time = None


def _make_spaces(num_of_spaces):
    return "\t\t\t" * num_of_spaces


def _get_timestring():
    time = ns.sim_time()
    global _previous_time
    if _previous_time is None or time > _previous_time:
        _previous_time = time
        logging.info("-" * 96)
    num_of_spaces = len(str(time))
    string = " " * (10 - num_of_spaces)
    return string + str(time)


def _get_logstring(index_sender, index_receiver, msg, position):
    """logstring for sending classical message"""
    msg = msg[0]
    if isinstance(msg, list):
        msgstring = ""
        for m in msg:
            msgstring += str(m)
    else:
        msgstring = str(msg)
    logstring = "{} --[{}]--> {}".format(index_sender, msgstring, index_receiver)
    return _get_timestring() + _make_spaces(position + 1) + logstring


def log_send(myindex, remotename, msg):
    return _get_logstring(index_sender=myindex,
                          index_receiver=remotename,
                          msg=msg,
                          position=myindex)


def log_received(myindex, remotename, msg):
    return _get_logstring(index_sender=remotename,
                          index_receiver=myindex,
                          msg=msg,
                          position=myindex)


def log_localinfo(myindex, info):
    return _get_timestring() + _make_spaces(myindex + 1) + info


def build_extensive_kwargs(fn, *args, **kwargs):
    function_signature = inspect.signature(fn)
    extensive_kwargs = function_signature.bind_partial(*args, **kwargs)
    self = extensive_kwargs.arguments.get('self', None)

    return ['{}={}'.format(key, val) for key, val in extensive_kwargs.arguments.items() if key != 'self'], self


def printing(prefix=None, suffix=None):
    """A decorator that wraps the passed in function and logs the input and output.

    The formatted string contains:

        [prefix: ]function_name(inputs) -> output [(suffix)]

    The parts in [] are optional. Here function_name reffers to `__qualname__` (see python docs).

    Parameters
    ----------
    prefix : str or func, optional
        what to write before the function name, default to None
    suffix : str or func, optional
        what to write after the outputs

    Returns
    -------
    decorator

    Notes
    -----
    Both `prefix` and `suffix` accept a function, if the decoratred function belongs to a class then
    the `self` parameter in the input will be passed to the `prefix` and `suffix` functions (if any) so that
    you can use it to print more detailed information about the class that is calling the function.
    """
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                output = func(*args, **kwargs)
            except Exception as e:
                output = e

            inputs, self = build_extensive_kwargs(func, *args, **kwargs)
            err = '{}({}) -> {}'.format(
                func.__qualname__,
                ', '.join(inputs),
                output)

            if prefix:
                err = '{}: {}'.format(
                    prefix(self) if callable(prefix) else prefix,
                    err
                )
            if suffix:
                err = '{} ({})'.format(
                    err,
                    suffix(self) if callable(suffix) else suffix
                )

            logging.info('{: >10} | {}'.format(int(ns.sim_time()), err))

            if isinstance(output, Exception):
                raise output

            return output

        return wrapper
    return decorator


def print_all_methods(prefix='', suffix='', exclude=None):
    """A decorator that wraps the passed class and logs the input and output for all functions.

    The formatted string contains:

        [prefix: ]function_name(inputs) -> output [(suffix)]

    The parts in [] are optional. Here function_name reffers to `__qualname__` (see python docs).
    The decorator is added to all the methods of the class unless they are explicitely excluded.

    Parameters
    ----------
    prefix : str or func, optional
        what to write before the function name, default to None
    suffix : str or func, optional
        what to write after the outputs
    excluded : list of str, optional
        excluded methods from printing, default to None which excludes the [`__init__`].
        If you also want to include the init method then pass an empty list.

    Returns
    -------
    decorator

    See Also
    --------
    :meth:`printing`
    """
    if exclude is None:
        exclude = ['__init__']

    def decorate(cls):
        for attr in cls.__dict__:
            if callable(getattr(cls, attr)) and attr not in exclude:
                setattr(cls, attr, printing(prefix, suffix)(getattr(cls, attr)))
        return cls
    return decorate
