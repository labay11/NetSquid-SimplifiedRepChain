from repchain.logic.task import Task


class Message:
    """Classical message interface for sending over a classical connection.

    There are three different "actions" for a message:
      * Swapping
      * Distilling
      * Neighbouring entanglement generation

    In addition to this, there are different subjects for a message:
      * Ask: the sender requests action
      * Confirm: the receiver confirms that it will start the action
      * Reject: the receiver rejects the sender's request
      * Distillation outcome (only for distillation): message that contains the distillation outcome in `content`.
      * Swap (only for swap): required subject for a swap message.

    Attributes
    ----------
    sender : int
        The id of the node that sent that message.
    action : str
        Indicates the topic of the message: entanglement generation/distillation/swapping.
        Should be among one of the following: `Action.SWAP`, `Action.DIST` and `Action.ENTGEN`.
    subject : str
        Indicates the subject of the message. The list of available subjects can be found at
        repchain.utils.message.Message.SUBJECTS but note that not all of them are available for all the actions,
        to see the list of subjects per actions take a look at repchain.utils.message.Message.ALLOWED_SUBJECTS_BY_ACTION
    intended_receiver : int, optional
        the id of the receiver node
    content : Any
        Can be used to store any addional data. In the case of a distillation outcome,
        this contains the distillation outcome. In the case of a swap update,  and possibly a correction Pauli.

    See Also
    --------
    repchain.logic.action.Action

    Notes
    -----
    It is strongly recommended to initialise this class via one of the available class methods:

     * :class:`repchain.utils.message.Message.swap`
     * :class:`repchain.utils.message.Message.distill`
     * :class:`repchain.utils.message.Message.from_task`

    See the documentation of any of the previous methods for more details.
    """

    # standard header types
    # TODO: do not hardcode again all actions, use Action ones
    ACTION_SWAP = 'SWAP'
    ACTION_ENTGEN = 'ENTGEN'
    ACTION_DIST = 'DIST'

    ACTIONS = [ACTION_SWAP, ACTION_ENTGEN, ACTION_DIST]

    # standards content types
    SUBJECT_SWAP = 'SWAP'
    SUBJECT_ASK = 'ASK'
    SUBJECT_CONFIRM = 'CONFIRM'
    SUBJECT_REJECT = 'REJECT'
    SUBJECT_DIST_OUTCOME = 'DISTOUTCOME'

    SUBJECTS_ALL = [SUBJECT_SWAP, SUBJECT_ASK, SUBJECT_CONFIRM, SUBJECT_REJECT, SUBJECT_DIST_OUTCOME]
    ALLOWED_SUBJECTS_BY_ACTION = {
        ACTION_SWAP: [SUBJECT_SWAP],
        ACTION_ENTGEN: [SUBJECT_ASK, SUBJECT_CONFIRM, SUBJECT_REJECT],
        ACTION_DIST: [SUBJECT_ASK, SUBJECT_CONFIRM, SUBJECT_REJECT, SUBJECT_DIST_OUTCOME]
    }

    def __init__(self, sender, action, subject, content=None,
                 intended_receiver=None, expires_after=None):
        # the sender is the index of the node, and should be of type `int`
        assert isinstance(sender, int)
        self.sender = sender

        # the header is the topic of the message
        self.action = Message._check_action(action)
        self.subject = Message._check_subject(subject, action)
        self.content = content

        self.intended_receiver = intended_receiver
        self.expires_after = expires_after

    def __eq__(self, other):
        return isinstance(other, Message) and\
            self.action == other.action and\
            self.subject == other.subject and\
            self.sender == other.sender and\
            self.intended_receiver == other.intended_receiver and\
            self.content == other.content

    def __neq__(self, other):
        return not (self == other)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        string = "({}, {}>{}, {})".format(self.action, self.sender, self.intended_receiver, self.subject)
        if self.content is not None:
            string = "{};{})".format(string[:-1], self.content)
        return string

    def has_subject(self, subject):
        """returns true if the message matches the subject passed without checking the action"""
        return self.subject == Message._check_subject(subject)

    def has_action(self, action):
        """returns true if the message matches the action passed"""
        return self.action == Message._check_action(action)

    def has_header(self, action, subject):
        """returns true if the current message matches both the action and the subject passed"""
        return self.has_action(action) and self.has_subject(subject)

    @staticmethod
    def _check_subject(subject, action=None):
        if not action and subject not in Message.SUBJECTS_ALL:
            raise ValueError("Subject {} must be one of: {}"
                             .format(subject, ', '.join(Message.SUBJECTS_ALL)))
        if action and subject not in Message.ALLOWED_SUBJECTS_BY_ACTION[action]:
            raise ValueError("Subject {} is not allowed for action {}, must be one of: {}"
                             .format(subject, action, ', '.join(Message.ACTIONS)))

        return subject

    @staticmethod
    def _check_action(action):
        if action not in Message.ACTIONS:
            raise ValueError("Action {} is not one of: {}".format(action, ', '.join(Message.ACTIONS)))

        return action

    @classmethod
    def swap(cls, sender, receiver, content=None, expires_after=None):
        """
        Initialises a swap message from the sender to the receiver,
        the content must have the qubits to be swapped as well as any extra information needed.

        Examples
        --------
        In this case we are creating a message from the 1st repeater to the second containing information
        about the link id to be swapped, the new node id, the correction to apply after swapping
        and the current run time

        >>> msg = Message.swap(sender=1, receiver=2, content={
                'link_id': link_id,
                'new_remote_node_id': new_remote_node_id,
                'correction': correction,
                'run_time': self._run_time
            })
        >>> msg.has_header(Message.ACTION_SWAP, Message.SUBJECT_SWAP)
        True
        """
        return cls(sender=sender, action=Message.ACTION_SWAP, subject=Message.SUBJECT_SWAP,
                   intended_receiver=receiver, content=content, expires_after=None)

    @classmethod
    def distill(cls, subject, sender, receiver, content=None, expires_after=None):
        """
        Initialises a distillation message from the sender to the receiver,
        the content must have the outcome of the distillation as well as the possible corrections to apply.

        All subjects are available except for Message.SUBJECT_SWAP.

        Examples
        --------
        In this case we are creating a message from the 1st repeater to the second containing a tuple
        where the first number represents the outcome of the measurement and the second the correction to apply
        as a Pauli index.

        >>> msg = Message.distill(subject=Message.SUBJECT_DIST_OUTCOME, sender=1, receiver=2, content=(0, 1))
        >>> msg.has_header(Message.ACTION_DIST, Message.SUBJECT_DIST_OUTCOME)
        True
        """
        return cls(sender=sender, action=Message.ACTION_DIST, subject=subject,
                   intended_receiver=receiver, content=content, expires_after=expires_after)

    @classmethod
    def generate_entanglement(cls, subject, sender, receiver=None, content=None, expires_after=None):
        return cls(sender=sender, action=Message.ACTION_ENTGEN, subject=subject,
                   intended_receiver=receiver, content=content, expires_after=expires_after)

    @classmethod
    def from_task(cls, sender, task):
        """
        Initialises a message from the sender with the details of the given task. The task is required
        to have a topic Task.SEND_MSG and have as parameters the action, subject, content, receiver_id
        and expires_after keys.

        Raises
        ------
        ValueError
            if the topic of the task is not repchain.logic.task.Task.SEND_MSG.

        See Also
        --------
        repchain.logic.task.Task.message

        Examples
        --------
        In this case we are creating first a task to send a message with action to generate entanglement
        to the second node. Then, the task is passed to this function together with the sender id to
        create the new message. In this case the content is empty.

        >>> task = Task.message(Message.ACTION_ENTGEN, Message.SUBJECT_CONFIRM, 2)
        >>> msg = Message.from_task(1, task)
        >>> msg.has_header(Message.ACTION_ENTGEN, Message.CONFIRM)
        True
        >>> msg.sender
        1
        >>> msg.intended_receiver
        2
        """
        if task.topic != Task.SEND_MSG:
            raise ValueError('Cannot construct a message from a task with a topic different from SEND_MSG')

        return cls(sender=sender, action=task['action'], subject=task['subject'], content=task['content'],
                   intended_receiver=task['receiver_id'], expires_after=task['expires_after'])
