from random import choices

from netsquid import sim_time

from repchain.utils.tools import index_of_Pauli_product


ELECTRON_POSITION = 0


class Link:
    """Represents an entangled pair, as seen locally from a node.

    Attributes
    ----------
    remote_node_id : int
        ID of the remote node that this link is entangled with.
    link_id : int
        ID of this link. This link_id should be identical for each
        of the two nodes that share the link.
    estimated_fidelity: float, optional
        estimated fidelity of the link, default to 0.7.
    cor_Pauli: int, optional
        Indicates which single-qubit Pauli *one* of the two nodes
        should apply in order to get the (expected) state Psi^+, where 0=Id, 1=X, 2=Y, 3=Z.
        Default to 0.
    last_accessed : int
        holds the time at which the current link was last modified in ns,
        it is initialised on creation at the current sim time.
    creation_time : int
        holds the creation time of the link in ns, it is initialised on creation at the current sim time.
    """

    def __init__(self, remote_node_id, link_id, estimated_fidelity=0.7, cor_Pauli=0):
        self.remote_node_id = remote_node_id
        self.link_id = link_id
        self.estimated_fidelity = estimated_fidelity
        self.cor_Pauli = cor_Pauli
        self.last_accessed = sim_time()
        self.creation_time = self.last_accessed

    def update_fidelity(self, fidelity):
        """Update the estimated fidelity and changes the last modification time to match the current one.

        Parameters
        ----------
        fidelity : float
            estimated fidelity of the link, between 0 and 1

        Raises
        ------
        AssertionError
            if the fidelity is not within 0 and 1 (included)
        """
        # TODO: take this back once we have a proper method for estimating the fidelity
        # assert 0 <= fidelity <= 1
        fidelity = min(fidelity, 1)

        self.estimated_fidelity = fidelity
        self.last_accessed = sim_time()

    def __repr__(self):
        return 'Link(id={}, to={}; {:.2f}, {:d})'.format(
            self.link_id, self.remote_node_id, self.estimated_fidelity, self.cor_Pauli
        )

    def __str__(self):
        return "<--{}-->{} ({}, {})".format(self.link_id, self.remote_node_id,
                                            self.estimated_fidelity,
                                            self.cor_Pauli)

    def __eq__(self, other):
        return self.remote_node_id == other.remote_node_id and self.link_id == other.link_id

    def __hash__(self):
        return hash(str(self.remote_node_id) + "---" + str(self.link_id))


class MemoryTracker:
    """Datatype that holds Link objects for every qubit memory position in the ProcessingDevice of a QuantumNode.

    Could also be called "entanglement tracker".

    Its purpose is to keep track of which qubits in the ProcessingDevice are currently entangled with a
    remote node, and if so, with which node in particular.

    Attributes
    ----------
    num_positions : int
        Number of qubit memory positions that the memory holds.

    Notes
    -----
    The MemoryManager is completely independent of the ProcessingDevice in the sense that
    if a qubit is added to the ProcessingDevice, one should manually also add this fact to the MemoryManager.
    """

    def __init__(self, num_positions, elementary_link_fidelity=0.7):
        self.num_positions = num_positions
        self.elementary_link_fidelity = elementary_link_fidelity

        # Storing the links that this MemoryTracker holds dictionary: keys=positions, values=Link objects
        self._links = {}

        # Keeping a counter of the number of links that this MemoryTracker holds with every remote node
        # dictionary: keys=remote_node_id, values=number of links shared with this remote node
        self._num_links = {}

        # for logging purposes
        self._max_number_of_pos_in_use = 0

    def reset(self):
        """Same as `start`."""
        self.start()

    def start(self):
        """Start the memory tracker with the passed number of positions and without any link."""
        self.__init__(self.num_positions, self.elementary_link_fidelity)

    def __str__(self):
        linksstring = "Links:\n"
        counterstring = "Counters:\n"
        remotes = set()
        positions = list(self._links.keys())
        sorted_positions = sorted(positions)
        for pos in sorted_positions:
            remote_node_id = self.remote_node_at(pos=pos)
            linksstring += "{} : {}\n".format(pos, self._links[pos])
            remotes.add(remote_node_id)
        for remote_node_id in remotes:
            counterstring += "{} : {}\n".format(remote_node_id, self._num_links[remote_node_id])
        return linksstring + counterstring

    ##############################################
    # *Setting* information in the MemoryTracker #
    ##############################################

    def entangle(self, pos, remote_node_id, cor_Pauli=0):
        """Entangles the node holding the `MemoryTracker` at the given qubit position to another node.

        Parameters
        ----------
        pos : int
            qubit position, between 0 and `num_positions` (included).
        remote_node_id : int
            index of the remote node
        cor_Pauli : int, optional
            pauli correction to apply to the link, see :class:`Link` for more details. Default to 0.

        Raises
        ------
        AssertionError
            if the qubit position `pos` is not within 0 (included) and `num_positions` (excluded).

        See Also
        --------
        repchain.utils.memorymanager.Link
        """
        assert 0 <= pos < self.num_positions

        link_id = self._new_link_id(remote_node_id=remote_node_id)
        self._links[pos] = Link(remote_node_id,
                                link_id,
                                cor_Pauli=cor_Pauli,
                                estimated_fidelity=self.elementary_link_fidelity)
        self._max_number_of_pos_in_use = max(self._max_number_of_pos_in_use, len(self._links))
        return self._links[pos]

    def release(self, pos):
        """Releases the link at the given position.

        Parameters
        ----------
        pos : int
            qubit position, between 0 and `num_positions` (included).

        Raises
        ------
        AssertionError
            if the qubit position `pos` is not within 0 and `num_positions`.
        Exception
            if the qubit at the given position does not hold any qubit.
        """
        assert 0 <= pos < self.num_positions

        if not self.is_free(pos):
            del self._links[pos]
        else:
            raise Exception("Trying to set a position free which is not used")

    def _new_link_id(self, remote_node_id):
        if not (remote_node_id in self._num_links):
            self._num_links[remote_node_id] = 1
        else:
            self._num_links[remote_node_id] += 1
        return self._num_links[remote_node_id]

    def swap(self, posA, posB):
        """Swap the links that position `posA` and `posB` hold.

        Parameters
        ----------
        posA : int
            qubit position holding the link
        posB : int
            qubit position to move the link to

        Raises
        ------
        AssertionError
            if the qubit position `posA` or `posB` are not within 0 and `num_positions` (both included).
        Exception
            if both qubits are free

        Notes
        -----
        Does **not** physically move the qubits, only the information that the memorymanager has about them.
        """
        assert 0 <= posA < self.num_positions
        assert 0 <= posB < self.num_positions

        if self.is_free(pos=posA):
            if self.is_free(pos=posB):
                raise Exception("Trying two swap two empty memory positions")
            else:
                self._links[posA] = self.get_link(pos=posB)
                self._links.pop(posB)
        else:
            linkA = self.get_link(pos=posA)
            if not self.is_free(pos=posB):
                self._links[posA] = self.get_link(pos=posB)
            else:
                self._links.pop(posA)
            self._links[posB] = linkA

    def update_cor_Pauli(self, pos, new_cor_Pauli):
        link = self.get_link(pos=pos)
        print('update pauli for {} in {} to {}'.format(link, pos, new_cor_Pauli))
        link.cor_Pauli = index_of_Pauli_product(Pauli_index_A=link.cor_Pauli, Pauli_index_B=new_cor_Pauli)

    def is_free(self, pos):
        """Checks if a position has no link.

        Parameters
        ----------
        pos : int
            Position of the qubit in memory.

        Returns
        -------
        bool
            Whether a link is associated with the qubit, or that it is considered free.

        Notes
        -----
        Does **not** make use of :obj:`easysquid.quantumMemory.QuantumProcessingDevice.in_use`.
        """
        return not (pos in self._links)

    def remote_node_at(self, pos):
        """returns the remote node associated to a link in the given position or raises if empty"""
        return self.get_link(pos).remote_node_id

    def get_link(self, pos):
        """returns the link at the given position.

        Raises
        ------
        AssertionError
             if the position is empty
        """
        assert not self.is_free(pos=pos)

        return self._links[pos]

    def has_free_positions(self, number=1):
        """returns true if the given number of positions in the node are empty"""
        # freeposcounter = 0
        # pos = 0
        #
        # while pos < self.num_positions and freeposcounter < number:
        #     if self.is_free(pos):
        #         freeposcounter += 1
        #     pos += 1
        # return freeposcounter == number
        return (self.num_positions - len(self._links)) >= number

    def find_pos(self, remote_node_id, link_id):
        """Find the position of the qubit that is entangled with the given node and uses the given link.

        Parameters
        ----------
        remote_node_id : int
        link_id : int

        Returns
        -------
        int or None
        """
        for pos, link in self._links.items():
            if link.remote_node_id == remote_node_id and link.link_id == link_id:
                return pos
        return None

    def link2pos(self, search_link):
        """returns the position that holds the given link or None."""
        for pos, link in self._links.items():
            if link == search_link:
                return pos
        return None

    def number_of_links_with(self, remote_node_id, fidelity_range=None):
        """returns the number of links shared with the remote node.

        See Also
        --------
        repchain.utils.memorymanager.MemoryTracker.entangled_positions_with
        """
        return len(self.entangled_positions_with(remote_node_id, fidelity_range))

    def entangled_positions_with(self, remote_node_id, fidelity_range=None):
        """Gives all the memory positions that are entangled with the given node in a fidelity range.

        Parameters
        ----------
        remote_node_id : int
        fidelity_range: list or tuple of 2 floats
            fidelity range, the first float is the minimum allwed fidelity and the second is the maximum.
            Default to (0, 1).

        Returns
        -------
        list of int
            list of qubit memory positions entangled with `remote_node_id` and whose link
            has a fidelity that falls inside `fidelity_range` (both extrems included).

        Notes
        -----
        Includes the end points of the fidelity interval range.
        """
        fidelity_range = fidelity_range or (0., 1.)

        return [
            pos
            for pos, link in self._links.items()
            if link.remote_node_id == remote_node_id
            and (fidelity_range[0] <= link.estimated_fidelity <= fidelity_range[1])
        ]

    def entangled_links_with(self, remote_node_id, fidelity_range=None):
        """Gives all the memory positions that are entangled with the given node in a fidelity range.

        Parameters
        ----------
        remote_node_id : int
        fidelity_range: list or tuple of 2 floats
            fidelity range, the first float is the minimum allwed fidelity and the second is the maximum.
            Default to (0, 1).

        Returns
        -------
        list of :class:`repchain.utils.memorymanager.Link`
            list of links entangled with `remote_node_id` and whose link
            has a fidelity that falls inside `fidelity_range` (both extrems included).

        Notes
        -----
        Includes the end points of the fidelity interval range.
        """
        fidelity_range = fidelity_range or (0., 1.)

        return [
            link
            for pos, link in self._links.items()
            if link.remote_node_id == remote_node_id
            and (fidelity_range[0] <= link.estimated_fidelity <= fidelity_range[1])
        ]

    def number_of_links_with_exceeds(self, remote_node_id, min_links=0, fidelity_range=None):
        """returns wether the node holds strickly more links than the ones specified with the remote node.

        Parameters
        ----------
        remote_node_id : int
        min_links : int, optional
            the minimum amount of links that is asumed to be shared with the remote node, default to 0
        fidelity_range : tuple of two floats
            the min and maximum fidelity of the links, default to (0, 1).

        Returns
        -------
        bool
            if the node associated to this memory shares at least `min_links` with the `remote_node_id`
            with a fidelity in the given range.
        """
        return self.number_of_links_with(remote_node_id, fidelity_range) > min_links

    def entangled_with(self, remote_node_id):
        """returns wether the current node is entangled at any position with the given node"""
        for pos, link in self._links.items():
            if link.remote_node_id == remote_node_id:
                return True
        return False

    def free_positions(self, excluded=None):
        """retuns the list of free positions excluding the given ones"""
        if excluded is None:
            excluded = []
        # return [pos for pos in range(0, self.num_positions) if self.is_free(pos=pos) and pos not in excluded]
        occupied_positions = set(self._links.keys())
        free_positions = set(range(self.num_positions)) - occupied_positions  # set complement
        return sorted(list(free_positions - set(excluded)))

    def random_positions(self, num=1, excluded=None):
        """returns a list of shuffled empty positions

        Parameters
        ----------
        num : int, optional
            the number of free positions to return, default to 1.
        excluded : list of int or None
            a list of excluded qubit positions that should not be returned, default to None (all included).

        Returns
        -------
        int or list of int
            the position (if `num = 1`) or list of empty positions

        Raises
        ------
        Exception
            if the amount of free positions is less that the number of required free positions
        """
        free_positions = self.free_positions(excluded)
        if len(free_positions) < num:
            raise Exception('Not enough empty positions, found {} but {} where required'
                            .format(len(free_positions), num))

        random_positions = choices(free_positions, k=num)
        return random_positions[0] if num == 1 else random_positions

    def first_free_positions(self, num=1, excluded=None):
        """returns a list of the first empty positions

        Parameters
        ----------
        num : int, optional
            the number of free positions to return, default to 1.
        excluded : list of int or None
            a list of excluded qubit positions that should not be returned, default to None (all included).

        Returns
        -------
        int or list of int
            the position (if `num = 1`) or list of empty positions

        Raises
        ------
        Exception
            if the amount of free positions is less that the number of required free positions
        """
        free_positions = self.free_positions(excluded)
        if len(free_positions) < num:
            raise Exception('Not enough empty positions, found {} but {} where required'
                            .format(len(free_positions), num))

        return free_positions[0] if num == 1 else free_positions[:num]


class MemoryManager(MemoryTracker):
    """Extension of `MemoryTracker` taking into account a discarding strategy.

    Parameters
    ----------
    num_positions : int
        number of qubit positions that can generate entanglement with other nodes
    discarding_strategy : :class:`repchain.logic.discarding_strategy.DiscardingStrategy` or None
        the strategy that the memory uses to discard links or None, default to None (no links are discarded)
    procDev : :class:`QuantumMemory` or None
        actual quantum memory that holds the entaglement with other nodes, it is used to discard
        the entanglement if the `discarding_strategy` says so, default to None
    communication_position : int
        position used to communicate, default to :obj:`repchain.utils.memorymanager.ELECTRON_POSITION`
    """

    def __init__(self, num_positions, elementary_link_fidelity=0.7, discarding_strategy=None, procDev=None):
        super().__init__(num_positions, elementary_link_fidelity)
        self.procDev = procDev
        self.discarding_strategy = discarding_strategy
        self.communication_position = ELECTRON_POSITION

    def reset(self):
        """reset the state of the MemoryManager by removing all links"""
        self.__init__(num_positions=self.num_positions,
                      elementary_link_fidelity=self.elementary_link_fidelity,
                      discarding_strategy=self.discarding_strategy,
                      procDev=self.procDev)

    def _update_estimated_fidelity(self, link):
        # TODO update estimated fidelity as function of dt, using
        # both the T1 and T2 time, and whether the position is the electron_position
        # (get T1 and T2 for both carbon and electron from self.procDev)
        link.update_fidelity(link.estimated_fidelity)

    def update_estimated_fidelities(self):
        """updates the fidelity of all links based on the properties of the quantum memory"""
        for link in self._links.values():
            self._update_estimated_fidelity(link)

    def discard(self, pos):
        """discards the link at the given position, also in the quantum memory"""
        self.release(pos)
        if self.procDev is not None:
            self.procDev.pop(positions=[pos])

    def discard_bad_entanglement(self, fidelity_threshold=0., excluded_positions=None):
        """discard all the links by the discarding strategy"""
        if self.discarding_strategy is None:
            return

        excluded_positions = [] if excluded_positions is None else excluded_positions

        to_be_discarded = [
            pos for pos, link in self._links.items()
            if self.discarding_strategy.should_discard(link) and pos not in excluded_positions
        ]

        for pos in to_be_discarded:
            self.discard(pos)

    def estimate_remaining_lifetime(self, pos):
        """updates the fidelity of the link at the given position according to the Quantum Memory parameters"""
        link = self.get_link(pos)
        self._update_estimated_fidelity(link)
        # TODO estimate lifetime given its estimated_fidelity
        # and the T1 and T2 time, and whether the position is the electron_position
        # (get T1 and T2 for both carbon and electron from self.procDev)
        return NotImplemented
