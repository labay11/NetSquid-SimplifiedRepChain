from repchain.utils import tools
from netsquid.components.instructions import INSTR_X
import netsquid as ns
import logging
# logging.basicConfig(level=logging.WARNING)


def _add_qubit(memorymanager, procDev, pos, remote_node_id, state="|0>"):
    assert(state in ["|0>", "|1>"])
    qubits = ns.qubits.qubitapi.create_qubits(1)
    procDev.put(qubits=qubits, positions=[pos])
    if state == "|1>":
        procDev.execute_instruction(INSTR_X, [pos], physical=False)
    memorymanager.entangle(pos, remote_node_id)


def _add_bell_state(memorymanager, qmem, posX, posY, remote_node_id_X, remote_node_id_Y, bell_index):
    tools._add_Bell_pair(qmemA=qmem,
                         qmemB=qmem,
                         posA=posX,
                         posB=posY,
                         bell_index=bell_index)
    for pos, remote_node_id in [(posX, remote_node_id_X), (posY, remote_node_id_Y)]:
        memorymanager.entangle(pos, remote_node_id)


class FakeTaskExecutor:

    def __init__(self):
        self.received_msgs = None

    def process_msgs(self, direction, msglist):
        self.received_msgs = (direction, msglist)


class FakeTaskDirector:

    def __init__(self):
        self.gate_restricted = False
        action = None
        parameters = [None, None]
        delay = 0
        self.next = [(action, parameters, delay)]

    def get_next(self):
        if len(self.next) == 0:
            return None
        else:
            return self.next.pop(0)


class StoreObject:

    evtype = ns.EventType("EVT", "Some event type")

    def __init__(self):
        self.object = None
        self.locallist = []

    def set(self, obj):
        logging.debug('StoreObject with id {}: setting to {}'.format(id(self), obj))
        self.object = obj

    def append(self, element):
        self.locallist.append(element)

    def reset(self):
        self.__init__()


class FakeChannel(StoreObject):

    def send(self, items):
        if isinstance(items, list):
            for item in items:
                self.append(element=item)
        else:
            self.append(element=items)

    def receive(self):
        return self.locallist[0]


class FakeConnection:

    def __init__(self):
        self._data = []

    def put_from(self, sender, data):
        self._data.append(data)

    def get_as(self, receiver):
        """The parameter `receiver` is not used!"""
        return self._data.pop(0)

    @property
    def data(self):
        if len(self._data) == 0:
            return None
        if len(self._data) == 1:
            return self._data[0]
        else:
            return self._data


class DummySubProtocol:

    _CORRECT_OUTPUT = "CorrectOutput"
    _NAME = "Dummy"

    def __init__(self):
        self.performed = False
        self.evtype_finished = ns.EventType("FIN", "FIN")

    def reset(self):
        self.__init__()

    def __str__(self):
        return "PROT[{}]".format(self._NAME)

    def trigger(self, **kwargs):
        self.performed = True
        ns.Entity()._schedule_now(self.evtype_finished)
        self.performed_time = ns.sim_time()
        logging.info("Perf. subprot {}:{}".format(self._NAME, kwargs))


class EntSwapDummy(DummySubProtocol):

    _NAME = "EntSwap"

    def trigger(self, qmem_posA, qmem_posB):
        super().trigger(qmem_posA=qmem_posA, qmem_posB=qmem_posB)
        return self._CORRECT_OUTPUT


class DistillDummy(DummySubProtocol):

    _NAME = "Distill"

    def trigger(self, qmem_posA, qmem_posB):
        super().trigger(qmem_posA=qmem_posA, qmem_posB=qmem_posB)
        return self._CORRECT_OUTPUT


class EntGenDummy(DummySubProtocol):

    _NAME = "EntGen"

    def trigger(self, remoteID, free_qmem_pos):
        super().trigger(remoteID=remoteID, free_qmem_pos=free_qmem_pos)
        return self._CORRECT_OUTPUT


class MoveDummy(DummySubProtocol):

    _NAME = "MOVE"

    def trigger(self, remoteID, free_qmem_pos):
        super().trigger(remoteID=remoteID, free_qmem_pos=free_qmem_pos)
        return self._CORRECT_OUTPUT
