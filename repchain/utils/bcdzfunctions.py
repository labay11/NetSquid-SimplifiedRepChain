"""
Convention: counting levels starts at 1. That is, a node with table of
height 1 has a single row, to which we refer as row 1 (instead of row 0).

TODO: extend to `number_of_nodes != 2^d + 1`
"""
import numpy as np
from .tools import is_valid_direction, is_outermost_node, DIRECTIONS, LEFT


def num2level(number_of_nodes):
    """Compute the highest level from the total number of nodes.

    We assume that the number of nodes has the form :math:`n = 2^d + 1` where `d` is the level.

    Parameters
    ----------
    number_of_nodes : int
        total number of nodes in the chain

    Returns
    -------
    level : int
        the `d` in `2^d + 1`

    Raises
    ------
    AssertionError
        if `number_of_nodes` is smaller than 2
    """
    assert number_of_nodes > 1

    if number_of_nodes == 2:
        return 0

    return int(np.log2(number_of_nodes - 1))


def level2num(level):
    """Compute the number of nodes for a chain with highest level equal to `level`.

    We assume that the number of nodes has the form :math:`n = 2^d + 1` where `d` is the level.

    Parameters
    ----------
    level : int
        the highest level of the chain

    Returns
    -------
    number_of_nodes : int
        `2^d + 1` where `d` is the level
    """
    return int(2**level + 1)


def num2numOneLower(number_of_nodes):
    """Compute the number of nodes for a chain that has a level lower than the level of the passed `number_of_nodes`.

    We assume that the number of nodes has the form :math:`n = 2^d + 1` where `d` is the level.

    Parameters
    ----------
    number_of_nodes : int
        total number of nodes in the chain

    Returns
    -------
    level : int
        `2^(d - 1) + 1` where `d` is the level of `number_of_nodes`

    Raises
    ------
    AssertionError
        if `number_of_nodes` is smaller than 2
    """
    return (number_of_nodes - 1) / 2 + 1


def _get_mirrorindex(number_of_nodes, index):
    return (number_of_nodes - 1) - index


def get_height(number_of_nodes, index):
    """The height of a node determines the level at which it swaps entanglement.

    The height :math:`h` is determined from the input index :math:`j` with the equation:

    .. math:: \\frac{j}{2^h} \\mod 2 = 1

    that is, the height is the number of times one have to divide the index until one obtains the number 1 as reminder.

    We also define that the height of the left most node is equal to height of the right most node, so :math:`d` where
    :math:`2^d + 1` is the number of nodes in the chain.

    Parameters
    ----------
    num : int
        Number of nodes in the chain
        (must be of the form 2^d+1 for some d>=1, but this is not checked!)
    index : int
        Index of one of the nodes (must be a number in the range [0, 1, ..., num-1])

    Returns
    -------
    int
        the height

    Raises
    ------
    AssertionError
        if `index` is not in the range [0, `number_of_nodes`)

    Examples
    --------
    For a chain of 5 repeaters, the height of each node is:

        +--------+---+---+---+---+---+
        | Index  | 0 | 1 | 2 | 3 | 4 |
        +--------+---+---+---+---+---+
        | Height | 2 | 0 | 1 | 0 | 2 |
        +--------+---+---+---+---+---+

    Notes
    -----
    Odd indexed nodes have height 0, they are responsible of initiating the entanglement request in the lowest level,
    swapping and distilling.

    All other nodes in between never generate the entanglement request but can swap and distill.

    The outermost nodes are listeners until they can distill between them.
    """
    assert 0 <= index < number_of_nodes

    if is_outermost_node(number_of_nodes, index):
        return num2level(number_of_nodes)

    k = 0
    while index % 2 == 0:
        index /= 2
        k += 1

    return k


def get_ngbh(number_of_nodes, index, level, direction, nonevalue=-1):
    r"""Compute the node that is entangled with the given node in a determined direction and level.

    If the node is an outermost node and the direction points outside then the result is `nonevalue`.

    In all other cases, the neighbour of :math:`j` in the level :math:`l` is evaluated using:

    .. math:: j \pm 2^l

    where the `+` and `-` signs are for the right and left direction respectively.

    Parameters
    ----------
    number_of_nodes : int
        total number of repeaters in the chain
    index : int
        position of the node in the chain
    level : int
        the level at which to look for the neightbour, number in the range [0, 1, ..., height],
    direction : one of [('L', 'R'), ('l', 'r'), (-1, +1)]
        the direction at which to look for the neighbour
    nonevalue : int, optional
        value to return if no neightbour is found, default to -1.

    Raises
    ------
    AssertionError
        if `number_of_nodes` is not bigger or equal to 2.

    ValueError
        if the level exceeds the height for the given node

    Examples
    --------
    In a repeater chain of 5 nodes, the second node (index 1) has two neighbours in the 0th level, to the left it has
    node 0 and to the right node 2. Similarly, for node 3 it has 2 on its left and 4 on its right. They do not have
    neighbours in any other level as their height is 0.

    The middle node (index 2) have neighbours in the 0th and 1st levels, in the former it has 1 on its left and 3 on
    its right and in the latter 0 and 4 on left and right respectively.

    Finally, the outermost nodes only have a single neighbour, 0 has 1 in the 0th level, 2 in the 1st and 4 in the last
    to its right while node 4 has 3 in the 0th level, 2 in the 1st and 0 in the last to its left.
    """
    direction = is_valid_direction(direction)
    if level > get_height(number_of_nodes, index):
        raise ValueError('You tried to find the neighbour of {} in a level where this node does not exist, {} > {}'
                         .format(index, level, get_height(number_of_nodes, index)))

    if is_outermost_node(number_of_nodes, index, direction):
        return nonevalue

    return index + (-1 if direction == LEFT else 1) * int(2**level)


def is_initiator(number_of_nodes, index, level):
    """Return whether node with index `index` is an initiator at level `level`.

    This means that the node initiates the entanglement generation request.

    Parameters
    ----------
    number_of_nodes : int
        Number of nodes in the chain.
    index : int
        Index of the node in the chain,
        where we start to count from the left
        and count 0, 1, 2, ..., number_of_nodes - 1
    level : int
    """
    # outermost nodes are always responders
    if is_outermost_node(number_of_nodes, index):
        return False

    height = get_height(number_of_nodes=number_of_nodes, index=index)
    return level == height


def get_level(index, remote_index):
    """Distance in levels between the nodes.

    Parameters
    ----------
    index : int
        position of one node in the chain
    remote_index : int
        position of another node in the chain

    Returns
    -------
    level : int

    Raises
    ------
    AssertionError
        if `index == remote_index`
    """
    assert index != remote_index

    return int(np.log2(np.abs(remote_index - index)))


def get_levels(number_of_nodes, index):
    """Compute the number of levels in which this node interacts.

    The action of a node in a level can be to swap entanglement in which case the node stops
    existing in the following levels or to hold the entanglement which implies that it existed on all previous
    levels and will continue exisiting till it is used to swap entanglement.

    Parameters
    ----------
    number_of_nodes : int
        total number of repeaters in the chain
    index : int
        the node

    Returns
    -------
    list of int
        all the levels in which this node has something to do, either swap or hold. Therefore, the result
        is all levels before the height of the node (included)
    """
    height = get_height(number_of_nodes, index)
    return list(range(height + 1))


def get_table(number_of_nodes, index, nonevalue=-1):
    """Compute a table of all neighbours per direction and level.

    Parameters
    ----------
    number_of_nodes : int
        the number of repeaters in the chain.
    index : int
        the node from where to compute the table
    nonevalue : int, optional
        default value if no neighbour is found, default to -1.

    Returns
    -------
    dict of (str, dict of (int, int))
      * first parameter is "direction"
      * second parameter is "level"

    Examples
    --------
    >>> table = get_table(number_of_nodes=5, index=2)
    >>> table[LEFT][2]
    0
    """
    levels = get_levels(number_of_nodes, index)

    return {
        direction: {
            level: get_ngbh(number_of_nodes,
                            index,
                            level,
                            direction,
                            nonevalue=nonevalue)
            for level in levels
        }
        for direction in DIRECTIONS
    }


def remote_index2table_position(number_of_nodes, index, remote_index):
    table = get_table(number_of_nodes=number_of_nodes, index=index)
    for direction in DIRECTIONS:
        # TODO: do it really needs to loop over direction, can't we just check `remote_index > index`?
        for level in get_levels(number_of_nodes, index):
            if remote_index == table[direction][level]:
                return direction, level
    return None, None
