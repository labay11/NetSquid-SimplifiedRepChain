.. repchain is a simple python class to evaluate the uncertainty
  for complex or very long calculations given the initial values
  together with its uncertainty.

Simplified Repeater Chain
=========================
Simple python class to evaluate the uncertainty
for complex or very long calculations given the initial values
together with its uncertainty.

.. toctree::
   :maxdepth: 2

   sample
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
