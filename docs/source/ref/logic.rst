Logic
=====
The logic concerns the decisions that a node takes: i.e. whether to perform distillation, entanglement swapping, sending
classical messages, and so forth.

The logic of a node is split up in four parts:

Action Generator
----------------
The ActionGenerator decides which of the following three actions a node will perform:
entanglement swapping, entanglement distillation or generating fresh entanglement with an adjacent node.
These actions represent high-level decisions of the form "entangle with remote node 23" or
"perform distillation on the entanglement on qubits on memory positions 5 and 19". The ActionGenerator
should be queried as soon as the node is idle.

.. automodule:: repchain.logic.actiongenerator
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: repchain.logic.swap_asap_action_generator
  :members:
  :undoc-members:
  :show-inheritance:

.. autoclass:: repchain.logic.action.Action
   :members:
   :undoc-members:
   :show-inheritance:

Task Executor
-------------
Holds a list of tasks (a "to-do-list") which it performs sequentially. Tasks can be added to the list during runtime.

.. automodule:: repchain.logic.taskexecutor
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: repchain.logic.task
  :members:
  :undoc-members:
  :show-inheritance:

Umbrella Logic
--------------
A mailbox, i.e. a list of classical messages that were sent to this node, which can be seen as a container
or basket that holds the
:class:`repchain.logic.actiongenerator.ActionGenerator` and :class:`repchain.logic.taskexecutor.TaskExecutor`
and converts actions (obtained by querying the ActionGenerator together with
mailbox content) into tasks (that are performed by the TaskExecutor.

.. automodule:: repchain.logic.umbrellalogic
  :members:
  :undoc-members:
  :show-inheritance:

Additional tools
----------------
The :class:`repchain.logic.umbrellalogic.UmbrellaLogic` and :class:`repchain.logic.actiongenerator.ActionGenerator`
can employ different strategies to determine which of the three actions to perform next.
Some of those strategies are put in the following separate modules:

Discarding Strategy
~~~~~~~~~~~~~~~~~~~
Strategies for discarding entanglement once it is believed its fidelity is too low.

.. automodule:: repchain.logic.discarding_strategy
   :members:
   :undoc-members:
   :show-inheritance:

Distillation Scheduling
~~~~~~~~~~~~~~~~~~~~~~~
Strategies for determining which entangled links to use for distillation.

.. automodule:: repchain.logic.distillation_scheduling
  :members:
  :undoc-members:
  :show-inheritance:

Task divider
~~~~~~~~~~~~
Tasks can either be actions with more details (such as "before performing this task,
wait for a classical message that confirms that
this task can indeed be performed") or are additional operations such as classical message sending or sleeping
(i.e. remain idle for a given amount of time).

.. autofunction:: repchain.logic.action.divide_swap_action_into_tasks


.. autofunction:: repchain.logic.action.divide_entgen_action_into_tasks


.. autofunction:: repchain.logic.action.divide_dist_action_into_tasks
