Network
========

This contains two functions that help create a Network from an initial set of parameters or from a network
configuration file.

.. automodule:: repchain.network.abstract_network_factory
   :members:
   :undoc-members:
   :show-inheritance:
