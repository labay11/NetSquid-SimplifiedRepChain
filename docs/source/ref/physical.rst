Physical
========

.. automodule:: repchain.physical.magic_distributor
   :members:
   :undoc-members:
   :show-inheritance:


.. automodule:: repchain.physical.simplified_delivery_model
  :members:
  :undoc-members:
  :show-inheritance:
