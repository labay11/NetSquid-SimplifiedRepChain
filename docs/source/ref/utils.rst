Utils
=========

Some utility functions and classes.

BDCZ protocol
------------
.. automodule:: repchain.utils.bcdzfunctions
   :members:
   :undoc-members:
   :show-inheritance:

Entanglement tracker
--------------------
.. automodule:: repchain.utils.memorymanager
   :members:
   :undoc-members:
   :show-inheritance:

Messages
--------
.. automodule:: repchain.utils.message
   :members:
   :undoc-members:
   :show-inheritance:

Utility functions for Pauli and Bell states indices
---------------------------------------------------
.. automodule:: repchain.utils.tools
   :members:
   :undoc-members:
   :show-inheritance:
