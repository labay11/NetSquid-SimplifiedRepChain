PYTHON        = python
PIP           = pip3
SOURCEDIR     = repchain
TESTDIR       = ./tests/
PIP_FLAGS     = --extra-index-url=https://pypi.netsquid.org

clean:
	@find . -name '*.pyc' -delete

lint:
	@$(PYTHON) -m flake8 $(SOURCEDIR)

test:
	@$(PYTHON) -m pytest -v --cov=repchain --cov-report=html $(TESTDIR)

python-deps:
	@$(PYTHON) -m pip install -r requirements.txt ${PIP_FLAGS}

_check_variables:
ifndef NETSQUIDPYPI_USER
	$(error Set the environment variable NETSQUIDPYPI_USER before uploading)
endif
ifndef NETSQUIDPYPI_PWD
	$(error Set the environment variable NETSQUIDPYPI_PWD before uploading)
endif

#verify: clean python-deps lint tests examples

#.PHONY: clean examples lint python-deps tests verify
