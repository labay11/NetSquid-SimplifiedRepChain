import unittest
import logging

import netsquid as ns

from repchain.logic.taskexecutor import TaskExecutor
from repchain.logic.task import Task
from repchain.utils.message import Message
from repchain.utils.testtools import EntSwapDummy, DistillDummy, EntGenDummy, MoveDummy, FakeChannel


class Test_TaskExecutor(unittest.TestCase):

    def test_perform_tasks(self):
        ns.sim_reset()
        entgenprot = EntGenDummy()
        channels = {direction: FakeChannel() for direction in ["L", "R"]}
        te = TaskExecutor(index=2,
                          entswapprot=EntSwapDummy(),
                          distilprot=DistillDummy(),
                          entgenprot=entgenprot,
                          moveprot=MoveDummy(),
                          class_channels=channels)

        # put a new todo on the list
        te.start()
        delay = 13.0
        remote_node_id = 1
        sendmsgtask = Task.message(Message.ACTION_ENTGEN, Message.SUBJECT_CONFIRM, receiver_id=remote_node_id)
        expected_msg = Message.generate_entanglement(subject=Message.SUBJECT_CONFIRM,
                                                     sender=te._index, receiver=remote_node_id)

        sleeptask = Task.sleep(delay)
        entgentask = Task.generate_entanglement(remote_node_id, free_pos=12)
        te.todolist.append(sendmsgtask)
        te.todolist.append(sleeptask)
        te.todolist.append(entgentask)

        self.assertFalse(entgenprot.performed)

        # start the simulation
        # ns.Entity()._schedule_now(te._EVT_PREVIOUS_TASK_DONE)
        te.continue_performing_tasks()
        ns.sim_run()
        self.assertEqual(ns.sim_time(), delay)
        self.assertTrue(entgenprot.performed)
        self.assertEqual(entgenprot.performed_time, delay)
        self.assertEqual(channels["L"].receive(), expected_msg)
        self.assertEqual(len(channels["R"].locallist), 0)

#    def test_that_two_TEs_work_independently(self):
#        print('now starting text')
#        channelsA = {direction: FakeChannel() for direction in ["L", "R"]}
#        channelsB = {direction: FakeChannel() for direction in ["L", "R"]}
#        remote_node_id = 12
#        teA = TaskExecutor(index=2,
#                          entswapprot=EntSwapDummy(),
#                          distilprot=DistillDummy(),
#                          entgenprot = EntGenDummy(),
#                          moveprot=MoveDummy(),
#                          class_channels=channelsA)
# #        teB = TaskExecutor(index=3,
# #                          entswapprot=EntSwapDummy(),
# #                          distilprot=DistillDummy(),
# #                          entgenprot = EntGenDummy(),
# #                          moveprot=MoveDummy(),
# #                          class_channels=channelsB)
#        sendmsgtask = Task(topic="SENDMSG",
#                           parameters={"remote_node_id": remote_node_id,
#                                       "header_type": "ENTGEN",
#                                       "content_type": "CONFIRM"})
#        teA.todolist.append(sendmsgtask)
#        #teB.todolist.append(sendmsgtask)
#        teA._status = STATUS_BUSY
#        teA._schedule_now(teA._EVT_PREVIOUS_TASK_DONE)
#        ns.sim_run()
#        #print(channelsA["R"].receive())
#        #print(channelsB["R"].receive())

    def test_task_expiry(self):
        remote_node_id = 42
        helper_entity = ns.Entity()
        entgenprot = EntGenDummy()

        # case: request is not responded to, so it will expire
        ###########################################
        ns.sim_reset()
        channels = {direction: FakeChannel() for direction in ["L", "R"]}
        te = TaskExecutor(index=2,
                          entswapprot=entgenprot,
                          distilprot=DistillDummy(),
                          entgenprot=EntGenDummy(),
                          moveprot=MoveDummy(),
                          class_channels=channels)

        expires_after = 17
        entgentask = Task.generate_entanglement(remote_node_id, free_pos=53,
                                                halt_until_confirm=True, halt_expires_after=expires_after)
        te.todolist.append(entgentask)

        # run simulations
        self.assertFalse(entgenprot.performed)
        te.start()
        te.continue_performing_tasks()
        ns.sim_run()
        self.assertEqual(ns.sim_time(), expires_after)
        self.assertFalse(entgenprot.performed)

        # case: there is a response to the request
        ###########################################
        ns.sim_reset()
        te = TaskExecutor(index=2,
                          entswapprot=EntSwapDummy(),
                          distilprot=DistillDummy(),
                          entgenprot=entgenprot,
                          moveprot=MoveDummy(),
                          class_channels=channels)

        te.start()
        te.todolist = [entgentask]
        self.assertFalse(entgenprot.performed)

        # respond to the pending task
        respond_time = 11

        def respond(taskexecutor):
            logging.debug("*In Test*: {}: responding to message".format(ns.sim_time()))
            response_msg = Message.generate_entanglement(Message.SUBJECT_CONFIRM,
                                                         sender=remote_node_id, receiver=taskexecutor._index)
            taskexecutor.process_msgs(direction="R", msglist=[response_msg])

        evtype_respond = ns.EventType("RESPOND", "Respond to ASK-message")
        helper_entity._wait_once(ns.EventHandler(lambda event, taskexecutor=te: respond(taskexecutor=taskexecutor)),
                                 event_type=evtype_respond)
        helper_entity._schedule_after(respond_time, evtype_respond)

        # run the simulation
        te.continue_performing_tasks()
        ns.sim_run()

        self.assertEqual(ns.sim_time(), expires_after)
        self.assertTrue(entgenprot.performed)
        self.assertEqual(respond_time, entgenprot.performed_time)


if __name__ == "__main__":
    unittest.main()
