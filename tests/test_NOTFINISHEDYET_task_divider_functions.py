import unittest
from repchain.logic.action import Action
from repchain.utils.testtools import StoreObject
import logging


class TestTaskDividingFunction(unittest.TestCase):

    def setUp(self):
        self.memorymanager = StoreObject()

    def test_divide_entgen_task(self):
        """
        Testing the output for an ENTGEN action.
        """
        # fixed parameters
        remote_node_id = 42
        action = Action(topic="ENTGEN", parameters={"remote_node_id": remote_node_id})
        electron_pos = 114
        delay = 13
        unoccupied_pos = 3
        self.memorymanager.free_position = \
            lambda num, excluded: unoccupied_pos
        self.memorymanager.communication_position = electron_pos
        self.memorymanager.first_free_positions = lambda num=1, excluded=[]: unoccupied_pos

        for is_initiator in [False, True]:
            action.parameters["is_initiator"] = is_initiator
            for respect_NV_structure in [False, True]:
                self.memorymanager.is_free = lambda pos: not respect_NV_structure

                # generate tasks
                sleep_delay = delay
                tasks = action.to_tasks(
                     sleep_delay=sleep_delay,
                     memorymanager=self.memorymanager,
                     respect_NV_structure=respect_NV_structure)

                # logging to console
                logging.info("\nTASKS (ENTGEN, is_initiator={}, respect_NV_structure={}:"
                             .format(is_initiator, respect_NV_structure))
                for task in tasks:
                    logging.info(task)

                # expected output: order
                if is_initiator:
                    if respect_NV_structure:
                        expected_topics = ["MOVE", "SENDMSG", "ENTGEN"]
                    else:
                        expected_topics = ["SENDMSG", "ENTGEN"]
                else:
                    if respect_NV_structure:
                        expected_topics = ["MOVE", "SENDMSG", "SLEEP", "ENTGEN"]
                    else:
                        expected_topics = ["SENDMSG", "SLEEP", "ENTGEN"]

                # assert expected output: order
                self.assertEqual(len(tasks), len(expected_topics))
                for task_index, task in enumerate(tasks):
                    self.assertEqual(task.topic, expected_topics[task_index])

                # expected output: MOVEs
                if respect_NV_structure:
                    move_task = tasks[0]
                    self.assertEqual(move_task.parameters["old_pos"], electron_pos)
                    self.assertEqual(move_task.parameters["new_pos"], unoccupied_pos)

                # expected output: free position used for entangling with remote
                expected_entgen_pos = electron_pos if respect_NV_structure else unoccupied_pos
                entgen_task = tasks[-1]
                self.assertEqual(entgen_task.parameters["free_pos"], expected_entgen_pos)

    def test_divide_distill_task(self):
        electron_pos = 114
        unoccupied_pos = 3
        delay = 13
        remote_node_id = 42
        action = Action(topic="DIST",
                        parameters={"remote_node_id": remote_node_id,
                                    "pos_lose": electron_pos,
                                    "pos_keep": 5,
                                    "strategy": ('DEJMPS', 2)})

        link_id_holder = StoreObject()
        link_id_holder.link_id = 19
        self.memorymanager.get_link = \
            lambda pos: link_id_holder
        self.memorymanager.communication_position = electron_pos
        self.memorymanager.free_position = \
            lambda num, excluded: electron_pos if len(excluded) == 0 else unoccupied_pos
        self.memorymanager.first_free_positions = self.memorymanager.free_position

        for is_initiator in [False, True]:
            action.parameters["is_initiator"] = is_initiator
            for respect_NV_structure in [False, True]:
                self.memorymanager.is_free = lambda pos: False
                sleep_delay = None if is_initiator else delay
                tasks = action.to_tasks(
                     sleep_delay=sleep_delay,
                     memorymanager=self.memorymanager,
                     respect_NV_structure=respect_NV_structure)

                # expected output: order
                # if respect_NV_structure:
                #    expected_topics = ["MOVE", "SENDMSG", "DIST"]
                # else:
                #    expected_topics = ["SENDMSG", "DIST"]

                # logging to console
                logging.info("\nTASKS (DIST, is_initiator={}, respect_NV_structure={}:"
                             .format(is_initiator, respect_NV_structure))
                for task in tasks:
                    logging.info(task)

                # TODO
                # assert expected output: order
                # self.assertEqual(len(tasks), len(expected_topics))
                # for task_index, task in enumerate(tasks):
                #    self.assertEqual(task.topic, expected_topics[task_index])

    def test_divide_swap_task(self):
        electron_pos = 114
        unoccupied_pos = 3
        action = Action(topic="SWAP",
                        parameters={"qmem_posA": 5,
                                    "qmem_posB": electron_pos})
        self.memorymanager.communication_position = electron_pos
        unoccupied_pos = 3
        self.memorymanager.free_position = \
            lambda num, excluded: unoccupied_pos
        self.memorymanager.first_free_positions = self.memorymanager.free_position
        self.memorymanager.is_free = lambda pos: False

        for is_initiator in [False, True]:
            for respect_NV_structure in [False, True]:
                tasks = action.to_tasks(
                     sleep_delay=None,
                     memorymanager=self.memorymanager,
                     respect_NV_structure=respect_NV_structure)
                # logging to console
                logging.info("\nTASKS (SWAP, is_initiator={}, respect_NV_structure={}:"
                             .format(is_initiator, respect_NV_structure))
                for task in tasks:
                    logging.info(task)


if __name__ == "__main__":
    unittest.main()
