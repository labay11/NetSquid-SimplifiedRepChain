"""
Test for the MagicEntGenProtocol.
Sets up a two-node network with a MagicDistributor which returns a fixed
pure two-qubit state. Runs two instances of the MagicEntGenProtocol
(one at each node) and checks if the state that is on the qubits in the
two respective nodes is indeed that fixed state.
"""

import unittest
import numpy as np

import netsquid as ns
from netsquid.qubits.state_sampler import StateSampler
from netsquid.nodes import Node
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.qubits.qubitapi import fidelity
from netsquid.qubits.ketutil import ket2dm

from repchain.protocols.magic_entgen_protocol import MagicEntGenProtocol
from repchain.utils.memorymanager import MemoryManager
# from repchain.network.magic_distributor_adaptor import MagicDistributorAdaptor
from repchain.network.abstract_network_factory import create_network_from_dic

from netsquid_magic.magic_distributor import MagicDistributor
from netsquid_magic.state_delivery_sampler import StateDeliverySampler, IStateDeliverySamplerFactory


#################################################
# Define the classes necessary
# for creating a MagicDistributor which returns
# a fixed state
#################################################

class _SingleStateDeliverySampler(StateDeliverySampler):
    """
    Delivers a fixed state with a single fixed label.

    :param qs_repr: representation of the quantum state.
                    Also see the identically-named input parameter
                    to :obj:`~netsquid.qubits.state_sampler.StateSampler`
    :param float cycle_time:
    :param Any label:
    """

    def __init__(self, qs_repr, cycle_time, label):
        state_sampler = StateSampler(qs_reprs=[qs_repr], labels=[label])
        super().__init__(state_sampler=state_sampler,
                         cycle_time=cycle_time)


class _SingleStateDeliverySamplerFactory(IStateDeliverySamplerFactory):
    """
    Produces _SingleStateDeliverySampler objects.

    :param qs_repr: representation of the quantum state.
                    Also see the identically-named input parameter
                    to :obj:`~netsquid.qubits.state_sampler.StateSampler`
    :param Any label:
    """

    def __init__(self, qs_repr, label):
        self.qs_repr = qs_repr
        self.label = label
        super().__init__()

    def create_state_delivery_sampler(self, cycle_time, md_index=0):
        return _SingleStateDeliverySampler(qs_repr=self.qs_repr, cycle_time=cycle_time, label=self.label)


class _SingleMagicDistributor(MagicDistributor):
    """
    Distributes fixed states magically.

    :param qs_repr: representation of the quantum state.
                    Also see the identically-named input parameter
                    to :obj:`~netsquid.qubits.state_sampler.StateSampler`
    :param float cycle_time:
    :param Any label:
    """

    def __init__(self, qs_repr, label, cycle_time, nodes):
        delivery_sampler_factory = _SingleStateDeliverySamplerFactory(qs_repr=qs_repr, label=label)
        super().__init__(delivery_sampler_factory=delivery_sampler_factory, nodes=nodes)

    def add_callback(self, callback, index, md_index=0):
        super().add_callback(callback, md_index)


####################################
# Testing the MagicEntGenProtocol
####################################

class TestMagicEntGenProtocol(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)
        self.indices = [0, 1]
        self.nodes = [Node(name=str(index), ID=index, qmemory=QuantumProcessor(name=str(index), num_positions=1))
                      for index in self.indices]

    def test_magicentgen(self):
        """
        Sets up two nodes, each running an instance of the MagicEntGenProtocol,
        together with a MagicDistributor which magically produces a single pre-specified
        state.
        Subsequently performs many time: run of the setup with different pre-specified
        magically-generated states.
        """
        N = 100
        for factor in range(N):
            angle = 2 * np.pi / N * factor
            self._check_magicentgen(angle=angle)

    def _create_distributor_adaptor(self, angle):
        r"""
          * Defines the fixed state |x> = cos(x)|0> + sin(x) |1>
          * Returns a magic distributor which creates the two-qubit states |x><x| \otimes |x><x|
        """
        single_qubit_ket = np.array([[np.cos(angle)], [np.sin(angle)]])
        two_qubit_ket = np.kron(single_qubit_ket, single_qubit_ket)
        two_qubit_dm = ket2dm(two_qubit_ket)

        label = np.random.random()

        # NOTE this cycle time seems not to be used -> TODO investigate possible bug in MagicDistributor
        magic_distributor = _SingleMagicDistributor(qs_repr=two_qubit_dm,
                                                    cycle_time=10,
                                                    nodes=self.nodes,
                                                    label=label)
        return label, two_qubit_ket, magic_distributor

    def _check_magicentgen(self, angle):
        """
        Trigger generation of a single entangled pair
        by having both node's MagicEntGenProtocol
        trigger a pair.
        Subsequently check if the correct pair was produced.
        """

        label, two_qubit_ket, magic_distributor_adaptor = self._create_distributor_adaptor(angle=angle)

        # setting up a MagicEntGenProtocol for each of the two nodes individually
        self.meps = {}

        network_params = {
            "number_of_nodes": 2,
            "tot_num_qubits": 4,
            "qmem_config_params": {
                "p_q1gate": 0,
                "p_q1init": 0,
                "p_q1meas": (0, 0),
                "p_q2gate": 0,
                "T1": 0,
                "T2": 0,
                "tot_num_qubits": 10,
                "t_q1gate": 0,
                "t_q1init": 0,
                "t_q1meas": 0,
                "t_q2gate": 0
            },
            "distributor_params": {
                "p_loss_length": 0.2,
                "elementary_link_fidelity": 0.7,
                "elementary_link_succ_prob": 1.,
                "werner": True,
                "eps": 0,
                "cycle_time": 1e3
            },
            "internode_distance": 0.002,
            "c": 3e5}

        delivery_params = network_params['distributor_params']

        logic_params = {
            "action_ordering": "default",
            "is_move_magical": True,
            "swap_fidelity_threshold": 0.,
            "delivery_params": delivery_params,
            "use_two_node_NV": False,
            "use_swap_only": False,
            "distillation_strategy": "default",
            "respect_NV_structure": False}

        self.factory = create_network_from_dic(network_params, logic_params)
        self.network = self.factory.network

        for index in self.indices:
            distributor_L = None if index == 0 else magic_distributor_adaptor
            distributor_R = None if index == 1 else magic_distributor_adaptor

            # We want to check if the MagicEntGenProtocol received the correct
            # label; we check this by storing this label in the Link.cor_Pauli
            # attribute
            def fn(midpoint_outcome):
                return midpoint_outcome

            self.meps[index] = \
                MagicEntGenProtocol(index=index,
                                    memmanager=MemoryManager(num_positions=1),
                                    class_channels=self.factory.class_channels_send[index],
                                    magic_distributor_L=distributor_L,
                                    magic_distributor_R=distributor_R,
                                    delivery_params={"cycle_time": 10},
                                    midpoint_outcome_to_cor_Pauli_fn=fn)

        for index in self.indices:
            self.meps[index].start()
            self.meps[index].trigger(remoteID=(index + 1) % 2, free_qmem_pos=0)

        ns.sim_run()

        # check that the right state was put on the memorys
        [q0] = self.nodes[0].qmemory.peek(positions=[0])
        [q1] = self.nodes[1].qmemory.peek(positions=[0])
        fid = fidelity(qubits=[q0, q1], reference_state=two_qubit_ket)
        self.assertTrue(np.isclose(fid, 1.0))

        # check that the correct label was found
        pauli0 = self.meps[0].memmanager.get_link(pos=0).cor_Pauli
        self.assertEqual(pauli0, 0)
        pauli1 = self.meps[1].memmanager.get_link(pos=0).cor_Pauli
        expected_label = label
        self.assertEqual(pauli1, expected_label)


if __name__ == "__main__":
    unittest.main()
