import pytest

import numpy as np

from repchain.parameters import BaselineParameter
from repchain.parameters.nv import NVOptimisticParameters, NVRestrictedParameters, NVApproxRestrictedParameters


def test_baseline_parameter():
    b = BaselineParameter('a', float, 0.24, 0.5, lambda x: x * 2, lambda x: x * 0.5)
    assert b.name == 'a'
    assert b.dtype == float
    assert b.baseline_value == 0.24
    assert b.perfect_value == 0.5

    assert np.isclose(b.cost(0.24), 1.)
    assert np.isinf(b.cost(0.5))

    assert b.improve(1.) == b.baseline_value
    assert b.baseline_value < b.improve(4.6) < b.perfect_value


def test_initialisation_nv_optimistic():
    NVOptimisticParameters(5, 200, 0.8, 0.3, distillation_strategy=0, uniform_strategy=False)


def test_initialisation_nv_optimistic_restricted():
    NVRestrictedParameters(5, 200, 0.2, distillation_strategy=0, uniform_strategy=False)


def test_initialisation_nv_optimistic_approx():
    alpha = 0.1
    p = NVApproxRestrictedParameters(5, 200, alpha, distillation_strategy=0, uniform_strategy=False)
    assert p.elementary_link_fidelity == (1. - alpha)


@pytest.mark.parametrize('nodes, param, strategy', [
    (2, 0, [None]),
    (2, 3, [None]),
    (3, 1, [None, None]),
    (3, 2, [('DEJMPS', 1), None]),
    (3, 5, [('EPL', 1), None]),
    (5, 4, [('DEJMPS', 2), ('DEJMPS', 2), None]),
    (5, 3, [('EPL', 1), ('EPL', 1), None])
])
def test_uniform_distillation_strategy(nodes, param, strategy):
    s = NVOptimisticParameters(nodes, 200, 0.8, 0.3, distillation_strategy=param, uniform_strategy=True)

    for a, b in zip(strategy, s.distillation_strategy):
        assert a == b


@pytest.mark.parametrize('nodes, param, strategy', [
    (2, 0, [None]),
    (3, 1, [None, None]),
    (3, 2, [('DEJMPS', 1), None]),
    (3, 5, [('EPL', 1), None]),
    (5, 4, [('DEJMPS', 2), None, None]),
    (5, 3, [('EPL', 1), None, None]),
    (5, 60, [('DEJMPS', 2), ('EPL', 1), None]),
    (5, 35, [('EPL', 1), ('DEJMPS', 2), None])
])
def test_level_distillation_strategy(nodes, param, strategy):
    s = NVRestrictedParameters(nodes, 200, 0.3, distillation_strategy=param, uniform_strategy=False)
    print(s.distillation_strategy)
    for a, b in zip(strategy, s.distillation_strategy):
        assert a == b


@pytest.mark.parametrize('label, value', [
    ('T1', 3.14),
    ('p_q2gate', 0.12),
    ('t_q1gate', 235.),
    ('cycle_time', 0.123456)
])
def test_override_parameter(label, value):
    s = NVOptimisticParameters(5, 200, 0.8, 0.3, **{label: value})
    assert getattr(s, label) == value
