import sys
from pathlib import Path
import logging

import pytest
from netsquid import logger

sys.path.insert(0, str(Path.cwd()))
sys.path.insert(0, str(Path.cwd().parent.joinpath('nlblueprint')))

import repchain  # noqa 402

# logger.setLevel(logging.DEBUG)
# logging.basicConfig(level=logging.DEBUG)


@pytest.fixture
def simsetup_params():
    network_params = {
        "number_of_nodes": 3,
        "tot_num_qubits": 4,
        "qmem_config_params": {
            "p_q1gate": 0,
            "p_q1init": 0,
            "p_q1meas": 0,
            "p_q2gate": 0,
            "T1": 0,
            "T2": 0,
            "tot_num_qubits": 10,
            "t_q1gate": 0,
            "t_q1init": 0,
            "t_q1meas": 0,
            "t_q2gate": 0
        },
        "distributor_params": {
            "p_loss_length": 0.2,
            "elementary_link_fidelity": 0.7,
            "elementary_link_succ_prob": 1.,
            "werner": True,
            "eps": 0,
        },
        "internode_distance": 0.002,
        "c": 3.e5
    }

    logic_params = {
        "action_ordering": "default",
        "is_move_magical": True,
        "swap_fidelity_threshold": 0.,
        "delivery_params": network_params['distributor_params'],
        "use_two_node_NV": False,
        "use_swap_only": False,
        "distillation_strategy": "default",
        "respect_NV_structure": False
    }
    return network_params, logic_params
