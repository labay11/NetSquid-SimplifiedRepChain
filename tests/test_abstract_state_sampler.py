from repchain.physical.simplified_delivery_model import get_states
import numpy as np


def test_perfect_NV_state():

    [perfect_NV_state_plus, perfect_NV_state_minus], prob = get_states(1.)
    target_state = np.array([[0., 0, 0, 0],
                             [0, 0.5, 0.5, 0],
                             [0, 0.5, 0.5, 0],
                             [0, 0, 0, 0]],
                            dtype=np.complex)

    assert np.all(perfect_NV_state_plus == target_state)


def test_perfect_werner_state():

    [perfect_werner_state_plus, perfect_werner_state_minus], prob = get_states(1., werner=True)
    target_state = np.array([[0., 0, 0, 0],
                             [0, 0.5, 0.5, 0],
                             [0, 0.5, 0.5, 0],
                             [0, 0, 0, 0]],
                            dtype=np.complex)

    assert np.all(perfect_werner_state_plus == target_state)


def test_noisy_NV_state():

    [noisy_NV_state_plus, noisy_NV_state_minus], prob = get_states(0.5)
    target_state = np.array([[0.5, 0, 0, 0],
                             [0, 0.25, 0.25, 0],
                             [0, 0.25, 0.25, 0],
                             [0, 0, 0, 0]],
                            dtype=np.complex)

    assert np.all(noisy_NV_state_plus == target_state)


def test_noisy_werner_state():

    [noisy_werner_state_plus, noisy_werner_state_minus], prob = get_states(0.7, werner=True)
    target_state = np.array([[0.1, 0, 0, 0],
                             [0, 0.4, 0.3, 0],
                             [0, 0.3, 0.4, 0],
                             [0, 0, 0, 0.1]],
                            dtype=np.complex)

    assert np.allclose(noisy_werner_state_plus, target_state)


def test_werner_nv_noise():

    [noisy_werner_state_plus, noisy_werner_state_minus], prob = get_states(0.7, werner=True, eps=0.1)
    target_state = np.array([[0.19, 0, 0, 0],
                             [0, 0.36, 0.27, 0],
                             [0, 0.27, 0.36, 0],
                             [0, 0, 0, 0.09]],
                            dtype=np.complex)

    assert np.allclose(noisy_werner_state_plus, target_state)
