import unittest

import netsquid as ns

from repchain.runtools.umbrellaprotocol import create_umbrellaprot
from repchain.utils.testtools import StoreObject
from repchain.utils.message import Message
from repchain.network.abstract_network_factory import create_network_from_dic


class Test_UmbrellaProtocol(unittest.TestCase):

    def setUp(self):
        ns.set_qstate_formalism(ns.QFormalism.DM)
        ns.sim_reset()
        self.number_of_nodes = 5

        network_params = {
            "number_of_nodes": self.number_of_nodes,
            "tot_num_qubits": 4,
            "qmem_config_params": {
                "p_q1gate": 0,
                "p_q1init": 0,
                "p_q1meas": (0, 0),
                "p_q2gate": 0,
                "T1": 0,
                "T2": 0,
                "tot_num_qubits": 10,
                "t_q1gate": 0,
                "t_q1init": 0,
                "t_q1meas": 0,
                "t_q2gate": 0
            },
            "distributor_params": {
                "p_loss_length": 0.2,
                "elementary_link_fidelity": 0.7,
                "elementary_link_succ_prob": 1.,
                "werner": True,
                "eps": 0,
                "cycle_time": 1e3
            },
            "internode_distance": 0.002,
            "c": 3e5}

        delivery_params = network_params['distributor_params']

        logic_params = {
            "action_ordering": "default",
            "is_move_magical": True,
            "swap_fidelity_threshold": 0.,
            "delivery_params": delivery_params,
            "use_two_node_NV": False,
            "use_swap_only": False,
            "distillation_scheduler": "default",
            "respect_NV_structure": False,
            "distillation_strategy": ('DEJMPS', 1)
        }

        self.factory = create_network_from_dic(network_params, logic_params)
        self.network = self.factory.network

    def test_receiving_messages(self):
        for use_swap_only in [True, False]:
            ns.sim_reset()
            index = self.number_of_nodes - 1

            network_params = {
                "number_of_nodes": self.number_of_nodes,
                "tot_num_qubits": 4,
                "qmem_config_params": {
                    "p_q1gate": 0,
                    "p_q1init": 0,
                    "p_q1meas": (0, 0),
                    "p_q2gate": 0,
                    "T1": 0,
                    "T2": 0,
                    "tot_num_qubits": 10,
                    "t_q1gate": 0,
                    "t_q1init": 0,
                    "t_q1meas": 0,
                    "t_q2gate": 0
                },
                "distributor_params": {
                    "p_loss_length": 0.2,
                    "elementary_link_fidelity": 0.7,
                    "elementary_link_succ_prob": 1.,
                    "werner": True,
                    "eps": 0,
                    "cycle_time": 1e3
                },
                "internode_distance": 0.002,
                "c": 3e5}

            delivery_params = network_params['distributor_params']

            logic_params = {
                "action_ordering": "default",
                "is_move_magical": True,
                "swap_fidelity_threshold": 0.,
                "delivery_params": delivery_params,
                "use_two_node_NV": False,
                "use_swap_only": False,
                "distillation_scheduler": "default",
                "respect_NV_structure": False,
                "distillation_strategy": ('DEJMPS', 1)
            }

            self.factory = create_network_from_dic(network_params, logic_params)

            up = create_umbrellaprot(index=index,
                                     network=self.factory.network,
                                     delivery_params=self.factory.delivery_params[index],
                                     number_of_nodes=self.factory.number_of_nodes,
                                     class_channels_send=self.factory.class_channels_send,
                                     class_channels_receive=self.factory.class_channels_receive,
                                     distributors=self.factory.distributors,
                                     logic_params=logic_params,
                                     network_params=network_params
                                     )
            up.start()

            up.remove_item('logic')
            up['logic'] = StoreObject()
            up['logic'].process_msgs = lambda direction, msglist: \
                up['logic'].set(obj=(direction, msglist))

            sender = 4
            msg_sent = Message.generate_entanglement(Message.SUBJECT_ASK, sender=sender)
            self.factory.class_channels_receive[index]["L"].send(items=msg_sent)
            ns.sim_run()
            (direction, [msg_received]) = up['logic'].object
            self.assertEqual(direction, "L")
            self.assertEqual(msg_sent, msg_received)


if __name__ == "__main__":
    unittest.main()
