import unittest
from netsquid.qubits.qubitapi import operate, create_qubits
from repchain.protocols.moveprotocol import ThreeCNOTMoveProgram, TwoCNOTMoveProgram, CXDirectionMoveProgram,\
    ReverseCXDirectionMoveProgram, MagicalMoveProgram, MagicalMoveRotatedTargetProgram
from repchain.network.abstract_network_factory import create_network_from_dic
import netsquid as ns
from netsquid.qubits.operators import X, H
from netsquid.qubits.ketstates import s0, s1, h0, h1
from netsquid.qubits.qubitapi import fidelity
import logging
from abc import ABCMeta
import numpy as np


class ITestMovePrograms(unittest.TestCase, metaclass=ABCMeta):

    prgm_cls = None

    electron = 0
    carbon = 1

    possible_electron_states = ["0", "1"]
    possible_carbon_states = ["0", "1"]

    def setUp(self):
        ns.set_qstate_formalism(ns.QFormalism.DM)
        self.positions = [self.electron, self.carbon]

    def test_outcome(self):
        if self.prgm_cls is None:
            return

        network_params = {
            "number_of_nodes": 1,
            "tot_num_qubits": 4,
            "qmem_config_params": {
                "p_q1gate": 0,
                "p_q1init": 0,
                "p_q1meas": (0, 0),
                "p_q2gate": 0,
                "T1": 0,
                "T2": 0,
                "tot_num_qubits": 4,
                "t_q1gate": 0,
                "t_q1init": 0,
                "t_q1meas": 0,
                "t_q2gate": 0
            },
            "distributor_params": {
                "p_loss_length": 0.2,
                "elementary_link_fidelity": 0.7,
                "elementary_link_succ_prob": 1.,
                "werner": True,
                "eps": 0,
                "cycle_time": 1e3
            },
            "internode_distance": 0.002,
            "c": 3e5}

        delivery_params = network_params['distributor_params']

        logic_params = {
            "action_ordering": "default",
            "is_move_magical": True,
            "swap_fidelity_threshold": 0.,
            "delivery_params": delivery_params,
            "use_two_node_NV": False,
            "use_swap_only": False,
            "distillation_strategy": "default",
            "respect_NV_structure": False}

        self.factory = create_network_from_dic(network_params, logic_params)
        self.network = self.factory.network

        node0 = self.network.get_node(name="Abstractnode{}".format(0))
        self.qmemory = node0.qmemory
        logging.info("Now testing {}".format(self.prgm_cls))
        for state_electron in self.possible_electron_states:
            for state_carbon in self.possible_carbon_states:
                logging.info('Testing the following states: electron |{}>, carbon |{}>'.format(state_electron,
                                                                                               state_carbon))
                ns.sim_reset()
                self.qmemory.reset()

                # put the physical qubits in the quantum memory
                qubits = create_qubits(2)
                self.qmemory.put(qubits=qubits, positions=self.positions)

                self._apply_intermediate_quantum_operations(state_electron, state_carbon)

                # run the circuit
                self.prgm = self.prgm_cls()
                self.qmemory.execute_program(
                    self.prgm, qubit_mapping=[self.electron, self.carbon])
                self.qmemory.set_program_done_callback(
                    callback=self._check_outcome,
                    initial_state_electron=state_electron,
                    initial_state_carbon=state_carbon)
                ns.sim_run()

    def _apply_intermediate_quantum_operations(self, state_electron, state_carbon):
        for (state, pos) in [(state_electron, self.electron),
                             (state_carbon, self.carbon)]:
            if state == "1":
                [q] = self.qmemory.peek(positions=[pos])
                operate(q, X)
            if state == "+":
                [q] = self.qmemory.peek(positions=[pos])
                operate(q, H)
            if state == "-":
                [q] = self.qmemory.peek(positions=[pos])
                operate(q, X)
                operate(q, H)

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        """
        Meant to be subclassed.

        Returns
        -------
        list of (int, str)
            Tuple (position, state) where state is either 0 or 1
        """
        pass

    def _check_outcome(self, initial_state_electron, initial_state_carbon):
        tuples_to_check = self._get_pos_vs_state_expectation(initial_state_electron, initial_state_carbon)
        for (pos, state) in tuples_to_check:
            [q] = self.qmemory.peek(positions=[pos])
            # print(state)
            # print(q.qstate.ket)
            if state == "0":
                expected_outcome_ket = s0
            elif state == "1":
                expected_outcome_ket = s1
            elif state == "+":
                expected_outcome_ket = h0
            elif state == "-":
                expected_outcome_ket = h1
            else:
                raise ValueError

            fid = fidelity([q], reference_state=expected_outcome_ket)
            self.assertTrue(np.isclose(fid, 1.0))


class TestMagicalMoveProgram(ITestMovePrograms):
    prgm_cls = MagicalMoveProgram

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        # print(initial_state_electron, initial_state_carbon)
        return [(cls.electron, initial_state_carbon),
                (cls.carbon, initial_state_electron)]


class TestMagicalMoveRotatedTargetProgram(ITestMovePrograms):
    prgm_cls = MagicalMoveRotatedTargetProgram
    possible_carbon_states = ["+", "-"]

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        if initial_state_electron == "0":
            expected_state_carbon = "+"
        elif initial_state_electron == "1":
            expected_state_carbon = "-"
        else:
            raise ValueError

        if initial_state_carbon == "+":
            expected_state_electron = "0"
        elif initial_state_carbon == "-":
            expected_state_electron = "1"
        else:
            raise ValueError

        return [(cls.electron, expected_state_electron),
                (cls.carbon, expected_state_carbon)]


class TestThreeCNOTMoveProgram(TestMagicalMoveProgram):
    prgm_cls = ThreeCNOTMoveProgram


class TwoCNOTMoveProgram(ITestMovePrograms):
    prgm_cls = TwoCNOTMoveProgram

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        return [(cls.electron, initial_state_carbon)]


class CXDirectionMoveProgram(ITestMovePrograms):
    prgm_cls = CXDirectionMoveProgram
    possible_electron_states = ["0", "1"]
    possible_carbon_states = ["0"]

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        if initial_state_electron == "0":
            expected_state_carbon = "+"
        elif initial_state_electron == "1":
            expected_state_carbon = "-"
        else:
            raise ValueError

        if initial_state_carbon == "0":
            expected_state_electron = "0"
        else:
            raise ValueError

        return [(cls.carbon, expected_state_carbon),
                (cls.electron, expected_state_electron)]


class ReverseCXDirectionMoveProgram(ITestMovePrograms):
    prgm_cls = ReverseCXDirectionMoveProgram
    possible_electron_states = ["0"]
    possible_carbon_states = ["+", "-"]

    @classmethod
    def _get_pos_vs_state_expectation(cls, initial_state_electron, initial_state_carbon):
        if initial_state_carbon == "+":
            expected_state_electron = "0"
        elif initial_state_carbon == "-":
            expected_state_electron = "1"
        else:
            raise ValueError

        if initial_state_electron == "0":
            expected_state_carbon = "0"
        else:
            raise ValueError

        return [(cls.electron, expected_state_electron),
                (cls.carbon, expected_state_carbon)]


if __name__ == "__main__":
    unittest.main()
