from repchain.runtools.simulation_setup import SimulationSetup
import unittest
from repchain.runtools.data_collection_tools import get_statetapper
import netsquid as ns
import numpy as np


def should_stop_fn(simsetup, index, remote_index):
    memmanager = simsetup.protocols[index]['memmanager']
    links = memmanager.entangled_positions_with(remote_node_id=remote_index)
    return len(links) > 0


class Test_SimulationSetup(unittest.TestCase):

    def _get_simsetup(self, number_of_nodes, use_swap_only):
        ns.sim_reset()

        network_params = {
            "number_of_nodes": number_of_nodes,
            "tot_num_qubits": 4,
            "qmem_config_params": {
                "p_q1gate": 0,
                "p_q1init": 0,
                "p_q1meas": (0, 0),
                "p_q2gate": 0,
                "T1": 0,
                "T2": 0,
                "tot_num_qubits": 10,
                "t_q1gate": 0,
                "t_q1init": 0,
                "t_q1meas": 0,
                "t_q2gate": 0
            },
            "distributor_params": {
                "p_loss_length": 0.2,
                "elementary_link_fidelity": 1.,
                "elementary_link_succ_prob": 1.,
                "werner": True,
                "eps": 0,
                "cycle_time": 1e3
            },
            "internode_distance": 0.002,
            "c": 3e5}

        delivery_params = network_params['distributor_params']

        logic_params = {
            "action_ordering": "default",
            "is_move_magical": True,
            "swap_fidelity_threshold": 0.,
            "delivery_params": delivery_params,
            "use_two_node_NV": False,
            "use_swap_only": use_swap_only,
            "distillation_strategy": ('DEJMPS', 1),
            "respect_NV_structure": False,
            "distillation_scheduler": "default"
        }

        simsetup = SimulationSetup.from_dict(
            network_params=network_params,
            logic_params=logic_params)

        for (index, remote_index) in [(0, number_of_nodes - 1), (number_of_nodes - 1, 0)]:
            simsetup.protocols[index]['logic'].should_stop_fn = \
                lambda simsetup=simsetup, index=index, remote_index=remote_index: should_stop_fn(
                    simsetup=simsetup, index=index, remote_index=remote_index)

        simsetup.set_automatic_stop()

        node1 = simsetup.network.get_node(name="Abstractnode{}".format(0))
        node2 = simsetup.network.get_node(name="Abstractnode{}".format(simsetup.number_of_nodes - 1))
        statetapper = get_statetapper(
            memorymanager_A=simsetup.protocols[0]['memmanager'],
            memorymanager_B=simsetup.protocols[simsetup.number_of_nodes - 1]['memmanager'],
            qmemory_A=node1.qmemory,
            qmemory_B=node2.qmemory,
            node_index_A=0,
            node_index_B=simsetup.number_of_nodes - 1,
            collect_postsimulation=True,
            apply_Hadamard_if_data_qubit=True)

        simsetup.add_datatapper(statetapper)

        return simsetup

    def test_single_run_many_nodes(self, number_of_nodes=2**1 + 1, uses_swap_only='default'):
        if uses_swap_only == 'default':
            uses_swap_only = [True, False]
        for use_swap_only in uses_swap_only:
            print('use_swap_only', use_swap_only)
            simsetup = self._get_simsetup(number_of_nodes=number_of_nodes,
                                          use_swap_only=use_swap_only)
            statetapper = simsetup.datatappers[0]
            simsetup.perform_run()
            self.assertEqual(len(statetapper.dataframe.index), 1)
            fidelity = statetapper.dataframe["fidelity"][0]
            print(fidelity)
            self.assertTrue(np.isclose(fidelity, 1.0))

    def test_many_runs(self):
        for use_swap_only in [True, False]:
            simsetup = self._get_simsetup(number_of_nodes=2**1 + 1,
                                          use_swap_only=use_swap_only)
            statetapper = simsetup.datatappers[0]

            number_of_runs = 10
            simsetup.perform_many_runs(number_of_runs)
            self.assertEqual(len(statetapper.dataframe.index), number_of_runs)
            fidelities = statetapper.dataframe["fidelity"]
            for fidelity in fidelities:
                print(fidelity)
                self.assertTrue(np.isclose(fidelity, 1.0))


if __name__ == "__main__":
    unittest.main()
