"""
Tests that the UnrestrictedDistillationProgram and NVDistillationProgram
give the correct output on all possibel 4x4 combinations of two input
Bell states.
"""
import unittest
import numpy as np
from abc import ABCMeta

import netsquid as ns
from netsquid.components.qprocessor import QuantumProcessor, PhysicalInstruction
from netsquid.components.instructions import \
    INSTR_CNOT, INSTR_MEASURE, INSTR_CXDIR, INSTR_H,\
    INSTR_INIT, INSTR_ROT_X, INSTR_ROT_Z, INSTR_ROT_Y
from netsquid.qubits.qubitapi import fidelity
import netsquid.qubits.qubitapi as qapi
from netsquid.qubits.dmtools import DMState
from netsquid_abstractmodel.abstract_node import AbstractProcessor

from repchain.physical.simplified_delivery_model import get_states
from repchain.protocols.distillation_protocol import NVDEJMPSProgram, DEJMPSProgram, EPLProgram
from repchain.utils.tools import _add_Bell_pair, bell_index_to_ket_state,\
    bell_index_to_stabilizer_coefficients, stabilizer_coefficients_to_bell_index
from repchain.utils.timlogging import logging


def convPM2ZO(outcome):
    """
    Convert a measurement outcome +1/-1 to 0/1
    """
    return (1 - outcome) / 2


def convZO2PM(outcome):
    """
    Convert a measurement outcome 0/1 to +1/-1
    """
    return 1 - 2 * outcome


class ITestDistillationProgram(unittest.TestCase, metaclass=ABCMeta):
    """
    Abstract base class for testing a QuantumProgram which implements
    the local operations of distillation.
    """
    pos_lose, pos_keep = 0, 1
    prgm_cls = None

    def setUp(self):
        """
        Setting up the two QuantumProcessors and
        their corresponding PhysicalInstructions.
        """
        ns.set_qstate_formalism(ns.QFormalism.DM)
        self.qmemorys = {}
        self.prgms = {}
        for name in ["A", "B"]:
            self.qmemorys[name] = \
                QuantumProcessor(name="TestQProcessor_{}".format(name),
                                 num_positions=10)
            for instr in [INSTR_CNOT, INSTR_H, INSTR_MEASURE, INSTR_CXDIR,
                          INSTR_H, INSTR_INIT, INSTR_ROT_X, INSTR_ROT_Z,
                          INSTR_ROT_Y]:
                self.qmemorys[name].add_physical_instruction(
                    phys_instr=PhysicalInstruction(instr, duration=0.))
        self.number_of_programs_that_finished = 0

    def test_program(self):
        """
        Checking whether the QuantumProgram runs correctly
        on all possible 4x4 combinations of two Bell states.
        """
        if self.prgm_cls is None:
            return
        else:
            for name in ["A", "B"]:
                self.prgms[name] = self.prgm_cls()

        logging.info('Now computing {}'.format(self.prgm_cls))
        for __ in range(100):
            ns.sim_reset()
            for bell_index_keep in [0, 1, 2, 3]:
                for bell_index_lose in [0, 1, 2, 3]:
                    self._test_program_once(bell_index_keep=bell_index_keep,
                                            bell_index_lose=bell_index_lose)
            # self.assertTrue(False)

    def _test_program_once(self, bell_index_keep, bell_index_lose):
        """
        Checking whether the quantum program runs correctly
        on a given combination of two input Bell states.
        """
        print(f'{bell_index_keep},{bell_index_lose}', end=',')
        # expected results
        expected_final_bell_index = \
            self._compute_expected_final_state(bell_index_keep=bell_index_keep,
                                               bell_index_lose=bell_index_lose)

        # the measurement outcomes are random, but they do satisfy
        # a particular relationship which we can test for
        expected_possible_measurement_outcomes = \
            self._compute_possible_measurement_outcomes(
                bell_index_keep=bell_index_keep,
                bell_index_lose=bell_index_lose)

        # run the simulation and compare the output to the expected output
        program_done_callback_params = \
            {"callback": self._compare_simulation_vs_analytics,
             "expected_final_bell_index": expected_final_bell_index,
             "expected_possible_measurement_outcomes":
                 expected_possible_measurement_outcomes}
        self._setup_run(
            bell_index_keep=bell_index_keep,
            bell_index_lose=bell_index_lose,
            program_done_callback_params=program_done_callback_params)
        ns.sim_run()

    @classmethod
    def _compute_expected_final_state(cls, bell_index_keep, bell_index_lose):
        """
        Meant to be subclassed.

        :rtype: int
        :return: Bell index of the expected physical state on the 'keep' qubits
        """
        pass

    @classmethod
    def _compute_possible_measurement_outcomes(
            cls, bell_index_keep, bell_index_lose):
        """
        :rtype: list of tuples (x, y) where x and y are among 0 and 1.
        """
        ret = []
        for first_outcome in [0, 1]:
            second_outcome = cls._get_second_measurement_outcome(
                bell_index_keep=bell_index_keep,
                bell_index_lose=bell_index_lose,
                first_measurement_outcome=first_outcome)
            outcome_pair = (first_outcome, second_outcome)
            ret.append(outcome_pair)
        return ret

    def _get_second_measurement_outcome(
            cls, bell_index_keep, bell_index_lose,
            first_measurement_outcome):
        """
        Meant to be subclassed.

        :param int first_measurement_outcome: either 0 or 1

        :rtype: 0 or 1
        :return: second measurement outcome given the first
        """
        pass

    def _compare_simulation_vs_analytics(
            self, expected_final_bell_index,
            expected_possible_measurement_outcomes):
        """
        Checks whether the measurement outcomes and the final
        state after the two quantum processors have
        finished their individual distillation operations
        in simulation and analytics coincide.
        """

        # ensure that we only perform the check once both nodes have finished
        if self.number_of_programs_that_finished == 0:
            self.number_of_programs_that_finished += 1
        elif self.number_of_programs_that_finished == 1:
            self.number_of_programs_that_finished = 0

            # assert that we get the expected relation between the outcomes
            outcomes = tuple([self.prgms[name].outcome for name in ["A", "B"]])
            print(outcomes, end=',')
            self.assertTrue(outcomes in expected_possible_measurement_outcomes)

            # assert that we get the expected output state
            expected_final_ket = bell_index_to_ket_state(
                bell_index=expected_final_bell_index)

            [q_keep_A] = self.qmemorys["A"].peek(positions=[self.pos_keep])
            [q_keep_B] = self.qmemorys["B"].peek(positions=[self.pos_keep])
            real_state = expected_final_bell_index
            for a in range(4):
                fid = fidelity([q_keep_A, q_keep_B],  reference_state=bell_index_to_ket_state(a))
                if fid > 0.9:
                    real_state = a
            print(f'{expected_final_bell_index},{real_state}')
            fid = fidelity([q_keep_A, q_keep_B], reference_state=expected_final_ket)
            self.assertTrue(np.isclose(fid, 1.0))

        else:
            raise Exception

    def _setup_run(self, bell_index_keep, bell_index_lose, program_done_callback_params):
        """
        Start with the following setup:

        +-------------+                        +--------------------+
        | qprocessor  |                        |  qprocessor        |
        |             |                        |                    |
        |             |    bell_index_keep     |                    |
        |     A0--------------------------------------B0            |
        |             |                        |                    |
        |             |    bell_index_lose     |                    |
        |     B0-------------------------------------B1             |
        |             |                        |                    |
        +-------------+                        +--------------------+
        (where A0, A1, B0 and B1 are qubits),
        and set up a quantum program for the two nodes each individually.
        """
        # setup and start the program
        ns.sim_reset()
        for name in ["A", "B"]:
            self.qmemorys[name].reset()

        # add the two bell states to quantum memory
        for (bell_index, name, pos) in \
                [(bell_index_keep, "A", self.pos_keep),
                 (bell_index_lose, "B", self.pos_lose)]:
            other_name = "A" if name == "B" else "B"
            _add_Bell_pair(qmemA=self.qmemorys[name],
                           qmemB=self.qmemorys[other_name],
                           posA=pos, posB=pos, bell_index=bell_index)

        # setup the local distillation operations as quantum program
        for name in ["A", "B"]:
            self.qmemorys[name].set_program_done_callback(
                **program_done_callback_params)
            self.qmemorys[name].execute_program(
                self.prgms[name],
                qubit_mapping=[self.pos_lose, self.pos_keep])


class TestUnrestrictedDistillationProgram(ITestDistillationProgram):
    prgm_cls = DEJMPSProgram

    @classmethod
    def _compute_expected_final_state(cls, bell_index_keep, bell_index_lose):
        (a, b) = bell_index_to_stabilizer_coefficients(bell_index=bell_index_keep)
        (c, d) = bell_index_to_stabilizer_coefficients(bell_index=bell_index_lose)
        stabilizer_coefficients_of_final_state = (a * c, b)
        return stabilizer_coefficients_to_bell_index(stabilizer_coefficients_of_final_state)

    @classmethod
    def _get_second_measurement_outcome(
            cls, bell_index_keep,
            bell_index_lose, first_measurement_outcome):
        (a, b) = bell_index_to_stabilizer_coefficients(bell_index_keep)
        (c, d) = bell_index_to_stabilizer_coefficients(bell_index_lose)
        return convPM2ZO(convZO2PM(first_measurement_outcome) * b * d)


class TestNVDistillationProgram(ITestDistillationProgram):
    prgm_cls = NVDEJMPSProgram

    @classmethod
    def _compute_expected_final_state(cls, bell_index_keep, bell_index_lose):

        # NOTE: the following values are the coefficients of the raw state
        # that is put on the qubits; it is thus the state that is already
        # rotated locally!

        # the electron: lose
        (a, b) = bell_index_to_stabilizer_coefficients(bell_index=bell_index_lose)

        # the carbon: keep
        (c, d) = bell_index_to_stabilizer_coefficients(bell_index=bell_index_keep)

        # the final state on the carbons
        stabilizer_coefficients_of_final_state = (c, -1 * a * d * c)

        # the final state on the electrons
        # stabilizer_coefficients_of_final_state = (-1 * a * b * c, b * c)

        # the final state on the electrons just before measurement
        # is (b *c, a * b * c)
        return stabilizer_coefficients_to_bell_index(stabilizer_coefficients_of_final_state)

    @classmethod
    def _get_second_measurement_outcome(
            cls, bell_index_keep,
            bell_index_lose, first_measurement_outcome):
        # the electron: lose
        (a, b) = bell_index_to_stabilizer_coefficients(bell_index=bell_index_lose)

        # the carbon: keep
        (c, d) = bell_index_to_stabilizer_coefficients(bell_index=bell_index_keep)

        return convPM2ZO(convZO2PM(first_measurement_outcome) * b * c)


class TestEPLProgram(unittest.TestCase, metaclass=ABCMeta):
    """
    Abstract base class for testing a QuantumProgram which implements
    the local operations of distillation.
    """
    pos_lose, pos_keep = 0, 1

    def setUp(self):
        """
        Setting up the two QuantumProcessors and
        their corresponding PhysicalInstructions.
        """
        ns.set_qstate_formalism(ns.QFormalism.DM)
        self.qmemorys = {}
        self.prgms = {}
        for name in ["A", "B"]:
            self.qmemorys[name] = AbstractProcessor(num_positions=10, noiseless=True, instantaneous=True)
        self.number_of_programs_that_finished = 0

    def test_program(self):
        """Checking whether the QuantumProgram runs correctly on all possible 4x4 combinations of two Bell states."""
        for name in ["A", "B"]:
            self.prgms[name] = EPLProgram()

        for f in [0.6, 0.7, 0.8, 0.9, 1.0]:
            for bell_index_keep in [1, 3]:
                for bell_index_lose in [1, 3]:
                    for _ in range(100):
                        self._test_program_once(bell_index_keep=bell_index_keep,
                                                bell_index_lose=bell_index_lose,
                                                fidelity=f)

    def _test_program_once(self, bell_index_keep, bell_index_lose, fidelity=1.0):
        """Checking whether the quantum program runs correctly on a given combination of two input Bell states."""
        # expected results
        expected_final_bell_index = \
            self._compute_expected_final_state(bell_index_keep=bell_index_keep, bell_index_lose=bell_index_lose)

        # run the simulation and compare the output to the expected output
        program_done_callback_params = {
            "callback": self._compare_simulation_vs_analytics,
            "expected_final_bell_index": expected_final_bell_index
        }
        self._setup_run(
            bell_index_keep=bell_index_keep,
            bell_index_lose=bell_index_lose,
            fidelity=fidelity,
            program_done_callback_params=program_done_callback_params)
        ns.sim_run()

    def _compute_expected_final_state(self, bell_index_keep, bell_index_lose):
        """State after distillation succeeds."""
        (a, b) = bell_index_to_stabilizer_coefficients(bell_index=bell_index_keep)
        (c, d) = bell_index_to_stabilizer_coefficients(bell_index=bell_index_lose)
        stabilizer_coefficients_of_final_state = (a * c, b)
        return stabilizer_coefficients_to_bell_index(stabilizer_coefficients_of_final_state)

    def _compare_simulation_vs_analytics(self, expected_final_bell_index):
        """Checks whether the measurement outcomes and the final state after the two quantum processors have
        finished their individual distillation operations in simulation and analytics coincide."""

        # ensure that we only perform the check once both nodes have finished
        if self.number_of_programs_that_finished == 0:
            self.number_of_programs_that_finished += 1
        elif self.number_of_programs_that_finished == 1:
            self.number_of_programs_that_finished = 0

            # assert that we get the expected relation between the outcomes
            outcomes = tuple([self.prgms[name].outcome for name in ["A", "B"]])
            if not all([self.prgms[name].was_distillation_successful(*outcomes) for name in ["A", "B"]]):
                return

            # assert that we get the expected output state
            expected_final_ket = bell_index_to_ket_state(expected_final_bell_index)

            [q_keep_A] = self.qmemorys["A"].peek(positions=[self.pos_keep])
            [q_keep_B] = self.qmemorys["B"].peek(positions=[self.pos_keep])
            fid = fidelity([q_keep_A, q_keep_B], reference_state=expected_final_ket)
            self.assertTrue(np.isclose(fid, 1.0))

        else:
            raise Exception

    def _setup_run(self, bell_index_keep, bell_index_lose, fidelity, program_done_callback_params):
        """Start with the following setup:

        +-------------+                        +--------------------+
        | qprocessor  |                        |  qprocessor        |
        |             |                        |                    |
        |             |    bell_index_keep     |                    |
        |     A0--------------------------------------B0            |
        |             |                        |                    |
        |             |    bell_index_lose     |                    |
        |     B0-------------------------------------B1             |
        |             |                        |                    |
        +-------------+                        +--------------------+
        (where A0, A1, B0 and B1 are qubits),
        and set up a quantum program for the two nodes each individually.
        """
        # setup and start the program
        ns.sim_reset()
        for name in ["A", "B"]:
            self.qmemorys[name].reset()

        # add the two bell states to quantum memory
        for (bell_index, name, pos) in [(bell_index_keep, "A", self.pos_keep), (bell_index_lose, "B", self.pos_lose)]:
            other_name = "A" if name == "B" else "B"
            q0, q1 = qapi.create_qubits(2)
            DMState(qubits=[q0, q1], dm=get_states(fidelity, werner=False)[0][0 if bell_index == 1 else 1])  # Psi+/Psi-
            qapi.dephase(q0, 0)
            qapi.dephase(q1, 0)
            self.qmemorys[name].put([q0], positions=[pos])
            self.qmemorys[other_name].put([q1], positions=[pos])

        # setup the local distillation operations as quantum program
        for name in ["A", "B"]:
            self.qmemorys[name].set_program_done_callback(**program_done_callback_params)
            self.qmemorys[name].execute_program(self.prgms[name], qubit_mapping=[self.pos_lose, self.pos_keep])


if __name__ == "__main__":
    unittest.main()
