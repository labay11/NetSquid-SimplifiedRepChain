"""
An integration test for entanglement swapping, involving three nodes
"""
import unittest

import numpy as np
import netsquid as ns
from netsquid.qubits.qubitapi import fidelity
from netsquid_abstractmodel.programs import AbstractSwapProgram

from repchain.protocols.entswapProtocol import EntSwapProtocol
from repchain.utils.memorymanager import MemoryManager
from repchain.utils.tools import _add_Bell_pair, bell_index_to_ket_state, apply_Pauli_to_one_side_of_bell_state,\
    index_of_Pauli_product
from repchain.network.abstract_network_factory import create_network_from_dic


class TestIntegrationEntSwap(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()

        network_params = {
            "number_of_nodes": 3,
            "tot_num_qubits": 4,
            "qmem_config_params": {
                "p_q1gate": 0,
                "p_q1init": 0,
                "p_q1meas": (0, 0),
                "p_q2gate": 0,
                "T1": 0,
                "T2": 0,
                "tot_num_qubits": 4,
                "t_q1gate": 0,
                "t_q1init": 0,
                "t_q1meas": 0,
                "t_q2gate": 0
            },
            "distributor_params": {
                "p_loss_length": 0.2,
                "elementary_link_fidelity": 0.7,
                "elementary_link_succ_prob": 1.,
                "werner": True,
                "eps": 0,
                "cycle_time": 1e3
            },
            "internode_distance": 0.002,
            "c": 3e5}

        delivery_params = network_params['distributor_params']

        logic_params = {
            "action_ordering": "default",
            "is_move_magical": True,
            "swap_fidelity_threshold": 0.,
            "delivery_params": delivery_params,
            "use_two_node_NV": False,
            "use_swap_only": False,
            "distillation_strategy": "default",
            "respect_NV_structure": False}

        self.factory = create_network_from_dic(network_params, logic_params)
        self.network = self.factory.network

    def _pass_on_channel_output_to_protocol(self, msg, channel, protocol):
        # TODO I think `direction` is irrelevant
        protocol.process_incoming_message(msg=msg.items[0])

    def test_on_Bell_states(self):
        # two links:
        # node 0          node 1           node 2
        #   0 <------------> 0
        #                    1 <------------> 1
        # and node 1 will perform the entanglement swap

        for P_node0_pos0 in range(4):
            for P_node1_pos0 in range(4):
                for P_node1_pos2 in range(4):
                    for P_node2_pos2 in range(4):
                        self._perform_test(P_node0_pos0,
                                           P_node1_pos0,
                                           P_node1_pos2,
                                           P_node2_pos2)

    def _perform_test(self, P_node0_pos0,
                      P_node1_pos0,
                      P_node1_pos2,
                      P_node2_pos2):
        protocols = [
            EntSwapProtocol(
                index=index,
                qmemory=self.network.get_node(name="Abstractnode{}".format(index)).qmemory,
                memmanager=MemoryManager(num_positions=100, elementary_link_fidelity=0.8),
                swap_update_receivers=None,
                # TODO should also test not-None case
                class_channels=self.factory.class_channels_send[index],
                respect_NV_structure=False)
            for index in range(3)]
        # tie the classical channels for sending to the
        # `process_msgs` of the protocols
        for (receiving_node_index, receiving_direction) in \
                [(0, "R"), (1, "L"), (1, "R"), (2, "L")]:
            receive_channel = \
                self.factory.class_channels_receive[receiving_node_index][receiving_direction]
            receiving_protocol = protocols[receiving_node_index]
            receive_channel.ports['recv'].bind_output_handler(
                msg_handler=lambda msg, channel=receive_channel, protocol=receiving_protocol:
                    self._pass_on_channel_output_to_protocol(msg=msg, channel=channel, protocol=protocol)
            )
            # receive_channel.register_handler(callback_function=lambda event, channel=receive_channel,
            #                                  protocol=receiving_protocol:
            #                                  self._pass_on_channel_output_to_protocol(channel, protocol))

        pos_0 = 0
        pos_2 = 1

        for node_ID in range(3):
            protocols[node_ID].memmanager.reset()
            protocols[node_ID].reset()
        for index in self.factory.indices:
            node = self.network.get_node(name="Abstractnode{}".format(index))
            node.qmemory.reset()

        # compute the bell indices by applying the local Paulis to Psi^+
        bell_index = 1
        virtual_bell_index_0 = \
            apply_Pauli_to_one_side_of_bell_state(bell_index, index_of_Pauli_product(P_node0_pos0, P_node1_pos0))
        virtual_bell_index_2 = \
            apply_Pauli_to_one_side_of_bell_state(bell_index, index_of_Pauli_product(P_node1_pos2, P_node2_pos2))

        physical_bell_index_0 = virtual_bell_index_0
        physical_bell_index_2 = virtual_bell_index_2
        # physical_bell_index_2 = apply_Hadamard_on_each_qubit(virtual_bell_index_2)

        # add states
        for (node_id_A, node_id_B, posA, posB, bell_index) in \
                [(0, 1, pos_0, pos_0, physical_bell_index_0),
                 (1, 2, pos_2, pos_2, physical_bell_index_2)]:
            nodeA = self.network.get_node(name="Abstractnode{}".format(node_id_A))
            nodeB = self.network.get_node(name="Abstractnode{}".format(node_id_B))
            _add_Bell_pair(qmemA=nodeA.qmemory,
                           qmemB=nodeB.qmemory,
                           posA=posA,
                           posB=posB,
                           bell_index=bell_index)
        for (node_id_self, remote_node_id, pos, cor_Pauli) in \
            [(0, 1, pos_0, P_node0_pos0),
             (1, 0, pos_0, P_node1_pos0),
             (1, 2, pos_2, P_node1_pos2),
             (2, 1, pos_2, P_node2_pos2)]:
            protocols[node_id_self].memmanager.entangle(
                pos=pos,
                remote_node_id=remote_node_id,
                cor_Pauli=cor_Pauli)

        # start the entanglement swapping
        protocols[1].trigger(qmem_posA=pos_0, qmem_posB=pos_2)
        ns.sim_run()

        # assert that we end up with the correct Bell state afterwards
        resulting_link_0 = protocols[0].memmanager.get_link(pos=pos_0)
        resulting_link_2 = protocols[2].memmanager.get_link(pos=pos_2)

        self.assertEqual(resulting_link_0.remote_node_id, 2)
        self.assertEqual(resulting_link_2.remote_node_id, 0)
        for pos in [pos_0, pos_2]:
            self.assertTrue(protocols[1].memmanager.is_free(pos=pos))

        node0 = self.network.get_node(name="Abstractnode{}".format(0))
        node2 = self.network.get_node(name="Abstractnode{}".format(2))
        [q0] = node0.qmemory.peek(positions=[pos_0])
        [q2] = node2.qmemory.peek(positions=[pos_2])
        cor_Pauli_0 = resulting_link_0.cor_Pauli
        cor_Pauli_2 = resulting_link_2.cor_Pauli
        expected_resulting_virtual_bell_index = \
            apply_Pauli_to_one_side_of_bell_state(
                bell_index=1,
                pauli_index=index_of_Pauli_product(cor_Pauli_0, cor_Pauli_2))
        expected_virtual_ket = bell_index_to_ket_state(expected_resulting_virtual_bell_index)

        # apply a hadamard on one side
        # expected_resulting_ket = ???->do this using operate(expected_virtual_ket, h)
        expected_resulting_ket = expected_virtual_ket

        fid = fidelity([q0, q2], reference_state=expected_resulting_ket)
        self.assertTrue(np.isclose(fid, 1.0))


class TestBSMPrograms(unittest.TestCase):

    def test_outcome(self):
        ns.set_qstate_formalism(ns.QFormalism.DM)

        network_params = {
            "number_of_nodes": 3,
            "tot_num_qubits": 4,
            "qmem_config_params": {
                "p_q1gate": 0,
                "p_q1init": 0,
                "p_q1meas": (0, 0),
                "p_q2gate": 0,
                "T1": 0,
                "T2": 0,
                "tot_num_qubits": 4,
                "t_q1gate": 0,
                "t_q1init": 0,
                "t_q1meas": 0,
                "t_q2gate": 0
            },
            "distributor_params": {
                "p_loss_length": 0.2,
                "elementary_link_fidelity": 0.7,
                "elementary_link_succ_prob": 1.,
                "werner": True,
                "eps": 0,
                "cycle_time": 1e3
            },
            "internode_distance": 0.002,
            "c": 3e5}

        delivery_params = network_params['distributor_params']

        logic_params = {
            "action_ordering": "default",
            "is_move_magical": True,
            "swap_fidelity_threshold": 0.,
            "delivery_params": delivery_params,
            "use_two_node_NV": False,
            "use_swap_only": False,
            "distillation_strategy": "default",
            "respect_NV_structure": False}

        self.factory = create_network_from_dic(network_params, logic_params)
        self.network = self.factory.network

        node0 = self.network.get_node(name="Abstractnode{}".format(0))
        qmemory = node0.qmemory
        print(qmemory)
        electron = 0
        carbon = 1

        for bell_index in [0, 1, 2, 3]:
            ns.sim_reset()
            qmemory.reset()

            # put the physical qubits in the quantum memories
            _add_Bell_pair(qmemA=qmemory,
                           qmemB=qmemory,
                           posA=electron,
                           posB=carbon,
                           bell_index=bell_index)
            # run the circuit
            self.prgm = AbstractSwapProgram()
            qmemory.execute_program(
                self.prgm, qubit_mapping=[electron, carbon])
            qmemory.set_program_done_callback(
                callback=self._check_outcome, expected_outcome=bell_index)
            ns.sim_run()

    def _check_outcome(self, expected_outcome):
        node0 = self.network.get_node(name="Abstractnode{}".format(0))
        qmemory = node0.qmemory
        [q_carbon] = qmemory.peek(positions=[1])
        # print('carbon state', q_carbon.qstate)
        # print('prgm outcome', self.prgm.get_outcome_as_bell_index)
        # print('prgm outcomes', self.prgm.output)
        # print('prgm class', self.prgm)
        self.assertEqual(expected_outcome, self.prgm.get_outcome_as_bell_index)


if __name__ == "__main__":
    unittest.main()
