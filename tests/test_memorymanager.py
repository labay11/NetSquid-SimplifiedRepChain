import math

import pytest

from repchain.utils.memorymanager import MemoryTracker
# import logging
# from netsquid import logger, set_console_debug
# set_console_debug

NUM_POSITIONS = 30
ELEMENTARY_LINK_FIDELITY = 0.9


@pytest.fixture
def memory_tracker():
    return MemoryTracker(num_positions=NUM_POSITIONS, elementary_link_fidelity=ELEMENTARY_LINK_FIDELITY)


def test_entangle_and_release(memory_tracker):
    memory_tracker.entangle(0, 42, cor_Pauli=1)

    assert memory_tracker.entangled_with(42)
    assert memory_tracker.remote_node_at(0) == 42

    link = memory_tracker.get_link(0)
    assert link.estimated_fidelity == ELEMENTARY_LINK_FIDELITY
    assert link.remote_node_id == 42
    assert link.cor_Pauli == 1

    memory_tracker.release(0)
    assert not memory_tracker.entangled_with(42)

    with pytest.raises(Exception):
        memory_tracker.release(0)

    with pytest.raises(AssertionError):
        memory_tracker.release(NUM_POSITIONS + 10)
        memory_tracker.entangle(NUM_POSITIONS + 10, 42)
        assert not memory_tracker.entangled_with(42)


def test_reset(memory_tracker):
    for n in range(NUM_POSITIONS):
        memory_tracker.entangle(n, n)

    assert len(memory_tracker.free_positions()) == 0
    memory_tracker.reset()
    assert len(memory_tracker.free_positions()) == NUM_POSITIONS


def test_setting_entanglement(memory_tracker):
    positions = [1, 3, 5, 7, 11, 13, 17, 19, 23, 29]
    remotes = [11, 13, 13, 13, 17, 17, 17, 17, 17, 17]
    for (pos, remote_node_id) in zip(positions, remotes):
        memory_tracker.entangle(pos=pos, remote_node_id=remote_node_id)
    # asserting only the entangled positions are not free
    for pos in range(NUM_POSITIONS):
        if pos in positions:
            assert not memory_tracker.is_free(pos)
        else:
            assert memory_tracker.is_free(pos)

    # test correctness of assigned link_ids
    counter = {remote: 1 for remote in remotes}
    for i in range(len(positions)):
        pos = positions[i]
        remote_node_id = remotes[i]
        assert memory_tracker.get_link(pos).link_id == counter[remote_node_id]
        counter[remote_node_id] += 1


def test_swap(memory_tracker):
    # set the entanglement with remote
    posA, posB = 13, 17
    remote_node_id_A, remote_node_id_B = posA, posB
    for (pos, remote_node_id) in [(posA, remote_node_id_A), (posB, remote_node_id_B)]:
        memory_tracker.entangle(pos=pos, remote_node_id=remote_node_id)
    # swap the entanglement with remote
    memory_tracker.swap(posA=posA, posB=posB)
    # assert
    for (pos, new_remote_node_id) in [(posA, remote_node_id_B), (posB, remote_node_id_A)]:
        assert memory_tracker.get_link(pos).remote_node_id == new_remote_node_id


def test_has_free_positions(memory_tracker):
    for n in range(NUM_POSITIONS):
        memory_tracker.reset()
        for pos in range(n):
            memory_tracker.entangle(pos=pos, remote_node_id=n)
        for k in range(NUM_POSITIONS):
            if k > NUM_POSITIONS - n:
                assert not memory_tracker.has_free_positions(number=k)
                # self.assertFalse(memory_tracker.has_free_positions(number=k))
            else:
                assert memory_tracker.has_free_positions(number=k)
                # self.assertTrue(memory_tracker.has_free_positions(number=k))


def test_entangled_positions_with(memory_tracker):
    remote_node_id = 3

    # without fidelity constraint
    for pos in range(NUM_POSITIONS):
        memory_tracker.entangle(pos=pos, remote_node_id=remote_node_id)
        all_ent = memory_tracker.entangled_positions_with(remote_node_id=remote_node_id)
        assert all_ent == list(range(pos + 1))
        # self.assertEqual(all_ent, list(range(pos + 1)))

    # including fidelity constraint
    memory_tracker.reset()
    positions_that_satisfy_condition = []
    for pos in range(NUM_POSITIONS):

        # set entanglement
        memory_tracker.entangle(pos=pos, remote_node_id=remote_node_id)
        condition = pos < NUM_POSITIONS / 2.
        memory_tracker.get_link(pos=pos).estimated_fidelity = 0.5 if condition else 1.
        if condition:
            positions_that_satisfy_condition.append(pos)

        # check correctness
        all_ent = memory_tracker.entangled_positions_with(remote_node_id=remote_node_id, fidelity_range=[0.3, 0.7])
        assert all_ent == positions_that_satisfy_condition


def test_get_free(memory_tracker):
    memory_tracker.reset()
    free_positions = list(range(NUM_POSITIONS))

    for pos in range(NUM_POSITIONS):

        # set entanglement
        remote_node_id = pos
        memory_tracker.entangle(pos=pos, remote_node_id=remote_node_id)
        free_positions.remove(pos)

        # check correctness
        # self.assertEqual(memory_tracker.free_positions(), free_positions)
        assert memory_tracker.free_positions() == free_positions
        excluded = [2 * pos for pos in range(int(math.ceil(NUM_POSITIONS / 2.)))]
        expected_output = sorted([pos for pos in free_positions if pos not in excluded])
        # self.assertEqual(sorted(memory_tracker.free_positions(excluded=excluded)), expected_output)
        assert memory_tracker.free_positions(excluded=excluded) == expected_output
