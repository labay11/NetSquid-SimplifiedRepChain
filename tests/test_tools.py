import pytest

from repchain.utils.tools import is_valid_pauli, is_valid_bell_index, commutes_with_Z, commutes_with_X,\
                                 conjugate_with_hadamard, bell_index_to_stabilizer_coefficients,\
                                 stabilizer_coefficients_to_bell_index, apply_Hadamard_on_each_qubit,\
                                 apply_Pauli_to_one_side_of_bell_state, index_of_Pauli_product,\
                                 pauli_index_to_get_to_psiplus, does_precisely_one_commute_with_Z,\
                                 is_valid_direction, LEFT, RIGHT, is_outermost_node,\
                                 neighbour_index, get_possible_directions, reverse_direction


@pytest.mark.parametrize('pauli, valid', [
    (i, 0 <= i <= 3) for i in range(-1, 5)
])
def test_is_valid_pauli(pauli, valid):
    if not valid:
        with pytest.raises(ValueError):
            is_valid_pauli(pauli)
    else:
        assert pauli == is_valid_pauli(pauli)


@pytest.mark.parametrize('bell, valid', [
    (i, 0 <= i <= 3) for i in range(-1, 5)
])
def test_is_bell_index(bell, valid):
    if not valid:
        with pytest.raises(ValueError):
            is_valid_bell_index(bell)
    else:
        assert bell == is_valid_bell_index(bell)


@pytest.mark.parametrize('pauli, commutes', [
    (i, i == 0 or i == 3) for i in range(4)
])
def test_commutes_with_Z(pauli, commutes):
    assert commutes_with_Z(pauli) == commutes


@pytest.mark.parametrize('pauli, commutes', [
    (i, i == 0 or i == 1) for i in range(4)
])
def test_commutes_with_X(pauli, commutes):
    assert commutes_with_X(pauli) == commutes


@pytest.mark.parametrize('pauli, final_pauli', [
    (0, 0), (1, 3), (2, 2), (3, 1)
])
def test_conjugate_with_hadamard(pauli, final_pauli):
    assert final_pauli == conjugate_with_hadamard(pauli)


@pytest.mark.parametrize('index, coef', [
    (0, (1, 1)),  # Phi_+
    (1, (1, -1)),  # Psi_+
    (2, (-1, 1)),  # Phi_-
    (3, (-1, -1))  # Psi_-
])
def test_bell_index_to_stabilizer_coefficients_and_viceversa(index, coef):
    assert bell_index_to_stabilizer_coefficients(index) == coef
    assert stabilizer_coefficients_to_bell_index(coef) == index


@pytest.mark.parametrize('index, expected_index', [
    (0, 0),
    (1, 2),
    (2, 1),
    (3, 3)
])
def test_apply_Hadamard_on_each_qubit(index, expected_index):
    assert apply_Hadamard_on_each_qubit(index) == expected_index


@pytest.mark.parametrize('index, pauli, expected_index', [
    (0, 0, 0),  # Id
    (1, 0, 1),
    (2, 0, 2),
    (3, 0, 3),
    (0, 1, 1),  # X
    (1, 1, 0),
    (2, 1, 3),
    (3, 1, 2),
    (0, 2, 3),  # Y
    (1, 2, 2),
    (2, 2, 1),
    (3, 2, 0),
    (0, 3, 2),  # Z
    (1, 3, 3),
    (2, 3, 0),
    (3, 3, 1)
])
def test_apply_pauli_to_one_side_of_bell_state(index, pauli, expected_index):
    assert apply_Pauli_to_one_side_of_bell_state(index, pauli) == expected_index


@pytest.mark.parametrize('pauliA, pauliB, expected_pauli', [
    (0, 0, 0),
    (0, 1, 1),
    (0, 2, 2),
    (0, 3, 3),
    (1, 0, 1),
    (1, 1, 0),
    (1, 2, 3),
    (1, 3, 2),
    (2, 0, 2),
    (2, 1, 3),
    (2, 2, 0),
    (2, 3, 1),
    (3, 0, 3),
    (3, 1, 2),
    (3, 2, 1),
    (3, 3, 0)
])
def test_index_of_Pauli_product(pauliA, pauliB, expected_pauli):
    computed_output_index = index_of_Pauli_product(pauliA, pauliB)
    assert computed_output_index == expected_pauli


@pytest.mark.parametrize('index, pauli', [
    (0, 1), (1, 0), (2, 2), (3, 3)
])
def test_pauli_index_to_get_psiplus(index, pauli):
    assert pauli_index_to_get_to_psiplus(index) == pauli


@pytest.mark.parametrize('index1, index2, commute', [
    (j, k, commutes_with_Z(j) ^ commutes_with_Z(k))
    for j in range(4)
    for k in range(4)
])
def test_does_precisely_one_commute_with_Z(index1, index2, commute):
    assert does_precisely_one_commute_with_Z(index1, index2) == commute


@pytest.mark.parametrize('dir, direction', [
    (-1, LEFT), (1, RIGHT), (0, None), (42, None),
    ('L', LEFT), ('R', RIGHT), ('A', None),
    ('l', LEFT), ('r', RIGHT), ('b', None)
])
def test_is_valid_direction(dir, direction):
    if direction is None:
        with pytest.raises(TypeError):
            is_valid_direction(dir)
    else:
        assert is_valid_direction(dir) == direction


@pytest.mark.parametrize('nodes, index, direction, outermost', [
    (5, 0, LEFT, True),
    (5, 0, RIGHT, False),
    (5, 1, LEFT, False),
    (5, 4, LEFT, False),
    (5, 4, RIGHT, True),
    (5, 4, None, True),
    (5, 0, None, True),
    (5, 3, None, False)
])
def test_is_outermost_node(nodes, index, direction, outermost):
    assert is_outermost_node(nodes, index, direction) == outermost


@pytest.mark.parametrize('nodes, index, direction, nghb', [
    (5, 0, LEFT, None),
    (5, 0, RIGHT, 1),
    (5, 1, LEFT, 0),
    (5, 4, LEFT, 3),
    (5, 4, RIGHT, None)
])
def test_neighbour_index(nodes, index, direction, nghb):
    assert neighbour_index(nodes, index, direction) == nghb


@pytest.mark.parametrize('nodes, index, dirs', [
    (5, 0, [RIGHT]),
    (5, 1, [LEFT, RIGHT]),
    (5, 3, [LEFT, RIGHT]),
    (5, 4, [LEFT]),
])
def test_get_possible_directions(nodes, index, dirs):
    assert get_possible_directions(nodes, index) == dirs


@pytest.mark.parametrize('dir, expected', [
    (LEFT, RIGHT),
    (RIGHT, LEFT)
])
def test_reverse_direction(dir, expected):
    assert reverse_direction(dir) == expected
