from repchain.network.abstract_node import AbstractNode


def test_abstract_node():
    test_node = AbstractNode(name="AbstractNode", num_positions=1)

    assert isinstance(test_node, AbstractNode)


def test_noiseless_abstract_processor():
    num_positions = 10
    test_node = AbstractNode(name="AbstractNode", num_positions=num_positions, noiseless=True)

    assert test_node.qmemory.num_positions == num_positions
    assert test_node.qmemory.properties['p_q1gate'] == 0.
    assert test_node.qmemory.properties['p_q2gate'] == 0.
    assert test_node.qmemory.properties["p_q1init"] == 0.
    assert test_node.qmemory.properties['p_q1meas'] == (0., 0.)
    assert test_node.qmemory.properties["T1"] == 0.
    assert test_node.qmemory.properties["T2"] == 0.


def test_instantaneous_abstract_processor():
    num_positions = 20
    test_node = AbstractNode(name="AbstractNode", num_positions=num_positions, durations=None)

    assert test_node.qmemory.num_positions == num_positions
    assert test_node.qmemory.properties["t_q1gate"] == 0
    assert test_node.qmemory.properties["t_q2gate"] == 0
    assert test_node.qmemory.properties["t_q1init"] == 0
    assert test_node.qmemory.properties["t_q1meas"] == 0


def test_noisy_abstract_processor():
    num_positions = 30
    props = {"T1": 10,
             "T2": 5,
             "p_q1init": 0.7,
             "p_q1gate": 0.6,
             "p_q2gate": 0.9,
             "p_q1meas": (0.8, 0.81),
             "t_q1init": 1000.,
             "t_q1meas": 3000.,
             "t_q1gate": 8.,
             "t_q2gate": 5.6e5}
    test_node = AbstractNode(name="AbstractNode", num_positions=num_positions,
                             T1=10, T2=5,
                             errors={
                                 "p_q1init": 0.7,
                                 "p_q1gate": 0.6,
                                 "p_q2gate": 0.9,
                                 "p_q1meas": (0.8, 0.81)
                             },
                             durations={
                                 "t_q1init": 1000.,
                                 "t_q1meas": 3000.,
                                 "t_q1gate": 8.,
                                 "t_q2gate": 5.6e5
                             })

    assert test_node.qmemory.num_positions == num_positions

    for key, v in props.items():
        assert test_node.qmemory.properties[key] == v
