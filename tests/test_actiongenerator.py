import unittest
from repchain.logic.actiongenerator import IActionGenerator, SingleNVActionGenerator
from repchain.utils.message import Message
from repchain.utils.memorymanager import MemoryManager, ELECTRON_POSITION


class TestIActionGenerator(unittest.TestCase):

    def setUp(self):
        self.ag = IActionGenerator()

    def test(self):
        self.assertFalse(self.ag.has_stopped)

        self.ag.should_stop_fn = lambda: True

        self.assertTrue(self.ag.has_stopped)


class TestSingleNVActionGenerator(unittest.TestCase):

    def setUp(self):
        self.memman = MemoryManager(num_positions=100)
        swap_fidelity_threshold = [0.8] * 3
        distillation_strategy = [('DEJMPS', 2)] * 3
        self.ag_init = SingleNVActionGenerator(number_of_nodes=9, index=1,
                                               swap_fidelity_threshold=swap_fidelity_threshold,
                                               distillation_strategy=distillation_strategy,
                                               memorymanager=self.memman)
        self.ag_resp = SingleNVActionGenerator(number_of_nodes=9, index=4,
                                               swap_fidelity_threshold=swap_fidelity_threshold,
                                               distillation_strategy=distillation_strategy,
                                               memorymanager=self.memman)

    def test_get_action_without_message(self):

        # no entanglement => entgen left
        self.memman.reset()
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.parameters["remote_node_id"], 0)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(is_message_used, False)

        # single bad entanglement with left => entgen left
        self.memman.reset()
        self.memman.entangle(pos=0, remote_node_id=0)
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(action.parameters["remote_node_id"], 0)
        self.assertEqual(is_message_used, False)

        # twice bad entanglement with left => distill left
        self.memman.reset()
        self.memman.entangle(pos=0, remote_node_id=0)
        self.memman.entangle(pos=1, remote_node_id=0)
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.parameters["remote_node_id"], 0)
        self.assertEqual(action.topic, "DIST")
        self.assertEqual(is_message_used, False)

        # single good entanglement with left => entgen right
        self.memman.reset()
        self.memman.entangle(pos=0, remote_node_id=0)
        link = self.memman.get_link(pos=0)
        link.estimated_fidelity = 1.0
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(action.parameters["remote_node_id"], 2)
        self.assertEqual(is_message_used, False)

        # single good entanglement with left + single bad with right => entgen right
        self.memman.reset()
        self.memman.entangle(pos=0, remote_node_id=0)
        link = self.memman.get_link(pos=0)
        link.estimated_fidelity = 1.0
        self.memman.entangle(pos=1, remote_node_id=2)
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(action.parameters["remote_node_id"], 2)
        self.assertEqual(is_message_used, False)

        # single good entanglement with left + twice bad with right => distill right
        self.memman.reset()
        self.memman.entangle(pos=0, remote_node_id=0)
        link = self.memman.get_link(pos=0)
        link.estimated_fidelity = 1.0
        self.memman.entangle(pos=1, remote_node_id=2)
        self.memman.entangle(pos=2, remote_node_id=2)
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.topic, "DIST")
        self.assertEqual(action.parameters["remote_node_id"], 2)
        self.assertEqual(is_message_used, False)

        # single good entanglement with left & right => swap
        self.memman.reset()
        posL, posR = 0, 1
        for (pos, remote_node_id) in [(posL, 0), (posR, 2)]:
            self.memman.entangle(pos=pos, remote_node_id=remote_node_id)
            link = self.memman.get_link(pos=pos)
            link.estimated_fidelity = 1.0
        action, is_message_used = self.ag_init.get_action(first_msg_on_inbox=None)
        self.assertEqual(action.topic, "SWAP")
        self.assertEqual(is_message_used, False)

    def test_get_action_return_electron_if_possible(self):
        # we define a few scenarios of quantum memory state;
        # in each scenario, the actiongenerator should return
        # the electron spin as `qmem_posA` if possible

        # scenario is a list of (memory position, remote node id)
        assert(ELECTRON_POSITION == 0)
        scenario_a = [(ELECTRON_POSITION, 0),
                      (1, 0),
                      (2, 2)]
        expected_control_a = ELECTRON_POSITION
        expected_target_a = 2

        scenario_b = [(2, 0),
                      (ELECTRON_POSITION, 0),
                      (1, 2)]
        expected_control_b = ELECTRON_POSITION
        expected_target_b = 1

        scenario_c = [(1, 0), (2, 2), (3, 0)]
        expected_control_c = 1
        expected_target_c = 2

        for scenario, expected_control, expected_target in \
                [(scenario_a, expected_control_a, expected_target_a),
                 (scenario_b, expected_control_b, expected_target_b),
                 (scenario_c, expected_control_c, expected_target_c)]:
            self.memman.reset()
            for (pos, remote_node_id) in scenario:
                self.memman.entangle(pos=pos, remote_node_id=remote_node_id)
                link = self.memman.get_link(pos=pos)
                link.estimated_fidelity = 1.0
            action, is_message_used = \
                self.ag_init.get_action(first_msg_on_inbox=None)
            self.assertEqual(action.topic, "SWAP")
            self.assertEqual(action.parameters["qmem_posA"],
                             expected_control)
            self.assertEqual(action.parameters["qmem_posB"],
                             expected_target)
            self.assertEqual(is_message_used, False)

    def test_get_action_with_ask_message(self):
        sender = 123
        msg = Message.generate_entanglement(subject=Message.SUBJECT_ASK, sender=sender)

        # msg used
        self.memman.reset()
        action, is_message_used = self.ag_resp.get_action(first_msg_on_inbox=msg)
        self.assertEqual(action.parameters["remote_node_id"], sender)
        self.assertEqual(action.topic, "ENTGEN")
        self.assertEqual(is_message_used, True)

        self.memman.reset()
        sender = 145
        link_idA = 5
        link_idB = 17
        for (pos, link_id) in [(12, link_idA), (13, link_idB)]:
            self.memman.entangle(pos=pos, remote_node_id=sender)
            link = self.memman.get_link(pos=pos)
            link.link_id = link_id
        msg = Message.distill(sender=sender, subject=Message.SUBJECT_ASK, receiver=None,
                              content=(link_idA, link_idB, ('DEJMPS', 2)))

        # msg used
        action, is_message_used = self.ag_resp.get_action(first_msg_on_inbox=msg)
        self.assertEqual(action.parameters["remote_node_id"], sender)
        self.assertEqual(action.topic, "DIST")
        self.assertEqual(is_message_used, True)


if __name__ == "__main__":
    unittest.main()
