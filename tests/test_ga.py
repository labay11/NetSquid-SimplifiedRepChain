import pytest
import numpy as np

from repchain.parameters import BaselineParameter
from repchain.opt.ga import get_parents, get_parents_roulette, crossover, mutate, next_generation,\
    initial_population_lhs, initial_population_random, migration


DEFAULT_SEED = 42


TEST_PARAMS = [
    BaselineParameter('x', float, -1.0, 10.0, None, None),
    BaselineParameter('y', float, -10.0, 5.0, None, None),
    BaselineParameter('n', float, 0, 2, None, None)
]


TEST_POPULATION_ARRAY = np.array([
    [3.14, -5.3, 5.0],
    [-1.14, 4.3, 1.0],
    [5.14, -2.3, 2.0],
    [8.14, -8.3, 4.0],
    [-0.14, 1.3, 2.0]
])

TEST_FITNESS_ARRAY = np.array([
    0.25, -0.5, 0.8, 0.9, -0.1
])


def constraint(x):
    return x[..., 0] * x[..., 1] < 0


def arbitrary_func(x, a):
    return np.sin(x[0] / a)


def test_get_parents():
    rng = np.random.default_rng(DEFAULT_SEED)
    number_best_candidates = 2
    pop, fit = get_parents(TEST_POPULATION_ARRAY, TEST_FITNESS_ARRAY, number_best_candidates, rng)
    assert fit.shape[0] == number_best_candidates
    assert fit[0] == -0.5
    assert fit[1] == -0.1

    assert pop.shape == (number_best_candidates, 3)


def test_get_parents_roulette():
    rng = np.random.default_rng(DEFAULT_SEED)
    number_best_candidates = 2
    pop = get_parents_roulette(TEST_POPULATION_ARRAY, TEST_FITNESS_ARRAY, number_best_candidates, rng)

    assert pop.shape[0] == number_best_candidates
    assert pop.shape == (2, 3)


def test_crossover():
    rng = np.random.default_rng(DEFAULT_SEED)

    children = crossover(TEST_POPULATION_ARRAY, 2, constraint, rng)

    assert children.shape[0] == 2
    assert np.all(constraint(children))


def test_mutation():
    rng = np.random.default_rng(DEFAULT_SEED)

    mutated = mutate(TEST_POPULATION_ARRAY, 0.5, 0.3, TEST_PARAMS, constraint, rng)

    assert mutated.shape == TEST_POPULATION_ARRAY.shape
    assert np.all(constraint(mutated))
    for i, param in enumerate(TEST_PARAMS):
        assert np.all(param.baseline_value <= mutated[:, i])
        assert np.all(param.perfect_value >= mutated[:, i])


def test_migration():
    rng = np.random.default_rng(DEFAULT_SEED)

    pop = migration(10, TEST_PARAMS, constraint, rng)

    assert pop.shape == (10, 3)
    assert np.all(constraint(pop))
    for i, param in enumerate(TEST_PARAMS):
        assert np.all(param.baseline_value <= pop[:, i])
        assert np.all(param.perfect_value >= pop[:, i])


def test_next_generation():
    rng = np.random.default_rng(DEFAULT_SEED)

    pop = next_generation(rng, TEST_POPULATION_ARRAY, TEST_FITNESS_ARRAY, TEST_PARAMS, constraint,
                          TEST_POPULATION_ARRAY.shape[0], 2, 2, 0.5)

    assert pop.shape == TEST_POPULATION_ARRAY.shape
    assert np.all(constraint(pop))
    # best individual is kept
    assert np.any(np.isclose(np.sum(np.power(pop - TEST_POPULATION_ARRAY[1], 2), axis=1), 0.0))


def test_population_lhs():
    rng = np.random.default_rng(DEFAULT_SEED)
    n_children = 10
    pop, fit = initial_population_lhs(rng, TEST_PARAMS, constraint, 100, n_children, arbitrary_func, kargs=(4, ))
    print(pop, fit)

    assert pop.shape == (n_children, 3)
    assert fit.shape[0] == n_children
    assert np.all(constraint(pop))

    for i in range(n_children):
        assert np.isclose(arbitrary_func(pop[i], 4), fit[i])


def test_population_random():
    rng = np.random.default_rng(DEFAULT_SEED)
    n_children = 5
    pop, fit = initial_population_random(rng, TEST_PARAMS, constraint, 10, n_children, arbitrary_func, kargs=(4, ))
    assert pop.shape == (n_children, 3)
    assert fit.shape[0] == n_children
    assert np.all(constraint(pop))

    for i in range(n_children):
        assert np.isclose(arbitrary_func(pop[i], 4), fit[i])


rng = np.random.default_rng(DEFAULT_SEED)

mutated = mutate(TEST_POPULATION_ARRAY, 0.5, 0.3, TEST_PARAMS, constraint, rng)
