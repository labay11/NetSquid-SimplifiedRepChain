import pytest

from repchain.logic.distillation_scheduling import GreedyTopDownScheduler, GreedyBottomUpScheduler,\
                                                   TimeScheduler, BandedScheduler, DistillationThresholdScheduler,\
                                                   TimedTopDownScheduler
from repchain.utils.memorymanager import Link


@pytest.fixture
def links():
    return [Link(remote_node_id=0, link_id=k, estimated_fidelity=k / 10.) for k in range(10)]


@pytest.mark.parametrize('lower, upper, expected', [
    (0, 1, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]),
    (0, 0.8, [0, 1, 2, 3, 4, 5, 6, 7]),
    (0.6, 0.8, [6, 7]),
    (0.4, 0.5, [4]),
    (0.4, 0.45, [4]),
    (0.4, 0.3, [])
])
def test_threshold_scheduler(links, lower, upper, expected):
    gs = DistillationThresholdScheduler(lower_threshold=lower, upper_threshold=upper)

    to_distill = gs.choose_links_to_distill(links)
    assert len(to_distill) == len(expected)

    for link, id in zip(to_distill, expected):
        assert link.link_id == id


def test_greedy():
    gs = GreedyTopDownScheduler(lower_threshold=0, upper_threshold=1)
    assert gs.choose_links_to_distill([]) == []


@pytest.mark.parametrize('lower, upper, expected', [
    (0, 1, [9, 8]),
    (0, 0.8, [7, 6]),
    (0.6, 0.8, [7, 6]),
    (0.4, 0.5, [])
])
def test_greedy_top_down_scheduler(links, lower, upper, expected):
    gs = GreedyTopDownScheduler(lower_threshold=lower, upper_threshold=upper)

    to_distill = gs.choose_links_to_distill(links)
    assert len(to_distill) == len(expected)

    for link, id in zip(to_distill, expected):
        assert link.link_id == id


@pytest.mark.parametrize('lower, upper, expected', [
    (0, 1, [0, 1]),
    (0, 0.5, [0, 1]),
    (0.6, 0.8, [6, 7]),
    (0.4, 0.5, [])
])
def test_greedy_bottom_up_scheduler(links, lower, upper, expected):
    gs = GreedyBottomUpScheduler(lower_threshold=lower, upper_threshold=upper)

    to_distill = gs.choose_links_to_distill(links)
    assert len(to_distill) == len(expected)

    for link, id in zip(to_distill, expected):
        assert link.link_id == id


def test_banded():
    for boundaries, expected_bands in [([0.5], [(0., 0.5), (0.5, 1.)]),
                                       ([0.3, 0.7], [(0., 0.3), (0.3, 0.7), (0.7, 1.0)])]:
        bs = BandedScheduler(band_boundaries=boundaries)
        assert bs._bands, expected_bands


def test_banded_choose(links):
    for band_boundaries, expected_output in [([0.4], [links[1], links[0]]),
                                             ([0.1], [links[1], links[0]]),
                                             ([0.05], [links[2], links[1]]),
                                             ([0.05, 0.1, 0.15], [links[3], links[2]])]:
        bs = BandedScheduler(band_boundaries=band_boundaries)
        output = bs.choose_links_to_distill(links=links)
        assert output == expected_output


def test_time_scheduler(links):
    ts = TimeScheduler(lower_threshold=0, upper_threshold=1)

    for i, link in enumerate(links):
        link.creation_time = i

    to_distill = ts.choose_links_to_distill(links)

    for link, id in zip(to_distill, [9, 8]):
        assert link.link_id == id


def test_time_topdown_scheduler():
    ds = TimedTopDownScheduler(0., 1.)

    link_params = [(0.2, 1.), (0.2, 2.), (0.8, 5), (0.8, 0.5), (0.5, 3.), (0.7, 2.5), (0.5, 6.)]
    links = [Link(remote_node_id=0, link_id=k, estimated_fidelity=link_params[k][0]) for k in range(len(link_params))]
    for i, link in enumerate(links):
        link.creation_time = link_params[i][1]

    output = ds.choose_links_to_distill(links)
    for link, (fid, time) in zip(output, [(0.8, 5), (0.8, 0.5)]):
        assert link.estimated_fidelity == fid
        assert link.creation_time == time

    # remove links in output
    links.pop(2)
    links.pop(2)

    output = ds.choose_links_to_distill(links)
    for link, (fid, time) in zip(output, [(0.7, 2.5), (0.5, 6.)]):
        assert link.estimated_fidelity == fid
        assert link.creation_time == time
