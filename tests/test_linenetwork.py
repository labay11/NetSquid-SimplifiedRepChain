import unittest
from netsquid.components.cchannel import ClassicalChannel
from netsquid.nodes.node import Node
# from netsquid_physlayer.easyfibre import MiddleHeraldedFibreConnection
from netsquid.components.qprocessor import QuantumProcessor
import netsquid as ns
import logging
from repchain.network.abstract_network_factory import create_network_from_dic


class Test_AbstractNetwork(unittest.TestCase):

    def setUp(self):
        ns.set_qstate_formalism(ns.QFormalism.DM)

    def test_init(self):
        for (connlistname, connection_cls) in [("class_channels_receive", ClassicalChannel),
                                               ("class_channels_send", ClassicalChannel)]:
            self.check_init(connlistname=connlistname,
                            connection_cls=connection_cls)

    def check_init(self, connlistname, connection_cls):
        for number_of_nodes in range(0, 10):
            network_params = {
                "qmem_config_params": {
                    "p_q1gate": 0,
                    "p_q1init": 0,
                    "p_q1meas": (0, 0),
                    "p_q2gate": 0,
                    "T1": 0,
                    "T2": 0,
                    "tot_num_qubits": 10,
                    "t_q1gate": 0,
                    "t_q1init": 0,
                    "t_q1meas": 0,
                    "t_q2gate": 0
                },
                "distributor_params": {
                    "p_loss_length": 0.2,
                    "elementary_link_fidelity": 0.7,
                    "elementary_link_succ_prob": 1.,
                    "werner": True,
                    "eps": 0,
                    "cycle_time": 1e3
                },
                "number_of_nodes": number_of_nodes,
                "tot_num_qubits": 4,
                "internode_distance": 1,
                "c": 3e5}
            logic_params = {"action_ordering": "default",
                            "is_move_magical": True,
                            "swap_fidelity_threshold": 0.,
                            "delivery_params":
                            {"cycle_time": 3.5e3,
                                "elementary_link_fidelity": 1.,
                                "elementary_link_succ_prob": 1.},
                            "use_two_node_NV": False,
                            "use_swap_only": True,
                            "distillation_strategy": "default",
                            "respect_NV_structure": False}
            factory = create_network_from_dic(network_params, logic_params)
            network = factory.network

            network.qnodes = []
            for index in range(number_of_nodes):
                node = network.get_node(name="Abstractnode{}".format(index))
                network.qnodes.append(node)

            self.assertEqual(len(network.qnodes), number_of_nodes)
            for qnode in network.qnodes:
                self.assertTrue(isinstance(qnode, Node))

            for index, qnode in enumerate(network.qnodes):
                # assert every node has a QuantumProcessingDevice
                self.assertTrue(isinstance(qnode.qmemory, QuantumProcessor))
                self._assert_connections_correct(network=network,
                                                 factory=factory,
                                                 number_of_nodes=number_of_nodes,
                                                 index=index,
                                                 connlistname=connlistname,
                                                 connection_cls=connection_cls)

    def _assert_connections_correct(self, connlistname, network, factory, number_of_nodes, index, connection_cls):
        """Assert the every node has the right amount and kind of connections"""
        connlist = getattr(factory, connlistname)
        logging.debug("ConnectionList:{}".format(connlist))
        for direction in ["L", "R"]:
            logging.info("Num:{}\tIndex:{}\tDir:{}\tConnectionClass:{}".format(number_of_nodes, index, direction,
                         connection_cls))
            if (index, direction) in [(0, "L"), (number_of_nodes - 1, "R")]:
                direction not in list(connlist[index].keys())
            else:
                self.assertTrue(isinstance(connlist[index][direction], connection_cls))


if __name__ == "__main__":
    unittest.main()
