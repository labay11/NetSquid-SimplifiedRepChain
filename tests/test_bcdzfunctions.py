import pytest

from repchain.utils.bcdzfunctions import num2level, level2num, num2numOneLower, get_height, get_ngbh, is_initiator,\
    get_table, get_levels, remote_index2table_position


def parameter_to_distillation_strategy(nodes, param):
    if param == 0:
        # swap only
        return True, None

    levels = num2level(nodes)
    strategy = []
    for i in range(levels + 1):
        mask = 1  # get the first bit
        protocol = 'EPL' if mask & param else 'DEJMPS'
        param >>= 1    # divide by two
        mask = 3  # get the next two bits
        n = mask & param
        strategy.append((protocol, n if protocol == 'DEJMPS' else min(n, 1)))
        param >>= 2

    return False, strategy


@pytest.mark.parametrize('nodes, param, swap_only, strategy', [
    (2, 0, True, None),
    (2, 0b001, False, [('EPL', 0)]),
    (2, 0b010, False, [('DEJMPS', 1)]),
    (2, 0b101, False, [('EPL', 1)]),
    (2, 0b110, False, [('DEJMPS', 3)]),
    (3, 0, True, None),
    (3, 0b001, False, [('EPL', 0), ('DEJMPS', 0)]),
    (3, 0b010, False, [('DEJMPS', 1), ('DEJMPS', 0)]),
    (3, 0b101, False, [('EPL', 1), ('DEJMPS', 0)]),
    (3, 0b110, False, [('DEJMPS', 3), ('DEJMPS', 0)]),
    (5, 0b000000, True, None),
    (5, 0b001000, False, [('DEJMPS', 0), ('EPL', 0), ('DEJMPS', 0)]),
    (5, 0b011100, False, [('DEJMPS', 2), ('EPL', 1), ('DEJMPS', 0)]),
    (5, 0b000001, False, [('EPL', 0), ('DEJMPS', 0), ('DEJMPS', 0)]),
    (5, 0b110011, False, [('EPL', 1), ('DEJMPS', 3), ('DEJMPS', 0)]),
    (9, 0b000000000, True, None),
    (9, 0b001001000, False, [('DEJMPS', 0), ('EPL', 0), ('EPL', 0), ('DEJMPS', 0)]),
    (9, 0b111011100, False, [('DEJMPS', 2), ('EPL', 1), ('EPL', 1), ('DEJMPS', 0)]),
    (9, 0b000000001, False, [('EPL', 0), ('DEJMPS', 0), ('DEJMPS', 0), ('DEJMPS', 0)]),
    (9, 0b110100011, False, [('EPL', 1), ('DEJMPS', 2), ('DEJMPS', 3), ('DEJMPS', 0)]),
])
def test_parameter_to_distillation_strategy(nodes, param, swap_only, strategy):
    use_swap, s = parameter_to_distillation_strategy(nodes, param)
    assert use_swap == swap_only
    if use_swap:
        assert s is None
    else:
        assert len(s) == len(strategy)
        for key, v in enumerate(strategy):
            assert v == s[key]


@pytest.mark.parametrize('nodes, level', [(2**d + 1, d) for d in range(1, 10)])
def test_num2level_and_level2num(nodes, level):
    assert num2level(nodes) == level
    assert level2num(level) == nodes


@pytest.mark.parametrize('nodes, number_one_lower', [(2**(d + 1) + 1, 2**d + 1) for d in range(1, 10)])
def test_num2numOneLower(nodes, number_one_lower):
    assert num2numOneLower(nodes) == number_one_lower


@pytest.mark.parametrize('nodes, index, height', [
    (3, 0, 1),
    (3, 1, 0),
    (3, 2, 1),
    (5, 0, 2),
    (5, 1, 0),
    (5, 2, 1),
    (5, 3, 0),
    (5, 4, 2),
    (9, 0, 3),
    (9, 1, 0),
    (9, 2, 1),
    (9, 3, 0),
    (9, 4, 2),
    (9, 5, 0),
    (9, 6, 1),
    (9, 7, 0),
    (9, 8, 3)])
def test_get_height(nodes, index, height):
    assert get_height(nodes, index) == height


@pytest.mark.parametrize('nodes, index, level, values', [
    (3, 0, 0, [-1, 1]),
    (3, 1, 0, [0, 2]),
    (3, 2, 0, [1, -1]),
    (3, 0, 1, [-1, 2]),
    (3, 2, 1, [0, -1]),
    (5, 0, 0, [-1, 1]),
    (5, 0, 1, [-1, 2]),
    (5, 0, 2, [-1, 4]),
    (5, 1, 0, [0, 2]),
    (5, 2, 0, [1, 3]),
    (5, 2, 1, [0, 4]),
    (5, 3, 0, [2, 4]),
    (5, 4, 0, [3, -1]),
    (5, 4, 1, [2, -1]),
    (5, 4, 2, [0, -1])
])
def test_get_ngbh(nodes, index, level, values):
    for dir_index, direction in enumerate(['L', 'R']):
        assert get_ngbh(nodes, index, level, direction, nonevalue=-1) == values[dir_index]


@pytest.mark.parametrize('nodes, index, level, initiator', [
    (3, 0, 0, False),
    (3, 1, 0, True),
    (3, 2, 0, False),
    (5, 0, 0, False),
    (5, 0, 1, False),
    (5, 1, 0, True),
    (5, 2, 0, False),
    (5, 2, 1, True),
    (5, 3, 0, True),
    (5, 4, 0, False),
    (5, 4, 1, False)
])
def test_initiator(nodes, index, level, initiator):
    assert is_initiator(nodes, index, level) == initiator


@pytest.mark.parametrize('nodes, expected_table', [
    (3, {
        0: {"L": {0: -1, 1: -1}, "R": {0: 1, 1: 2}},
        1: {"L": {0: 0}, "R": {0: 2}},
        2: {"L": {0: 1, 1: 0}, "R": {0: -1, 1: -1}}
        }),
    (5, {
        0: {"L": {0: -1, 1: -1, 2: -1}, "R": {0: 1, 1: 2, 2: 4}},
        1: {"L": {0: 0}, "R": {0: 2}},
        2: {"L": {0: 1, 1: 0}, "R": {0: 3, 1: 4}},
        3: {"L": {0: 2}, "R": {0: 4}},
        4: {"L": {0: 3, 1: 2, 2: 0}, "R": {0: -1, 1: -1, 2: -1}}
    })
], ids=['3', '5'])
def test_get_table(nodes, expected_table):
    for index in range(nodes):
        table = get_table(number_of_nodes=nodes,
                          index=index,
                          nonevalue=-1)
        levels = get_levels(number_of_nodes=nodes, index=index)
        for direction in ["L", "R"]:
            for level in levels:
                assert expected_table[index][direction][level] == table[direction][level]


@pytest.mark.parametrize('nodes, index, remote, direction, level', [
    (5, 0, 1, 'R', 0),
    (5, 0, 2, 'R', 1),
    (5, 4, 3, 'L', 0),
    (5, 3, 4, 'R', 0),
    (5, 3, 2, 'L', 0),
    (5, 0, 4, 'R', 2),
    (5, 1, 4, None, None)
])
def test_remote_index2table_position(nodes, index, remote, direction, level):
    d, a = remote_index2table_position(nodes, index, remote)
    assert d == direction
    assert a == level
