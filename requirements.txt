netsquid>=0.10.0,<0.11.0
netsquid-magic>=6.0.0,<7.0.0
netsquid-nv>=5.0.1,<6.0.0
netsquid-physlayer>=4.0.0,<5.0.0
netsquid-abstractmodel>=1.1.1
netsquid-netconf>=1.0.0
netsquid-nvrepchain==1.0.3
